﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class AccessRights
    {
        public void UpdateUserAccessRights(clsAccessRights rights)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE AccessRight SET " +
                "CanAccess=@CanAccess, " +
                "Adding=@Adding, " +
                "Deleting=@Deleting, " +
                "Updating=@Updating, " +
                "Printing=@Printing " +
                "WHERE (UserId=@UserId and ScreenId=@ScreenId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CanAccess", SqlDbType.Bit).Value = rights.CanAccess;
                command.Parameters.Add("@Adding", SqlDbType.Bit).Value = rights.Adding;
                command.Parameters.Add("@Deleting", SqlDbType.Bit).Value = rights.Deleting;
                command.Parameters.Add("@Updating", SqlDbType.Bit).Value = rights.Updating;
                command.Parameters.Add("@Printing", SqlDbType.Bit).Value = rights.Printing;
                command.Parameters.Add("@UserId", SqlDbType.Int).Value = rights.UserId;
                command.Parameters.Add("@ScreenId", SqlDbType.Int).Value = rights.ScreenId;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public  void AddUserAccess(clsAccessRights rights)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO AccessRight (" +
                    "CanAccess, " +
                    "Adding, " +
                    "Deleting, " +
                    "Updating, " +
                    "Printing, " +
                    "UserId, " +
                    "ScreenId) " +
                    "VALUES (" +
                    "@CanAccess, " +
                    "@Adding, " +
                    "@Deleting, " +
                    "@Updating, " +
                    "@Printing, " +
                    "@UserId, " +
                    "@ScreenId);";


                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CanAccess", SqlDbType.Bit).Value = rights.CanAccess;
                command.Parameters.Add("@Adding", SqlDbType.Bit).Value = rights.Adding;
                command.Parameters.Add("@Deleting", SqlDbType.Bit).Value = rights.Deleting;
                command.Parameters.Add("@Updating", SqlDbType.Bit).Value = rights.Updating;
                command.Parameters.Add("@Printing", SqlDbType.Bit).Value = rights.Printing;
                command.Parameters.Add("@UserId", SqlDbType.Int).Value = rights.UserId;
                command.Parameters.Add("@ScreenId", SqlDbType.Int).Value = rights.ScreenId;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public  bool UserRightsExists(int UserId ,int ScreenId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT UserId,ScreenId FROM AccessRight WHERE(UserId = " + UserId + " and ScreenId= "+ScreenId+ ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        public  clsAccessRights getUserScreenRights(int UserId, int ScreenId)
        {
            clsAccessRights rights = new clsAccessRights();
            
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM AccessRight where UserId = "+UserId+ " and ScreenId = "+ScreenId+ " ; ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                

                while (dr.Read())
                {
                    
                    if (dr["CanAccess"] == DBNull.Value)
                    { rights.CanAccess = false; }
                    else
                    { rights.CanAccess = bool.Parse(dr["CanAccess"].ToString()); }

                    if (dr["Adding"] == DBNull.Value)
                    { rights.Adding = false; }
                    else
                    { rights.Adding = bool.Parse(dr["Adding"].ToString()); }

                    if (dr["Deleting"] == DBNull.Value)
                    { rights.Deleting = false; }
                    else
                    { rights.Deleting = bool.Parse(dr["Deleting"].ToString()); }

                    if (dr["Updating"] == DBNull.Value)
                    { rights.Updating = false; }
                    else
                    { rights.Updating = bool.Parse(dr["Updating"].ToString()); }

                    if (dr["Printing"] == DBNull.Value)
                    { rights.Printing = false; }
                    else
                    { rights.Printing = bool.Parse(dr["Printing"].ToString()); }

                    rights.UserId = UserId;
                    rights.ScreenId = ScreenId;


                    
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return rights;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return rights;
            }
        }

       

    }
}
