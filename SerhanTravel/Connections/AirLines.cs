﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;


namespace SerhanTravel.Connections
{
    class AirLines
    {
        public  ArrayList AirlineArrayList()
        {
            ArrayList DisplayArray;
            DisplayArray = new ArrayList();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM AirLine ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsAirLine airLine;

                while (dr.Read())
                {
                    airLine = new clsAirLine();
                    if (dr["AirLineId"] == DBNull.Value)
                    { airLine.AirLineId = 0; }
                    else
                    { airLine.AirLineId = int.Parse(dr["AirLineId"].ToString()); }

                    if (dr["AirLineName"] == DBNull.Value)
                    { airLine.AirLineName = " "; }
                    else
                    { airLine.AirLineName = dr["AirLineName"].ToString(); }

                    DisplayArray.Add(airLine);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public  clsAirLine getAirLineById(int id)
        {
            clsAirLine airLine = new clsAirLine();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM AirLine where AirLineId=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["AirLineId"] == DBNull.Value)
                    { airLine.AirLineId = 0; }
                    else
                    { airLine.AirLineId = int.Parse(dr["AirLineId"].ToString()); }

                    if (dr["AirLineName"] == DBNull.Value)
                    { airLine.AirLineName = " "; }
                    else
                    { airLine.AirLineName = dr["AirLineName"].ToString(); }




                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return airLine;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return airLine;
            }
        }

        
        public void UpdateAirLine(clsAirLine airLine)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE AirLine SET " +
                "AirLineName=@AirLineName " +
                "WHERE (AirLineId=@AirLineId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);


                command.Parameters.Add("@AirLineName", SqlDbType.NVarChar).Value = airLine.AirLineName;
                command.Parameters.Add("@AirLineId", SqlDbType.Int).Value = airLine.AirLineId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public void AddAirLine(clsAirLine airLine)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO AirLine (" +
                    "AirLineName " +
                    ") " +
                    "VALUES (" +
                    "@AirLineName " +
                    " );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@AirLineName", SqlDbType.NVarChar).Value = airLine.AirLineName;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool DeleteAirLine(int airLineId)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM AirLine WHERE (AirLineId = " + airLineId + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }
        public bool AirLineUsed(int AirLineId)
        {
            if(!ExistsTicket(AirLineId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsTicket(int AirLineId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT AirlineId FROM Ticket WHERE(AirlineId = " + AirLineId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool AirLineIDExists(int AirLineId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT AirLineId FROM AirLine WHERE(AirLineId = " + AirLineId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
