﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;

namespace SerhanTravel.Connections
{
    class Bank
    {
        public  clsBank getBankById(int id)
        {
            clsBank bank = new clsBank();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Bank where BankId=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["BankId"] == DBNull.Value)
                    { bank.BankId = 0; }
                    else
                    { bank.BankId = int.Parse(dr["BankId"].ToString()); }

                    if (dr["BankName"] == DBNull.Value)
                    { bank.BankName = " "; }
                    else
                    { bank.BankName = dr["BankName"].ToString(); }




                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return bank;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return bank;
            }
        }

        public  List<clsBank> BankArrayList()
        {
            List<clsBank> DisplayArray;
            DisplayArray = new List<clsBank>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Bank ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsBank bank;

                while (dr.Read())
                {
                    bank = new clsBank();
                    if (dr["BankId"] == DBNull.Value)
                    { bank.BankId = 0; }
                    else
                    { bank.BankId = int.Parse(dr["BankId"].ToString()); }

                    if (dr["BankName"] == DBNull.Value)
                    { bank.BankName = " "; }
                    else
                    { bank.BankName = dr["BankName"].ToString(); }

                    DisplayArray.Add(bank);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public  bool BankIDExists(int BankId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BankId FROM Bank WHERE(BankId = " + BankId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void UpdateBank(clsBank bank)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Bank SET " +
                "BankName=@BankName " +
                "WHERE (BankId=@BankId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);


                command.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = bank.BankName;
                command.Parameters.Add("@BankId", SqlDbType.Int).Value = bank.BankId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public void AddBank(clsBank bank)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Bank (" +
                    "BankName " +
                    ") " +
                    "VALUES (" +
                    "@BankName " +
                    " );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@BankName", SqlDbType.NVarChar).Value = bank.BankName;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteBank(int bankId)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Bank WHERE (BankId = " + bankId + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }
        public bool BankUsed(int BankId)
        {
            if(!ExistsInTicketsPayment(BankId) && !ExistsInSupplierTicketPayments(BankId) && !ExistsInSupplierItemPayments(BankId) && !ExistsInPackagePayments(BankId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInTicketsPayment(int BankId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BankId FROM TicketsPayment WHERE(BankId = " + BankId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplierTicketPayments(int BankId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BankId FROM SupplierTicketPayment WHERE(BankId = " + BankId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplierItemPayments(int BankId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BankId FROM SupplierItemPayments WHERE(BankId = " + BankId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackagePayments(int BankId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BankId FROM PackagePayments WHERE(BankId = " + BankId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
