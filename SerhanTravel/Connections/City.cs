﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;

namespace SerhanTravel.Connections
{
    class City
    {

        public  List<clsCity> CitiesArrayList()
        {
            List<clsCity> DisplayArray;
            DisplayArray = new List<clsCity>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM City;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsCity city;

                while (dr.Read())
                {
                    city = new clsCity();
                    if (dr["CityId"] == DBNull.Value)
                    { city.CityId = 0; }
                    else
                    { city.CityId = int.Parse(dr["CityId"].ToString()); }

                    if (dr["ArabicName"] == DBNull.Value)
                    { city.ArabicName = " "; }
                    else
                    { city.ArabicName = dr["ArabicName"].ToString(); }

                    if (dr["EnglishName"] == DBNull.Value)
                    { city.EnglishName = " "; }
                    else
                    { city.EnglishName = dr["EnglishName"].ToString(); }
                    if (dr["CountryId"] == DBNull.Value)
                    { city.CountryId = 0; }
                    else
                    { city.CountryId = int.Parse(dr["CountryId"].ToString()); }

                    DisplayArray.Add(city);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public  List<clsCity> GetCitiesByCountryId(int CountryId)
        {
            List<clsCity> DisplayArray;
            DisplayArray = new List<clsCity>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM City Where CountryId = " + CountryId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsCity city;

                while (dr.Read())
                {
                    city = new clsCity();
                    if (dr["CityId"] == DBNull.Value)
                    { city.CityId = 0; }
                    else
                    { city.CityId = int.Parse(dr["CityId"].ToString()); }

                    if (dr["ArabicName"] == DBNull.Value)
                    { city.ArabicName = " "; }
                    else
                    { city.ArabicName = dr["ArabicName"].ToString(); }

                    if (dr["EnglishName"] == DBNull.Value)
                    { city.EnglishName = " "; }
                    else
                    { city.EnglishName = dr["EnglishName"].ToString(); }
                    if (dr["CountryId"] == DBNull.Value)
                    { city.CountryId = 0; }
                    else
                    { city.CountryId = int.Parse(dr["CountryId"].ToString()); }

                    DisplayArray.Add(city);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public void UpdateCity(clsCity city)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE City SET " +
                "ArabicName=@CityArabicName, " +
                "EnglishName=@CityEnglishName, " +
                "CountryId=@CountryID " +
                "WHERE (CityId=@CityID);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CityArabicName", SqlDbType.NVarChar).Value = city.ArabicName;
                command.Parameters.Add("@CityEnglishName", SqlDbType.NVarChar).Value = city.EnglishName;
                command.Parameters.Add("@CountryID", SqlDbType.Int).Value = city.CountryId;
                command.Parameters.Add("@CityID", SqlDbType.Int).Value = city.CityId;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddCity(clsCity city)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO City (" +
                    "ArabicName, " +
                    "EnglishName, " +
                    "CountryId) " +
                    "VALUES (" +
                    "@CityArabicName, " +
                    "@CityEnglishName, " +
                    "@CountryID);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CityArabicName", SqlDbType.NVarChar).Value = city.ArabicName;
                command.Parameters.Add("@CityEnglishName", SqlDbType.NVarChar).Value = city.EnglishName;
                command.Parameters.Add("@CountryID", SqlDbType.Int).Value = city.CountryId;
                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteCity(int cityID)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM City WHERE (CityId = " + cityID + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }
        public bool CityUsed(int CityId)
        {
            if(!ExistsInSupplier(CityId) && !ExistsInCustomer(CityId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInCustomer(int CityId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CityId FROM Customer WHERE(CityId = " + CityId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplier(int CityId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CityId FROM Supplier WHERE(CityId = " + CityId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool CityIDExists(int CityId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CityId FROM City WHERE(CityId = " + CityId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
