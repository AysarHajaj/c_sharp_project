﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Company
    {

        public List<clsCompany> CompanyInfoArrayList()
        {
           List<clsCompany> DisplayArray = new List<clsCompany>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM CompanyInfo";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
             
                clsCompany company;

                while (dr.Read())
                {
                    company  = new clsCompany();
                    if (dr["BranchId"] == DBNull.Value)
                    { company.BranchID = 0; }
                    else
                    { company.BranchID = int.Parse(dr["BranchId"].ToString()); }

                    if (dr["CompanyName"] == DBNull.Value)
                    { company.CompanyName = " "; }
                    else
                    { company.CompanyName = dr["CompanyName"].ToString(); }

                    if (dr["CompanyActivity"] == DBNull.Value)
                    { company.CompanyActivity = " "; }
                    else
                    { company.CompanyActivity = dr["CompanyActivity"].ToString(); }

                    if (dr["CRNumber"] == DBNull.Value)
                    { company.CRNumber = string.Empty; }
                    else
                    { company.CRNumber = dr["CRNumber"].ToString(); }

                    if (dr["Address"] == DBNull.Value)
                    { company.Address =" "; }
                    else
                    { company.Address = dr["Address"].ToString(); }

                    if (dr["PhoneNumber"] == DBNull.Value)
                    { company.PhoneNumber= string.Empty; }
                    else
                    { company.PhoneNumber = dr["PhoneNumber"].ToString(); }

                    if (dr["FaxNumber"] == DBNull.Value)
                    { company.FaxNumber = string.Empty; }
                    else
                    { company.FaxNumber = dr["FaxNumber"].ToString(); }

                    if (dr["EmailAddress"] == DBNull.Value)
                    { company.EmailAddress =" "; }
                    else
                    { company.EmailAddress = dr["EmailAddress"].ToString(); }

                    if (dr["WebSite"] == DBNull.Value)
                    { company.WebSite = " "; }
                    else
                    { company.WebSite = dr["WebSite"].ToString(); }

                    DisplayArray.Add(company);
                  
                }
             
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal bool BranchIDExists(int BranchId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT BranchId FROM CompanyInfo WHERE(BranchId = " + BranchId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        internal void UpdateCompanyInfo(clsCompany company)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update CompanyInfo set " +
                        "CompanyName = @CompanyName, " +
                        "CompanyActivity = @CompanyActivity, " +
                        "CRNumber = @CRNumber, " +
                        "Address = @Address, " +
                        "PhoneNumber = @PhoneNumber, " +
                        "FaxNumber = @FaxNumber, " +
                        "EmailAddress = @EmailAddress, " +
                        "Website = @Website "+
                        "WHERE (BranchId = @BranchId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = company.CompanyName;
                myCommand.Parameters.Add("@CompanyActivity", SqlDbType.NVarChar).Value = company.CompanyActivity;
                myCommand.Parameters.Add("@CRNumber", SqlDbType.NVarChar).Value = company.CRNumber;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = company.Address;
                myCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar).Value = company.PhoneNumber;
                myCommand.Parameters.Add("@FaxNumber", SqlDbType.NVarChar).Value = company.FaxNumber;
                myCommand.Parameters.Add("@EmailAddress", SqlDbType.Text).Value = company.EmailAddress;
                myCommand.Parameters.Add("@Website", SqlDbType.Text).Value = company.WebSite;
                myCommand.Parameters.Add("@BranchId", SqlDbType.Int).Value = company.BranchID;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(BranchId) FROM CompanyInfo;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }

               
                
            
              
             
                myTrans.Commit();
               
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        internal void AddNewCompany(clsCompany company)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into CompanyInfo (" +
                        "CompanyName, " +
                        "CompanyActivity , " +
                        "CRNumber, " +
                        "Address, " +
                        "PhoneNumber , " +
                        "FaxNumber, " +
                        "EmailAddress, " +
                        "Website " +
                        ") " +
                      "VALUES (" +
                        "@CompanyName, " +
                        "@CompanyActivity, " +
                        "@CRNumber, " +
                        "@Address, " +
                        "@PhoneNumber, " +
                        "@FaxNumber, " +
                        "@EmailAddress, " +
                        "@Website);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = company.CompanyName;
                myCommand.Parameters.Add("@CompanyActivity", SqlDbType.NVarChar).Value = company.CompanyActivity;
                myCommand.Parameters.Add("@CRNumber", SqlDbType.NVarChar).Value = company.CRNumber;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = company.Address;
                myCommand.Parameters.Add("@PhoneNumber", SqlDbType.NVarChar).Value = company.PhoneNumber;
                myCommand.Parameters.Add("@FaxNumber", SqlDbType.NVarChar).Value = company.FaxNumber;
                myCommand.Parameters.Add("@EmailAddress", SqlDbType.Text).Value = company.EmailAddress;
                myCommand.Parameters.Add("@Website", SqlDbType.Text).Value = company.WebSite;



                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
