﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;

namespace SerhanTravel.Connections
{
    class Country
    {
        public  clsCountry getCountryById(int id)
        {
            clsCountry country = new clsCountry();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Country where CountryID=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["CountryID"] == DBNull.Value)
                    { country.CountryId = 0; }
                    else
                    { country.CountryId = int.Parse(dr["CountryID"].ToString()); }

                    if (dr["ArabicName"] == DBNull.Value)
                    { country.ArabicName = " "; }
                    else
                    { country.ArabicName = dr["ArabicName"].ToString(); }

                    if (dr["EnglishName"] == DBNull.Value)
                    { country.EnglishName = " "; }
                    else
                    { country.EnglishName = dr["EnglishName"].ToString(); }



                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return country;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return country;
            }
        }
        public  List<clsCountry> CountryArrayList()
        {
            List<clsCountry> DisplayArray;
            DisplayArray = new List<clsCountry>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Country;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsCountry country;

                while (dr.Read())
                {
                    country = new clsCountry();
                    if (dr["CountryID"] == DBNull.Value)
                    { country.CountryId = 0; }
                    else
                    { country.CountryId = int.Parse(dr["CountryID"].ToString()); }

                    if (dr["ArabicName"] == DBNull.Value)
                    { country.ArabicName = " "; }
                    else
                    { country.ArabicName = dr["ArabicName"].ToString(); }

                    if (dr["EnglishName"] == DBNull.Value)
                    { country.EnglishName = " "; }
                    else
                    { country.EnglishName = dr["EnglishName"].ToString(); }


                    DisplayArray.Add(country);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public  bool CountryIDExists(int CountryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CountryId FROM Country WHERE(CountryId = " + CountryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void UpdateCoutry(clsCountry country)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Country SET " +
                "ArabicName=@CountryArabicName, " +
                "EnglishName=@CountryEnglishName " +
                "WHERE (CountryId=@CountryId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CountryArabicName", SqlDbType.NVarChar).Value = country.ArabicName;
                command.Parameters.Add("@CountryEnglishName", SqlDbType.NVarChar).Value = country.EnglishName;
                command.Parameters.Add("@CountryID", SqlDbType.Int).Value = country.CountryId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddCountry(clsCountry country)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Country (" +
                    "ArabicName, " +
                    "EnglishName ) " +
                    "VALUES (" +
                    "@CountryArabicName, " +
                    "@CountryEnglishName );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CountryArabicName", SqlDbType.NVarChar).Value = country.ArabicName;
                command.Parameters.Add("@CountryEnglishName", SqlDbType.NVarChar).Value = country.EnglishName;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool DeleteCountry(int countryId)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Country WHERE (CountryId = " + countryId + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

        public bool CountryUsed(int CountryId)
        {
            if(!ExistsInPackage(CountryId) && !ExistsInCity(CountryId) && !ExistsInCustomer(CountryId) && !ExistsInSupplier(CountryId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInSupplier(int CountryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CountryId FROM Supplier WHERE(CountryId = " + CountryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInCustomer(int CountryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CountryId FROM Customer WHERE(CountryId = " + CountryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInCity(int CountryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CountryId FROM City WHERE(CountryId = " + CountryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackage(int CountryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CountryID FROM Package WHERE(CountryID = " + CountryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
