﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Currency
    {
        public  List<clsCurrency> CurrencyArrayList()
        {
            List<clsCurrency> DisplayArray;
            DisplayArray = new List<clsCurrency>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Currency;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsCurrency currency;

                while (dr.Read())
                {
                    currency = new clsCurrency();
                    if (dr["CurrencyId"] == DBNull.Value)
                    { currency.CurrencyId = 0; }
                    else
                    { currency.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    { currency.CurrencyRate = 0; }
                    else
                    { currency.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }

                    if (dr["CurrencySymbol"] == DBNull.Value)
                    { currency.CurrencySymbol = " "; }
                    else
                    { currency.CurrencySymbol = dr["CurrencySymbol"].ToString(); }
                    if (dr["CurrencyName"] == DBNull.Value)
                    { currency.CurrencyName = " "; }
                    else
                    { currency.CurrencyName = dr["CurrencyName"].ToString(); }

                    DisplayArray.Add(currency);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public  clsCurrency GetCurrencyById(int CurrecnyId)
        {
            clsCurrency currency = null;

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Currency Where CurrencyId = " + CurrecnyId + " ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    currency = new clsCurrency();


                    if (dr["CurrencyId"] == DBNull.Value)
                    { currency.CurrencyId = 0; }
                    else
                    { currency.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    { currency.CurrencyRate = 0; }
                    else
                    { currency.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }

                    if (dr["CurrencySymbol"] == DBNull.Value)
                    { currency.CurrencySymbol = " "; }
                    else
                    { currency.CurrencySymbol = dr["CurrencySymbol"].ToString(); }
                    if (dr["CurrencyName"] == DBNull.Value)
                    { currency.CurrencyName = " "; }
                    else
                    { currency.CurrencyName = dr["CurrencyName"].ToString(); }



                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return currency;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return currency;
            }
        }
        public  bool CurrencyIDExists(int currencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM Currency WHERE(CurrencyId = " + currencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        public bool DeleteCurrency(int currencyID)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Currency WHERE (CurrencyId = " + currencyID + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

        public void UpdateCurrency(clsCurrency currency)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Currency SET " +
                "CurrencyName=@CurrencyName, " +
                "CurrencySymbol=@CurrencySymbol, " +
                "CurrencyRate=@CurrencyRate " +
                "WHERE (CurrencyId=@CurrencyId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CurrencyName", SqlDbType.NVarChar).Value = currency.CurrencyName;
                command.Parameters.Add("@CurrencySymbol", SqlDbType.NVarChar).Value = currency.CurrencySymbol;
                command.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = currency.CurrencyRate;
                command.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = currency.CurrencyId;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddCurrency(clsCurrency currency)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Currency (" +
                    "CurrencyName, " +
                    "CurrencySymbol, " +
                    "CurrencyRate) " +
                    "VALUES (" +
                    "@CurrencyName, " +
                    "@CurrencySymbol, " +
                    "@CurrencyRate);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@CurrencyName", SqlDbType.NVarChar).Value = currency.CurrencyName;
                command.Parameters.Add("@CurrencySymbol", SqlDbType.NVarChar).Value = currency.CurrencySymbol;
                command.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = currency.CurrencyRate;
                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool CurrencyUsed(int CurrencyId)
        {
            if(!ExistsInCustomerBalance(CurrencyId) && !ExistsInExpenses(CurrencyId) && !ExistsInInvoicePackages(CurrencyId) &&!ExistsInItem(CurrencyId)
                && !ExistsInPackage(CurrencyId) && !ExistsInPackagePayments(CurrencyId) && !ExistsInReturnedCustomerPackage(CurrencyId) && !ExistsInReturnedCustomerTicket(CurrencyId)
                && !ExistsInReturnedSupplierItem(CurrencyId) && !ExistsInReturnedSupplierTicket(CurrencyId) && !ExistsInSupplierBalanceInventory(CurrencyId) && !ExistsInSupplierItemPayments(CurrencyId)
                && !ExistsInSupplierTicketPayments(CurrencyId) && !ExistsInTicket(CurrencyId) && !ExistsInTicketsPayment(CurrencyId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInTicketsPayment(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM TicketsPayment WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInTicket(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM Ticket WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplierTicketPayments(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM SupplierTicketPayment WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplierItemPayments(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM SupplierItemPayments WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInSupplierBalanceInventory(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM SupplierBalanceInventory WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedSupplierTicket(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM ReturnedSupplierTicket WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedSupplierItem(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM ReturnedSupplierItem WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedCustomerTicket(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM ReturnedCustomerTicket WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedCustomerPackage(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM ReturnedCustomerPackage WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackagePayments(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM PackagePayments WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackage(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM Package WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInItem(int CurrencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM Item WHERE(CurrencyId = " + CurrencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInInvoicePackages(int currencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM InvoicePackages WHERE(CurrencyId = " + currencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInCustomerBalance(int currencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CurrencyId FROM CustomerBalanceInventory WHERE(CurrencyId = " + currencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInExpenses(int currencyId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ExpensesCurrencyId FROM Expenses WHERE(ExpensesCurrencyId = " + currencyId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
