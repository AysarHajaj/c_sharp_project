﻿using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Customer
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsCustomer> CustomersArrayList()
        {
            List<clsCustomer> DisplayArray = new List<clsCustomer>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Customer ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsCustomer customer;
                
                
                while (dr.Read())
                {
                    customer = new clsCustomer();
                    if (dr["CustomerId"] == DBNull.Value)
                    { customer.CustomerId = 0; }
                    else
                    { customer.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                   
                    if (dr["Name"] == DBNull.Value)
                    { customer.CustomerName = " "; }
                    else
                    { customer.CustomerName = dr["Name"].ToString(); }

                    if (dr["CustomerType"] == DBNull.Value)
                    { customer.CustomerType = 0; }
                    else
                    { customer.CustomerType = int.Parse(dr["CustomerType"].ToString()); }

                    if (dr["NationalityId"] == DBNull.Value)
                    { customer.NationalityId = 0; }
                    else
                    { customer.NationalityId = int.Parse(dr["NationalityId"].ToString()); }

                    if (dr["CountryId"] == DBNull.Value)
                    { customer.CountryId = 0; }
                    else
                    { customer.CountryId = int.Parse(dr["CountryId"].ToString()); }

                    if (dr["CityId"] == DBNull.Value)
                    { customer.CityId = 0; }
                    else
                    { customer.CityId = int.Parse(dr["CityId"].ToString()); }

                    if (dr["Address"] == DBNull.Value)
                    { customer.Address = " "; }
                    else
                    { customer.Address = dr["Address"].ToString(); }

                    if (dr["BirthDate"] == DBNull.Value)
                    { customer.BirthDate = DefaultDate; }
                    else
                    { customer.BirthDate = DateTime.Parse(dr["BirthDate"].ToString()); }

                    if (dr["BirthPlace"] == DBNull.Value)
                    { customer.BirthPlace = " "; }
                    else
                    { customer.BirthPlace = dr["BirthPlace"].ToString(); }
                    if (dr["MobilePhone"] == DBNull.Value)
                    { customer.MobilePhone = string.Empty; }
                    else
                    { customer.MobilePhone = dr["MobilePhone"].ToString(); }
                    if (dr["LandPhone"] == DBNull.Value)
                    { customer.LandPhone = " "; }
                    else
                    { customer.LandPhone = dr["LandPhone"].ToString(); }
                    if (dr["FaxNo"] == DBNull.Value)
                    { customer.FaxNo = string.Empty; }
                    else
                    { customer.FaxNo = dr["FaxNo"].ToString(); }
                    if (dr["Email"] == DBNull.Value)
                    { customer.Email = " "; }
                    else
                    { customer.Email = dr["Email"].ToString(); }
                    if (dr["Website"] == DBNull.Value)
                    { customer.WebSite = " "; }
                    else
                    { customer.WebSite = dr["Website"].ToString(); }
                    if (dr["Status"] == DBNull.Value)
                    { customer.Status = 0; }
                    else
                    { customer.Status =int.Parse(dr["Status"].ToString()) ; }

                    DisplayArray.Add(customer);
                   
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal bool CusomterIDExists(int CustomerId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CustomerId FROM Customer WHERE(CustomerId = " + CustomerId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        internal void UpdateCustomerInfo(clsCustomer customer)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update Customer set " +
                        "Name = @Name, " +
                        "NationalityId = @NationalityId, " +
                        "CustomerType = @CutomerType, " +
                        "CountryId = @CountryId, " +
                        "CityId = @CityId, " +
                        "Address = @Address, " +
                        "BirthDate = @BirthDate, " +
                        "BirthPlace = @BirhtPlace, " +
                         "MobilePhone = @MobilePhone, " +
                        "LandPhone = @LandPhone, " +
                        "FaxNo = @FaxNo, " +
                        "Email = @Email, " +
                         "Website = @Website, " +
                        "Status = @Status " +
                        "WHERE (CustomerId  = @CustomerId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = customer.CustomerName;
                myCommand.Parameters.Add("@NationalityId", SqlDbType.Int).Value = customer.NationalityId;
                myCommand.Parameters.Add("@CutomerType", SqlDbType.Int).Value = customer.CustomerType;
                myCommand.Parameters.Add("@CountryId", SqlDbType.Int).Value = customer.CountryId;
                myCommand.Parameters.Add("@CityId", SqlDbType.Int).Value = customer.CityId;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = customer.Address;
                myCommand.Parameters.Add("@BirthDate", SqlDbType.Date).Value = customer.BirthDate;
                myCommand.Parameters.Add("@BirhtPlace", SqlDbType.NVarChar).Value = customer.BirthPlace;
                myCommand.Parameters.Add("@MobilePhone", SqlDbType.NVarChar).Value = customer.MobilePhone;
                myCommand.Parameters.Add("@LandPhone", SqlDbType.NVarChar).Value = customer.LandPhone;
                myCommand.Parameters.Add("@FaxNo", SqlDbType.NVarChar).Value = customer.FaxNo;
                myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = customer.Email;
                myCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = customer.WebSite;
                myCommand.Parameters.Add("@Status", SqlDbType.Int).Value = customer.Status;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = customer.CustomerId;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
      
        internal void AddNewCustomer(clsCustomer customer)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into Customer (" +
                        "Name, " +
                        "NationalityId , " +
                        "CustomerType, " +
                        "CountryId, " +
                        "CityId , " +
                        "Address, " +
                        "BirthDate, " +
                        "BirthPlace, " +
                       "MobilePhone, " +
                        "LandPhone, " +
                        "FaxNo, " +
                        "Email , " +
                        "Website, " +
                        "Status" +
                        ") " +
                      "VALUES (" +
                        "@Name, " +
                        "@NationalityId, " +
                        "@CutomerType, " +
                        "@CountryId, " +
                        "@CityId, " +
                        "@Address, " +
                        "@BirthDate, " +
                        "@BirhtPlace, " +
                         "@MobilePhone, " +
                        "@LandPhone, " +
                        "@FaxNo, " +
                        "@Email, " +
                        "@Website," +
                        "@Status " +
                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = customer.CustomerName;
                myCommand.Parameters.Add("@NationalityId", SqlDbType.Int).Value = customer.NationalityId;
                myCommand.Parameters.Add("@CutomerType", SqlDbType.Int).Value = customer.CustomerType;
                myCommand.Parameters.Add("@CountryId", SqlDbType.Int).Value = customer.CountryId;
                myCommand.Parameters.Add("@CityId", SqlDbType.Int).Value = customer.CityId;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = customer.Address;
                myCommand.Parameters.Add("@BirthDate", SqlDbType.Date).Value = customer.BirthDate;
                myCommand.Parameters.Add("@BirhtPlace", SqlDbType.NVarChar).Value = customer.BirthPlace;
                myCommand.Parameters.Add("@MobilePhone", SqlDbType.NVarChar).Value = customer.MobilePhone;
                myCommand.Parameters.Add("@LandPhone", SqlDbType.NVarChar).Value = customer.LandPhone;
                myCommand.Parameters.Add("@FaxNo", SqlDbType.NVarChar).Value = customer.FaxNo;
                myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = customer.Email;
                myCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = customer.WebSite;
                myCommand.Parameters.Add("@Status", SqlDbType.Int).Value = customer.Status;




                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public bool DeleteCustomer(int CustomerId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM Customer WHERE (CustomerId = " + CustomerId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        public clsCustomer getCustomerById(int id)
        {
            clsCustomer customer = new clsCustomer();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Customer where CustomerId=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["CustomerId"] == DBNull.Value)
                    { customer.CustomerId = 0; }
                    else
                    { customer.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { customer.CustomerName = " "; }
                    else
                    { customer.CustomerName = dr["Name"].ToString(); }

                    if (dr["CustomerType"] == DBNull.Value)
                    { customer.CustomerType = 0; }
                    else
                    { customer.CustomerType = int.Parse(dr["CustomerType"].ToString()); }

                    if (dr["NationalityId"] == DBNull.Value)
                    { customer.NationalityId = 0; }
                    else
                    { customer.NationalityId = int.Parse(dr["NationalityId"].ToString()); }

                    if (dr["CountryId"] == DBNull.Value)
                    { customer.CountryId = 0; }
                    else
                    { customer.CountryId = int.Parse(dr["CountryId"].ToString()); }

                    if (dr["CityId"] == DBNull.Value)
                    { customer.CityId = 0; }
                    else
                    { customer.CityId = int.Parse(dr["CityId"].ToString()); }

                    if (dr["Address"] == DBNull.Value)
                    { customer.Address = " "; }
                    else
                    { customer.Address = dr["Address"].ToString(); }

                    if (dr["BirthDate"] == DBNull.Value)
                    { customer.BirthDate = DefaultDate; }
                    else
                    { customer.BirthDate = DateTime.Parse(dr["BirthDate"].ToString()); }

                    if (dr["BirthPlace"] == DBNull.Value)
                    { customer.BirthPlace = " "; }
                    else
                    { customer.BirthPlace = dr["BirthPlace"].ToString(); }
                    if (dr["MobilePhone"] == DBNull.Value)
                    { customer.MobilePhone = string.Empty; }
                    else
                    { customer.MobilePhone = dr["MobilePhone"].ToString(); }
                    if (dr["LandPhone"] == DBNull.Value)
                    { customer.LandPhone = " "; }
                    else
                    { customer.LandPhone = dr["LandPhone"].ToString(); }
                    if (dr["FaxNo"] == DBNull.Value)
                    { customer.FaxNo = string.Empty; }
                    else
                    { customer.FaxNo = dr["FaxNo"].ToString(); }
                    if (dr["Email"] == DBNull.Value)
                    { customer.Email = " "; }
                    else
                    { customer.Email = dr["Email"].ToString(); }
                    if (dr["Website"] == DBNull.Value)
                    { customer.WebSite = " "; }
                    else
                    { customer.WebSite = dr["Website"].ToString(); }
                    if (dr["Status"] == DBNull.Value)
                    { customer.Status = 0; }
                    else
                    { customer.Status = int.Parse(dr["Status"].ToString()); }



                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return customer;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return customer;
            }
        }
        public List<clsTickets> getUnReturnedTicekts(int customerId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * " +
                    " FROM Ticket where CustomerId =" + customerId+ " and Ticket.TicketId not In(Select TicketId from ReturnedCustomerTicket )";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public decimal getCustomerBalance(int CustomerId,DateTime InventoryDate,DateTime fromDate)
        {
             decimal balance = 0;
            try
            {
                balance += this.UnPaidTickets(CustomerId, InventoryDate, fromDate);
                //balance += this.UnCompletePaidTickets(CustomerId, InventoryDate, fromDate);
                balance += this.UnPaidPackage(CustomerId, InventoryDate, fromDate);
               // balance += this.UnCompletePaidPackage(CustomerId, InventoryDate, fromDate);
                return balance;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }

        public decimal UnPaidTickets(int CustomerId,DateTime InventoryDate,DateTime FromDate)
        {   
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =

                  " Select Sum(TI.Price*TI.CurrencyRate) as Balance  " +
                   " from Ticket as TI  " +
                   " where TI.CustomerId = @CustomerId and TI.TicketDate > @InventoryDate " +
                   " and TI.TicketDate < @FromDate " +
                   "and TI.TicketId NOt In (Select RT.TicketId from ReturnedCustomerTicket as RT )"; 
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
               " Select (TI.Price*TI.CurrencyRate-(RT.ReturnAmount * RT.CurrencyRate)) as Balance  " +
                " from Ticket as TI Inner Join ReturnedCustomerTicket as RT on RT.TicketId = TI.TicketId " +
                 "where TI.CustomerId = @CustomerId and TI.TicketDate > @InventoryDate " +
                 "and TI.TicketDate <@FromDate  ";
               
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                
                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }
        /*public decimal UnCompletePaidTickets(int CustomerId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                
                string query =
            " Select((TI.Price*TI.CurrencyRate) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance " +
                   "from Ticket as TI Inner Join TicketsPayment as Tp on Tp.TicketId = TI.TicketId " +
                   "where TI.CustomerId =  @CustomerId and TI.TicketDate > @InventoryDate  and TI.TicketDate < @FromDate " +
                   "and TI.TicketId NOt In (Select RT.TicketId from ReturnedCustomerTicket as RT ) " +
                   "Group By TI.Price,TI.CurrencyRate " +
                  "having TI.Price*TI.CurrencyRate > SUM(Tp.Payment * Tp.CurrencyRate);";
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
                " Select((TI.Price*TI.CurrencyRate - IsNull(amount.ReturnedAmount,0)) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance " +
                " from Ticket as TI Inner Join TicketsPayment as Tp on Tp.TicketId = TI.TicketId " +
                "  Right Join (Select sum(RT.ReturnAmount*RT.CurrencyRate ) as ReturnedAmount,RT.TicketId from ReturnedCustomerTicket  RT " +
                "  Group by RT.TicketId) as amount on TI.TicketId= amount.TicketId  " +
                "  where TI.CustomerId = @CustomerId and TI.TicketDate > @InventoryDate  and TI.TicketDate < @FromDate " +
                " Group By TI.Price ,TI.TicketId,amount.ReturnedAmount,TI.CurrencyRate " +
                "   having(TI.Price*TI.CurrencyRate - IsNull(amount.ReturnedAmount,0)) > SUM(Tp.Payment * Tp.CurrencyRate);";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
            
                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }*/

        public decimal UnPaidPackage(int CustomerId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                  " Select Sum((TI.NetPrice-(TI.NetPrice*TI.Discount*0.01))) as Balance " +
                " from PackageInvoice as TI " +
                " where TI.CustomerId = @CustomerId and TI.Date > @InventoryDate and TI.Date < @FromDate  " +
                " and TI.InvoiceId Not in(Select RP.InvoiceId from ReturnedCustomerPackage as RP );";
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query=
                    "Select(((TI.NetPrice-(TI.NetPrice*TI.Discount*0.01)) - (RP.ReturnAmount * RP.CurrencyRate))) as Balance " +
                    "from PackageInvoice as TI "+
                    "Inner Join ReturnedCustomerPackage As RP on RP.InvoiceId = TI.InvoiceId "+
                    "where TI.CustomerId = @CustomerId and TI.Date > @InventoryDate  and TI.Date < @FromDate ";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                 dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
             
                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }


        /*public decimal UnCompletePaidPackage(int CustomerId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                "Select((TI.NetPrice) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance " +
               " from PackageInvoice as TI Inner Join PackagePayments as Tp on Tp.PackageInvoiceId = TI.InvoiceId " +
               " where TI.CustomerId = @CustomerId  and TI.Date > @InventoryDate  and TI.Date < @FromDate " +
               "and TI.InvoiceId NOt In (Select RT.InvoiceId from ReturnedCustomerPackage as RT )  " +
               " Group By TI.NetPrice " +
               " having TI.NetPrice > SUM(Tp.Payment * Tp.CurrencyRate); ";
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
                    "Select((TI.NetPrice - IsNull(amount.ReturnedAmount,0)) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance  " +
                    "from PackageInvoice as TI Inner Join PackagePayments as Tp on Tp.PackageInvoiceId = TI.InvoiceId " +
                    "Right Join (Select sum(RP.ReturnAmount*RP.CurrencyRate ) as ReturnedAmount,RP.InvoiceId from ReturnedCustomerPackage  RP " +
                    "Group by RP.InvoiceId) as amount on TI.InvoiceId= amount.InvoiceId " +
                    "where TI.CustomerId = @CustomerId  and TI.Date > @InventoryDate and TI.Date < @FromDate " +
                    "Group By TI.NetPrice ,TI.InvoiceId,amount.ReturnedAmount " +
                    "having (TI.NetPrice - IsNull(amount.ReturnedAmount,0)) > SUM(Tp.Payment * Tp.CurrencyRate)   ; ";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }

                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }*/

        public bool CustomerUsed(int CustomerId)
        {
            if (!ExistsInPackageInvoice(CustomerId) && !ExistsInPackagePayment(CustomerId) && !ExistsInTicket(CustomerId) && !ExistsInTicketPayment(CustomerId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInTicketPayment(int CustomerId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CustomerId FROM TicketsPayment WHERE(CustomerId = " + CustomerId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInTicket(int CustomerId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CustomerId FROM Ticket WHERE(CustomerId = " + CustomerId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackageInvoice(int CustomerId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CustomerId FROM PackageInvoice WHERE(CustomerId = " + CustomerId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackagePayment(int CustomerId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT CustomerId FROM PackagePayments WHERE(CustomerId = " + CustomerId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

    }
}