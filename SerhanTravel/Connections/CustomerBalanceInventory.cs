﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class CustomerBalanceInventory
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        internal bool InventoryIDExists(int inventoryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InventoryId FROM CustomerBalanceInventory WHERE(InventoryId = " + inventoryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        internal  List<clsCustomerInventory> getCustomerInventoryList(int supplierId)
        {
            List<clsCustomerInventory> Items = new List<clsCustomerInventory>();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM CustomerBalanceInventory Where CustomerId = "+supplierId, con);
                SqlDataReader reader = command.ExecuteReader();

                clsCustomerInventory Inventory;
                while (reader.Read())
                {
                    Inventory = new clsCustomerInventory();
                    if (reader["InventoryId"] == DBNull.Value)
                    { Inventory.InventoryId = 0; }
                    else
                    { Inventory.InventoryId = int.Parse(reader["InventoryId"].ToString()); }
                    if (reader["Balance"] == DBNull.Value)
                    { Inventory.Balance = 0; }
                    else
                    { Inventory.Balance = decimal.Parse(reader["Balance"].ToString()); }
                   
                    if (reader["CustomerId"] == DBNull.Value)
                    { Inventory.CustomerId = 0; }
                    else
                    { Inventory.CustomerId = int.Parse(reader["CustomerId"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { Inventory.CurrencyId = 0; }
                    else
                    { Inventory.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["Date"] == DBNull.Value)
                    { Inventory.Date = DefaultDate; }
                    else
                    { Inventory.Date = DateTime.Parse(reader["Date"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { Inventory.CurrencyRate = 0; }
                    else
                    { Inventory.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }
                    

                    Items.Add(Inventory);
                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Items;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Items;
            }

        }
       
            internal void UpdateInventoryInfo(clsCustomerInventory Inventory)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "update CustomerBalanceInventory set " +

                         
                            "CustomerId = @CustomerId, " +
                            "Balance = @Balance, " +
                            "CurrencyId=@CurrencyId, " +
                            "CurrencyRate=@CurrencyRate, " +
                            "Date = @Date " +

                            "WHERE (InventoryId  = @InventoryId);";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

            

                    myCommand.Parameters.Add("@Balance", SqlDbType.Money).Value =Inventory.Balance;
                    myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Inventory.Date;
                myCommand.Parameters.Add("@InventoryId", SqlDbType.Int).Value = Inventory.InventoryId;
                      myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Inventory.CurrencyId;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = Inventory.CustomerId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Inventory.CurrencyRate;

                cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

      


        internal void AddNewInventory(clsCustomerInventory Inventory)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }



                    string sqlQuery = "insert into CustomerBalanceInventory (" +
                      
                    
                                "Balance, " +
                                "CustomerId , " +
                                "CurrencyId,  " +
                                 "CurrencyRate,  " +
                                "Date  " +
                                ") " +
                                "VALUES (" +
                                "@Balance, " +
                                "@CustomerId, " +
                                "@CurrencyId, " +
                                "@CurrencyRate, " +
                                "@Date " +
                                ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Balance", SqlDbType.Money).Value = Inventory.Balance;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Inventory.CurrencyRate;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Inventory.Date;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = Inventory.CustomerId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Inventory.CurrencyId; 

                 cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }



            public bool DeleteInventory(int InventoryId)
            {
                int cmdResult;
                bool myResult;
                try
                {
                    myResult = true;
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "DELETE FROM CustomerBalanceInventory WHERE (InventoryId = " + InventoryId + ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                    cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    myResult = true;
                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return myResult;
                }
                catch (Exception ex)
                {
                    myResult = false;
                    MessageBox.Show(ex.Message);
                    return myResult;
                }
            }
            public int ReadLastNo()
            {
                int maxId;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "SELECT MAX(InventoryId) FROM CustomerBalanceInventory;";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());

                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return maxId;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return 0;
                }
            }


        internal clsCustomerInventory CustomerLastInventory(int CustomerId)
        {
            clsCustomerInventory Inventory = null;
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                string query =
                        "SELECT CB.Date,CB.CurrencyId,CB.InventoryId,(CB.Balance * CB.CurrencyRate) as Balance,CB.CustomerId " +
                        "FROM CustomerBalanceInventory as CB " +
                        "Where CB.CustomerId = " + CustomerId + "and CB.Date In(Select Max(CB.Date) as Date from CustomerBalanceInventory as CB Where CB.CustomerId =" + CustomerId + ")";
                SqlCommand command = new SqlCommand(query, con);
                SqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {

                    reader.Read();
                    Inventory = new clsCustomerInventory();
                    if (reader["InventoryId"] == DBNull.Value)
                    { Inventory.InventoryId = 0; }
                    else
                    { Inventory.InventoryId = int.Parse(reader["InventoryId"].ToString()); }
                    if (reader["Balance"] == DBNull.Value)
                    { Inventory.Balance = 0; }
                    else
                    { Inventory.Balance = decimal.Parse(reader["Balance"].ToString()); }

                    if (reader["CustomerId"] == DBNull.Value)
                    { Inventory.CustomerId = 0; }
                    else
                    { Inventory.CustomerId = int.Parse(reader["CustomerId"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { Inventory.CurrencyId = 0; }
                    else
                    { Inventory.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["Date"] == DBNull.Value)
                    { Inventory.Date = DefaultDate; }
                    else
                    { Inventory.Date = DateTime.Parse(reader["Date"].ToString()); }


                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Inventory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Inventory;
            }
        }

    }
}
