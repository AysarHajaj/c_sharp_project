﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SerhanTravel.Connections
{
    class Expenses
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public  List<clsExpenses> ExpensesArrayList(DateTime ExpensesDate)
        {
            List<clsExpenses> DisplayArray;
            DisplayArray = new List<clsExpenses>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Expenses where ExpensesDate=@ExpensesDate;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@ExpensesDate", SqlDbType.Date).Value = ExpensesDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsExpenses expenses;

                while (dr.Read())
                {
                    expenses = new clsExpenses();
                    if (dr["ExpensesId"] == DBNull.Value)
                    { expenses.ExpensesId = 0; }
                    else
                    { expenses.ExpensesId = int.Parse(dr["ExpensesId"].ToString()); }

                    if (dr["ExpensesAmount"] == DBNull.Value)
                    { expenses.ExpensesAmount = 0; }
                    else
                    { expenses.ExpensesAmount = decimal.Parse(dr["ExpensesAmount"].ToString()); }

                    if (dr["ExpensesRemarks"] == DBNull.Value)
                    { expenses.ExpensesRemarks = " "; }
                    else
                    { expenses.ExpensesRemarks = dr["ExpensesRemarks"].ToString(); }
                    if (dr["ExpensesDate"] == DBNull.Value)
                    { expenses.ExpensesDate = DefaultDate; }
                    else
                    { expenses.ExpensesDate =DateTime.Parse( dr["ExpensesDate"].ToString()); }
                    if (dr["ExpensesCurrencyId"] == DBNull.Value)
                    { expenses.ExpensesCurrencyId = 0; }
                    else
                    { expenses.ExpensesCurrencyId = int.Parse(dr["ExpensesCurrencyId"].ToString()); }
                    if (dr["ExpensesInvoiceId"] == DBNull.Value)
                    { expenses.ExpensesInvoiceId = 0; }
                    else
                    { expenses.ExpensesInvoiceId = int.Parse(dr["ExpensesInvoiceId"].ToString()); }
                    if (dr["ExpensesType"] == DBNull.Value)
                    { expenses.ExpensesType = 0; }
                    else
                    { expenses.ExpensesType = int.Parse(dr["ExpensesType"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { expenses.CurrencyRate = 0; }
                    else
                    { expenses.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(expenses);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public  clsExpenses GetExpensesById(int expensesId)
        {
            clsExpenses expenses = null;

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Expenses Where ExpensesId = " + expensesId + " ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    expenses = new clsExpenses();


                    if (dr["ExpensesId"] == DBNull.Value)
                    { expenses.ExpensesId = 0; }
                    else
                    { expenses.ExpensesId = int.Parse(dr["ExpensesId"].ToString()); }

                    if (dr["ExpensesAmount"] == DBNull.Value)
                    { expenses.ExpensesAmount = 0; }
                    else
                    { expenses.ExpensesAmount = decimal.Parse(dr["ExpensesAmount"].ToString()); }

                    if (dr["ExpensesRemarks"] == DBNull.Value)
                    { expenses.ExpensesRemarks = " "; }
                    else
                    { expenses.ExpensesRemarks = dr["ExpensesRemarks"].ToString(); }
                    if (dr["ExpensesDate"] == DBNull.Value)
                    { expenses.ExpensesDate = DefaultDate; }
                    else
                    { expenses.ExpensesDate = DateTime.Parse(dr["ExpensesDate"].ToString()); }
                    if (dr["ExpensesCurrencyId"] == DBNull.Value)
                    { expenses.ExpensesCurrencyId = 0; }
                    else
                    { expenses.ExpensesCurrencyId = int.Parse(dr["ExpensesCurrencyId"].ToString()); }
                    if (dr["ExpensesInvoiceId"] == DBNull.Value)
                    { expenses.ExpensesInvoiceId = 0; }
                    else
                    { expenses.ExpensesInvoiceId = int.Parse(dr["ExpensesInvoiceId"].ToString()); }
                    if (dr["ExpensesType"] == DBNull.Value)
                    { expenses.ExpensesType = 0; }
                    else
                    { expenses.ExpensesType = int.Parse(dr["ExpensesType"].ToString()); }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    { expenses.CurrencyRate = 0; }
                    else
                    { expenses.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return expenses;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return expenses;
            }
        }

        public  bool ExpensesIDExists(int expensesid)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ExpensesId FROM Expenses WHERE(ExpensesId = " + expensesid + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void UpdateExpenses(clsExpenses expenses)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                

                string sql = "UPDATE Expenses SET " +
                "ExpensesDate=@ExpensesDate, " +
                "ExpensesCurrencyId=@ExpensesCurrencyId, " +
                "ExpensesType=@ExpensesType, " +
                "ExpensesInvoiceId=@ExpensesInvoiceId, " +
                "ExpensesRemarks=@ExpensesRemarks, " +
                "CurrencyRate=@CurrencyRate, " +
                "ExpensesAmount=@ExpensesAmount " +
                "WHERE (ExpensesId=@ExpensesId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@ExpensesDate", SqlDbType.Date).Value = expenses.ExpensesDate;
                command.Parameters.Add("@ExpensesRemarks", SqlDbType.NVarChar).Value = expenses.ExpensesRemarks;
                command.Parameters.Add("@ExpensesAmount", SqlDbType.Money).Value = expenses.ExpensesAmount;
                command.Parameters.Add("@ExpensesId", SqlDbType.Int).Value = expenses.ExpensesId;
                command.Parameters.Add("@ExpensesCurrencyId", SqlDbType.Int).Value = expenses.ExpensesCurrencyId;
                command.Parameters.Add("@ExpensesType", SqlDbType.Int).Value = expenses.ExpensesType;
                command.Parameters.Add("@ExpensesInvoiceId", SqlDbType.Int).Value = expenses.ExpensesInvoiceId;
                command.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = expenses.CurrencyRate;
                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void AddExpenses(clsExpenses expenses)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Expenses (" +
                            "ExpensesDate, " +
                            "ExpensesCurrencyId, " +
                            "ExpensesType, " +
                            "ExpensesInvoiceId, " +
                            "ExpensesRemarks, " +
                            "CurrencyRate, " +
                            "ExpensesAmount) " +
                            "VALUES (" +
                            "@ExpensesDate, " +
                            "@ExpensesCurrencyId, " +
                            "@ExpensesType, " +
                            "@ExpensesInvoiceId, " +
                            "@ExpensesRemarks, " +
                            "@CurrencyRate, " +
                            "@ExpensesAmount);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);


                command.Parameters.Add("@ExpensesDate", SqlDbType.Date).Value = expenses.ExpensesDate;
                command.Parameters.Add("@ExpensesRemarks", SqlDbType.NVarChar).Value = expenses.ExpensesRemarks;
                command.Parameters.Add("@ExpensesAmount", SqlDbType.Money).Value = expenses.ExpensesAmount;
                command.Parameters.Add("@ExpensesId", SqlDbType.Int).Value = expenses.ExpensesId;
                command.Parameters.Add("@ExpensesCurrencyId", SqlDbType.Int).Value = expenses.ExpensesCurrencyId;
                command.Parameters.Add("@ExpensesType", SqlDbType.Int).Value = expenses.ExpensesType;
                command.Parameters.Add("@ExpensesInvoiceId", SqlDbType.Int).Value = expenses.ExpensesInvoiceId;
                command.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = expenses.CurrencyRate;
                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteExpenses(int expenses)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Expenses WHERE (ExpensesId = " + expenses + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

        public int ReadLastNO()
        {
            int id=0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT Max(ExpensesId) as  ExpensesId FROM Expenses ;", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    id = int.Parse(dr["ExpensesId"].ToString());
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return id;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return id;
            }
        }

        public List<clsExpenses> ExpensesListByTypeId(DateTime fromDate,DateTime toDate,int TypeId)
        {
            List<clsExpenses> DisplayArray;
            DisplayArray = new List<clsExpenses>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql;
                if (TypeId==0)
                {
                    sql = "SELECT * FROM Expenses where ExpensesDate between @fromDate and @toDate ; ";
                }
                else
                {
                    sql = "SELECT * FROM Expenses where ExpensesDate between @fromDate and @toDate and ExpensesType = "+TypeId+" ; ";
                }
               

                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsExpenses expenses;

                while (dr.Read())
                {
                    expenses = new clsExpenses();
                    if (dr["ExpensesId"] == DBNull.Value)
                    { expenses.ExpensesId = 0; }
                    else
                    { expenses.ExpensesId = int.Parse(dr["ExpensesId"].ToString()); }

                    if (dr["ExpensesAmount"] == DBNull.Value)
                    { expenses.ExpensesAmount = 0; }
                    else
                    { expenses.ExpensesAmount = decimal.Parse(dr["ExpensesAmount"].ToString()); }

                    if (dr["ExpensesRemarks"] == DBNull.Value)
                    { expenses.ExpensesRemarks = " "; }
                    else
                    { expenses.ExpensesRemarks = dr["ExpensesRemarks"].ToString(); }
                    if (dr["ExpensesDate"] == DBNull.Value)
                    { expenses.ExpensesDate = DefaultDate; }
                    else
                    { expenses.ExpensesDate = DateTime.Parse(dr["ExpensesDate"].ToString()); }
                    if (dr["ExpensesCurrencyId"] == DBNull.Value)
                    { expenses.ExpensesCurrencyId = 0; }
                    else
                    { expenses.ExpensesCurrencyId = int.Parse(dr["ExpensesCurrencyId"].ToString()); }
                    if (dr["ExpensesInvoiceId"] == DBNull.Value)
                    { expenses.ExpensesInvoiceId = 0; }
                    else
                    { expenses.ExpensesInvoiceId = int.Parse(dr["ExpensesInvoiceId"].ToString()); }
                    if (dr["ExpensesType"] == DBNull.Value)
                    { expenses.ExpensesType = 0; }
                    else
                    { expenses.ExpensesType = int.Parse(dr["ExpensesType"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { expenses.CurrencyRate = 0; }
                    else
                    { expenses.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(expenses);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
