﻿
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class ExpensesType
    {
        public  List<clsExpensesType> ExpensesTypeArrayList()
        {
            List<clsExpensesType> DisplayArray;
            DisplayArray = new List<clsExpensesType>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM ExpensesType ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsExpensesType expensesType;

                while (dr.Read())
                {
                    expensesType = new clsExpensesType();
                    if (dr["ExpensesTypeId"] == DBNull.Value)
                    { expensesType.ExpensesTypeId = 0; }
                    else
                    { expensesType.ExpensesTypeId = int.Parse(dr["ExpensesTypeId"].ToString()); }

                    if (dr["ExpensesTypeEngName"] == DBNull.Value)
                    { expensesType.ExpensesTypeEngName = " "; }
                    else
                    { expensesType.ExpensesTypeEngName = dr["ExpensesTypeEngName"].ToString(); }

                    if (dr["ExpensesTypeArabName"] == DBNull.Value)
                    { expensesType.ExpensesTypeArabName = " "; }
                    else
                    { expensesType.ExpensesTypeArabName = dr["ExpensesTypeArabName"].ToString(); }

                    DisplayArray.Add(expensesType);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public clsExpensesType getExpensesTypeById(int id)
        {
            clsExpensesType expensesType = new clsExpensesType();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM ExpensesType where ExpensesTypeId=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["ExpensesTypeId"] == DBNull.Value)
                    { expensesType.ExpensesTypeId = 0; }
                    else
                    { expensesType.ExpensesTypeId = int.Parse(dr["ExpensesTypeId"].ToString()); }

                    if (dr["ExpensesTypeEngName"] == DBNull.Value )
                    { expensesType.ExpensesTypeEngName = " "; }
                    else
                    { expensesType.ExpensesTypeEngName = dr["ExpensesTypeEngName"].ToString(); }

                    if (dr["ExpensesTypeArabName"] == DBNull.Value)
                    { expensesType.ExpensesTypeArabName = " "; }
                    else
                    { expensesType.ExpensesTypeArabName = dr["ExpensesTypeArabName"].ToString(); }




                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return expensesType;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return expensesType;
            }
        }

        public  bool ExpensesTypeIDExists(int ExpensesTypeId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ExpensesTypeId FROM ExpensesType WHERE(ExpensesTypeId = " + ExpensesTypeId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public int   ReadLastNo()
        {
            int id = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT Max(ExpensesTypeId) as ExpensesTypeId  FROM ExpensesType ", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    id = int.Parse(dr["ExpensesTypeId"].ToString());
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return id;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return id;
            }
        }


        public void UpdateExpensesType(clsExpensesType expensesType)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE ExpensesType SET " +
                "ExpensesTypeEngName=@ExpensesTypeEngName, " +
                 "ExpensesTypeArabName=@ExpensesTypeArabName " +
                "WHERE (ExpensesTypeId=@ExpensesTypeId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);


                command.Parameters.Add("@ExpensesTypeEngName", SqlDbType.NVarChar).Value = expensesType.ExpensesTypeEngName;
                command.Parameters.Add("@ExpensesTypeArabName", SqlDbType.NVarChar).Value = expensesType.ExpensesTypeArabName;
                command.Parameters.Add("@ExpensesTypeId", SqlDbType.Int).Value = expensesType.ExpensesTypeId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public void AddExpensesType(clsExpensesType expensesType)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                
                    string sql = "INSERT INTO ExpensesType (" +
                    "ExpensesTypeEngName, " +
                    "ExpensesTypeArabName " +
                    ") " +
                    "VALUES (" +
                    "@ExpensesTypeEngName, " +
                    "@ExpensesTypeArabName " +
                    " );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@ExpensesTypeEngName", SqlDbType.NVarChar).Value = expensesType.ExpensesTypeEngName;
                command.Parameters.Add("@ExpensesTypeArabName", SqlDbType.NVarChar).Value = expensesType.ExpensesTypeArabName;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteExpensesType(int eid)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM ExpensesType WHERE (ExpensesTypeId = " + eid + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

        public bool TypeUsed(int TypeId)
        {
            if(!ExistsInExpenses(TypeId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInExpenses(int ExpensesTypeId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ExpensesType FROM Expenses WHERE(ExpensesType = " + ExpensesTypeId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

    }
}
