﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;
namespace SerhanTravel.Connections
{
    class Groups
    {

        
        public  ArrayList GroupArrayList()
        {
            ArrayList DisplayArray;
            DisplayArray = new ArrayList();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Groups ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsGroup group;

                while (dr.Read())
                {
                    group = new clsGroup();
                    if (dr["GroupId"] == DBNull.Value)
                    { group.GroupId = 0; }
                    else
                    { group.GroupId = int.Parse(dr["GroupId"].ToString()); }

                    if (dr["GroupName"] == DBNull.Value)
                    { group.GroupName = " "; }
                    else
                    { group.GroupName = dr["GroupName"].ToString(); }

                    DisplayArray.Add(group);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public void UpdateGroup(clsGroup group)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Groups SET " +
                "GroupName=@GroupName " +
                "WHERE (GroupId=@GroupId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);


                command.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = group.GroupName;
                command.Parameters.Add("@GroupId", SqlDbType.Int).Value = group.GroupId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public void AddGroup(clsGroup group)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Groups (" +
                    "GroupName " +
                    ") " +
                    "VALUES (" +
                    "@GroupName " +
                    " );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = group.GroupName;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool GroupUsed(int GroupId)
        {
            if(!ExistsInSupplier(GroupId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInSupplier(int GroupId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT GroupId FROM Supplier WHERE(GroupId = " + GroupId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool GroupIDExists(int GroupId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT GroupId FROM Groups WHERE(GroupId = " + GroupId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool DeleteGroup(int groupId)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Groups WHERE (GroupId = " + groupId + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

    }
}
