﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Item
    {
        internal  bool ItemIDExists(int itemId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ItemId FROM Item WHERE(ItemId = " + itemId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        internal  List<clsItem> getItemList()
        {
            List<clsItem> Items = new List<clsItem>();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM Item", con);
                SqlDataReader reader = command.ExecuteReader();

                clsItem item;
                while (reader.Read())
                {
                    item = new clsItem();
                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(reader["CategoryId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }
                    Items.Add(item);
                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Items;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Items;
            }

        }
        internal clsItem ReadItem(int itemID)
        {
            clsItem item = new clsItem();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM Item Where (ItemId = " + itemID + ");", con);
                SqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {
                    reader.Read();

                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(reader["CategoryId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }

                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return item;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return item;
            }
        }
            internal void UpdateItemInfo(clsItem item)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "update Item set " +

                                    "Name = @Name, " +
                                    "SupplierId = @SupplierId, " +
                                    "Description = @Description, " +
                                    "CurrencyId=@CurrencyId, " +
                                    "CategoryId = @CategoryId, " +
                                    "CurrencyRate = @CurrencyRate, " +
                                    "Cost = @Cost " +
                                    "WHERE (ItemId  = @ItemId);";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                    myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = item.itemName;
                    myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value =item.SupplierId;
                    myCommand.Parameters.Add("@Description", SqlDbType.Text).Value = item.itemDescription;
                myCommand.Parameters.Add("@Cost", SqlDbType.Money).Value = item.Cost;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = item.itemID;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@CategoryId", SqlDbType.Int).Value = item.CategoryId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;
                cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        internal clsItem MoveToRecord(int ItemID, string Direction)
        {
            clsItem item = new clsItem();
            try
            {
                int myCount;
                int myIndex;
                DataRow myRow;

                object[] myKey = new object[1];
                myKey[0] = ItemID;

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                DataSet dsItems = new DataSet();
                string sqlQuery = "SELECT * FROM Item";
                DataTable dtItems = new DataTable();
                SqlDataAdapter daItems = new SqlDataAdapter(sqlQuery, sqlCon);
                daItems.FillSchema(dsItems, SchemaType.Source, "Item");
                daItems.Fill(dsItems, "Item");
                dtItems = dsItems.Tables["Item"];

                myCount = dtItems.Rows.Count;
                if (myCount > 0)
                {
                    myCount -= 1;
                }
                myRow = dtItems.Rows.Find(myKey);
                myIndex = dtItems.Rows.IndexOf(myRow);
                if (Direction == "Forward")
                {
                    myIndex += 1;
                    if (myIndex > myCount)
                    {
                        myIndex = 0;
                    }
                }

                if (Direction == "Backward")
                {
                    myIndex -= 1;
                    if (myIndex < 0)
                    {
                        myIndex = myCount;
                    }
                }
                if (dtItems.Rows[myIndex]["ItemId"] == DBNull.Value)
                { item.itemID = 0; }
                else
                { item.itemID = int.Parse(dtItems.Rows[myIndex]["ItemId"].ToString()); }
                if (dtItems.Rows[myIndex]["Description"] == DBNull.Value)
                { item.itemDescription = ""; }
                else
                { item.itemDescription = dtItems.Rows[myIndex]["Description"].ToString(); }
                if (dtItems.Rows[myIndex]["Name"] == DBNull.Value)
                { item.itemName = ""; }
                else
                { item.itemName = dtItems.Rows[myIndex]["Name"].ToString(); }
                if (dtItems.Rows[myIndex]["SupplierId"] == DBNull.Value)
                { item.SupplierId = 0; }
                else
                { item.SupplierId = int.Parse(dtItems.Rows[myIndex]["SupplierId"].ToString()); }
                if (dtItems.Rows[myIndex]["Cost"] == DBNull.Value)
                { item.Cost = 0; }
                else
                { item.Cost = decimal.Parse(dtItems.Rows[myIndex]["Cost"].ToString()); }
                sqlCon.Close();
                sqlCon.Dispose();
                daItems.Dispose();
                dtItems.Dispose();
                return item;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return item;
            }
        }

        internal void AddNewItem(clsItem item)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }



                    string sqlQuery = "insert into Item (" +
                                        "Name, " +
                                        "SupplierId , " +
                                        "CategoryId , " +
                                        "Description , " +
                                        "CurrencyId, "+
                                        "CurrencyRate, " +
                                        "Cost" +
                                        ") " +
                                        "VALUES (" +
                                        "@Name, " +
                                        "@SupplierId, " +
                                        "@CategoryId, " +
                                        "@Description, " +
                                        "@CurrencyId, "+
                                        "@CurrencyRate, " +
                                        "@Cost " +
                                        ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = item.itemName;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = item.SupplierId;
                myCommand.Parameters.Add("@Description", SqlDbType.Text).Value = item.itemDescription;
                myCommand.Parameters.Add("@Cost", SqlDbType.Money).Value = item.Cost;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@CategoryId", SqlDbType.Int).Value = item.CategoryId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;
                cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }



            public bool DeleteItem(int itemID)
            {
                int cmdResult;
                bool myResult;
                try
                {
                    myResult = true;
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "DELETE FROM Item WHERE (ItemId = " + itemID + ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                    cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    myResult = true;
                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return myResult;
                }
                catch (Exception ex)
                {
                    myResult = false;
                    MessageBox.Show(ex.Message);
                    return myResult;
                }
            }
            public int ReadLastNo()
            {
                int maxId;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "SELECT MAX(ItemId) FROM Item;";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());

                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return maxId;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return 0;
                }
            }


        internal List<clsItem> SupplierItemsList(int id)
        {
            List<clsItem> Items = new List<clsItem>();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM Item where CategoryId=1 and SupplierId=" + id, con);
                SqlDataReader reader = command.ExecuteReader();

                clsItem item;
                while (reader.Read())
                {
                    item = new clsItem();
                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(reader["CategoryId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }
                    Items.Add(item);
                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Items;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Items;
            }

        }

        public bool ItemUsed(int ItemId)
        {
            if (!ExistsInReturnedSupplierItem(ItemId) && !ExistsInReturnedCustomerPackage(ItemId) && !ExistsInInvoicePackageDetail(ItemId) && !ExistsInPackageItemsDetails(ItemId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInReturnedSupplierItem(int ItemId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ItemId FROM ReturnedSupplierItem WHERE(ItemId = " + ItemId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedCustomerPackage(int ItemId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ItemId FROM ReturnedCustomerPackage WHERE(ItemId = " + ItemId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInInvoicePackageDetail(int ItemId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ItemId FROM InvoicePackageDetail WHERE(ItemId = " + ItemId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackageItemsDetails(int ItemId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT ItemId FROM PackageItemsDetails WHERE(ItemId = " + ItemId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
