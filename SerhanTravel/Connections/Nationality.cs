﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using SerhanTravel.Objects;
namespace SerhanTravel.Connections
{
    class Nationality
    {
        public  ArrayList NationalitiesArrayList()
        {
            ArrayList DisplayArray;
            DisplayArray = new ArrayList();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Nationality;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsNationality nationality;

                while (dr.Read())
                {
                    nationality = new clsNationality();
                    if (dr["NationalityID"] == DBNull.Value)
                    { nationality.NationalityId = 0; }
                    else
                    { nationality.NationalityId = int.Parse(dr["NationalityID"].ToString()); }

                    if (dr["ArabicName"] == DBNull.Value)
                    { nationality.ArabicName = " "; }
                    else
                    { nationality.ArabicName = dr["ArabicName"].ToString(); }

                    if (dr["EnglishName"] == DBNull.Value)
                    { nationality.EnglishName = " "; }
                    else
                    { nationality.EnglishName = dr["EnglishName"].ToString(); }


                    DisplayArray.Add(nationality);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public  bool NationalityIDExists(int nationalityId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT NationalityID FROM Nationality WHERE(NationalityID = " + nationalityId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void UpdateNationality(clsNationality nationality)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Nationality SET " +
                "ArabicName=@ArabicName, " +
                "EnglishName=@EnglishName " +
                "WHERE (NationalityID=@NationalityID);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@ArabicName", SqlDbType.NVarChar).Value = nationality.ArabicName;
                command.Parameters.Add("@EnglishName", SqlDbType.NVarChar).Value = nationality.EnglishName;
                command.Parameters.Add("@NationalityID", SqlDbType.Int).Value = nationality.NationalityId;


                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddNationality(clsNationality nationality)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Nationality (" +
                    "ArabicName, " +
                    "EnglishName ) " +
                    "VALUES (" +
                    "@ArabicName, " +
                    "@EnglishName );";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@ArabicName", SqlDbType.NVarChar).Value = nationality.ArabicName;
                command.Parameters.Add("@EnglishName", SqlDbType.NVarChar).Value = nationality.EnglishName;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool NationalityUsed(int NationalityId)
        {
            if(!ExistsCustomer(NationalityId) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
     
        public bool ExistsCustomer(int NationalityId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT NationalityId FROM Customer WHERE(NationalityId = " + NationalityId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool DeleteNationality(int nationalityId)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Nationality WHERE (NationalityID = " + nationalityId + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }
    }
}
