﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Package
    {
     

        public List<clsPackage> PackageArrayList()
        {
            List<clsPackage> DisplayArray = new List<clsPackage>();
            PackageItemDetails connection = new PackageItemDetails();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Package ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsPackage package;


                while (dr.Read())
                {
                    package = new clsPackage();
                    if (dr["PackageID"] == DBNull.Value)
                    { package.packageID = 0; }
                    else
                    { package.packageID = int.Parse(dr["PackageID"].ToString()); }

                    if (dr["PackageName"] == DBNull.Value)
                    { package.packageName = ""; }
                    else
                    { package.packageName = dr["PackageName"].ToString(); }

                    if (dr["CountryID"] == DBNull.Value)
                    { package.countryID = 0; }
                    else
                    { package.countryID = int.Parse(dr["CountryID"].ToString()); }

                    if (dr["DaysNumber"] == DBNull.Value)
                    { package.packageDays= 0; }
                    else
                    { package.packageDays = int.Parse(dr["DaysNumber"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { package.CurrencyId = 0; }
                    else
                    { package.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["Price"] == DBNull.Value)
                    { package.Price = 0; }
                    else
                    { package.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { package.CurrencyRate = 0; }
                    else
                    { package.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["Profit"] == DBNull.Value)
                    { package.Profit = 0; }
                    else
                    { package.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    package.packageItems = connection.PackageItemDetailArrayList(package.packageID);
                    DisplayArray.Add(package);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public clsPackage getPackageInfo(int PackageId)
        {
            clsPackage package = new clsPackage();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Package where PackageID= "+ PackageId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                PackageItemDetails connection = new PackageItemDetails();


                if (dr.HasRows)
                {
                    dr.Read();
                    
                    if (dr["PackageID"] == DBNull.Value)
                    { package.packageID = 0; }
                    else
                    { package.packageID = int.Parse(dr["PackageID"].ToString()); }

                    if (dr["PackageName"] == DBNull.Value)
                    { package.packageName = ""; }
                    else
                    { package.packageName = dr["PackageName"].ToString(); }

                    if (dr["CountryID"] == DBNull.Value)
                    { package.countryID = 0; }
                    else
                    { package.countryID = int.Parse(dr["CountryID"].ToString()); }

                    if (dr["DaysNumber"] == DBNull.Value)
                    { package.packageDays = 0; }
                    else
                    { package.packageDays = int.Parse(dr["DaysNumber"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { package.CurrencyId = 0; }
                    else
                    { package.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["Price"] == DBNull.Value)
                    { package.Price = 0; }
                    else
                    { package.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { package.CurrencyRate = 0; }
                    else
                    { package.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["Profit"] == DBNull.Value)
                    { package.Profit = 0; }
                    else
                    { package.Profit = decimal.Parse(dr["Profit"].ToString()); }

                    package.packageItems = connection.PackageItemDetailArrayList(package.packageID);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return package;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return package;
            }
        }

        internal bool PackageIDExists(int packageID)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PackageID FROM Package WHERE(PackageID = " + packageID + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        internal void UpdatePackageInfo(clsPackage Package)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update Package set " +
                                "Price = @Price, " +
                                "CurrencyRate = @CurrencyRate, " +
                                "CurrencyId = @CurrencyId, " +
                                "PackageName = @PackageName, " +
                                "Profit = @Profit, " +
                                "CountryID = @CountryID, " +
                                "DaysNumber = @DaysNumber " +
                                "WHERE (PackageID  = @PackageID);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@PackageName", SqlDbType.NVarChar).Value = Package.packageName;
                myCommand.Parameters.Add("@CountryID", SqlDbType.Int).Value = Package.countryID;
                myCommand.Parameters.Add("@DaysNumber", SqlDbType.Int).Value = Package.packageDays;
                myCommand.Parameters.Add("@PackageID", SqlDbType.Int).Value = Package.packageID;
                myCommand.Parameters.Add("@Price", SqlDbType.Money).Value = Package.Price;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Package.CurrencyRate;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Package.CurrencyId;
                myCommand.Parameters.Add("@Profit", SqlDbType.Money).Value = Package.Profit;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal void AddNewPackage(clsPackage Package)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }


            
                string sqlQuery = "insert into Package (" +
                                    "Price, " +
                                    "CurrencyRate, " +
                                    "CurrencyId, " +
                                    "PackageName, " +
                                    "CountryID , " +
                                     "Profit , " +
                                    "DaysNumber " +
                                    ") " +
                                    "VALUES (" +
                                    "@Price, " +
                                    "@CurrencyRate, " +
                                    "@CurrencyId, " +
                                    "@PackageName, " +
                                    "@CountryID, " +
                                    "@Profit, " +
                                    "@DaysNumber " +
                   
                                    ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@PackageName", SqlDbType.NVarChar).Value = Package.packageName;
                myCommand.Parameters.Add("@CountryID", SqlDbType.Int).Value = Package.countryID;
                myCommand.Parameters.Add("@DaysNumber", SqlDbType.Int).Value = Package.packageDays;
                myCommand.Parameters.Add("@PackageID", SqlDbType.Int).Value = Package.packageID;
                myCommand.Parameters.Add("@Price", SqlDbType.Money).Value = Package.Price;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Package.CurrencyRate;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Package.CurrencyId;
                myCommand.Parameters.Add("@Profit", SqlDbType.Money).Value = Package.Profit;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public bool DeletePackage(int packageId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM Package WHERE (PackageID = " + packageId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(PackageID) FROM Package;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                maxId = Convert.ToInt32(myCommand.ExecuteScalar());

                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }
        public bool PackageUsed(int PackageId)
        {
            if (!ExistsInInvoicePackages(PackageId) && !ExistsInReturnedCustomerPackage(PackageId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInReturnedCustomerPackage(int PackageId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PackageId FROM ReturnedCustomerPackage WHERE(PackageId = " + PackageId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInInvoicePackages(int PackageId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PackageId FROM InvoicePackages WHERE(PackageId = " + PackageId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }

}

