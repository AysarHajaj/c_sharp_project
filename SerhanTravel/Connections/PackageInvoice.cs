﻿using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class PackageInvoice
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsPackageInvoice> CustomerInvoicesArrayList(int CustomerId)
        {
            List<clsPackageInvoice> DisplayArray = new List<clsPackageInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM PackageInvoice where ( CustomerId = " + CustomerId + " );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsPackageInvoice invoice;


                while (dr.Read())
                {
                    invoice = new clsPackageInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.InvoiceDate = DefaultDate; }
                    else
                    { invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { invoice.CustomerId = 0; }
                    else
                    { invoice.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { invoice.CustomerName = " "; }
                    else
                    { invoice.CustomerName = dr["Name"].ToString(); }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["Discount"] == DBNull.Value)
                    {
                        invoice.Discount = 0;
                    }
                    else
                    {
                        invoice.Discount = decimal.Parse(dr["Discount"].ToString());
                    }

                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public List<clsPackage> PackagesInvoicesArrayList(int InvoiceId)
        {
            List<clsPackage> DisplayArray = new List<clsPackage>();
            List<clsInvoicePackagesDetails> packageIDS = new List<clsInvoicePackagesDetails>();
            clsInvoicePackagesDetails invoicePackageDetail;
            Package packageConnection = new Package();
            Item itemConnection = new Item();
          
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT P.[PackageID],P.[PackageName],P.[CountryID],P.[DaysNumber],IP.[Price] "+
                            ", IP.[CurrencyId],IP.[CurrencyRate] ,P.[Profit] ,IP.InvoicePackagesId,IP.CustomerName " +
                            "FROM InvoicePackages as IP Inner join Package as P on P.PackageID=Ip.PackageId where (InvoiceId="+ InvoiceId + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    invoicePackageDetail = new clsInvoicePackagesDetails();
                    clsPackage package = new clsPackage();
                    if (dr["InvoicePackagesId"] == DBNull.Value)
                    { invoicePackageDetail.InvoicePackagesId = 0; }
                    else
                    { invoicePackageDetail.InvoicePackagesId = int.Parse(dr["InvoicePackagesId"].ToString()); }
                    if (dr["PackageID"] == DBNull.Value)
                    { package.packageID = 0; }
                    else
                    { package.packageID = int.Parse(dr["PackageID"].ToString()); }

                    if (dr["PackageName"] == DBNull.Value)
                    { package.packageName = ""; }
                    else
                    { package.packageName = dr["PackageName"].ToString(); }

                    if (dr["CountryID"] == DBNull.Value)
                    { package.countryID = 0; }
                    else
                    { package.countryID = int.Parse(dr["CountryID"].ToString()); }
                  
                    if (dr["DaysNumber"] == DBNull.Value)
                    { package.packageDays = 0; }
                    else
                    { package.packageDays = int.Parse(dr["DaysNumber"].ToString()); }
                   
                    if (dr["CurrencyId"] == DBNull.Value)
                    { package.CurrencyId = 0; }
                    else
                    { package.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["Price"] == DBNull.Value)
                    { package.Price = 0; }
                    else
                    { package.Price = decimal.Parse(dr["Price"].ToString()); }
                  
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { package.CurrencyRate = 0; }
                    else
                    { package.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["Profit"] == DBNull.Value)
                    { package.Profit = 0; }
                    else
                    { package.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CustomerName"] == DBNull.Value)
                    { package.CustomerName = ""; }
                    else
                    { package.CustomerName = dr["CustomerName"].ToString(); }


                    invoicePackageDetail.Package=package;
                    invoicePackageDetail.InvoiceId = InvoiceId;
                    packageIDS.Add(invoicePackageDetail);

                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                for (int i = 0; i < packageIDS.Count; i++)
                {
                 

                    SqlConnection sqlConnection;
                    BuildConnection anewBuildConnection = new BuildConnection();
                    sqlConnection = anewBuildConnection.AddSqlConnection();
                    List<int> itemIDs = new List<int>();

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                    string sqlquery = "Select * "+
                                               " from Item "+
                                                           " where ItemId in ( "+
                                               " SELECT ItemId FROM InvoicePackageDetail where(InvPackDetails= "+ packageIDS [i].InvoicePackagesId+ " )); ";
                    SqlCommand command = new SqlCommand(sqlquery, sqlConnection);
                    SqlDataReader dataReader = command.ExecuteReader();
                 
                    List<clsItem> items = new List<clsItem>();
                  
                    clsItem item;
                    while (dataReader.Read())
                    {
                        item = new clsItem();
                        if (dataReader["ItemId"] == DBNull.Value)
                        { item.itemID = 0; }
                        else
                        { item.itemID = int.Parse(dataReader["ItemId"].ToString()); }
                        if (dataReader["Description"] == DBNull.Value)
                        { item.itemDescription = ""; }
                        else
                        { item.itemDescription = dataReader["Description"].ToString(); }
                        if (dataReader["Name"] == DBNull.Value)
                        { item.itemName = ""; }
                        else
                        { item.itemName = dataReader["Name"].ToString(); }
                        if (dataReader["SupplierId"] == DBNull.Value)
                        { item.SupplierId = 0; }
                        else
                        { item.SupplierId = int.Parse(dataReader["SupplierId"].ToString()); }
                        if (dataReader["Cost"] == DBNull.Value)
                        { item.Cost = 0; }
                        else
                        { item.Cost = decimal.Parse(dataReader["Cost"].ToString()); }
                        if (dataReader["CurrencyId"] == DBNull.Value)
                        { item.CurrencyId = 0; }
                        else
                        { item.CurrencyId = int.Parse(dataReader["CurrencyId"].ToString()); }
                        if (dataReader["CurrencyRate"] == DBNull.Value)
                        { item.CurrencyRate = 0; }
                        else
                        { item.CurrencyRate = decimal.Parse(dataReader["CurrencyRate"].ToString()); }

                        items.Add(item);
                    }
                    dataReader.Close();
                    command.Dispose();



                    packageIDS[i].Package.packageItems = items;
                    packageIDS[i].Package.Profit = packageIDS[i].Package.Price - packageIDS[i].Package.Cost;
                    DisplayArray.Add(packageIDS[i].Package);
                    dataReader.Close();
                    command.Dispose();
                }

                sqlCon.Close();
                sqlCon.Dispose();
                
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal bool InvoiceIDExists(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM PackageInvoice WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        internal void AddNewInvoice(clsPackageInvoice Invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into PackageInvoice(" +
                        "Date, " +
                        "NetPrice, " +
                        "Discount, " +
                        "CustomerId, " +
                        "Name " +
                        ") " +
                      "VALUES (" +
                        "@Date, " +
                        "@NetPrice, " +
                        "@Discount, " +
                        "@CustomerId, " +
                       "@Name " +
                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Invoice.InvoiceDate;
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Invoice.CustomerName;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Money).Value = Invoice.NetPrice;
                myCommand.Parameters.Add("@Discount", SqlDbType.Decimal).Value = Invoice.Discount;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = Invoice.CustomerId;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(InvoiceId) FROM PackageInvoice;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                maxId = Convert.ToInt32(myCommand.ExecuteScalar());

                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }


        internal void AddInvoicePackages(int invoiceId, clsPackage package,int i)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into InvoicePackages (" +
                                    "InvoiceId, " +
                                    "PackageId,  " +
                                    "CurrencyId, " +
                                    "CurrencyRate, " +
                                    "CustomerName, " +
                                    "Price, " +
                                    "i  " +
                                    ") " +
                                    "VALUES (" +
                                    "@InvoiceId, " +
                                    "@PackageId, " +
                                    "@CurrencyId, " +
                                    "@CurrencyRate, " +
                                    "@CustomerName, " +
                                    "@Price, " +
                                    "@i " +
                                    ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoiceId;
                myCommand.Parameters.Add("@PackageId", SqlDbType.Int).Value = package.packageID;
                myCommand.Parameters.Add("@i", SqlDbType.Int).Value = i;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = package.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = package.CurrencyRate;
                myCommand.Parameters.Add("@Price", SqlDbType.Money).Value = package.Price;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = package.CustomerName;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void AddInvoicePackage(int InvoiceID, List<clsPackage> packages)
        {
            for (int i = 0; i <= packages.Count - 1; i++)
            {
                AddInvoicePackages(InvoiceID, packages[i],i);
            }

        }




        public int getInvoicePackageId(int invoiceId,int packageId,int i)
        {
            int invoicePackageId = 0;

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT InvoicePackagesId FROM InvoicePackages where InvoiceId= " + invoiceId + " and PackageId= "+ packageId+" and i= "+i;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["InvoicePackagesId"] == DBNull.Value)
                    { invoicePackageId = 0; }
                    else
                    { invoicePackageId = int.Parse(dr["InvoicePackagesId"].ToString()); }

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return invoicePackageId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return invoicePackageId;
            }
        }


        internal void AddInvoicePackagesDetails(int InvoicePackageId, int ItemID)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into InvoicePackageDetail (" +
                        "InvPackDetails, " +
                        "ItemId  " +
                        
                        ") " +
                      "VALUES (" +
                        "@InvPackDetails, " +
                        "@ItemId " +
                        
                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvPackDetails", SqlDbType.Int).Value = InvoicePackageId;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = ItemID;
                

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void AddInvoicePackageItems(int InvoicePackageId, List<clsItem> items)
        {
            for (int i = 0; i <= items.Count - 1; i++)
            {
                AddInvoicePackagesDetails(InvoicePackageId, items[i].itemID);
            }

        }

        public void AddFinal(int invoiceId,List<clsPackage> packages)
        {
            for(int i = 0; i < packages.Count; i++)
            {
                int invoicePackage = getInvoicePackageId(invoiceId, packages[i].packageID, i);
                AddInvoicePackageItems(invoicePackage, packages[i].packageItems);
            }
        }


        public bool DeleteInvoicePackageDetails(int invoiceId)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from InvoicePackageDetail where InvPackDetails IN (select InvoicePackagesId from InvoicePackages where InvoiceId = " + invoiceId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        public bool DeleteInvoicePackages(int invoiceId)
        {
            int cmdResult;
            bool myResult;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from InvoicePackages where InvoiceId IN (select InvoiceId from InvoicePackages where InvoiceId = " +
                    invoiceId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        public bool DeleteInvoice(int invoiceId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM PackageInvoice WHERE (InvoiceId = " + invoiceId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        internal void UpdateInvoiceDetails(int InvoiceID, List<clsPackage> packages)
        {
            try
            {
                if(DeleteInvoicePackageDetails(InvoiceID))
                {
                if (DeleteInvoicePackages(InvoiceID))
                    {
                        AddInvoicePackage(InvoiceID, packages);
                        AddFinal(InvoiceID, packages);
                    }
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        internal void UpdateInvoiceInfo(clsPackageInvoice packageInvoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update PackageInvoice set " +

                        "Date = @Date, " +
                        "NetPrice = @NetPrice, " +
                        "Discount = @Discount, " +
                        "CustomerId = @CustomerId, " +
                        "Name = @Name " +
                        "WHERE (InvoiceId  = @InvoiceId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = packageInvoice.InvoiceDate;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Money).Value = packageInvoice.NetPrice;
                myCommand.Parameters.Add("@Discount", SqlDbType.Decimal).Value = packageInvoice.Discount;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = packageInvoice.CustomerId;
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = packageInvoice.CustomerName;
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = packageInvoice.InvoiceId;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<clsPackageInvoice> CustomerInvoicesAfterInventory(DateTime fromdate,DateTime toDate,int CustomerId)
        {
            List<clsPackageInvoice> DisplayArray = new List<clsPackageInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM PackageInvoice as PIN where ( PIN.CustomerId = @CustomerId and PIN.Date between @fromdate and @toDate );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromdate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsPackageInvoice invoice;


                while (dr.Read())
                {
                    invoice = new clsPackageInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.InvoiceDate = DefaultDate; }
                    else
                    { invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { invoice.CustomerId = 0; }
                    else
                    { invoice.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { invoice.CustomerName = " "; }
                    else
                    { invoice.CustomerName = dr["Name"].ToString(); }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["Discount"] == DBNull.Value)
                    {
                        invoice.Discount = 0;
                    }
                    else
                    {
                        invoice.Discount = decimal.Parse(dr["Discount"].ToString());
                    }
                    invoice.NetPrice = (invoice.NetPrice - (invoice.NetPrice * invoice.Discount * Convert.ToDecimal(0.01)));

                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public decimal TotalPrice(int CustomerId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum((PIN.NetPrice-(PIN.NetPrice*PIN.Discount*0.01)))  as totalPrice " +
                    "from PackageInvoice as PIN where PIN.CustomerId= " + CustomerId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPrice"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPrice"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
        public List<clsPackageInvoice> GetInvoicesList(DateTime fromdate, DateTime toDate)
        {
            List<clsPackageInvoice> DisplayArray = new List<clsPackageInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM PackageInvoice as PIN where (PIN.Date between @fromdate and @toDate );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromdate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsPackageInvoice invoice;


                while (dr.Read())
                {
                    invoice = new clsPackageInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.InvoiceDate = DefaultDate; }
                    else
                    { invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { invoice.CustomerId = 0; }
                    else
                    { invoice.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { invoice.CustomerName = " "; }
                    else
                    { invoice.CustomerName = dr["Name"].ToString(); }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["Discount"] == DBNull.Value)
                    {
                        invoice.Discount = 0;
                    }
                    else
                    {
                        invoice.Discount = decimal.Parse(dr["Discount"].ToString());
                    }
                    invoice.NetPrice = (invoice.NetPrice - (invoice.NetPrice * invoice.Discount * Convert.ToDecimal(0.01)));

                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public bool InvoiceUsed(int InvoiceId)
        {
            if (!ExistsInPackagePayments(InvoiceId) && !ExistsInReturnedCustomerPackage(InvoiceId) && !ExistsInReturnedSupplierItem(InvoiceId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInReturnedCustomerPackage(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM ReturnedCustomerPackage WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedSupplierItem(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM ReturnedSupplierItem WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInPackagePayments(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PackageInvoiceId FROM PackagePayments WHERE(PackageInvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
