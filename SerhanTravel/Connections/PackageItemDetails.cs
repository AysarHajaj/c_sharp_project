﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class PackageItemDetails
    {
        public List<clsItem> PackageItemDetailArrayList(int PackageId)
        {
            List<clsItem> DisplayArray = new List<clsItem>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select * from Item "+
                         " where Item.ItemId in (SELECT ItemId FROM PackageItemsDetails where(PackageId = "+ PackageId + " ) );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader reader = cmd.ExecuteReader();

                clsItem item;
                while (reader.Read())
                {
                    item = new clsItem();
                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }

                    DisplayArray.Add(item);
                }

                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                reader.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

  
        internal void UpdatePackageInfo(int PackageID, List<clsItem> items)
        {
            try
            {
              
                if (DeletePackageItems(PackageID))
                {
                
                   AddPackageItem(PackageID, items);
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
            
        }
      
        public void AddPackageItem(int PackageID,List<clsItem> items)
        {
            for(int i=0;i<=items.Count-1;i++)
            {
                AddPackageItemDetails(PackageID, items[i].itemID);
            }

        }
        internal void AddPackageItemDetails(int packageId,int ItemID)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into PackageItemsDetails (" +
                        "PackageId, " +
                        "ItemId  " +
                      
                        ") " +
                      "VALUES (" +
                        "@PackageId, " +
                        "@ItemId " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = ItemID;
                myCommand.Parameters.Add("@PackageId", SqlDbType.Int).Value =packageId;
        

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public bool DeletePackageItems(int packageId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from PackageItemsDetails where PackageId IN (select PackageId from PackageItemsDetails where PackageId = " +
                    packageId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }


    }

}

