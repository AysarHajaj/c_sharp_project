﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class PackagesSold
    {
        public List<clsPackage> getPackagesSoldList(DateTime fromDate,DateTime toDate,int CustomerId)
        {
            List<clsPackage> DisplayArray = new List<clsPackage>();
            List<int> invoiceIDS = getCustomerInvoiceIDS(fromDate,toDate,CustomerId);

            foreach(int i in invoiceIDS)
            {
                List<clsPackage> ps = getInvoicePackageList(i);
                foreach(clsPackage p in ps)
                {
                    DisplayArray.Add(p);
                }
            }

            return DisplayArray;

        }

        public List<int> getCustomerInvoiceIDS(DateTime fromDate,DateTime toDate,int CustomerId)
        {
            List<int> invoiceIDS = new List<int>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT InvoiceId FROM PackageInvoice where(CustomerId=@CustomerId and Date between @fromDate and @toDate);";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@CustomerId ", SqlDbType.Int).Value = CustomerId;
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                int invoiceId = 0;
                while (dr.Read())
                {
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoiceId = 0; }
                    else
                    { invoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    invoiceIDS.Add(invoiceId);
                }

                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return invoiceIDS;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return invoiceIDS;
            }
        }

        public List<clsItem> getItems(int InvPackDetails)
        {
            List<clsItem> itemlist = new List<clsItem>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * "+
                    "from InvoicePackageDetail as IPD "+
                    "Inner Join Item As IT on IT.ItemId = IPD.ItemId " +
                    " where(IPD.InvPackDetails="+ InvPackDetails + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsItem item;
                while (dr.Read())
                {
                    item = new clsItem();
                    if (dr["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = dr["Name"].ToString(); }
                    if (dr["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = dr["Description"].ToString(); }
                   
                    if (dr["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                    if(dr["CurrencyId"]==DBNull.Value)
                    {
                        item.CurrencyId = 0;
                    }
                    else
                    {
                        item.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    itemlist.Add(item);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return itemlist;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return itemlist;
            }
        }


        public List<clsPackage> getInvoicePackageList(int InvoiceId)
        {
            List<clsPackage> Packages = new List<clsPackage>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT P.[PackageID],P.[PackageName],P.[CountryID],P.[DaysNumber],IP.[Price] " +
                            ", IP.[CurrencyId],IP.[CurrencyRate] ,P.[Profit] ,IP.InvoicePackagesId " +
                            "FROM InvoicePackages as IP Inner join Package as P on P.PackageID=Ip.PackageId where (InvoiceId=" + InvoiceId + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                
                int InvoicePackagesId = 0;

                while (dr.Read())
                {
                   
                    if (dr["InvoicePackagesId"] == DBNull.Value)
                    { InvoicePackagesId = 0; }
                    else
                    { InvoicePackagesId = int.Parse(dr["InvoicePackagesId"].ToString()); }
                    
                    List<clsItem> Items = getItems(InvoicePackagesId);

                    clsPackage package = new clsPackage(); 
                    if (dr["PackageID"] == DBNull.Value)
                    { package.packageID = 0; }
                    else
                    { package.packageID = int.Parse(dr["PackageID"].ToString()); }

                    if (dr["PackageName"] == DBNull.Value)
                    { package.packageName = ""; }
                    else
                    { package.packageName = dr["PackageName"].ToString(); }

                    if (dr["CountryID"] == DBNull.Value)
                    { package.countryID = 0; }
                    else
                    { package.countryID = int.Parse(dr["CountryID"].ToString()); }

                    if (dr["DaysNumber"] == DBNull.Value)
                    { package.packageDays = 0; }
                    else
                    { package.packageDays = int.Parse(dr["DaysNumber"].ToString()); }

                    if (dr["CurrencyId"] == DBNull.Value)
                    { package.CurrencyId = 0; }
                    else
                    { package.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["Price"] == DBNull.Value)
                    { package.Price = 0; }
                    else
                    { package.Price = decimal.Parse(dr["Price"].ToString()); }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    { package.CurrencyRate = 0; }
                    else
                    { package.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["Profit"] == DBNull.Value)
                    { package.Profit = 0; }
                    else
                    { package.Profit = decimal.Parse(dr["Profit"].ToString()); }

                    package.packageItems = Items;
                    Packages.Add(package);


                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return Packages;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Packages;
            }
        }

       

        
    }
}
