﻿using SerhanTravel.Objects;
using SerhanTravel.reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class ReturnedCustomerPackage
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public List<clsReturnedCustomerPackages> getReturnedPackage(int InvoiceId)
        {
            List<clsReturnedCustomerPackages> DisplayArray = new List<clsReturnedCustomerPackages>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select  * " +
               " from ReturnedCustomerPackage as RT " +
                " where (RT.InvoiceId = " + InvoiceId + " ); ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedCustomerPackages item;


                while (dr.Read())
                {
                    item = new clsReturnedCustomerPackages();

                    if (dr["InvoiceId"] == DBNull.Value)
                    { item.InvoiceId = 0; }
                    else
                    { item.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { item.Date = DefaultDate; }
                    else
                    { item.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["Id"] == DBNull.Value)
                    { item.Id = 0; }
                    else
                    { item.Id = int.Parse(dr["Id"].ToString()); }


                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        item.ReturnAmount = 0;
                    }
                    else
                    {
                        item.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        item.CurrencyId = 0;
                    }
                    else
                    {
                        item.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ItemId"] == DBNull.Value)
                    {
                        item.ItemId = 0;
                    }
                    else
                    {
                        item.ItemId = int.Parse(dr["ItemId"].ToString());
                    }
                    if (dr["PackageId"] == DBNull.Value)
                    {
                        item.PackageId = 0;
                    }
                    else
                    {
                        item.PackageId = int.Parse(dr["PackageId"].ToString());
                    }

                    if (item.ItemId == 0)
                    {
                        // The Returned Item Is package

                        Package packageConnection = new Package();
                        item.Item = packageConnection.getPackageInfo(item.PackageId);
                    }
                    else
                    {
                        // The Returned Item Is package Item not a whole Package
                        Item itemConnection = new Item();
                        item.Item = itemConnection.ReadItem(item.ItemId);
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    {
                        item.CurrencyRate = 0;
                    }
                    else
                    {
                        item.CurrencyRate = Decimal.Parse(dr["CurrencyRate"].ToString());
                    }
                    if (dr["Customer"] == DBNull.Value)
                    {
                        item.Customer = " ";
                    }
                    else
                    {
                        item.Customer = dr["Customer"].ToString();
                    }
                    DisplayArray.Add(item);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public void AddReturnedItem(clsReturnedCustomerPackages item)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into ReturnedCustomerPackage (" +
                                    "InvoiceId, " +
                                    "ReturnAmount, " +
                                    "PackageId, " +
                                    "ItemId, " +
                                    "CurrencyRate, " +
                                    "CurrencyId, " +
                                    "Customer, " +
                                    "Date  " +

                                    ") " +
                                    "VALUES (" +
                                    "@InvoiceId, " +
                                    "@ReturnAmount, " +
                                    "@PackageId, " +
                                    "@ItemId, " +
                                    "@CurrencyRate, " +
                                    "@CurrencyId, " +
                                    "@Customer, " +
                                    "@Date " +

                                    ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = item.InvoiceId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@PackageId", SqlDbType.Int).Value = item.PackageId;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = item.ItemId;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Money).Value = item.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = item.Date;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;
                myCommand.Parameters.Add("@Customer", SqlDbType.NVarChar).Value = item.Customer;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdateReturnedItem(clsReturnedCustomerPackages item)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update ReturnedCustomerPackage set " +
                        "InvoiceId = @InvoiceId, " +
                        "ReturnAmount=@ReturnAmount," +
                         "CurrencyRate=@CurrencyRate," +
                        "PackageId=@PackageId," +
                         "CurrencyId=@CurrencyId," +
                        "Date = @Date " +
                        "WHERE (Id  = @Id);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = item.InvoiceId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@PackageId", SqlDbType.Int).Value = item.PackageId;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = item.ItemId;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Decimal).Value = item.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = item.Date;
                myCommand.Parameters.Add("@Id", SqlDbType.Int).Value = item.Id;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteReturnedTickets(int Id)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from ReturnedCustomerPackage where Id =" + Id;

     SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);

                cmdResult = myCommand.ExecuteNonQuery();
               

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
           
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }


     
        public bool InvoiceExist(int InvoiceId)
        {
        
            bool Result = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select InvoiceId from ReturnedCustomerPackage where (InvoiceId=" + InvoiceId + " );";



                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    Result = true;
                }


                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                Result = false;
                MessageBox.Show(ex.Message);
                return Result;
            }
        }


        public decimal getReturnAmount(int InvoiceId)
        {

            decimal amount = 0;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select SUM(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount " +
                                  "  from ReturnedCustomerPackage as RT " +
                                   " where (RT.InvoiceId = " + InvoiceId + ");";



                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if (reader["ReturnAmount"] == DBNull.Value)
                    {
                        amount = 0;
                    }
                    else
                    {
                        amount = decimal.Parse(reader["ReturnAmount"].ToString());
                    }
                }

                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return amount;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return amount;
            }
        }

        public List<clsReturnedItems> getReturnedPackagesList(DateTime fromDate, DateTime toDate)
        {
            List<clsReturnedItems> DisplayArray = new List<clsReturnedItems>();
            List<int> ids = new List<int>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql =
                        "Select TI.InvoiceId,TI.Name,(TI.NetPrice-(TI.Discount*TI.NetPrice*0.01)) as NetPrice ,RT.Date as ReturnDate,TI.Date as InvoiceDate,(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.ItemId,RT.PackageId  " +
                        "from PackageInvoice as TI Inner Join ReturnedCustomerPackage as RT ON RT.InvoiceId = TI.InvoiceId " +
                        "where RT.Date between @fromDate and @toDate " +
                        "order by TI.InvoiceId";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedItems item;
                Package packageConnection = new Package();
                Item itemConnection = new Item();

                while (dr.Read())
                {
                    item = new clsReturnedItems();
                    clsTickets ticket = new clsTickets();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { item.InvoiceId = 0; }
                    else
                    { item.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["ReturnDate"] == DBNull.Value)
                    { item.ReturnedDate = DefaultDate; }
                    else
                    { item.ReturnedDate = DateTime.Parse(dr["ReturnDate"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { item.Name = " "; }
                    else
                    { item.Name = dr["Name"].ToString(); }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        item.NetPrice = 0;
                    }
                    else
                    {
                        item.NetPrice = Decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["InvoiceDate"] == DBNull.Value)
                    { item.InvoiceDate = DefaultDate; }
                    else
                    { item.InvoiceDate = DateTime.Parse(dr["InvoiceDate"].ToString()); }


                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        item.ReturnAmount = 0;
                    }
                    else
                    {
                        item.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    int PcakgeId;
                    if (dr["PackageId"] == DBNull.Value)
                    {
                        PcakgeId = 0;
                    }
                    else
                    {
                        PcakgeId = int.Parse(dr["PackageId"].ToString());
                    }
                    int id;
                    if (dr["ItemId"] == DBNull.Value)
                    {
                         id = 0;
                    }
                    else
                    {
                        id = int.Parse(dr["ItemId"].ToString());
                    }
                    if(id==0)
                    {
                      
                        item.ReturnType = packageConnection.getPackageInfo(PcakgeId).packageName;
                    }
                    else
                    {
                        item.ReturnType = itemConnection.ReadItem(id).itemName;
                    }
                    item.RemainedAmount = item.NetPrice - item.ReturnAmount;
                    if (ids.Contains(item.InvoiceId))
                    {
                        int index = ids.LastIndexOf(item.InvoiceId);
                        item.RemainedAmount = DisplayArray[index].RemainedAmount - item.ReturnAmount;
                    }
                        
                    ids.Add(item.InvoiceId);
                    DisplayArray.Add(item);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
