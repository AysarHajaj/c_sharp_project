﻿using SerhanTravel.Objects;
using SerhanTravel.reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class ReturnedSupplierItem
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public List<clsReturnedSupplierItem> getReturnedItems(int InvoiceId)
        {
            List<clsReturnedSupplierItem> DisplayArray = new List<clsReturnedSupplierItem>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                    string sql = "SELECT RT.CustomerName,RT.Id,RT.CurrencyRate,RT.InvoiceId,RT.Date,RT.ReturnAmount,RT.ItemId,RT.CurrencyId as Currency,I.CurrencyId,I.CategoryId,I.Cost,I.Description,I.Name,I.CurrencyRate as ItemRate,I.SupplierId " +
                   " FROM ReturnedSupplierItem As RT Inner Join Item aS  I on I.ItemId = RT.ItemId  "+
                    "where(InvoiceId = "+InvoiceId+"); ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedSupplierItem items;


                while (dr.Read())
                {
                    items = new clsReturnedSupplierItem();
                    clsItem item = new clsItem();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { items.InvoiceId = 0; }
                    else
                    { items.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { items.Date = DefaultDate; }
                    else
                    { items.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["Id"] == DBNull.Value)
                    { items.Id = 0; }
                    else
                    { items.Id = int.Parse(dr["Id"].ToString()); }
                    if (dr["Currency"] == DBNull.Value)
                    { items.CurrencyId = 0; }
                    else
                    { items.CurrencyId = int.Parse(dr["Currency"].ToString()); }

                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        items.ReturnAmount = 0;
                    }
                    else
                    {
                        items.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    {
                        items.CurrencyRate = 0;
                    }
                    else
                    {
                        items.CurrencyRate = Decimal.Parse(dr["CurrencyRate"].ToString());
                    }
                    if (dr["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(dr["ItemId"].ToString()); }
                    if (dr["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = dr["Description"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = dr["Name"].ToString(); }
                    if (dr["CustomerName"] == DBNull.Value)
                    { item.CustomerName = ""; }
                    else
                    { item.CustomerName = dr["CustomerName"].ToString(); }
                    if (dr["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(dr["CategoryId"].ToString()); }
                    if (dr["ItemRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(dr["ItemRate"].ToString()); }
                    items.Item = item;
                    DisplayArray.Add(items);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public clsReturnedSupplierItem getReturnedItem(int InvoiceId,int itemId,string CustomerName)
        {
            clsReturnedSupplierItem items=new clsReturnedSupplierItem();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT RT.CustomerName,RT.Id,RT.CurrencyRate,RT.InvoiceId,RT.Date,RT.ReturnAmount,RT.ItemId,RT.CurrencyId as Currency,I.CurrencyId,I.CategoryId,I.Cost,I.Description,I.Name,I.CurrencyRate as ItemRate,I.SupplierId " +
               " FROM ReturnedSupplierItem As RT Inner Join Item aS  I on I.ItemId = RT.ItemId  " +
                "where(RT.InvoiceId=@InvoiceId  and  RT.ItemId=@itemId and RT.CustomerName=@CustomerName); ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = InvoiceId;
                cmd.Parameters.Add("@itemId", SqlDbType.Int).Value = itemId;
                cmd.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = CustomerName;
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    items = new clsReturnedSupplierItem();
                    clsItem item = new clsItem();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { items.InvoiceId = 0; }
                    else
                    { items.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { items.Date = DefaultDate; }
                    else
                    { items.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["Id"] == DBNull.Value)
                    { items.Id = 0; }
                    else
                    { items.Id = int.Parse(dr["Id"].ToString()); }
                    if (dr["Currency"] == DBNull.Value)
                    { items.CurrencyId = 0; }
                    else
                    { items.CurrencyId = int.Parse(dr["Currency"].ToString()); }

                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        items.ReturnAmount = 0;
                    }
                    else
                    {
                        items.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    {
                        items.CurrencyRate = 0;
                    }
                    else
                    {
                        items.CurrencyRate = Decimal.Parse(dr["CurrencyRate"].ToString());
                    }
                    if (dr["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(dr["ItemId"].ToString()); }
                    if (dr["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = dr["Description"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = dr["Name"].ToString(); }
                    if (dr["CustomerName"] == DBNull.Value)
                    { item.CustomerName = ""; }
                    else
                    { item.CustomerName = dr["CustomerName"].ToString(); }
                    if (dr["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(dr["CategoryId"].ToString()); }
                    if (dr["ItemRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(dr["ItemRate"].ToString()); }
                    items.Item = item;
                  

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return items;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return items;
            }
        }
        public void AddReturnedItem(clsReturnedSupplierItem items)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into ReturnedSupplierItem (" +
                            "InvoiceId, " +
                            "ReturnAmount, " +
                            "ItemId, " +
                            "CurrencyRate, " +
                            "CurrencyId, " +
                             "CustomerName, " +
                            "Date  " +

                            ") " +
                            "VALUES (" +
                            "@InvoiceId, " +
                            "@ReturnAmount, " +
                            "@ItemId, " +
                            "@CurrencyRate, " +
                            "@CurrencyId, " +
                            "@CustomerName, " +
                            "@Date " +

                            ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = items.InvoiceId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = items.CurrencyId;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = items.Item.itemID;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Money).Value = items.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = items.Date;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = items.CurrencyRate;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = items.Item.CustomerName;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdateReturnedItem(clsReturnedSupplierItem items)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update ReturnedSupplierItem set " +
                                    "InvoiceId = @InvoiceId, " +
                                    "ReturnAmount=@ReturnAmount," +
                                     "CustomerName=@CustomerName," +
                                    "ItemId=@ItemId," +
                                    "CurrencyRate=@CurrencyRate," +
                                    "CurrencyId=@CurrencyId," +
                                    "Date = @Date " +
                                    "WHERE (Id  = @Id);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = items.InvoiceId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = items.CurrencyId;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = items.Item.itemID;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Money).Value = items.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = items.Date;
                myCommand.Parameters.Add("@Id", SqlDbType.Int).Value = items.Id;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = items.CurrencyRate;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = items.Item.CustomerName;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteReturnedItem(int Id)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from ReturnedSupplierItem where Id =" + Id;
                    

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        public bool InvoiceExist(int InvoiceId)
        {
         
            bool Result = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select InvoiceId from ReturnedSupplierItem where (InvoiceId=" + InvoiceId + " );";


            
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    Result = true;
                }
               

                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                Result = false;
                MessageBox.Show(ex.Message);
                return Result;
            }
        }

        public bool IsItemReturned(int InvoiceId,int itemId,string CustomerName)
        {

            bool Result = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select InvoiceId from ReturnedSupplierItem where (InvoiceId=@InvoiceId  and  ItemId=@itemId and CustomerName=@CustomerName );";



                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = InvoiceId;
                myCommand.Parameters.Add("@itemId", SqlDbType.Int).Value = itemId;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = CustomerName;
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    Result = true;
                }


                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                Result = false;
                MessageBox.Show(ex.Message);
                return Result;
            }
        }
        public decimal getReturnAmount(int InvoiceId, int itemId, string CustomerName)
        {

            decimal amount = 0;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select (RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount " +
                                  "  from ReturnedSupplierItem as RT " +
                                   " where (RT.InvoiceId=@InvoiceId  and  RT.ItemId=@itemId and RT.CustomerName=@CustomerName);";



                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = InvoiceId;
                myCommand.Parameters.Add("@itemId", SqlDbType.Int).Value = itemId;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = CustomerName;
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if (reader["ReturnAmount"] == DBNull.Value)
                    {
                        amount = 0;
                    }
                    else
                    {
                        amount = decimal.Parse(reader["ReturnAmount"].ToString());
                    }
                }

                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return amount;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return amount;
            }
        }
        public decimal getReturnAmount(int InvoiceId)
        {
           
            decimal amount = 0;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select SUM(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount " +
                                  "  from ReturnedSupplierItem as RT "+
                                   " where (RT.InvoiceId = "+InvoiceId+");";


            
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if(reader["ReturnAmount"]==DBNull.Value)
                    {
                        amount = 0;
                    }
                    else
                    {
                        amount = decimal.Parse(reader["ReturnAmount"].ToString());
                    }
                }
          
                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return amount;
            }
            catch (Exception ex)
            {
               
                MessageBox.Show(ex.Message);
                return amount;
            }
        }
        public List<clsReturnedItems> getReturnedItemList(int SupplierId,DateTime fromDate, DateTime toDate)
        {
            List<clsReturnedItems> DisplayArray = new List<clsReturnedItems>();
            List<int> ids = new List<int>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql =
                        "Select (IT.Cost*IT.CurrencyRate) as NetPrice,IT.ItemId,PIN.InvoiceId,INP.CustomerName,PIN.Date as InvoiceDate,RTI.Date as ReturnDate,IT.Name,(RTI.ReturnAmount*RTI.CurrencyRate) as ReturnAmount  " +
                        "from ReturnedSupplierItem as RTI Inner Join PackageInvoice as PIN on PIN.InvoiceId=RTI.InvoiceId  " +
                        "Inner Join InvoicePackages as 	INP on INP.InvoiceId = PIN.InvoiceId  and INP.CustomerName=RTI.CustomerName  " +
                        "Inner Join 	InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId 	and INPD.ItemId=RTI.ItemId " +
                        " Inner Join Item as IT on IT.ItemId =RTI.ItemId " +
                        " where IT.SupplierId =@SupplierId and IT.CategoryId = 1 and RTI.Date between @fromDate and @toDate";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedItems item;


                while (dr.Read())
                {
                    item = new clsReturnedItems();
                    clsTickets ticket = new clsTickets();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { item.InvoiceId = 0; }
                    else
                    { item.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["ReturnDate"] == DBNull.Value)
                    { item.ReturnedDate = DefaultDate; }
                    else
                    { item.ReturnedDate = DateTime.Parse(dr["ReturnDate"].ToString()); }

             

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        item.NetPrice = 0;
                    }
                    else
                    {
                        item.NetPrice = Decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["InvoiceDate"] == DBNull.Value)
                    { item.InvoiceDate = DefaultDate; }
                    else
                    { item.InvoiceDate = DateTime.Parse(dr["InvoiceDate"].ToString()); }
                    
                    if (dr["CustomerName"] == DBNull.Value)
                    { item.Name = " "; }
                    else
                    { item.Name = (dr["CustomerName"].ToString()); }
                    if (dr["Name"] == DBNull.Value)
                    { item.ReturnType = " "; }
                    else
                    { item.ReturnType = (dr["Name"].ToString()); }
                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        item.ReturnAmount = 0;
                    }
                    else
                    {
                        item.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    
                    item.RemainedAmount = item.NetPrice - item.ReturnAmount;
                    if (ids.Contains(item.InvoiceId))
                    {
                        int index = ids.LastIndexOf(item.InvoiceId);
                        item.RemainedAmount = DisplayArray[index].RemainedAmount - item.ReturnAmount;
                    }

                    ids.Add(item.InvoiceId);
                    DisplayArray.Add(item);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
