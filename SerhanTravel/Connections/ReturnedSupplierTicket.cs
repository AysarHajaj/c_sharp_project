﻿using SerhanTravel.Objects;
using SerhanTravel.reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class ReturnedSupplierTicket
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public List<clsReturnedSupplierTicket> getReturnedTickets(int supplierId)
        {
            List<clsReturnedSupplierTicket> DisplayArray = new List<clsReturnedSupplierTicket>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                    string sql = "SELECT RT.Id,RT.CurrencyRate,RT.Date,RT.ReturnAmount,RT.TicketId,T.TicketDate,T.AirlineId,RT.CurrencyId as Currency,   " +
                    "T.SupplierId,T.Remarks,T.ClassId,T.FlightNo,T.Destination,T.TicketNo,T.FlightSchedual,T.Name,T.Price,T.CustomerId,T.Cost,T.CurrencyId,T.CurrencyRate as TicketRate " +
                    "FROM ReturnedSupplierTicket As RT Inner Join Ticket aS  T on T.TicketId = RT.TicketId  " +
                    "where(T.SupplierId= " + supplierId + "); ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedSupplierTicket item;


                while (dr.Read())
                {
                    item = new clsReturnedSupplierTicket();
                    clsTickets ticket = new clsTickets();
                    if (dr["Date"] == DBNull.Value)
                    { item.Date = DefaultDate; }
                    else
                    { item.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["Id"] == DBNull.Value)
                    { item.Id = 0; }
                    else
                    { item.Id = int.Parse(dr["Id"].ToString()); }
                    if (dr["Currency"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(dr["Currency"].ToString()); }

                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        item.ReturnAmount = 0;
                    }
                    else
                    {
                        item.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    {
                        item.CurrencyRate = 0;
                    }
                    else
                    {
                        item.CurrencyRate = Decimal.Parse(dr["CurrencyRate"].ToString());
                    }
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }
                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = " "; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = " "; }
                    else
                    { ticket.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        ticket.CurrencyId = 0;
                    }
                    else
                    {
                        ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["TicketRate"] == DBNull.Value)
                    {
                        ticket.CurrencyRate = 0;
                    }
                    else
                    {
                        ticket.CurrencyRate = Decimal.Parse(dr["TicketRate"].ToString());
                    }
                    item.Ticket = ticket;

                    DisplayArray.Add(item);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public void AddReturnedTicket(clsReturnedSupplierTicket item)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into ReturnedSupplierTicket (" +
                             "ReturnAmount, " +
                            "TicketId, " +
                            "CurrencyId, " +
                            "CurrencyRate, " +
                            "Date  " +


                            ") " +
                            "VALUES (" +
                            "@ReturnAmount, " +
                            "@TicketId, " +
                            "@CurrencyId, " +
                            "@CurrencyRate, " +
                            "@Date " +

                            ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = item.Ticket.TicketId;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Money).Value = item.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = item.Date;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdateReturnedItem(clsReturnedSupplierTicket item)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update ReturnedSupplierTicket set " +
                   "ReturnAmount=@ReturnAmount," +
                         "CurrencyRate=@CurrencyRate," +
                        "TicketId=@TicketId," +
                         "CurrencyId=@CurrencyId," +
                        "Date = @Date " +
                        "WHERE (Id  = @Id);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = item.CurrencyId;
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = item.Ticket.TicketId;
                myCommand.Parameters.Add("@ReturnAmount", SqlDbType.Decimal).Value = item.ReturnAmount;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = item.Date;
                myCommand.Parameters.Add("@Id", SqlDbType.Int).Value = item.Id;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = item.CurrencyRate;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteReturnedTickets(int Id)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from ReturnedSupplierTicket where Id =" + Id;
                    

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        public bool TicektIDExist(int TicketId)
        {
            
            bool Result = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select TicketId from ReturnedSupplierTicket where (TicketId=" + TicketId + " );";


            
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    Result = true;
                }
               

                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                Result = false;
                MessageBox.Show(ex.Message);
                return Result;
            }
        }


        public decimal getReturnAmount(int TicketId)
        {
           
            decimal amount = 0;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select SUM(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount " +
                                  "  from ReturnedSupplierTicket as RT "+
                                   " where (RT.TicketId = " + TicketId + ");";


            
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if(reader["ReturnAmount"]==DBNull.Value)
                    {
                        amount = 0;
                    }
                    else
                    {
                        amount = decimal.Parse(reader["ReturnAmount"].ToString());
                    }
                }
          
                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return amount;
            }
            catch (Exception ex)
            {
               
                MessageBox.Show(ex.Message);
                return amount;
            }
        }
        public List<clsReturnedItems> getReturnedTicketsArrayList(int SupplierId,DateTime fromDate, DateTime toDate)
        {
            List<clsReturnedItems> DisplayArray = new List<clsReturnedItems>();
            List<int> ids = new List<int>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql =
                        "Select TI.TicketNo ,(TI.Cost*TI.CurrencyRate) as NetPrice,RT.Date as ReturnDate,TI.TicketDate as InvoiceDate,(RT.ReturnAmount * RT.CurrencyRate) as ReturnAmount,TI.Name " +
                        " from Ticket as TI Inner Join ReturnedSupplierTicket as RT ON RT.TicketId = TI.TicketId  " +
                        "where RT.Date between @fromDate and @toDate and TI.SupplierId=@SupplierId " +
                        "order by TI.TicketId";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsReturnedItems item;


                while (dr.Read())
                {
                    item = new clsReturnedItems();
                    clsTickets ticket = new clsTickets();
                    if (dr["TicketNo"] == DBNull.Value)
                    { item.TicketNo = " "; }
                    else
                    { item.TicketNo = (dr["TicketNo"].ToString()); }

                    if (dr["ReturnDate"] == DBNull.Value)
                    { item.ReturnedDate = DefaultDate; }
                    else
                    { item.ReturnedDate = DateTime.Parse(dr["ReturnDate"].ToString()); }
                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        item.NetPrice = 0;
                    }
                    else
                    {
                        item.NetPrice = Decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["InvoiceDate"] == DBNull.Value)
                    { item.InvoiceDate = DefaultDate; }
                    else
                    { item.InvoiceDate = DateTime.Parse(dr["InvoiceDate"].ToString()); }
                    if (dr["Name"] == DBNull.Value)
                    { item.Name = " "; }
                    else
                    { item.Name = (dr["Name"].ToString()); }

                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        item.ReturnAmount = 0;
                    }
                    else
                    {
                        item.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }
                    item.ReturnType = "Ticket";
                    item.RemainedAmount = item.NetPrice - item.ReturnAmount;
                    DisplayArray.Add(item);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public bool TicketReturned(int ticketId)
        {

            bool result = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Select RT.TicketId" +
                                  "  from ReturnedSupplierTicket as RT " +
                                  "Where (RT.TicketId=" + ticketId + " );";




                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon);
                SqlDataReader reader = myCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    result = true;
                }

                reader.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                result = false;
                MessageBox.Show(ex.Message);
                return result;
            }
        }
        public clsReturnedSupplierTicket getReturnedTicket(int ticketId)
        {
            clsReturnedSupplierTicket returnedTicket = new clsReturnedSupplierTicket();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT RT.Id,RT.CurrencyRate,RT.Date,RT.ReturnAmount,RT.TicketId,T.TicketDate,T.AirlineId,RT.CurrencyId as Currency,  " +
                "T.SupplierId,T.Remarks,T.ClassId,T.FlightNo,T.Destination,T.TicketNo,T.FlightSchedual,T.Name,T.Price,T.CustomerId,T.Cost,T.CurrencyId,T.CurrencyRate as TicketRate " +
                "FROM ReturnedSupplierTicket As RT Inner Join Ticket aS  T on T.TicketId = RT.TicketId " +
                "where (T.TicketId=" + ticketId + "); ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();

                    clsTickets ticket = new clsTickets();


                    if (dr["Date"] == DBNull.Value)
                    { returnedTicket.Date = DefaultDate; }
                    else
                    { returnedTicket.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["Id"] == DBNull.Value)
                    { returnedTicket.Id = 0; }
                    else
                    { returnedTicket.Id = int.Parse(dr["Id"].ToString()); }
                    if (dr["Currency"] == DBNull.Value)
                    { returnedTicket.CurrencyId = 0; }
                    else
                    { returnedTicket.CurrencyId = int.Parse(dr["Currency"].ToString()); }

                    if (dr["ReturnAmount"] == DBNull.Value)
                    {
                        returnedTicket.ReturnAmount = 0;
                    }
                    else
                    {
                        returnedTicket.ReturnAmount = Decimal.Parse(dr["ReturnAmount"].ToString());
                    }

                    if (dr["CurrencyRate"] == DBNull.Value)
                    {
                        returnedTicket.CurrencyRate = 0;
                    }
                    else
                    {
                        returnedTicket.CurrencyRate = Decimal.Parse(dr["CurrencyRate"].ToString());
                    }
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }
                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = " "; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = " "; }
                    else
                    { ticket.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        ticket.CurrencyId = 0;
                    }
                    else
                    {
                        ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["TicketRate"] == DBNull.Value)
                    {
                        ticket.CurrencyRate = 0;
                    }
                    else
                    {
                        ticket.CurrencyRate = Decimal.Parse(dr["TicketRate"].ToString());
                    }
                    returnedTicket.Ticket = ticket;



                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return returnedTicket;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return returnedTicket;
            }
        }
    }
}
