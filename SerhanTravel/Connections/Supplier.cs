﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Supplier
    {


        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public List<clsSupplier> SupplierArrayList()
        {
            List<clsSupplier> DisplayArray = new List<clsSupplier>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Supplier ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplier supplier;


                while (dr.Read())
                {
                    supplier = new clsSupplier();
                    if (dr["SupplierId"] == DBNull.Value)
                    { supplier.SupplierId = 0; }
                    else
                    { supplier.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Name"] == DBNull.Value)
                    { supplier.SupplierName = " "; }
                    else
                    { supplier.SupplierName = dr["Name"].ToString(); }
                   
                    if (dr["CountryId"] == DBNull.Value)
                    { supplier.CountryId = 0; }
                    else
                    { supplier.CountryId = int.Parse(dr["CountryId"].ToString()); }

                    if (dr["CityId"] == DBNull.Value)
                    { supplier.CityId = 0; }
                    else
                    { supplier.CityId = int.Parse(dr["CityId"].ToString()); }
                    if (dr["ContactPerson"] == DBNull.Value)
                    { supplier.ContactPerson = " "; }
                    else
                    { supplier.ContactPerson = dr["ContactPerson"].ToString(); }
                    if (dr["Address"] == DBNull.Value)
                    { supplier.Address = " "; }
                    else
                    { supplier.Address = dr["Address"].ToString(); }
                    if (dr["GroupId"] == DBNull.Value)
                    { supplier.GroupId = 0; }
                    else
                    { supplier.GroupId = int.Parse(dr["GroupId"].ToString()); }
                    if (dr["MobilePhone"] == DBNull.Value)
                    { supplier.MobilePhone =string.Empty; }
                    else
                    { supplier.MobilePhone = dr["MobilePhone"].ToString(); }
                    if (dr["LandPhone"] == DBNull.Value)
                    { supplier.LandPhone = " "; }
                    else
                    { supplier.LandPhone = dr["LandPhone"].ToString(); }
                    if (dr["FaxNo"] == DBNull.Value)
                    { supplier.FaxNo = string.Empty; }
                    else
                    { supplier.FaxNo = dr["FaxNo"].ToString(); }
                    if (dr["Email"] == DBNull.Value)
                    { supplier.Email = " "; }
                    else
                    { supplier.Email = dr["Email"].ToString(); }
                    if (dr["Website"] == DBNull.Value)
                    { supplier.WebSite = " "; }
                    else
                    { supplier.WebSite = dr["Website"].ToString(); }
                    if (dr["Status"] == DBNull.Value)
                    { supplier.Status = 0; }
                    else
                    { supplier.Status = int.Parse(dr["Status"].ToString()); }

                    DisplayArray.Add(supplier);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal string getSupplier(int spplierID)
        {
            string suppName = "";
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT Name FROM Supplier where SupplierId = "+spplierID;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
            
                while(dr.Read())
                {
                    suppName =dr["Name"].ToString();
                }
               
               
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return suppName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return suppName;
            }
        }
        

        internal void UpdateSupplierInfo(clsSupplier supplier)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update Supplier set " +
                        "Name = @Name, " +
                        "ContactPerson = @ContactPerson, " +
                        "GroupId = @GroupId, " +
                        "CountryId = @CountryId, " +
                        "CityId = @CityId, " +
                        "Address = @Address, " +
                         "MobilePhone = @MobilePhone, " +
                        "LandPhone = @LandPhone, " +
                        "FaxNo = @FaxNo, " +
                        "Email = @Email, " +
                         "Website = @Website, " +
                        "Status = @Status " +
                        "WHERE (SupplierId  = @SupplierId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = supplier.SupplierName;
                myCommand.Parameters.Add("@CountryId", SqlDbType.Int).Value = supplier.CountryId;
                myCommand.Parameters.Add("@CityId", SqlDbType.Int).Value = supplier.CityId;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = supplier.Address;
                myCommand.Parameters.Add("@ContactPerson", SqlDbType.NVarChar).Value = supplier.ContactPerson;
                myCommand.Parameters.Add("@GroupId", SqlDbType.Int).Value = supplier.GroupId;
                myCommand.Parameters.Add("@MobilePhone", SqlDbType.NVarChar).Value = supplier.MobilePhone;
                myCommand.Parameters.Add("@LandPhone", SqlDbType.NVarChar).Value = supplier.LandPhone;
                myCommand.Parameters.Add("@FaxNo", SqlDbType.NVarChar).Value = supplier.FaxNo;
                myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = supplier.Email;
                myCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = supplier.WebSite;
                myCommand.Parameters.Add("@Status", SqlDbType.Int).Value = supplier.Status;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = supplier.SupplierId;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        internal void AddNewSupplier(clsSupplier supplier)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into Supplier (" +
                        "Name, " +
                        "GroupId, " +
                        "ContactPerson, " +
                        "CountryId, " +
                        "CityId , " +
                        "Address, " +
                       "MobilePhone, " +
                        "LandPhone, " +
                        "FaxNo, " +
                        "Email , " +
                        "Website, " +
                        "Status" +
                        ") " +
                      "VALUES (" +
                        "@Name, " +
                        "@GroupId, " +
                        "@ContactPerson, " +
                        "@CountryId, " +
                        "@CityId, " +
                        "@Address, " +
                         "@MobilePhone, " +
                        "@LandPhone, " +
                        "@FaxNo, " +
                        "@Email, " +
                        "@Website," +
                        "@Status " +
                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = supplier.SupplierName;
                myCommand.Parameters.Add("@GroupId", SqlDbType.Int).Value = supplier.GroupId;
                myCommand.Parameters.Add("@ContactPerson", SqlDbType.NVarChar).Value = supplier.ContactPerson;
                myCommand.Parameters.Add("@CountryId", SqlDbType.Int).Value = supplier.CountryId;
                myCommand.Parameters.Add("@CityId", SqlDbType.Int).Value = supplier.CityId;
                myCommand.Parameters.Add("@Address", SqlDbType.Text).Value = supplier.Address;
                myCommand.Parameters.Add("@MobilePhone", SqlDbType.NVarChar).Value = supplier.MobilePhone;
                myCommand.Parameters.Add("@LandPhone", SqlDbType.NVarChar).Value = supplier.LandPhone;
                myCommand.Parameters.Add("@FaxNo", SqlDbType.NVarChar).Value = supplier.FaxNo;
                myCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = supplier.Email;
                myCommand.Parameters.Add("@Website", SqlDbType.NVarChar).Value = supplier.WebSite;
                myCommand.Parameters.Add("@Status", SqlDbType.Int).Value = supplier.Status;




                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal bool SupplierIDExists(int supplierId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT SupplierId FROM Supplier WHERE(SupplierId = " + supplierId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool DeleteSupplier(int SupplierId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM Supplier WHERE (SupplierId = " + SupplierId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        public List<clsTickets> getUnReturnedTicekts(int supplierId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * " +
                    " FROM Ticket where SupplierId =" + supplierId + " and Ticket.TicketId not In(Select TicketId from ReturnedSupplierTicket )";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public decimal getSupplierBalance(int SupplierId,DateTime InventoryDate,DateTime fromDate)
        {
            decimal balance = 0;
            try
            {
                balance += this.UnPaidTickets(SupplierId, InventoryDate, fromDate);
               // balance += this.UnCompletePaidTickets(SupplierId, InventoryDate, fromDate);
                balance += this.UnPaidItem(SupplierId, InventoryDate, fromDate);
               // balance += this.UnCompletePaidItem(SupplierId, InventoryDate, fromDate);
                return balance;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }
        public decimal UnPaidTickets(int SupplierId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                "Select Sum(TI.Cost*TI.CurrencyRate) as Balance   " +
                "from Ticket as TI   " +
                "where TI.SupplierId = @SupplierId and TI.TicketDate > @InventoryDate " +
               " and TI.TicketDate < @FromDate " +
               "and TI.TicketId not  In(Select RT.TicketId  from ReturnedSupplierTicket RT)";

                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                            balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =

               "  Select (TI.Cost*TI.CurrencyRate-(RT.ReturnAmount * RT.CurrencyRate)) as Balance   " +
                 " from Ticket as TI Inner Join ReturnedSupplierTicket as RT on RT.TicketId = TI.TicketId " +
               "  where TI.SupplierId = @SupplierId and TI.TicketDate > @InventoryDate " +
                " and TI.TicketDate < @FromDate  ";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();

                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }
      /*  public decimal UnCompletePaidTickets(int SupplierId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                       " Select((TI.Cost*TI.CurrencyRate) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance  " +
                   " from Ticket as TI Inner Join SupplierTicketPayment as Tp on Tp.TicketId = TI.TicketId " +
                   " where TI.SupplierId = @SupplierId  and TI.TicketDate > @InventoryDate and TI.TicketDate < @FromDate " +
                   "and TI.TicketId NOt In (Select RT.TicketId from ReturnedSupplierTicket as RT ) " +
                    " Group By TI.Cost,TI.CurrencyRate  " +
                   "                   having TI.Cost*TI.CurrencyRate > SUM(Tp.Payment * Tp.CurrencyRate); ";
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
                  "   Select((TI.Cost*TI.CurrencyRate - IsNull(amount.ReturnedAmount,0)) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance  " +
                "  from Ticket as TI Inner Join SupplierTicketPayment as Tp on Tp.TicketId = TI.TicketId " +
               "   Right Join (Select sum(RT.ReturnAmount*RT.CurrencyRate ) as ReturnedAmount,RT.TicketId from ReturnedSupplierTicket  RT " +
               "   Group by RT.TicketId) as amount on TI.TicketId= amount.TicketId  " +
                "  where TI.SupplierId = @SupplierId and TI.TicketDate > @InventoryDate   and TI.TicketDate < @FromDate " +
                "   Group By TI.Cost ,TI.TicketId,amount.ReturnedAmount,TI.CurrencyRate  " +
               " having(TI.Cost*TI.CurrencyRate - IsNull(amount.ReturnedAmount,0)) > SUM(Tp.Payment * Tp.CurrencyRate); ";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();

                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }*/

        public decimal UnPaidItem(int SupplierId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                "Select sum(IT.Cost*IT.CurrencyRate) as Balance " +
                                "from PackageInvoice as PIN inner JOin InvoicePackages as INP on INP.InvoiceId = PIN.InvoiceId " +
                               " Inner Join InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId " +
                               " Inner Join Item as IT on IT.ItemId = INPD.ItemId " +
                                "where IT.SupplierId =@SupplierId and IT.CategoryId = 1 and PIN.Date > @InventoryDate and PIN.Date < @FromDate " +
                                "and PIN.InvoiceId not in(select InvoiceId from ReturnedSupplierItem) " +
                                "Group by IT.Cost,IT.CurrencyRate " ;
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
                    "Select (IT.Cost*IT.CurrencyRate) as Balance,IT.ItemId,PIN.InvoiceId,INP.CustomerName " +
                                "from PackageInvoice as PIN inner JOin InvoicePackages as INP on INP.InvoiceId = PIN.InvoiceId " +
                               " Inner Join InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId " +
                               " Inner Join Item as IT on IT.ItemId = INPD.ItemId " +
                               "Inner Join ReturnedSupplierItem as RTI on RTI.InvoiceId=PIN.InvoiceId  " +
                                "where IT.SupplierId =@SupplierId and IT.CategoryId = 1 and PIN.Date > @InventoryDate and PIN.Date < @FromDate " +
                                "Group by IT.Cost,IT.CurrencyRate,IT.ItemId,PIN.InvoiceId,INP.CustomerName ";

                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                dr = cmd.ExecuteReader();
                ReturnedSupplierItem returnedItem = new ReturnedSupplierItem();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }
                    int ItemId, InvoiceId;
                    string CustomerName;
                    if (dr["ItemId"] == DBNull.Value)
                    {
                        ItemId = 0;
                    }
                    else
                    {
                        ItemId = int.Parse(dr["ItemId"].ToString());
                    }
                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        InvoiceId = 0;
                    }
                    else
                    {
                        InvoiceId = int.Parse(dr["InvoiceId"].ToString());
                    }
                    if (dr["CustomerName"] == DBNull.Value)
                    {
                        CustomerName =  "";
                    }
                    else
                    {
                        CustomerName = dr["CustomerName"].ToString();
                    }

                    if(returnedItem.IsItemReturned(InvoiceId,ItemId,CustomerName))
                    {
                        balance -= returnedItem.getReturnAmount(InvoiceId, ItemId, CustomerName);
                    }
                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();

                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }


        /*public decimal UnCompletePaidItem(int SupplierId, DateTime InventoryDate, DateTime FromDate)
        {
            decimal balance = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string query =
                    "  Select((SI.NetPrice) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance  " +
                    " from SupplierItemInvoice as SI Inner Join SupplierItemPayments as Tp on Tp.SupplierInvoiceId = SI.InvoiceId " +
                    " where SI.SupplierId =@SupplierId and SI.Date > @InventoryDate and SI.Date < @FromDate " +
                    "and SI.InvoiceId not in(Select RI.InvoiceId from ReturnedSupplierItem as RI) " +
                    " Group By SI.NetPrice " +
                    " having SI.NetPrice > SUM(Tp.Payment * Tp.CurrencyRate) ";
                SqlCommand cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                query =
                "  Select((TI.NetPrice - IsNull(amount.ReturnedAmount, 0)) - Sum(Tp.Payment * Tp.CurrencyRate)) as Balance  " +
                "    from SupplierItemInvoice as TI Inner Join SupplierItemPayments as Tp on Tp.SupplierInvoiceId = TI.InvoiceId  " +
             " Right Join(Select sum(RT.ReturnAmount* RT.CurrencyRate ) as ReturnedAmount,RT.InvoiceId from ReturnedSupplierItem RT  " +
             " Group by RT.InvoiceId) as amount on TI.InvoiceId = amount.InvoiceId " +
               "   where TI.SupplierId = @SupplierId and TI.Date > @InventoryDate  and TI.Date < @FromDate " +
                "  Group By TI.NetPrice,TI.InvoiceId,amount.ReturnedAmount " +
               "  having(TI.NetPrice - IsNull(amount.ReturnedAmount, 0)) > SUM(Tp.Payment * Tp.CurrencyRate);  ";
                cmd = new SqlCommand(query, sqlCon);
                cmd.Parameters.Add("@InventoryDate", SqlDbType.Date).Value = InventoryDate;
                cmd.Parameters.Add("@FromDate", SqlDbType.Date).Value = FromDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Balance"] == DBNull.Value)
                    {
                        balance += 0;
                    }
                    else
                    {
                        balance += decimal.Parse(dr["Balance"].ToString());
                    }


                }
                dr.Close();
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();

                return balance;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return balance;
            }
        }
        */

        public bool SupplierUsed(int SupplierId)
        {
            if (!ExistsInTicket(SupplierId) && !ExistsInItem(SupplierId) && !ExistsInItemPayment(SupplierId) && !ExistsInITicketPayment(SupplierId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool ExistsInITicketPayment(int SupplierId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT SupplierId FROM SupplierTicketPayment WHERE(SupplierId = " + SupplierId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInItemPayment(int SupplierId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT SupplierId FROM SupplierItemPayments WHERE(SupplierId = " + SupplierId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInItem(int SupplierId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT SupplierId FROM Item WHERE(SupplierId = " + SupplierId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInTicket(int SupplierId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT SupplierId FROM Ticket WHERE(SupplierId = " + SupplierId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }

}
