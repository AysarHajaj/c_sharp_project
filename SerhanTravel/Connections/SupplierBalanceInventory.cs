﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class SupplierBalanceInventory
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        internal bool InventoryIDExists(int inventoryId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InventoryId FROM SupplierBalanceInventory WHERE(InventoryId = " + inventoryId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        internal  List<clsSupplierInventory> getSuppierInventoryList(int supplierId)
        {
            List<clsSupplierInventory> Items = new List<clsSupplierInventory>();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM SupplierBalanceInventory Where SupplierId = "+supplierId, con);
                SqlDataReader reader = command.ExecuteReader();

                clsSupplierInventory Inventory;
                while (reader.Read())
                {
                    Inventory = new clsSupplierInventory();
                    if (reader["InventoryId"] == DBNull.Value)
                    { Inventory.InventoryId = 0; }
                    else
                    { Inventory.InventoryId = int.Parse(reader["InventoryId"].ToString()); }
                    if (reader["Balance"] == DBNull.Value)
                    { Inventory.Balance = 0; }
                    else
                    { Inventory.Balance = decimal.Parse(reader["Balance"].ToString()); }
                   
                    if (reader["SupplierId"] == DBNull.Value)
                    { Inventory.SupplierId = 0; }
                    else
                    { Inventory.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { Inventory.CurrencyId = 0; }
                    else
                    { Inventory.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["Date"] == DBNull.Value)
                    { Inventory.Date = DefaultDate; }
                    else
                    { Inventory.Date = DateTime.Parse(reader["Date"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { Inventory.CurrencyRate = 0; }
                    else
                    { Inventory.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }

                    Items.Add(Inventory);
                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Items;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Items;
            }

        }
        internal clsSupplierInventory ReadInventory(int InventoryId)
        {
            clsSupplierInventory Inventory = new clsSupplierInventory();
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SELECT * FROM SupplierBalanceInventory Where (InventoryId = " + InventoryId + ");", con);
                SqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {
                    reader.Read();

                    if (reader["InventoryId"] == DBNull.Value)
                    { Inventory.InventoryId = 0; }
                    else
                    { Inventory.InventoryId = int.Parse(reader["InventoryId"].ToString()); }
                    if (reader["Balance"] == DBNull.Value)
                    { Inventory.Balance = 0; }
                    else
                    { Inventory.Balance = decimal.Parse(reader["Balance"].ToString()); }

                    if (reader["SupplierId"] == DBNull.Value)
                    { Inventory.SupplierId = 0; }
                    else
                    { Inventory.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { Inventory.CurrencyId = 0; }
                    else
                    { Inventory.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["Date"] == DBNull.Value)
                    { Inventory.Date = DefaultDate; }
                    else
                    { Inventory.Date = DateTime.Parse(reader["Date"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { Inventory.CurrencyRate = 0; }
                    else
                    { Inventory.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }


                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Inventory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Inventory;
            }
        }
            internal void UpdateInventoryInfo(clsSupplierInventory Inventory)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "update SupplierBalanceInventory set " +

                         
                            "SupplierId = @SupplierId, " +
                            "Balance = @Balance, " +
                             "CurrencyRate=@CurrencyRate, " +
                            "CurrencyId=@CurrencyId, " +
                            "Date = @Date " +

                            "WHERE (InventoryId  = @InventoryId);";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

            

                    myCommand.Parameters.Add("@Balance", SqlDbType.Decimal).Value =Inventory.Balance;
                    myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Inventory.Date;
                myCommand.Parameters.Add("@InventoryId", SqlDbType.Int).Value = Inventory.InventoryId;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = Inventory.SupplierId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Inventory.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Inventory.CurrencyRate;
                cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

      


        internal void AddNewInventory(clsSupplierInventory Inventory)
            {
                int cmdResult;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }



                    string sqlQuery = "insert into SupplierBalanceInventory (" +
                      
                    
                                "Balance, " +
                                "SupplierId , " +
                                "CurrencyId, " +
                                "CurrencyRate,  " +
                                "Date  " +
                   
                    
                                ") " +
                                "VALUES (" +
                        
                            
                                "@Balance, " +
                                "@SupplierId, " +
                                "@CurrencyId, " +
                                 "@CurrencyRate, " +
                                "@Date " +
                      
                                ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Balance", SqlDbType.Decimal).Value = Inventory.Balance;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = Inventory.Date;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = Inventory.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = Inventory.CurrencyRate;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = Inventory.SupplierId;
                cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }



            public bool DeleteInventory(int InventoryId)
            {
                int cmdResult;
                bool myResult;
                try
                {
                    myResult = true;
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "DELETE FROM SupplierBalanceInventory WHERE (InventoryId = " + InventoryId + ");";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                    cmdResult = myCommand.ExecuteNonQuery();
                    myTrans.Commit();

                    myResult = true;
                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return myResult;
                }
                catch (Exception ex)
                {
                    myResult = false;
                    MessageBox.Show(ex.Message);
                    return myResult;
                }
            }
            public int ReadLastNo()
            {
                int maxId;
                try
                {
                    SqlConnection sqlCon;
                    BuildConnection aBuildConnection = new BuildConnection();
                    sqlCon = aBuildConnection.AddSqlConnection();

                    if (sqlCon.State != ConnectionState.Open)
                    {
                        sqlCon.Open();
                    }

                    string sqlQuery = "SELECT MAX(InventoryId) FROM SupplierBalanceInventory;";

                    SqlTransaction myTrans = sqlCon.BeginTransaction();
                    SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());

                    myTrans.Commit();

                    sqlCon.Close();
                    sqlCon.Dispose();
                    myCommand.Dispose();
                    myTrans.Dispose();
                    return maxId;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return 0;
                }
            }
        internal clsSupplierInventory SupplierLastInventory(int supplierId)
        {
            clsSupplierInventory Inventory = null;
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
                string query =
                        "SELECT CB.Date,CB.CurrencyId,CB.InventoryId,(CB.Balance * CB.CurrencyRate) as Balance,CB.SupplierId " +
                        "FROM SupplierBalanceInventory as CB " +
                        "Where CB.SupplierId = " + supplierId + "and CB.Date In(Select Max(CB.Date) as Date from SupplierBalanceInventory as CB Where CB.SupplierId =" + supplierId + ")";
                SqlCommand command = new SqlCommand(query, con);
                SqlDataReader reader = command.ExecuteReader();


                if (reader.HasRows)
                {
                    reader.Read();
                    Inventory = new clsSupplierInventory();
                    if (reader["InventoryId"] == DBNull.Value)
                    { Inventory.InventoryId = 0; }
                    else
                    { Inventory.InventoryId = int.Parse(reader["InventoryId"].ToString()); }
                    if (reader["Balance"] == DBNull.Value)
                    { Inventory.Balance = 0; }
                    else
                    { Inventory.Balance = decimal.Parse(reader["Balance"].ToString()); }

                    if (reader["SupplierId"] == DBNull.Value)
                    { Inventory.SupplierId = 0; }
                    else
                    { Inventory.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { Inventory.CurrencyId = 0; }
                    else
                    { Inventory.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["Date"] == DBNull.Value)
                    { Inventory.Date = DefaultDate; }
                    else
                    { Inventory.Date = DateTime.Parse(reader["Date"].ToString()); }


                }
                con.Close();
                con.Dispose();
                command.Dispose();
                reader.Close();
                return Inventory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return Inventory;
            }
        }



    }
}
