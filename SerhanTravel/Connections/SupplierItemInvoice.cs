﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class SupplierItemInvoice
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsSupplierItemInvoice> SupplierInvoicesArrayList(int SupplierId)
        {
            List<clsSupplierItemInvoice> DisplayArray = new List<clsSupplierItemInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select IT.ItemId,IT.Name,IT.Description,IT.Cost,IT.SupplierId,IT.CurrencyId,IT.CategoryId,IT.CurrencyRate,PIN.Date, " +
                                "INP.CustomerName,PIN.InvoiceId "+
                                "from PackageInvoice as PIN inner JOin InvoicePackages as INP on INP.InvoiceId = PIN.InvoiceId "+
                               " Inner Join InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId "+
                               " Inner Join Item as IT on IT.ItemId = INPD.ItemId "+
                                "where IT.SupplierId ="+ SupplierId + " and IT.CategoryId = 1 "+
                                "order by PIN.InvoiceId";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplierItemInvoice invoice;
                List<int> idList = new List<int>();
                clsItem item;
                while (dr.Read())
                {
                    invoice = new clsSupplierItemInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }
                    if (dr["Date"] == DBNull.Value)
                    { invoice.Date = DefaultDate; }
                    else
                    { invoice.Date = DateTime.Parse(dr["Date"].ToString()); }
                    invoice.SupplierId = SupplierId;
                     item = new clsItem();
                    if (dr["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(dr["ItemId"].ToString()); }
                    if (dr["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = dr["Description"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = dr["Name"].ToString(); }
                    if (dr["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(dr["CategoryId"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerName"] == DBNull.Value)
                    { item.CustomerName = ""; }
                    else
                    { item.CustomerName = dr["CustomerName"].ToString(); }

                    if(idList.Contains(invoice.InvoiceId))
                    {
                        int index = idList.IndexOf(invoice.InvoiceId);
                        DisplayArray[index].Items.Add(item);
                        
                    }
                    else
                    {
                        invoice.Items.Add(item);
                        DisplayArray.Add(invoice);
                        idList.Add(invoice.InvoiceId);
                    }
              

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public List<clsItem> SupplierItemsArrayList(int SupplierId)
        {
            List<clsItem> DisplayArray = new List<clsItem>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Item where(SupplierId=" + SupplierId + " and CategoryId=1);";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader reader = cmd.ExecuteReader();
                clsItem item;


                while (reader.Read())
                {
                    item = new clsItem();
                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(reader["CategoryId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }
                    DisplayArray.Add(item);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                reader.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        /*public List<clsItem> ItemsInvoicesArrayList(int InvoiceId)
        {
            List<clsItem> DisplayArray = new List<clsItem>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "select I.ItemId,I.Name,I.Description,I.Cost,I.SupplierId,I.CurrencyId,I.CurrencyRate,I.CategoryId,SIID.Quantity " +
                            " from Item as I Inner Join SupplierItemInvoiceDetails as SIID on I.ItemId = SIID.ItemId " +
                            " where I.ItemId = SIID.ItemId and SIID.InvoiceId = " + InvoiceId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader reader = cmd.ExecuteReader();
                clsItem item;
                while (reader.Read())
                {
                    item = new clsItem();
                    if (reader["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(reader["ItemId"].ToString()); }
                    if (reader["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = reader["Description"].ToString(); }
                    if (reader["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = reader["Name"].ToString(); }
                    if (reader["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(reader["SupplierId"].ToString()); }
                    if (reader["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(reader["Cost"].ToString()); }
                    if (reader["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(reader["CategoryId"].ToString()); }
                    if (reader["Quantity"] == DBNull.Value)
                    { item.Quantity = 0; }
                    else
                    { item.Quantity = int.Parse(reader["Quantity"].ToString()); }
                    if (reader["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(reader["CurrencyId"].ToString()); }
                    if (reader["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(reader["CurrencyRate"].ToString()); }
                    DisplayArray.Add(item);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                reader.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }

        }*/

      /*  internal bool InvoiceIDExists(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM SupplierItemInvoice WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        */
       /* internal void UpdateInvoiceInfo(clsSupplierItemInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update SupplierItemInvoice set " +
                        "SupplierId = @SupplierId, " +
                        "Date = @Date, " +
                        "NetPrice = @NetPrice " +
                        "WHERE (InvoiceId  = @InvoiceId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = invoice.Date;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = invoice.SupplierId;
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoice.InvoiceId;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Money).Value = invoice.NetPrice;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        */
     /*   internal void UpdateInvoiceDetails(int invoiceId, List<clsItem> items)
        {
            try
            {
                if (DeleteInvoiceItems(invoiceId))
                {

                    AddInvoiceItems(invoiceId, items);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        */
      /*  public bool DeleteInvoiceItems(int invoiceId)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from SupplierItemInvoiceDetails where InvoiceId IN (select InvoiceId from SupplierItemInvoiceDetails where InvoiceId = " + invoiceId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }*/

        /*public void AddInvoiceItems(int invoiceId, List<clsItem> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                AddInvoiceDetails(invoiceId, items[i].itemID,i, items[i].Quantity);
            }

        }*/

       /* public void AddInvoiceDetails(int invoicId, int ItemId,int i,int quantity)
       {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into SupplierItemInvoiceDetails (" +
                        "InvoiceId, " +
                        "i,"+
                        "Quantity," +
                        "ItemId  " +

                        ") " +
                      "VALUES (" +
                        "@InvoiceId, " +
                        "@i, "+
                        "@Quantity, " +
                        "@ItemId " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoicId;
                myCommand.Parameters.Add("@i", SqlDbType.Int).Value = i;
                myCommand.Parameters.Add("@ItemId", SqlDbType.Int).Value = ItemId;
                myCommand.Parameters.Add("@Quantity", SqlDbType.Int).Value = quantity;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        */
        /*internal void AddNewSupplierInvoice(clsSupplierItemInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into SupplierItemInvoice (" +
                        "SupplierId, " +
                        "Date , " +
                        "NetPrice" +
                        ") " +
                      "VALUES (" +
                        "@SupplierId, " +
                        "@Date, " +
                        "@NetPrice " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = invoice.Date;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = invoice.SupplierId;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Money).Value = invoice.NetPrice;
               


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        */
      /*  public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(InvoiceId) FROM SupplierItemInvoice;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }






                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }*/

       /* public bool DeleteInVoice(int invoiceId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM SupplierItemInvoice WHERE (InvoiceId = " + invoiceId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        */
        
        public int NumberOfItemIssued(int itemId)
        {
            int quantity = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "select (COUNT(IP.ItemId)) as quantity " +
                    "from InvoicePackageDetail as IP " +
                    "where IP.ItemId= @itemId ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@itemId", SqlDbType.Int).Value = itemId;
                SqlDataReader dr = cmd.ExecuteReader();
                


                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["quantity"] == DBNull.Value)
                    { quantity = 0; }
                    else
                    { quantity = int.Parse(dr["quantity"].ToString()); }

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return quantity;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return quantity;
            }
        }

        public int NumberOfInvoiced(int itemId)
        {
            int quantity = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "select SUM(Quantity) as quantity from SupplierItemInvoiceDetails where ItemId = " + itemId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["quantity"] == DBNull.Value)
                    { quantity = 0; }
                    else
                    { quantity = int.Parse(dr["quantity"].ToString()); }

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return quantity;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return quantity;
            }
        }
       
        public List<clsSupplierItemInvoice> SupplierInvoices(DateTime fromDate,DateTime toDate, int SupplierId)
        {
            List<clsSupplierItemInvoice> DisplayArray = new List<clsSupplierItemInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select IT.ItemId,IT.Name,IT.Description,IT.Cost,IT.SupplierId,IT.CurrencyId,IT.CategoryId,IT.CurrencyRate,PIN.Date, " +
                                "INP.CustomerName,PIN.InvoiceId " +
                                "from PackageInvoice as PIN inner JOin InvoicePackages as INP on INP.InvoiceId = PIN.InvoiceId " +
                               " Inner Join InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId " +
                               " Inner Join Item as IT on IT.ItemId = INPD.ItemId " +
                                "where IT.SupplierId =" + SupplierId + " and IT.CategoryId = 1  and PIN.Date between @fromdate and @toDate  " +
                                "order by PIN.InvoiceId";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplierItemInvoice invoice;
                List<int> idList = new List<int>();
                while (dr.Read())
                {
                    invoice = new clsSupplierItemInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }
                    if (dr["Date"] == DBNull.Value)
                    { invoice.Date = DefaultDate; }
                    else
                    { invoice.Date = DateTime.Parse(dr["Date"].ToString()); }
                    invoice.SupplierId = SupplierId;
                    clsItem item = new clsItem();
                    if (dr["ItemId"] == DBNull.Value)
                    { item.itemID = 0; }
                    else
                    { item.itemID = int.Parse(dr["ItemId"].ToString()); }
                    if (dr["Description"] == DBNull.Value)
                    { item.itemDescription = ""; }
                    else
                    { item.itemDescription = dr["Description"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { item.itemName = ""; }
                    else
                    { item.itemName = dr["Name"].ToString(); }
                    if (dr["SupplierId"] == DBNull.Value)
                    { item.SupplierId = 0; }
                    else
                    { item.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { item.Cost = 0; }
                    else
                    { item.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { item.CurrencyId = 0; }
                    else
                    { item.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    if (dr["CategoryId"] == DBNull.Value)
                    { item.CategoryId = 0; }
                    else
                    { item.CategoryId = int.Parse(dr["CategoryId"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { item.CurrencyRate = 0; }
                    else
                    { item.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerName"] == DBNull.Value)
                    { item.CustomerName = ""; }
                    else
                    { item.CustomerName = dr["CustomerName"].ToString(); }

                    if (idList.Contains(invoice.InvoiceId))
                    {
                        int index = idList.IndexOf(invoice.InvoiceId);
                        DisplayArray[index].Items.Add(item);

                    }
                    else
                    {
                        invoice.Items.Add(item);
                        DisplayArray.Add(invoice);
                        idList.Add(invoice.InvoiceId);
                    }


                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }


        }

        public decimal TotalPrice(int SupplierId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select sum(IT.Cost*IT.CurrencyRate) as totalPrice " +
                                "from PackageInvoice as PIN inner JOin InvoicePackages as INP on INP.InvoiceId = PIN.InvoiceId " +
                               " Inner Join InvoicePackageDetail as INPD on INPD.InvPackDetails = INP.InvoicePackagesId " +
                               " Inner Join Item as IT on IT.ItemId = INPD.ItemId " +
                                "where IT.SupplierId =" + SupplierId + " and IT.CategoryId = 1 ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPrice"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPrice"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }


    }
}
