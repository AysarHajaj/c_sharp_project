﻿using SerhanTravel.reports;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class SupplierPayments
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsSupplierPayments> getSupplierPayments(int type,int SupplierId,DateTime fromDate,DateTime toDate)
        {
            List<clsSupplierPayments> DisplayArray = new List<clsSupplierPayments>();

            string ticketQuery= "select  TI.TicketNo as InvoiceId,TI.TicketDate as InvoiceDate,(TP.Payment*TP.CurrencyRate) as Payment, " +
                                "TP.ChequeNo,TP.PaymentType,((TI.Cost*TI.CurrencyRate) -ISNULL(amount.ReturnAmount,0)) as NetPrice,TP.Date as PaymentDate " +
                                "from SupplierTicketPayment as TP Inner join Ticket as TI on TI.TicketId = TP.TicketId " +
                                "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.TicketId  " +
                                "from ReturnedSupplierTicket as RT Group by RT.TicketId) as amount on amount.TicketId=TI.TicketId " +
                                "where  TI.SupplierId =@SupplierId and TP.Date between @fromDate and @toDate ;";

            string itemQuery = "select IP.SupplierInvoiceId as InvoiceId ,IP.Date as PaymentDate ,(IP.Payment*IP.CurrencyRate) as Payment, " +
                               "IP.ChequeNo,IP.PaymentType,(ITN.NetPrice-ISNULL(amount.ReturnAmount,0)) as NetPrice,ITN.Date as InvoiceDate from SupplierItemPayments as IP " +
                               "Inner join SupplierItemInvoice as ITN on IP.SupplierInvoiceId = ITN.InvoiceId " +
                               "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.InvoiceId  " +
                                "from ReturnedSupplierItem as RT Group by RT.InvoiceId) as amount on amount.InvoiceId = ITN.InvoiceId " +
                               "where ITN.SupplierId =@SupplierId and IP.Date between @fromDate and @toDate ; ";

            string ticketCashQuery = "select  TI.TicketNo as InvoiceId,TI.TicketDate as InvoiceDate,(TP.Payment*TP.CurrencyRate) as Payment, " +
                                "TP.ChequeNo,TP.PaymentType,((TI.Cost*TI.CurrencyRate) -ISNULL(amount.ReturnAmount,0))  as NetPrice,TP.Date as PaymentDate " +
                                "from SupplierTicketPayment as TP Inner join Ticket as TI on TI.TicketId = TP.TicketId " +
                                "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.TicketId " +
                                "from ReturnedSupplierTicket as RT Group by RT.TicketId) as amount on amount.TicketId=TI.TicketId " +
                                "where  TI.SupplierId =@SupplierId and TP.Date between @fromDate and @toDate and TP.PaymentType=0  ;";

            string itemCashQuery = "select IP.SupplierInvoiceId as InvoiceId ,IP.Date as PaymentDate ,(IP.Payment*IP.CurrencyRate) as Payment, " +
                               "IP.ChequeNo,IP.PaymentType,(ITN.NetPrice-ISNULL(amount.ReturnAmount,0)) as NetPrice,ITN.Date as InvoiceDate from SupplierItemPayments as IP " +
                               "Inner join SupplierItemInvoice as ITN on IP.SupplierInvoiceId = ITN.InvoiceId " +
                               "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.InvoiceId " +
                               "from ReturnedSupplierItem as RT Group by RT.InvoiceId) as amount on amount.InvoiceId = ITN.InvoiceId  " +
                               "where ITN.SupplierId =@SupplierId and IP.Date between @fromDate and @toDate and IP.PaymentType=0 ; ";

            string ticketChequeQuery = "select  TI.TicketNo as InvoiceId,TI.TicketDate as InvoiceDate,(TP.Payment*TP.CurrencyRate) as Payment, " +
                                "TP.ChequeNo,TP.PaymentType,((TI.Cost*TI.CurrencyRate) -ISNULL(amount.ReturnAmount,0))  as NetPrice,TP.Date as PaymentDate " +
                                "from SupplierTicketPayment as TP Inner join Ticket as TI on TI.TicketId = TP.TicketId " +
                                "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.TicketId " +
                                "from ReturnedSupplierTicket as RT Group by RT.TicketId) as amount on amount.TicketId=TI.TicketId " +
                                "where  TI.SupplierId =@SupplierId and TP.Date between @fromDate and @toDate and TP.PaymentType=1  ;";

            string itemChequeQuery = "select IP.SupplierInvoiceId as InvoiceId ,IP.Date as PaymentDate ,(IP.Payment*IP.CurrencyRate) as Payment, " +
                               "IP.ChequeNo,IP.PaymentType,(ITN.NetPrice-ISNULL(amount.ReturnAmount,0)) as NetPrice,ITN.Date as InvoiceDate from SupplierItemPayments as IP " +
                               "Inner join SupplierItemInvoice as ITN on IP.SupplierInvoiceId = ITN.InvoiceId " +
                               "Left Join ( Select Sum(RT.ReturnAmount*RT.CurrencyRate) as ReturnAmount,RT.InvoiceId " +
                               "from ReturnedSupplierItem as RT Group by RT.InvoiceId) as amount on amount.InvoiceId=ITN.InvoiceId " +
                               "where ITN.SupplierId =@SupplierId and IP.Date between @fromDate and @toDate and IP.PaymentType=1 ; ";
            try
            {
                string currencyFormat = "#,##0;-#,##0;0";
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sql = "";

                if (type == 0)
                {
                    sql = ticketQuery;
                }
                else if(type==1)
                {
                    sql = itemQuery;
                }
                else if (type == 2)
                {
                    sql = ticketCashQuery;
                }
                else if (type == 3)
                {
                    sql = itemCashQuery;
                }
                else if (type == 4)
                {
                    sql = ticketChequeQuery;
                }
                else if (type == 5)
                {
                    sql = itemChequeQuery;
                }


                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplierPayments payments;
                while (dr.Read())
                {
                    payments = new clsSupplierPayments();
                    if (dr["NetPrice"] == DBNull.Value)
                    { payments.NetPrice = 0; }
                    else
                    {

                        payments.NetPrice =  decimal.Parse(dr["NetPrice"].ToString()); ;
                    }
                
                    string invoiceId;
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoiceId =string.Empty; }
                    else
                    { invoiceId = dr["InvoiceId"].ToString(); }

                    if (type == 0 ||type==2||type==4)
                    {
                        payments.Description = "TICKET PAYMENT :\n";
                        if(!string.IsNullOrEmpty(invoiceId))
                        {
                            payments.Description += "TICKET No: "+ invoiceId+"\n";
                        }
                        
                        
                    }
                    else
                    {
                        payments.Description = "ITEM PAYMENT :\n";
                        if (!string.IsNullOrEmpty(invoiceId))
                        {
                            payments.Description += "Invoice ID: " + invoiceId + "\n";
                        }
                    }

                    if (dr["InvoiceDate"] != DBNull.Value)
                    {
                        payments.Description += "Reservation Date:" + DateTime.Parse(dr["InvoiceDate"].ToString()).ToShortDateString()+"\n";
                    }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = string.Empty; }
                    else
                    {
                        payments.Payment = dr["Payment"].ToString();
                    }


                    if (dr["PaymentDate"] == DBNull.Value)
                    {
                        payments.Date = "";
                    }
                    else
                    {      
                        payments.Date = DateTime.Parse(dr["PaymentDate"].ToString()).ToShortDateString();
                    }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = "";
                    }
                    else
                    {
                        int paymentType = int.Parse(dr["PaymentType"].ToString());
                        if (paymentType == 0)
                        {
                            payments.PaymentType = "Cash";
                        }
                        else
                        {
                            payments.PaymentType = "Cheque of number:\t";
                            if (dr["ChequeNo"] == DBNull.Value)
                            {
                                payments.PaymentType = payments.PaymentType;
                            }
                            else
                            {
                                string s = (dr["ChequeNo"].ToString());
                                payments.PaymentType = payments.PaymentType+"\t"+ s;
                            }
                        }
                    }
                    DisplayArray.Add(payments);
                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
