﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class SupplierTicketInvoice
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsSupplierTicketInvoice> SupplierInvoicesArrayList(int SupplierId)
        {
            List<clsSupplierTicketInvoice> DisplayArray = new List<clsSupplierTicketInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM SupplierTicketInvoice where ( SupplierId = " + SupplierId + " );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplierTicketInvoice invoice;
                while (dr.Read())
                {
                    invoice = new clsSupplierTicketInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.Date = DefaultDate; }
                    else
                    { invoice.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { invoice.SupplierId = 0; }
                    else
                    { invoice.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Tcost"] == DBNull.Value)
                    { invoice.TotalPrice = 0; }
                    else
                    { invoice.TotalPrice = decimal.Parse(dr["Tcost"].ToString()); }

                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public List<clsTickets> SupplierTicketsArrayList(int SupplierId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select * from Ticket where SupplierId="+SupplierId+
                    " and TicketId not in (Select TicketId from SupplierTicketInvoiceDetails)";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets ticket;


                while (dr.Read())
                {
                    ticket = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = ""; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = ""; }
                    else
                    { ticket.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["CurrencyId"] == DBNull.Value)
                    { ticket.CurrencyId = 0; }
                    else
                    { ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    DisplayArray.Add(ticket);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public List<clsTickets> TicketsInvoicesArrayList(int InvoiceId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "select * from Ticket as T "+
                    "Inner join SupplierTicketInvoiceDetails as S "+
                    "on T.TicketId = S.TicketId "+
                    "where S.InvoiceId ="+ InvoiceId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets ticket;

                
                while (dr.Read())
                {
                    ticket = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = ""; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = ""; }
                    else
                    { ticket.FlightSchedual =dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    { ticket.CurrencyId = 0; }
                    else
                    { ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString()); }
                    DisplayArray.Add(ticket);


                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();
                return DisplayArray;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal bool InvoiceIDExists(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM SupplierTicketInvoice WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        internal void UpdateInvoiceInfo(clsSupplierTicketInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update SupplierTicketInvoice set " +
                        "SupplierId = @SupplierId, " +
                        "Date = @Date, " +
                        "Tcost = @Tcost " +

                        "WHERE (InvoiceId  = @InvoiceId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = invoice.Date;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = invoice.SupplierId;
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoice.InvoiceId;
                myCommand.Parameters.Add("@Tcost", SqlDbType.Decimal).Value = invoice.TotalPrice;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal void UpdateInvoiceDetails(int invoiceId, List<clsTickets> tickets)
        {
            try
            {
                if (DeleteInvoiceTickets(invoiceId))
                {

                    AddInvoiceTickets(invoiceId, tickets);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        public bool DeleteInvoiceTickets(int invoiceId)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from SupplierTicketInvoiceDetails where InvoiceId IN (select InvoiceId from SupplierTicketInvoiceDetails where InvoiceId = " + invoiceId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        public void AddInvoiceTickets(int invoiceId, List<clsTickets> tickets)
        {
            for (int i = 0; i < tickets.Count; i++)
            {
                AddInvoiceDetails(invoiceId, tickets[i].TicketId,i);
            }

        }

        public void AddInvoiceDetails(int invoicId, int ItemId,int i)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into SupplierTicketInvoiceDetails (" +
                        "InvoiceId, " +
                        "i,"+
                        "TicketId  " +

                        ") " +
                      "VALUES (" +
                        "@InvoiceId, " +
                        "@i, "+
                        "@TicketId " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoicId;
                myCommand.Parameters.Add("@i", SqlDbType.Int).Value = i;
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = ItemId;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal void AddNewSupplierInvoice(clsSupplierTicketInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into SupplierTicketInvoice (" +
                        "SupplierId, " +
                        "Date , " +
                        "Tcost" +
                        ") " +
                      "VALUES (" +
                        "@SupplierId, " +
                        "@Date, " +
                        "@Tcost " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = invoice.Date;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = invoice.SupplierId;
                myCommand.Parameters.Add("@Tcost", SqlDbType.Decimal).Value = invoice.TotalPrice;
               


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(InvoiceId) FROM SupplierTicketInvoice;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }






                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public bool DeleteInVoice(int invoiceId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM SupplierTicketInvoice WHERE (InvoiceId = " + invoiceId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        public List<clsSupplierTicketInvoice> SupplierInvoicesAfterInventory(DateTime fromDate,DateTime toDate, int SupplierId)
        {
            List<clsSupplierTicketInvoice> DisplayArray = new List<clsSupplierTicketInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM SupplierTicketInvoice as SI where ( SI.SupplierId = " + SupplierId + " )  and SI.Date between @fromdate and @toDate ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsSupplierTicketInvoice invoice;
                while (dr.Read())
                {
                    invoice = new clsSupplierTicketInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.Date = DefaultDate; }
                    else
                    { invoice.Date = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { invoice.SupplierId = 0; }
                    else
                    { invoice.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Tcost"] == DBNull.Value)
                    { invoice.TotalPrice = 0; }
                    else
                    { invoice.TotalPrice = decimal.Parse(dr["Tcost"].ToString()); }

                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

    }
}
