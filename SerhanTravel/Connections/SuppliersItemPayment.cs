﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class SuppliersItemPayment
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsSuppliersItemPayment> SupplierPayment(int SupplierId)
        {
            List<clsSuppliersItemPayment> DisplayArray = new List<clsSuppliersItemPayment>();


            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM SupplierItemPayments where(SupplierId =" + SupplierId + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                clsSuppliersItemPayment payments;
                while (dr.Read())
                {

                    payments = new clsSuppliersItemPayment();


                    if (dr["SupplierInvoiceId"] == DBNull.Value)
                    { payments.SupplierInvoiceId = 0; }
                    else
                    { payments.SupplierInvoiceId = int.Parse(dr["SupplierInvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = Convert.ToDecimal(dr["Payment"]); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType == 1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate = DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString(); }
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["SupplierId"] == DBNull.Value)
                    {
                        payments.SupplierId = 0;
                    }
                    else
                    {
                        payments.SupplierId = int.Parse(dr["SupplierId"].ToString());
                    }
                    DisplayArray.Add(payments);



                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public List<clsSuppliersItemPayment> SupplierPayment(int SupplierId, DateTime fromDate, DateTime toDate)
        {
            List<clsSuppliersItemPayment> DisplayArray = new List<clsSuppliersItemPayment>();


            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM SupplierItemPayments where(SupplierId =@SupplierId and  Date between @fromDate and @toDate);";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsSuppliersItemPayment payments;
                while (dr.Read())
                {

                    payments = new clsSuppliersItemPayment();


                    if (dr["SupplierInvoiceId"] == DBNull.Value)
                    { payments.SupplierInvoiceId = 0; }
                    else
                    { payments.SupplierInvoiceId = int.Parse(dr["SupplierInvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = Convert.ToDecimal(dr["Payment"]); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType == 1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate = DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString(); }
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["SupplierId"] == DBNull.Value)
                    {
                        payments.SupplierId = 0;
                    }
                    else
                    {
                        payments.SupplierId = int.Parse(dr["SupplierId"].ToString());
                    }
                    DisplayArray.Add(payments);



                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }





        internal bool invoiceIDExists(int paymentId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PaymentId FROM SupplierItemPayments WHERE(PaymentId = " + paymentId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        public void AddPaymentDetails(clsSuppliersItemPayment payment)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into SupplierItemPayments (" +
                        "SupplierInvoiceId, " +
                         "Date, " +
                          "Payment, " +
                          "CurrencyId, "+
                           "ChequeNo, " +
                            "BankId, " +
                             "ChequeDate, " +
                                "CurrencyRate, " +
                              "CollectedDate, " +
                               "SupplierId, " +
                                "PaymentType  " +

                        ") " +
                      "VALUES (" +
                        "@SupplierInvoiceId, " +
                          "@Date, " +
                            "@Payment, " +
                             "@CurrencyId, "+
                              "@ChequeNo, " +
                                "@BankId, " +
                                  "@ChequeDate, " +
                                     "@CurrencyRate, " +
                                    "@CollectedDate, " +
                                      "@SupplierId, " +
                                        "@PaymentType " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                SqlDateTime chequeDate = SqlDateTime.Null;
                SqlDateTime collecteddate = SqlDateTime.Null;
                if (payment.ChequeDate == null)
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = chequeDate;
                }
                else
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.ChequeDate);
                }
                if (payment.CollectedDate == null)
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = collecteddate;
                }
                else
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.CollectedDate);
                }
                myCommand.Parameters.Add("@SupplierInvoiceId", SqlDbType.Int).Value = payment.SupplierInvoiceId;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = payment.Date;
                myCommand.Parameters.Add("@Payment", SqlDbType.Money).Value = payment.Payment;
                myCommand.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = payment.ChequeNo;
                myCommand.Parameters.Add("@BankId", SqlDbType.Int).Value = payment.BankId;
                myCommand.Parameters.Add("@PaymentType", SqlDbType.Int).Value = payment.PaymentType;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = payment.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = payment.CurrencyRate;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = payment.SupplierId;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdatePaymentDetails(clsSuppliersItemPayment payment)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "update  SupplierItemPayments set " +
                                    "SupplierInvoiceId=@SupplierInvoiceId, " +
                                    "Date=@Date, " +
                                    "CurrencyRate=@CurrencyRate, " +
                                    "Payment=@Payment, " +
                                    "CurrencyId=@CurrencyId, " +
                                    "ChequeNo=@ChequeNo, " +
                                    "BankId=@BankId, " +
                                    "ChequeDate=@ChequeDate, " +
                                    "CollectedDate=@CollectedDate, " +
                                    "SupplierId=@SupplierId, " +
                                    "PaymentType=@PaymentType  " +
                                    "where( PaymentId=@PaymentId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                SqlDateTime chequeDate = SqlDateTime.Null;
                SqlDateTime collecteddate = SqlDateTime.Null;
                if (payment.ChequeDate == null)
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = chequeDate;
                }
                else
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.ChequeDate);
                }
                if (payment.CollectedDate == null)
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = collecteddate;
                }
                else
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.CollectedDate);
                }
                myCommand.Parameters.Add("@SupplierInvoiceId", SqlDbType.Int).Value = payment.SupplierInvoiceId;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = payment.Date;
                myCommand.Parameters.Add("@Payment", SqlDbType.Decimal).Value = payment.Payment;
                myCommand.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = payment.ChequeNo;
                myCommand.Parameters.Add("@BankId", SqlDbType.Int).Value = payment.BankId;
                myCommand.Parameters.Add("@PaymentType", SqlDbType.Int).Value = payment.PaymentType;
                myCommand.Parameters.Add("@PaymentId", SqlDbType.Int).Value = payment.PaymentId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = payment.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = payment.CurrencyRate;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = payment.SupplierId;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteInvoicePayments(int paymentId)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from SupplierItemPayments where PaymentId = " + paymentId;

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }






        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(PaymentId) FROM SupplierItemPayments;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }






                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public decimal TotalPaid(int SupplierId, DateTime fromDate, DateTime toDate)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TP.Payment*TP.CurrencyRate) as totalPaid " +
                    "from SupplierItemPayments AS TP where(TP.SupplierId =@SupplierId and  TP.Date >@fromDate and  TP.Date<@toDate) ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPaid"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPaid"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
        public decimal TotalPaid(int SupplierId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TP.Payment*TP.CurrencyRate) as totalPaid " +
                    "from SupplierItemPayments AS TP where(TP.SupplierId =@SupplierId ) ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@SupplierId", SqlDbType.Int).Value = SupplierId;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPaid"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPaid"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
    }
}
