﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class Ticket
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsTickets> TicketsArrayList()
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate =DateTime.Parse( dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }
                  
                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId =0; }
                    else
                    { tickets.TicketClassId =int.Parse(dr["ClassId"].ToString()) ; }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo= " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName= " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual =" "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo =dr["TicketNo"].ToString(); }
                   
                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if(dr["CurrencyId"] ==DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }

                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        internal bool TicketIDExists(int TicketId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT TicketId FROM Ticket WHERE(TicketId = " + TicketId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        internal void UpdateTicketInfo(clsTickets tickets)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update Ticket set " +
                                "TicketDate = @TicketDate, " +
                                "AirlineId = @AirlineId, " +
                                "SupplierId = @SupplierId, " +
                                "Remarks = @Remarks, " +
                                "ClassId = @ClassId, " +
                                "FlightNo = @FlightNo, " +
                                "Destination = @Destination, " +
                                "TicketNo = @TicketNo, " +
                                "FlightSchedual = @FlightSchedual, " +
                                "Name = @Name, " +
                                "CustomerId= @CustomerId, " +
                                "Cost= @Cost, " +
                                "CurrencyId=@CurrencyId, " +
                                "Profit = @Profit, " +
                                 "CurrencyRate= @CurrencyRate, " +
                                "ReferenceNo = @ReferenceNo, " +
                                "Price = @Price " +
                                "WHERE (TicketId  = @TicketId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@TicketDate", SqlDbType.Date).Value =tickets.TicketDate;
                myCommand.Parameters.Add("@AirlineId", SqlDbType.Int).Value =tickets.AirLineId;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = tickets.SupplierId;
                myCommand.Parameters.Add("@Remarks", SqlDbType.Text).Value =tickets.Remarks;
                myCommand.Parameters.Add("@ClassId", SqlDbType.Int).Value = tickets.TicketClassId;
                myCommand.Parameters.Add("@FlightNo", SqlDbType.NVarChar).Value = tickets.FlightNo;
                myCommand.Parameters.Add("@Destination", SqlDbType.NVarChar).Value = tickets.Destination;
                myCommand.Parameters.Add("@TicketNo", SqlDbType.NVarChar).Value = tickets.TicketNo;
                myCommand.Parameters.Add("@FlightSchedual", SqlDbType.Text).Value = tickets.FlightSchedual;
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = tickets.TravellerName;
                myCommand.Parameters.Add("@Price", SqlDbType.Money).Value = tickets.Price;
                myCommand.Parameters.Add("@Cost", SqlDbType.Money).Value = tickets.Cost;
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = tickets.TicketId;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = tickets.CustomerId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = tickets.CurrencyId;
                myCommand.Parameters.Add("@ReferenceNo", SqlDbType.NVarChar).Value = tickets.ReferenceNo;
                myCommand.Parameters.Add("@Profit", SqlDbType.Money).Value = tickets.Profit;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = tickets.CurrencyRate;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal void AddNewTicketReservation(clsTickets tickets)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into Ticket (" +
                            "TicketDate, " +
                            "AirlineId , " +
                            "SupplierId, " +
                            "Remarks, " +
                            "ClassId , " +
                            "FlightNo, " +
                            "Destination, " +
                            "TicketNo, " +
                            "FlightSchedual, " +
                            "Name, " +
                            "ReferenceNo, " +
                            "Profit , " +
                            "CurrencyRate , " +
                            "CustomerId, " +
                            "Cost, " +
                            "CurrencyId, " +
                            "Price " +
                    
                            ") " +
                            "VALUES (" +
                            "@TicketDate, " +
                            "@AirlineId, " +
                            "@SupplierId, " +
                            "@Remarks, " +
                            "@ClassId, " +
                            "@FlightNo, " +
                            "@Destination, " +
                            "@TicketNo, " +
                            "@FlightSchedual, " +
                            "@Name, " +
                            "@ReferenceNo, " +
                            "@Profit , " +
                            "@CurrencyRate , " +
                            "@CustomerId, " +
                            "@Cost, " +
                            "@CurrencyId, " +
                            "@Price " +
                            ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@TicketDate", SqlDbType.Date).Value = tickets.TicketDate;
                myCommand.Parameters.Add("@AirlineId", SqlDbType.Int).Value = tickets.AirLineId;
                myCommand.Parameters.Add("@SupplierId", SqlDbType.Int).Value = tickets.SupplierId;
                myCommand.Parameters.Add("@Remarks", SqlDbType.Text).Value = tickets.Remarks;
                myCommand.Parameters.Add("@ClassId", SqlDbType.Int).Value = tickets.TicketClassId;
                myCommand.Parameters.Add("@FlightNo", SqlDbType.NVarChar).Value = tickets.FlightNo;
                myCommand.Parameters.Add("@Destination", SqlDbType.NVarChar).Value = tickets.Destination;
                myCommand.Parameters.Add("@TicketNo", SqlDbType.NVarChar).Value = tickets.TicketNo;
                myCommand.Parameters.Add("@FlightSchedual", SqlDbType.Text).Value = tickets.FlightSchedual;
                myCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = tickets.TravellerName;
                myCommand.Parameters.Add("@Price", SqlDbType.Money).Value = tickets.Price;
                myCommand.Parameters.Add("@Cost", SqlDbType.Money).Value = tickets.Cost;
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = tickets.TicketId;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = tickets.CustomerId;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = tickets.CurrencyId;
                myCommand.Parameters.Add("@ReferenceNo", SqlDbType.NVarChar).Value = tickets.ReferenceNo;
                myCommand.Parameters.Add("@Profit", SqlDbType.Money).Value = tickets.Profit;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = tickets.CurrencyRate;
                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public bool DeleteTicket(int TicketId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM Ticket WHERE (TicketId = " + TicketId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }

        public List<clsTickets> SupplierTickets(int id)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where SupplierId=" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " " ; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public clsTickets getTicketById(int ticketId)
        {
            clsTickets tickets = new clsTickets();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where TicketId =" + ticketId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                


                if (dr.HasRows)
                {
                    dr.Read();
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
             

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return tickets;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return tickets;
            }
        }

        public List<clsTickets> CustomerTickets(int id)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where CustomerId =" + id;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public List<clsTickets> CustomerTickets(DateTime fromDate,DateTime toDate,int CustomerId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where  CustomerId=@CustomerId and TicketDate between @fromDate and  @toDate " +
                    " Order by TicketDate ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@CustomerId ", SqlDbType.Int).Value = CustomerId;
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public List<clsTickets> SupplierTickets(DateTime fromDate, DateTime toDate, int SupplierId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where  SupplierId=@SupplierId and TicketDate between @fromDate and  @toDate ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@SupplierId ", SqlDbType.Int).Value = SupplierId;
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public decimal TotalPrice(int CustomerId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TI.Price*TI.CurrencyRate) as totalPrice " +
                    "from Ticket as TI  Where TI.CustomerId = " + CustomerId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
           


                if (dr.HasRows)
                {
                    dr.Read();
                    if(dr["totalPrice"]!=DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPrice"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
        public decimal TotalCost(int SupplierId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TI.Cost*TI.CurrencyRate) as totalCost " +
                    "  from Ticket as TI  Where TI.SupplierId =  " + SupplierId;
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalCost"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalCost"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
        public List<clsTickets> TicketsArrayList(DateTime fromDate,DateTime toDate)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket  Where TicketDate between @fromDate and @toDate ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets tickets;


                while (dr.Read())
                {
                    tickets = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { tickets.TicketId = 0; }
                    else
                    { tickets.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { tickets.TicketDate = DefaultDate; }
                    else
                    { tickets.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { tickets.AirLineId = 0; }
                    else
                    { tickets.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { tickets.SupplierId = 0; }
                    else
                    { tickets.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { tickets.Remarks = " "; }
                    else
                    { tickets.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { tickets.TicketClassId = 0; }
                    else
                    { tickets.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { tickets.FlightNo = " "; }
                    else
                    { tickets.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { tickets.TravellerName = " "; }
                    else
                    { tickets.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { tickets.FlightSchedual = " "; }
                    else
                    { tickets.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { tickets.Price = 0; }
                    else
                    { tickets.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { tickets.Cost = 0; }
                    else
                    { tickets.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { tickets.Destination = " "; }
                    else
                    { tickets.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { tickets.TicketNo = " "; }
                    else
                    { tickets.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { tickets.CustomerId = 0; }
                    else
                    { tickets.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        tickets.CurrencyId = 0;
                    }
                    else
                    {
                        tickets.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (dr["ReferenceNo"] == DBNull.Value)
                    { tickets.ReferenceNo = " "; }
                    else
                    { tickets.ReferenceNo = dr["ReferenceNo"].ToString(); }
                    if (dr["Profit"] == DBNull.Value)
                    { tickets.Profit = 0; }
                    else
                    { tickets.Profit = decimal.Parse(dr["Profit"].ToString()); }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { tickets.CurrencyRate = 0; }
                    else
                    { tickets.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }

                    DisplayArray.Add(tickets);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public bool TicketUsed(int TicketId)
        {
            if (!ExistsInReturnedCutomerTicket(TicketId) && !ExistsInReturnedSupplierTicket(TicketId) && ! ExistsInTicketPayment(TicketId))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool ExistsInTicketPayment(int TicketId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT TicketId FROM TicketsPayment WHERE(TicketId = " + TicketId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool ExistsInReturnedCutomerTicket(int TicketId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT TicketId FROM ReturnedCustomerTicket WHERE(TicketId = " + TicketId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public bool ExistsInReturnedSupplierTicket(int TicketId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT TicketId FROM ReturnedSupplierTicket WHERE(TicketId = " + TicketId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }

}
