﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
 
        class TicketInvoice
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        public List<clsTicketInvoice> CustomerInvoicesArrayList(int CustomerId)
        {
            List<clsTicketInvoice> DisplayArray = new List<clsTicketInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketInvoice where ( CustomerId = "+CustomerId+" );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTicketInvoice invoice;


                while (dr.Read())
                {
                    invoice = new clsTicketInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId= 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.InvoiceDate = DefaultDate; }
                    else
                    { invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { invoice.CustomerId= 0; }
                    else
                    { invoice.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["CustomerName"] == DBNull.Value)
                    { invoice.CustomerName= " "; }
                    else
                    { invoice.CustomerName = dr["CustomerName"].ToString(); }
                    if(dr["Discount"]==DBNull.Value)
                    {
                        invoice.Discount = 0;
                    }
                    else
                    {
                        invoice.Discount = decimal.Parse(dr["Discount"].ToString());

                    }
                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else 
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());

                    }

                    
                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public List<clsTickets> TicketsInvoicesArrayList(int InvoiceId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            List<int> ticketsIDS = new List<int>();

            try
            {
                
                    SqlConnection sqlConnection;
                    BuildConnection anewBuildConnection = new BuildConnection();
                    sqlConnection = anewBuildConnection.AddSqlConnection();

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                    string sqlquery =
                     "   SELECT  T.TicketId,T.TicketDate,T.AirlineId,T.SupplierId,T.Remarks,T.ClassId,T.FlightNo,T.Destination,T.TicketNo,T.FlightSchedual,T.Name,T.Price,T.CustomerId,T.Cost,T.CurrencyId " +
                       " FROM TicketInvoiceDetails As TDL Inner Join Ticket as T on T.TicketId = TDL.TicketsId "+
                       " where(TDL.InvoiceId = " + InvoiceId + ");";
                SqlCommand command = new SqlCommand(sqlquery, sqlConnection);
                    SqlDataReader dataReader = command.ExecuteReader();
                    clsTickets ticket ;


               while (dataReader.Read())
                    {
                    ticket = new clsTickets();

                    if (dataReader["TicketId"] == DBNull.Value)
                        { ticket.TicketId = 0; }
                        else
                        { ticket.TicketId = int.Parse(dataReader["TicketId"].ToString()); }

                        if (dataReader["TicketDate"] == DBNull.Value)
                        { ticket.TicketDate = DefaultDate; }
                        else
                        { ticket.TicketDate = DateTime.Parse(dataReader["TicketDate"].ToString()); }

                        if (dataReader["AirlineId"] == DBNull.Value)
                        { ticket.AirLineId = 0; }
                        else
                        { ticket.AirLineId = int.Parse(dataReader["AirlineId"].ToString()); }

                        if (dataReader["SupplierId"] == DBNull.Value)
                        { ticket.SupplierId = 0; }
                        else
                        { ticket.SupplierId = int.Parse(dataReader["SupplierId"].ToString()); }

                        if (dataReader["Remarks"] == DBNull.Value)
                        { ticket.Remarks = " "; }
                        else
                        { ticket.Remarks = dataReader["Remarks"].ToString(); }

                        if (dataReader["ClassId"] == DBNull.Value)
                        { ticket.TicketClassId = 0; }
                        else
                        { ticket.TicketClassId = int.Parse(dataReader["ClassId"].ToString()); }

                        if (dataReader["FlightNo"] == DBNull.Value)
                        { ticket.FlightNo = " "; }
                        else
                        { ticket.FlightNo = dataReader["FlightNo"].ToString(); }
                        if (dataReader["Name"] == DBNull.Value)
                        { ticket.TravellerName = " "; }
                        else
                        { ticket.TravellerName = dataReader["Name"].ToString(); }
                        if (dataReader["FlightSchedual"] == DBNull.Value)
                        { ticket.FlightSchedual = " "; }
                        else
                        { ticket.FlightSchedual = dataReader["FlightSchedual"].ToString(); }
                        if (dataReader["Price"] == DBNull.Value)
                        { ticket.Price = 0; }
                        else
                        { ticket.Price = decimal.Parse(dataReader["Price"].ToString()); }
                        if (dataReader["Cost"] == DBNull.Value)
                        { ticket.Cost = 0; }
                        else
                        { ticket.Cost = decimal.Parse(dataReader["Cost"].ToString()); }
                        if (dataReader["Destination"] == DBNull.Value)
                        { ticket.Destination = " "; }
                        else
                        { ticket.Destination = dataReader["Destination"].ToString(); }
                        if (dataReader["TicketNo"] == DBNull.Value)
                        { ticket.TicketNo = " "; }
                        else
                        { ticket.TicketNo = dataReader["TicketNo"].ToString(); }

                        if (dataReader["CustomerId"] == DBNull.Value)
                        { ticket.CustomerId = 0; }
                        else
                        { ticket.CustomerId = int.Parse(dataReader["CustomerId"].ToString()); }
                        if (dataReader["CurrencyId"] == DBNull.Value)
                        {
                            ticket.CurrencyId = 0;
                        }
                        else
                        {
                            ticket.CurrencyId = int.Parse(dataReader["CurrencyId"].ToString());
                        }
                        DisplayArray.Add(ticket);

                    }

                dataReader.Close();
                command.Dispose();
                sqlConnection.Close();
                sqlConnection.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

       

        public List<clsTickets> CustomerTicketsArrayList(int CustomerId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where(CustomerId="+CustomerId+ ") and TicketId not in(select TicketsId from TicketInvoiceDetails) ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets ticket;


                while (dr.Read())
                {
                    ticket = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = " "; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = " "; }
                    else
                    { ticket.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        ticket.CurrencyId = 0;
                    }
                    else
                    {
                        ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    DisplayArray.Add(ticket);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public List<clsTickets> AllCustomerTicketsArrayList(int CustomerId)
        {
            List<clsTickets> DisplayArray = new List<clsTickets>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Ticket where(CustomerId=" + CustomerId + ") ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsTickets ticket;


                while (dr.Read())
                {
                    ticket = new clsTickets();
                    if (dr["TicketId"] == DBNull.Value)
                    { ticket.TicketId = 0; }
                    else
                    { ticket.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["TicketDate"] == DBNull.Value)
                    { ticket.TicketDate = DefaultDate; }
                    else
                    { ticket.TicketDate = DateTime.Parse(dr["TicketDate"].ToString()); }

                    if (dr["AirlineId"] == DBNull.Value)
                    { ticket.AirLineId = 0; }
                    else
                    { ticket.AirLineId = int.Parse(dr["AirlineId"].ToString()); }

                    if (dr["SupplierId"] == DBNull.Value)
                    { ticket.SupplierId = 0; }
                    else
                    { ticket.SupplierId = int.Parse(dr["SupplierId"].ToString()); }

                    if (dr["Remarks"] == DBNull.Value)
                    { ticket.Remarks = " "; }
                    else
                    { ticket.Remarks = dr["Remarks"].ToString(); }

                    if (dr["ClassId"] == DBNull.Value)
                    { ticket.TicketClassId = 0; }
                    else
                    { ticket.TicketClassId = int.Parse(dr["ClassId"].ToString()); }

                    if (dr["FlightNo"] == DBNull.Value)
                    { ticket.FlightNo = " "; }
                    else
                    { ticket.FlightNo = dr["FlightNo"].ToString(); }
                    if (dr["Name"] == DBNull.Value)
                    { ticket.TravellerName = " "; }
                    else
                    { ticket.TravellerName = dr["Name"].ToString(); }
                    if (dr["FlightSchedual"] == DBNull.Value)
                    { ticket.FlightSchedual = " "; }
                    else
                    { ticket.FlightSchedual = dr["FlightSchedual"].ToString(); }
                    if (dr["Price"] == DBNull.Value)
                    { ticket.Price = 0; }
                    else
                    { ticket.Price = decimal.Parse(dr["Price"].ToString()); }
                    if (dr["Cost"] == DBNull.Value)
                    { ticket.Cost = 0; }
                    else
                    { ticket.Cost = decimal.Parse(dr["Cost"].ToString()); }
                    if (dr["Destination"] == DBNull.Value)
                    { ticket.Destination = " "; }
                    else
                    { ticket.Destination = dr["Destination"].ToString(); }
                    if (dr["TicketNo"] == DBNull.Value)
                    { ticket.TicketNo = " "; }
                    else
                    { ticket.TicketNo = dr["TicketNo"].ToString(); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { ticket.CustomerId = 0; }
                    else
                    { ticket.CustomerId = int.Parse(dr["CustomerId"].ToString()); }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        ticket.CurrencyId = 0;
                    }
                    else
                    {
                        ticket.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    DisplayArray.Add(ticket);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }



        internal bool InvoiceIDExists(int InvoiceId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT InvoiceId FROM TicketInvoice WHERE(InvoiceId = " + InvoiceId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        internal void UpdateInvoiceDetails(int invoiceId, List<clsTickets> tickets)
        {
            try
            {
                if ( DeleteInvoiceTickets(invoiceId))
                {

                  AddInvoiceTickets(invoiceId, tickets);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        public void AddInvoiceTickets(int invoiceId, List<clsTickets> tickets)
        {
            for (int i = 0; i <tickets.Count; i++)
            {
                AddInvoiceDetails(invoiceId, tickets[i].TicketId);
            }

        }

        public void AddInvoiceDetails(int invoicId,int TicketId)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into TicketInvoiceDetails (" +
                        "InvoiceId, " +
                        "TicketsId  " +

                        ") " +
                      "VALUES (" +
                        "@InvoiceId, " +
                        "@TicketsId " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoicId;
                myCommand.Parameters.Add("@TicketsId", SqlDbType.Int).Value = TicketId;


                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeleteInvoiceTickets(int invoiceId)
        {
            int cmdResult;
            bool myResult = false ;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from TicketInvoiceDetails where InvoiceId IN (select InvoiceId from TicketInvoiceDetails where InvoiceId = " +invoiceId + ")";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();
              
                 myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        internal void UpdateInvoiceInfo(clsTicketInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update TicketInvoice set " +
                        "CustomerId = @CustomerId, " +
                        "NetPrice=@NetPrice,"+
                        "Discount=@Discount,"+
                        "Date = @Date, " +
                        "CustomerName = @CustomerName " +

                        "WHERE (InvoiceId  = @InvoiceId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                myCommand.Parameters.Add("@Discount", SqlDbType.Decimal).Value = invoice.Discount;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Decimal).Value = invoice.NetPrice;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value =invoice.InvoiceDate;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value =invoice.CustomerId;
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoice.InvoiceId;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = invoice.CustomerName;

                

                 cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        internal void AddNewTicketInvoice(clsTicketInvoice invoice)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "insert into TicketInvoice (" +
                        "CustomerId, " +
                        "NetPrice,"+
                        "Discount,"+
                        "Date , " +
                        "CustomerName" +
                        ") " +
                      "VALUES (" +
                        "@CustomerId, " +
                        "@NetPrice,"+
                        "@Discount," +
                        "@Date, " +
                        "@CustomerName " +
              
                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = invoice.InvoiceDate;
                myCommand.Parameters.Add("@Discount", SqlDbType.Decimal).Value = invoice.Discount;
                myCommand.Parameters.Add("@NetPrice", SqlDbType.Decimal).Value = invoice.NetPrice;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = invoice.CustomerId;
                myCommand.Parameters.Add("@InvoiceId", SqlDbType.Int).Value = invoice.InvoiceId;
                myCommand.Parameters.Add("@CustomerName", SqlDbType.NVarChar).Value = invoice.CustomerName;
          

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        public bool DeleteInVoice(int invoiceId)
        {
            int cmdResult;
            bool myResult;
            try
            {
                myResult = true;
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "DELETE FROM TicketInvoice WHERE (InvoiceId = " + invoiceId + ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
        internal bool   TicketInvoiced(int ticketId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT TicketsId FROM TicketInvoiceDetails WHERE(TicketsId = " + ticketId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(InvoiceId) FROM TicketInvoice;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }






                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public List<clsTicketInvoice> CustomerInvoices(DateTime fromDate,DateTime toDate,int CustomerId)
        {
            List<clsTicketInvoice> DisplayArray = new List<clsTicketInvoice>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketInvoice as TI where ( TI.CustomerId = @CustomerId and TI.Date between @fromDate and @toDate );";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();
                clsTicketInvoice invoice;


                while (dr.Read())
                {
                    invoice = new clsTicketInvoice();
                    if (dr["InvoiceId"] == DBNull.Value)
                    { invoice.InvoiceId = 0; }
                    else
                    { invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { invoice.InvoiceDate = DefaultDate; }
                    else
                    { invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString()); }

                    if (dr["CustomerId"] == DBNull.Value)
                    { invoice.CustomerId = 0; }
                    else
                    { invoice.CustomerId = int.Parse(dr["CustomerId"].ToString()); }

                    if (dr["CustomerName"] == DBNull.Value)
                    { invoice.CustomerName = " "; }
                    else
                    { invoice.CustomerName = dr["CustomerName"].ToString(); }
                    if (dr["Discount"] == DBNull.Value)
                    {
                        invoice.Discount = 0;
                    }
                    else
                    {
                        invoice.Discount = decimal.Parse(dr["Discount"].ToString());

                    }
                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());

                    }


                    DisplayArray.Add(invoice);

                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
