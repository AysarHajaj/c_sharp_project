﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class TicketsPayment
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        
   public List<clsTicketsPayment> TicketsPayments(int ticketId)
        {
            List<clsTicketsPayment> DisplayArray = new List<clsTicketsPayment>();
          

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketsPayment where(TicketId =" + ticketId + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();

             
                clsTicketsPayment payments;
                while (dr.Read())
                {
               
                    payments = new clsTicketsPayment();


                    if (dr["TicketId"] == DBNull.Value)
                    { payments.TicketId = 0; }
                    else
                    { payments.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }
                    

                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = decimal.Parse(dr["Payment"].ToString()); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"]==DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType==1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate =DateTime.Parse( dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate =DateTime.Parse( dr["CollectedDate"].ToString()).ToShortDateString(); }
                        

                        
                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        payments.CustomerId = 0;
                    }
                    else
                    {
                        payments.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    DisplayArray.Add(payments);

                   

                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }



    

        internal bool paymentIDExists(int paymentId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT PaymentId FROM TicketsPayment WHERE(PaymentId = " + paymentId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
       

        public void AddPaymentDetails(clsTicketsPayment payment)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "insert into TicketsPayment (" +
                        "TicketId, " +
                         "Date, " +
                          "Payment, " +
                           "ChequeNo, " +
                            "BankId, " +
                             "ChequeDate, " +
                              "CollectedDate, " +
                                "CurrencyId, "+
                                 "CurrencyRate, "+
                                   "CustomerId, " +
                                   "PaymentType  " +

                        ") " +
                      "VALUES (" +
                        "@TicketId, " +
                          "@Date, " +
                            "@Payment, " +
                              "@ChequeNo, " +
                                "@BankId, " +
                                  "@ChequeDate, " +
                                    "@CollectedDate, " +
                                      "@CurrencyId, " +
                                         "@CurrencyRate, " +
                                          "@CustomerId, " +
                                            "@PaymentType " +

                        ");";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                SqlDateTime chequeDate= SqlDateTime.Null;
                SqlDateTime collecteddate=SqlDateTime.Null;
                if(payment.ChequeDate==null)
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = chequeDate;
                }
                else
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.ChequeDate);
                }
                if (payment.CollectedDate == null)
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = collecteddate;
                }
                else
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.CollectedDate);
                }
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = payment.TicketId;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = payment.Date;
                myCommand.Parameters.Add("@Payment", SqlDbType.Money).Value = payment.Payment;
                myCommand.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = payment.ChequeNo;
                myCommand.Parameters.Add("@BankId", SqlDbType.Int).Value = payment.BankId;
                myCommand.Parameters.Add("@PaymentType", SqlDbType.Int).Value = payment.PaymentType;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = payment.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = payment.CurrencyRate;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = payment.CustomerId;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void UpdatePaymentDetails(clsTicketsPayment payment)
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }



                string sqlQuery = "update  TicketsPayment set " +
                        "TicketId=@TicketId, " +
                         "CurrencyRate=@CurrencyRate, " +
                         "Date=@Date, " +
                          "Payment=@Payment, " +
                           "ChequeNo=@ChequeNo, " +
                            "BankId=@BankId, " +
                             "ChequeDate=@ChequeDate, " +
                              "CollectedDate=@CollectedDate, " +
                              "CurrencyId=@CurrencyId, "+
                              "CustomerId=@CustomerId, " +
                                "PaymentType=@PaymentType  " +

                        "where( PaymentId=@PaymentId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);
                SqlDateTime chequeDate = SqlDateTime.Null;
                SqlDateTime collecteddate = SqlDateTime.Null;
                if (payment.ChequeDate == null)
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = chequeDate;
                }
                else
                {
                    myCommand.Parameters.Add("@ChequeDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.ChequeDate);
                }
                if (payment.CollectedDate == null)
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = collecteddate;
                }
                else
                {
                    myCommand.Parameters.Add("@CollectedDate", SqlDbType.Date).Value = Convert.ToDateTime(payment.CollectedDate);
                }
                myCommand.Parameters.Add("@TicketId", SqlDbType.Int).Value = payment.TicketId;
                myCommand.Parameters.Add("@Date", SqlDbType.Date).Value = payment.Date;
                myCommand.Parameters.Add("@Payment", SqlDbType.Money).Value = payment.Payment;
                myCommand.Parameters.Add("@ChequeNo", SqlDbType.NVarChar).Value = payment.ChequeNo;
                myCommand.Parameters.Add("@BankId", SqlDbType.Int).Value = payment.BankId;
                myCommand.Parameters.Add("@PaymentType", SqlDbType.Int).Value = payment.PaymentType;
                myCommand.Parameters.Add("@CurrencyId", SqlDbType.Int).Value = payment.CurrencyId;
                myCommand.Parameters.Add("@CurrencyRate", SqlDbType.Money).Value = payment.CurrencyRate;
                myCommand.Parameters.Add("@PaymentId", SqlDbType.Int).Value = payment.PaymentId;
                myCommand.Parameters.Add("@CustomerId", SqlDbType.Int).Value = payment.CustomerId;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool DeletePayment(int paymentId)
        {
            int cmdResult;
            bool myResult = false;
            try
            {

                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "Delete from TicketsPayment where PaymentId = " + paymentId ;

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();

                myResult = true;
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return myResult;
            }
            catch (Exception ex)
            {
                myResult = false;
                MessageBox.Show(ex.Message);
                return myResult;
            }
        }
      
     


       
       
        public int ReadLastNo()
        {
            int maxId;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "SELECT MAX(PaymentId) FROM TicketsPayment;";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                object result = myCommand.ExecuteScalar();
                maxId = 0;
                if (result != null)
                {
                    maxId = Convert.ToInt32(myCommand.ExecuteScalar());
                }






                myTrans.Commit();

                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return maxId;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }
        }

        public List<clsTicketsPayment> CustomerPaymment(int CustomerId)
        {
            List<clsTicketsPayment> DisplayArray = new List<clsTicketsPayment>();


            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketsPayment where(CustomerId =" + CustomerId + ");";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();


                clsTicketsPayment payments;
                while (dr.Read())
                {

                    payments = new clsTicketsPayment();


                    if (dr["TicketId"] == DBNull.Value)
                    { payments.TicketId = 0; }
                    else
                    { payments.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = decimal.Parse(dr["Payment"].ToString()); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType == 1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate = DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString(); }



                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        payments.CustomerId = 0;
                    }
                    else
                    {
                        payments.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    DisplayArray.Add(payments);



                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public List<clsTicketsPayment> CustomerPaymment(int CustomerId,DateTime fromDate,DateTime toDate)
        {
            List<clsTicketsPayment> DisplayArray = new List<clsTicketsPayment>();


            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketsPayment where(CustomerId =@CustomerId and  Date between @fromDate and @toDate) " +
                    " Order by Date ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();


                clsTicketsPayment payments;
                while (dr.Read())
                {

                    payments = new clsTicketsPayment();


                    if (dr["TicketId"] == DBNull.Value)
                    { payments.TicketId = 0; }
                    else
                    { payments.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = decimal.Parse(dr["Payment"].ToString()); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType == 1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate = DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString(); }



                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        payments.CustomerId = 0;
                    }
                    else
                    {
                        payments.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    DisplayArray.Add(payments);



                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        public decimal TotalPaid(int CustomerId, DateTime fromDate, DateTime toDate)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TP.Payment*TP.CurrencyRate) as totalPaid " +
                    "from TicketsPayment AS TP where(TP.CustomerId =@CustomerId and  TP.Date >@fromDate and  TP.Date<@toDate) ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPaid"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPaid"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }
        public decimal TotalPaid(int CustomerId)
        {
            decimal totalPrice = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select Sum(TP.Payment*TP.CurrencyRate) as totalPaid " +
                    "from TicketsPayment AS TP where(TP.CustomerId =@CustomerId ) ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@CustomerId", SqlDbType.Int).Value = CustomerId;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();
                    if (dr["totalPaid"] != DBNull.Value)
                    {
                        totalPrice = decimal.Parse(dr["totalPaid"].ToString());
                    }
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return totalPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return totalPrice;
            }
        }

        public List<clsTicketsPayment> CustomerPaymment( DateTime fromDate, DateTime toDate)
        {
            List<clsTicketsPayment> DisplayArray = new List<clsTicketsPayment>();


            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM TicketsPayment where(Date between @fromDate and @toDate) " +
                    " Order by Date ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = fromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                SqlDataReader dr = cmd.ExecuteReader();


                clsTicketsPayment payments;
                while (dr.Read())
                {

                    payments = new clsTicketsPayment();


                    if (dr["TicketId"] == DBNull.Value)
                    { payments.TicketId = 0; }
                    else
                    { payments.TicketId = int.Parse(dr["TicketId"].ToString()); }

                    if (dr["Date"] == DBNull.Value)
                    { payments.Date = DefaultDate; }
                    else
                    { payments.Date = DateTime.Parse(dr["Date"].ToString()); }


                    if (dr["Payment"] == DBNull.Value)
                    { payments.Payment = 0; }
                    else
                    { payments.Payment = decimal.Parse(dr["Payment"].ToString()); }

                    if (dr["PaymentId"] == DBNull.Value)
                    { payments.PaymentId = 0; }
                    else
                    { payments.PaymentId = int.Parse(dr["PaymentId"].ToString()); }

                    if (dr["PaymentType"] == DBNull.Value)
                    {
                        payments.PaymentType = 0;
                    }
                    else
                    {
                        payments.PaymentType = int.Parse(dr["PaymentType"].ToString());
                    }
                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        payments.CurrencyId = 0;
                    }
                    else
                    {
                        payments.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    if (payments.PaymentType == 1)
                    {
                        if (dr["ChequeNo"] == DBNull.Value)
                        { payments.ChequeNo = " "; }
                        else
                        { payments.ChequeNo = dr["ChequeNo"].ToString(); }
                        if (dr["BankId"] == DBNull.Value)
                        { payments.BankId = 0; }
                        else
                        { payments.BankId = int.Parse(dr["BankId"].ToString()); }
                        if (dr["ChequeDate"] == DBNull.Value)
                        { payments.ChequeDate = ""; }
                        else
                        { payments.ChequeDate = DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString(); }
                        if (dr["CollectedDate"] == DBNull.Value)
                        { payments.CollectedDate = ""; }
                        else
                        { payments.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString(); }



                    }
                    if (dr["CurrencyRate"] == DBNull.Value)
                    { payments.CurrencyRate = 0; }
                    else
                    { payments.CurrencyRate = decimal.Parse(dr["CurrencyRate"].ToString()); }
                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        payments.CustomerId = 0;
                    }
                    else
                    {
                        payments.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    DisplayArray.Add(payments);



                }
                dr.Close();
                cmd.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
