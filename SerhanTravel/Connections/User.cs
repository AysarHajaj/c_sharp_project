﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Connections
{
    class User
    {
        public  List<clsUser> UsersArrayList()
        {
            List<clsUser> DisplayArray;
            DisplayArray = new List<clsUser>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Users;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsUser user;

                while (dr.Read())
                {
                    user = new clsUser();
                    if (dr["UserId"] == DBNull.Value)
                    { user.UserId = 0; }
                    else
                    { user.UserId = int.Parse(dr["UserId"].ToString()); }

                    if (dr["UserName"] == DBNull.Value)
                    { user.UserName = " "; }
                    else
                    { user.UserName = dr["UserName"].ToString(); }

                    if (dr["RoleId"] == DBNull.Value)
                    { user.RoleId = 0; }
                    else
                    { user.RoleId = int.Parse(dr["RoleId"].ToString()); }

                    if (dr["Password"] == DBNull.Value)
                    { user.Password = " "; }
                    else
                    { user.Password = dr["Password"].ToString(); }

                    if (dr["Name"] == DBNull.Value)
                    { user.Name = " "; }
                    else
                    { user.Name = dr["Name"].ToString(); }


                    DisplayArray.Add(user);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }

        public  bool UserIDExists(int UserId)
        {
            Boolean IdExists = false;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                SqlCommand myCommand = new SqlCommand("SELECT UserId FROM Users WHERE(UserId = " + UserId + ");", sqlCon);
                SqlDataReader dr = myCommand.ExecuteReader();
                if (dr.HasRows)
                {
                    IdExists = true;
                }
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                dr.Close();
                return IdExists;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public  void UpdateUser(clsUser user)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "UPDATE Users SET " +
                "UserName=@UserName, " +
                "Password=@Password, " +
                "RoleId=@RoleId, " +
                "Name=@Name " +
                "WHERE (UserId=@UserId);";

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = user.UserName;
                command.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.Password;
                command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = user.Name;
                command.Parameters.Add("@RoleId", SqlDbType.Int).Value = user.RoleId;
                command.Parameters.Add("@UserId", SqlDbType.Int).Value = user.UserId;

                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public  void AddUser(clsUser user)
        {
            try
            {
                SqlConnection con;
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "INSERT INTO Users (" +
                    "UserName, " +
                    "Password, " +
                    "RoleId, " +
                    "Name) " +
                    "VALUES (" +
                    "@UserName, " +
                    "@Password, " +
                    "@RoleId, " +
                    "@Name);";
                

                SqlTransaction transaciont = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaciont);

                command.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = user.UserName;
                command.Parameters.Add("@Password", SqlDbType.NVarChar).Value = user.Password;
                command.Parameters.Add("@Name", SqlDbType.NVarChar).Value = user.Name;
                command.Parameters.Add("@RoleId", SqlDbType.Int).Value = user.RoleId;
                int result = command.ExecuteNonQuery();
                transaciont.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaciont.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public  bool DeleteUser(int userID)
        {
            int cmdResult;
            bool result;
            try
            {
                SqlConnection con = new SqlConnection();
                BuildConnection build = new BuildConnection();
                con = build.AddSqlConnection();
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }

                string sql = "DELETE FROM Users WHERE (UserId = " + userID + ");";
                SqlTransaction transaction = con.BeginTransaction();
                SqlCommand command = new SqlCommand(sql, con, transaction);
                cmdResult = command.ExecuteNonQuery();
                result = true;
                transaction.Commit();
                con.Close();
                con.Dispose();
                command.Dispose();
                transaction.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                result = false;
                return result;
            }
        }

        public  List<string> UserNameArrayList()
        {
            List<string> DisplayArray;
            DisplayArray = new List<string>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT UserName FROM Users;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                string userName;

                while (dr.Read())
                {
                    

                    if (dr["UserName"] == DBNull.Value)
                    { userName = " "; }
                    else
                    { userName = dr["UserName"].ToString(); }

                    


                    DisplayArray.Add(userName);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
        public  List<clsUser> EmployeeUsersArrayList()
        {
            List<clsUser> DisplayArray;
            DisplayArray = new List<clsUser>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "SELECT * FROM Users where RoleId=2;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                clsUser user;

                while (dr.Read())
                {
                    user = new clsUser();
                    if (dr["UserId"] == DBNull.Value)
                    { user.UserId = 0; }
                    else
                    { user.UserId = int.Parse(dr["UserId"].ToString()); }

                    if (dr["UserName"] == DBNull.Value)
                    { user.UserName = " "; }
                    else
                    { user.UserName = dr["UserName"].ToString(); }

                    if (dr["RoleId"] == DBNull.Value)
                    { user.RoleId = 0; }
                    else
                    { user.RoleId = int.Parse(dr["RoleId"].ToString()); }

                    if (dr["Password"] == DBNull.Value)
                    { user.Password = " "; }
                    else
                    { user.Password = dr["Password"].ToString(); }

                    if (dr["Name"] == DBNull.Value)
                    { user.Name = " "; }
                    else
                    { user.Name = dr["Name"].ToString(); }


                    DisplayArray.Add(user);
                }
                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }
    }
}
