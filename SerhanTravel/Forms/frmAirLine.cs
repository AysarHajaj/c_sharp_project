﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmAirLine : Form
    {
        private int SavedID;
        int AirLineId = 0;
        ArrayList airLineList;
        bool iamAdding = false;
        bool DataChange = false;
        clsAirLine airLineData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        AirLines airLinConnection;
        internal frmAirLine(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmAirLine_Load(object sender, EventArgs e)
        {
            accessRight();
            airLinConnection = new AirLines();
            airLineList = airLinConnection.AirlineArrayList();
            cmbAirLine.ValueMember = "airlineId";
            cmbAirLine.DataSource = airLineList;
            cmbAirLine.DisplayMember = "airLineName";
            ResetFeilds();
            AirLineId = 0;
            DataChange = false;
        }

        public void Display(int airLineId)
        {
            iamAdding = false;
            cmbAirLine.DataSource = null;
            cmbAirLine.ValueMember = "airlineId";
            cmbAirLine.DataSource = airLineList;
            cmbAirLine.DisplayMember = "airLineName";
            cmbAirLine.SelectedValue = airLineId;
            this.AirLineId = airLineId;
            DataChange = false;


        }

        public void ResetFeilds()
        {
            cmbAirLine.SelectedIndex = -1;
            stsLblInfo.Text = "";
        }

        private void cmbAirLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAirLine.SelectedIndex != -1 && airLineList.Count > 0)
            {
                this.AirLineId = int.Parse(cmbAirLine.SelectedValue.ToString());
                airLineData =(clsAirLine) airLineList[cmbAirLine.SelectedIndex];
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsAirLine airLine = new clsAirLine();
                airLine.AirLineId = this.AirLineId;

                if (!string.IsNullOrWhiteSpace(cmbAirLine.Text))
                {
                    airLine.AirLineName = cmbAirLine.Text;
                }
                else
                {
                    airLine.AirLineName = string.Empty;
                }

                IdExists = airLinConnection.AirLineIDExists(airLine.AirLineId);

                if (IdExists)
                {
                    airLinConnection.UpdateAirLine(airLine);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    airLinConnection.AddAirLine(airLine);
                    stsLblInfo.Text = "New AirLine has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                airLineList = airLinConnection.AirlineArrayList();
                if(iamAdding&& airLineList.Count>0)
                {
                    clsAirLine airLines = (clsAirLine)airLineList[airLineList.Count - 1];
                    this.AirLineId = airLines.AirLineId;
                }
                Display(this.AirLineId);
                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }


     
        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add AirLine";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = this.AirLineId;
            this.AirLineId = 0;
            ResetFeilds();
            cmbAirLine.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if(canUpdateOnly && AirLineId==0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.AirLineId = SavedID;
            Display(this.AirLineId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.AirLineId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete AirLine", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (airLinConnection.AirLineUsed(AirLineId))
                        {
                            MessageBox.Show("This AirLine Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = airLinConnection.DeleteAirLine(this.AirLineId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        airLineList = airLinConnection.AirlineArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

      
        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbAirLine.Items.Count > 0)
            {
                if (cmbAirLine.SelectedIndex == 0 || cmbAirLine.SelectedIndex == -1)
                {
                    int Cities = cmbAirLine.Items.Count;
                    cmbAirLine.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbAirLine.SelectedIndex = cmbAirLine.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbAirLine.Items.Count > 0)
            {
                if (cmbAirLine.SelectedIndex == cmbAirLine.Items.Count - 1 || cmbAirLine.SelectedIndex == -1)
                {

                    cmbAirLine.SelectedIndex = 0;
                }
                else
                {
                    cmbAirLine.SelectedIndex = cmbAirLine.SelectedIndex + 1;
                }
            }
        }

        private void cmbAirLine_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if(!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbAirLine.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void unReadOnlyEveryThing()
        {
            cmbAirLine.DropDownStyle = ComboBoxStyle.DropDown;
        }
    }
}
