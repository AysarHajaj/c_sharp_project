﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmBanks : Form
    {
        int BankId = 0;
        List<clsBank> bankList;
        bool iamAdding = false;
        bool DataChange = false;
        clsBank bankData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        private int SavedID;
        Bank bankConnection;
        internal frmBanks(clsAccessRights right)
        {
            InitializeComponent();
            this.right=right;
        }

        private void frmBanks_Load(object sender, EventArgs e)
        {
            bankConnection = new Bank();
            bankList = bankConnection.BankArrayList();
            accessRight();
            cmbBank.ValueMember = "bankId";
            cmbBank.DataSource = bankList;
            cmbBank.DisplayMember = "bankName";
            ResetFeilds();
            BankId = 0;
            DataChange = false;

        }

        public void Display(int bankId)
        {
            iamAdding = false;
            cmbBank.DataSource = null;
            cmbBank.ValueMember = "bankId";
            cmbBank.DataSource = bankList;
            cmbBank.DisplayMember = "bankName";
            cmbBank.SelectedValue = bankId;
            this.BankId = bankId;
            DataChange = false;


        }

        public void ResetFeilds()
        {
            cmbBank.SelectedIndex = -1;
            stsLblInfo.Text = "";
        }

        private void cmbAirLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBank.SelectedIndex != -1 && bankList.Count > 0)
            {
                this.BankId = int.Parse(cmbBank.SelectedValue.ToString());
                bankData = bankList[cmbBank.SelectedIndex];
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsBank bank = new clsBank();
                bank.BankId = this.BankId;

                if (!string.IsNullOrWhiteSpace(cmbBank.Text))
                {
                    bank.BankName = cmbBank.Text;
                }
                else
                {
                    bank.BankName = string.Empty;
                }

                IdExists = bankConnection.BankIDExists(bank.BankId);

                if (IdExists)
                {
                    bankConnection.UpdateBank(bank);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    bankConnection. AddBank(bank);
                    stsLblInfo.Text = "New AirLine has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                bankList = bankConnection.BankArrayList();
                if (iamAdding && bankList.Count > 0)
                {
                    clsBank banks = (clsBank)bankList[bankList.Count - 1];
                    this.BankId = banks.BankId;
                }
                Display(this.BankId);
                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }


        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Bank";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = BankId;
            this.BankId = 0;
            ResetFeilds();
            cmbBank.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && BankId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.BankId = SavedID;
            Display(this.BankId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.BankId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Bank", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (bankConnection.BankUsed(BankId))
                        {
                            MessageBox.Show("This Bank Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult =bankConnection.DeleteBank(this.BankId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        bankList = bankConnection.BankArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

     
        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbBank.Items.Count > 0)
            {
                if (cmbBank.SelectedIndex == 0 || cmbBank.SelectedIndex == -1)
                {
                    int Cities = cmbBank.Items.Count;
                    cmbBank.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbBank.SelectedIndex = cmbBank.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbBank.Items.Count > 0)
            {
                if (cmbBank.SelectedIndex == cmbBank.Items.Count - 1 || cmbBank.SelectedIndex == -1)
                {

                    cmbBank.SelectedIndex = 0;
                }
                else
                {
                    cmbBank.SelectedIndex = cmbBank.SelectedIndex + 1;
                }
            }
        }

        private void cmbAirLine_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbBank.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void unReadOnlyEveryThing()
        {
            cmbBank.DropDownStyle = ComboBoxStyle.DropDown;
        }
    }
}
