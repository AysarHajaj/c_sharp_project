﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SerhanTravel.Objects;
using System.Collections;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmCity : Form
    {
        int CityID = 0;
        int SavedID;
        List<clsCity> CityList;
        bool iamAdding=false;
        bool DataChange=false;
        clsCity CityData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        Country countryConnection;
        City cityConnection;
        internal frmCity(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;

        }

        private void frmCity_Load(object sender, EventArgs e)
        {
            accessRight();
            cityConnection = new City();
            countryConnection = new Country();
            CityList = cityConnection.CitiesArrayList();
            cbCityID.ValueMember = "cityId";
            cbCityID.DataSource = CityList;
            cbCityID.DisplayMember = "englishName";
            fillComboBox();
            ResetFeilds();
            this.CityID = 0;
            DataChange = false;
            
        }

        public void fillComboBox()
        {
            List<clsCountry> CountryList = countryConnection.CountryArrayList();
            cbCountry.ValueMember = "CountryId";
            cbCountry.DataSource = CountryList;
            cbCountry.DisplayMember = "EnglishName";
            cbCountry.SelectedIndex = -1;
        }

        public void Display(int ctId)
        {
            iamAdding = false;
            cbCityID.DataSource = null;
            cbCityID.ValueMember = "cityId";
            cbCityID.DataSource = CityList;
            cbCityID.DisplayMember = "englishName";
            cbCityID.SelectedValue = ctId;
            DataChange = false;
            

        }

        public void ResetFeilds()
        {
            cbCityID.SelectedIndex = -1;
            cbCountry.SelectedIndex = -1;
            tvArabicName.Text = "";
            cbCityID.Text = "";
            stsLblInfo.Text = "";
            DataChange = false;
        }

        private void cbCityID_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cbCityID.SelectedIndex != -1 && CityList.Count>0)
            {
                this.CityID = int.Parse(cbCityID.SelectedValue.ToString());
                CityData = CityList[cbCityID.SelectedIndex];
                cbCountry.SelectedValue = CityData.CountryId;
                tvArabicName.Text = CityData.ArabicName;
               
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsCity city = new clsCity();
                city.CityId = this.CityID;

                if(!string.IsNullOrWhiteSpace(cbCityID.Text))
                {
                    city.EnglishName = cbCityID.Text;
                }
                else
                {
                    city.EnglishName = string.Empty;
                }
                if (cbCityID.SelectedIndex==-1)
                {
                    city.CityId = 0;
                }
                else
                {
                    city.CityId = int.Parse(cbCityID.SelectedValue.ToString());
                }

                if (cbCountry.SelectedIndex==-1)
                {
                    city.CountryId = 0;
                }
                else
                {
                    city.CountryId = int.Parse(cbCountry.SelectedValue.ToString());
                }

                if (string.IsNullOrWhiteSpace(tvArabicName.Text))
                {
                    city.ArabicName = "";
                }
                else
                {
                    city.ArabicName = tvArabicName.Text;
                }

                

                IdExists = cityConnection.CityIDExists(city.CityId);

                if (IdExists)
                {
                    cityConnection. UpdateCity(city);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    cityConnection.AddCity(city);
                    stsLblInfo.Text = "New city has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                CityList = cityConnection.CitiesArrayList();
                if (iamAdding && CityList.Count > 0)
                {
                    clsCity cit = (clsCity)CityList[CityList.Count - 1];
                    this.CityID = cit.CityId;
                }
                Display(this.CityID);
                DataChange = false;

            }
            catch(Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }

       

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add City";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = this.CityID;
            this.CityID = 0;
            ResetFeilds();
            cbCityID.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void cbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void tvArabicName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void tvEnglishName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.CityID == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.CityID = SavedID;
            Display(this.CityID);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.CityID != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete City", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    
                    if (myReplay == DialogResult.Yes)
                    {
                        if (cityConnection.CityUsed(CityID))
                        {
                            MessageBox.Show("This City Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = cityConnection.DeleteCity(this.CityID);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        CityList = cityConnection.CitiesArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }
                    
                    
                }
                catch(Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }



        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cbCityID.Items.Count > 0)
            {
                if (cbCityID.SelectedIndex == 0 || cbCityID.SelectedIndex == -1)
                {
                    int Cities = cbCityID.Items.Count;
                    cbCityID.SelectedIndex = Cities - 1;
                }
                else
                {
                    cbCityID.SelectedIndex = cbCityID.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cbCityID.Items.Count > 0)
            {
                if (cbCityID.SelectedIndex == cbCityID.Items.Count - 1 || cbCityID.SelectedIndex == -1)
                {

                    cbCityID.SelectedIndex = 0;
                }
                else
                {
                    cbCityID.SelectedIndex = cbCityID.SelectedIndex + 1;
                }
            }
        }

        

     
        private void cbCountry_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cbCountry.SelectedIndex != -1)
            {
                id = int.Parse(cbCountry.SelectedValue.ToString());
            }
            List<clsCountry> CountryList = countryConnection.CountryArrayList();
            cbCountry.ValueMember = "CountryId";
            cbCountry.DataSource = CountryList;
            cbCountry.DisplayMember = "EnglishName";
            cbCountry.SelectedValue = id;
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cbCityID.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCountry.Enabled = false;
            tvArabicName.ReadOnly = true;
          

        }

        private void unReadOnlyEveryThing()
        {
            cbCityID.DropDownStyle = ComboBoxStyle.DropDown;
            cbCountry.Enabled = true;
            tvArabicName.ReadOnly = false;
          
        }
    }
}
