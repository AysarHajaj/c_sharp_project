﻿namespace SerhanTravel.Forms
{
    partial class frmCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEnglishFaxNo = new System.Windows.Forms.Label();
            this.txtCompanyEnglishFaxNo = new System.Windows.Forms.TextBox();
            this.lblCompanyIdNo = new System.Windows.Forms.Label();
            this.lblEnglishCRNo = new System.Windows.Forms.Label();
            this.cboCompanyId = new System.Windows.Forms.ComboBox();
            this.lblEnglishCompanyWebSite = new System.Windows.Forms.Label();
            this.txtCompanyWebSite = new System.Windows.Forms.TextBox();
            this.lblEnglishCompanyEmail = new System.Windows.Forms.Label();
            this.txtCompanyEmail = new System.Windows.Forms.TextBox();
            this.lblEnglishPhoneNo = new System.Windows.Forms.Label();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.imgSave = new System.Windows.Forms.ToolStripButton();
            this.txtCompanyEnglishPhoneNo = new System.Windows.Forms.TextBox();
            this.lblEnglishAddress = new System.Windows.Forms.Label();
            this.txtCompanyEnglishAddress = new System.Windows.Forms.TextBox();
            this.lblEnglishActivity = new System.Windows.Forms.Label();
            this.txtCompanyEnglishActivity = new System.Windows.Forms.TextBox();
            this.lblCompanyEnglishName = new System.Windows.Forms.Label();
            this.txtCompanyEnglishName = new System.Windows.Forms.TextBox();
            this.txtCompanyEnglishCRNo = new System.Windows.Forms.TextBox();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TStrip.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEnglishFaxNo
            // 
            this.lblEnglishFaxNo.AccessibleDescription = "lblEnglishFaxNo";
            this.lblEnglishFaxNo.AccessibleName = "lblEnglishFaxNo";
            this.lblEnglishFaxNo.AutoSize = true;
            this.lblEnglishFaxNo.Location = new System.Drawing.Point(13, 55);
            this.lblEnglishFaxNo.Name = "lblEnglishFaxNo";
            this.lblEnglishFaxNo.Size = new System.Drawing.Size(47, 15);
            this.lblEnglishFaxNo.TabIndex = 25;
            this.lblEnglishFaxNo.Text = "Fax No.";
            // 
            // txtCompanyEnglishFaxNo
            // 
            this.txtCompanyEnglishFaxNo.AccessibleDescription = "txtCompanyEnglishFaxNo";
            this.txtCompanyEnglishFaxNo.AccessibleName = "txtCompanyEnglishFaxNo";
            this.txtCompanyEnglishFaxNo.Location = new System.Drawing.Point(114, 52);
            this.txtCompanyEnglishFaxNo.Name = "txtCompanyEnglishFaxNo";
            this.txtCompanyEnglishFaxNo.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishFaxNo.TabIndex = 5;
            this.txtCompanyEnglishFaxNo.TextChanged += new System.EventHandler(this.txtCompanyEnglishFaxNo_TextChanged);
            this.txtCompanyEnglishFaxNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCompanyEnglishFaxNo_KeyPress);
            // 
            // lblCompanyIdNo
            // 
            this.lblCompanyIdNo.AccessibleDescription = "lblCompanyIdNo";
            this.lblCompanyIdNo.AccessibleName = "lblCompanyIdNo";
            this.lblCompanyIdNo.AutoSize = true;
            this.lblCompanyIdNo.Location = new System.Drawing.Point(8, 5);
            this.lblCompanyIdNo.Name = "lblCompanyIdNo";
            this.lblCompanyIdNo.Size = new System.Drawing.Size(64, 15);
            this.lblCompanyIdNo.TabIndex = 155;
            this.lblCompanyIdNo.Text = "Branch ID";
            // 
            // lblEnglishCRNo
            // 
            this.lblEnglishCRNo.AccessibleDescription = "lblEnglishCRNo";
            this.lblEnglishCRNo.AccessibleName = "lblEnglishCRNo";
            this.lblEnglishCRNo.AutoSize = true;
            this.lblEnglishCRNo.Location = new System.Drawing.Point(8, 97);
            this.lblEnglishCRNo.Name = "lblEnglishCRNo";
            this.lblEnglishCRNo.Size = new System.Drawing.Size(72, 15);
            this.lblEnglishCRNo.TabIndex = 19;
            this.lblEnglishCRNo.Text = "CR Number";
            // 
            // cboCompanyId
            // 
            this.cboCompanyId.AccessibleDescription = "cboCompanyIdNo";
            this.cboCompanyId.AccessibleName = "cboCompanyIdNo";
            this.cboCompanyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompanyId.FormattingEnabled = true;
            this.cboCompanyId.Location = new System.Drawing.Point(115, 5);
            this.cboCompanyId.Name = "cboCompanyId";
            this.cboCompanyId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboCompanyId.Size = new System.Drawing.Size(264, 23);
            this.cboCompanyId.TabIndex = 156;
            this.cboCompanyId.SelectedIndexChanged += new System.EventHandler(this.cboCompanyId_SelectedIndexChanged);
            // 
            // lblEnglishCompanyWebSite
            // 
            this.lblEnglishCompanyWebSite.AccessibleDescription = "lblEnglishCompanyWebSite";
            this.lblEnglishCompanyWebSite.AccessibleName = "lblEnglishCompanyWebSite";
            this.lblEnglishCompanyWebSite.AutoSize = true;
            this.lblEnglishCompanyWebSite.Location = new System.Drawing.Point(13, 107);
            this.lblEnglishCompanyWebSite.Name = "lblEnglishCompanyWebSite";
            this.lblEnglishCompanyWebSite.Size = new System.Drawing.Size(103, 15);
            this.lblEnglishCompanyWebSite.TabIndex = 153;
            this.lblEnglishCompanyWebSite.Text = "Web Site Address";
            // 
            // txtCompanyWebSite
            // 
            this.txtCompanyWebSite.AccessibleDescription = "txtCompanyWebSite";
            this.txtCompanyWebSite.AccessibleName = "txtCompanyWebSite";
            this.txtCompanyWebSite.Location = new System.Drawing.Point(114, 104);
            this.txtCompanyWebSite.Name = "txtCompanyWebSite";
            this.txtCompanyWebSite.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyWebSite.TabIndex = 148;
            this.txtCompanyWebSite.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCompanyWebSite.TextChanged += new System.EventHandler(this.txtCompanyWebSite_TextChanged);
            // 
            // lblEnglishCompanyEmail
            // 
            this.lblEnglishCompanyEmail.AccessibleDescription = "lblEnglishCompanyEmail";
            this.lblEnglishCompanyEmail.AccessibleName = "lblEnglishCompanyEmail";
            this.lblEnglishCompanyEmail.AutoSize = true;
            this.lblEnglishCompanyEmail.Location = new System.Drawing.Point(13, 81);
            this.lblEnglishCompanyEmail.Name = "lblEnglishCompanyEmail";
            this.lblEnglishCompanyEmail.Size = new System.Drawing.Size(85, 15);
            this.lblEnglishCompanyEmail.TabIndex = 151;
            this.lblEnglishCompanyEmail.Text = "Email Address";
            // 
            // txtCompanyEmail
            // 
            this.txtCompanyEmail.AccessibleDescription = "txtCompanyEmail";
            this.txtCompanyEmail.AccessibleName = "txtCompanyEmail";
            this.txtCompanyEmail.Location = new System.Drawing.Point(114, 78);
            this.txtCompanyEmail.Name = "txtCompanyEmail";
            this.txtCompanyEmail.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEmail.TabIndex = 147;
            this.txtCompanyEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCompanyEmail.TextChanged += new System.EventHandler(this.txtCompanyEmail_TextChanged);
            // 
            // lblEnglishPhoneNo
            // 
            this.lblEnglishPhoneNo.AccessibleDescription = "lblEnglishPhoneNo";
            this.lblEnglishPhoneNo.AccessibleName = "lblEnglishPhoneNo";
            this.lblEnglishPhoneNo.AutoSize = true;
            this.lblEnglishPhoneNo.Location = new System.Drawing.Point(13, 29);
            this.lblEnglishPhoneNo.Name = "lblEnglishPhoneNo";
            this.lblEnglishPhoneNo.Size = new System.Drawing.Size(62, 15);
            this.lblEnglishPhoneNo.TabIndex = 23;
            this.lblEnglishPhoneNo.Text = "Phone No.";
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.toolStripSeparator1,
            this.imgSave});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TStrip.Size = new System.Drawing.Size(823, 62);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 149;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.Text = "New";
            this.TStripNew.ToolTipText = "New";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 62);
            // 
            // imgSave
            // 
            this.imgSave.AccessibleDescription = "TStripSave";
            this.imgSave.AccessibleName = "TStripSave";
            this.imgSave.AutoSize = false;
            this.imgSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.imgSave.Image = global::SerhanTravel.Properties.Resources.Save01;
            this.imgSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.imgSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.imgSave.Name = "imgSave";
            this.imgSave.Size = new System.Drawing.Size(40, 38);
            this.imgSave.Text = "Save";
            this.imgSave.ToolTipText = "Save";
            this.imgSave.Click += new System.EventHandler(this.imgSave_Click);
            // 
            // txtCompanyEnglishPhoneNo
            // 
            this.txtCompanyEnglishPhoneNo.AccessibleDescription = "txtCompanyEnglishPhoneNu";
            this.txtCompanyEnglishPhoneNo.AccessibleName = "txtCompanyEnglishPhoneNo";
            this.txtCompanyEnglishPhoneNo.Location = new System.Drawing.Point(114, 26);
            this.txtCompanyEnglishPhoneNo.Name = "txtCompanyEnglishPhoneNo";
            this.txtCompanyEnglishPhoneNo.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishPhoneNo.TabIndex = 4;
            this.txtCompanyEnglishPhoneNo.TextChanged += new System.EventHandler(this.txtCompanyEnglishPhoneNo_TextChanged);
            this.txtCompanyEnglishPhoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCompanyEnglishPhoneNo_KeyPress);
            // 
            // lblEnglishAddress
            // 
            this.lblEnglishAddress.AccessibleDescription = "lblEnglishAddress";
            this.lblEnglishAddress.AccessibleName = "lblEnglishAddress";
            this.lblEnglishAddress.AutoSize = true;
            this.lblEnglishAddress.Location = new System.Drawing.Point(13, 5);
            this.lblEnglishAddress.Name = "lblEnglishAddress";
            this.lblEnglishAddress.Size = new System.Drawing.Size(52, 15);
            this.lblEnglishAddress.TabIndex = 21;
            this.lblEnglishAddress.Text = "Address";
            // 
            // txtCompanyEnglishAddress
            // 
            this.txtCompanyEnglishAddress.AccessibleDescription = "txtCompanyEnglishAddress";
            this.txtCompanyEnglishAddress.AccessibleName = "txtCompanyEnglishAddress";
            this.txtCompanyEnglishAddress.Location = new System.Drawing.Point(114, 2);
            this.txtCompanyEnglishAddress.Name = "txtCompanyEnglishAddress";
            this.txtCompanyEnglishAddress.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishAddress.TabIndex = 3;
            this.txtCompanyEnglishAddress.TextChanged += new System.EventHandler(this.txtCompanyEnglishAddress_TextChanged);
            // 
            // lblEnglishActivity
            // 
            this.lblEnglishActivity.AccessibleDescription = "lblEnglishActivity";
            this.lblEnglishActivity.AccessibleName = "lblEnglishActivity";
            this.lblEnglishActivity.AutoSize = true;
            this.lblEnglishActivity.Location = new System.Drawing.Point(8, 65);
            this.lblEnglishActivity.Name = "lblEnglishActivity";
            this.lblEnglishActivity.Size = new System.Drawing.Size(101, 15);
            this.lblEnglishActivity.TabIndex = 17;
            this.lblEnglishActivity.Text = "Comapny Activity";
            // 
            // txtCompanyEnglishActivity
            // 
            this.txtCompanyEnglishActivity.AccessibleDescription = "txtCompanyEnglishActivity";
            this.txtCompanyEnglishActivity.AccessibleName = "txtCompanyEnglishActivity";
            this.txtCompanyEnglishActivity.Location = new System.Drawing.Point(115, 65);
            this.txtCompanyEnglishActivity.Name = "txtCompanyEnglishActivity";
            this.txtCompanyEnglishActivity.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishActivity.TabIndex = 5;
            this.txtCompanyEnglishActivity.TextChanged += new System.EventHandler(this.txtCompanyEnglishActivity_TextChanged);
            // 
            // lblCompanyEnglishName
            // 
            this.lblCompanyEnglishName.AccessibleDescription = "lblCompanyEnglishName";
            this.lblCompanyEnglishName.AccessibleName = "lblCompanyEnglishName";
            this.lblCompanyEnglishName.AutoSize = true;
            this.lblCompanyEnglishName.Location = new System.Drawing.Point(8, 39);
            this.lblCompanyEnglishName.Name = "lblCompanyEnglishName";
            this.lblCompanyEnglishName.Size = new System.Drawing.Size(91, 15);
            this.lblCompanyEnglishName.TabIndex = 14;
            this.lblCompanyEnglishName.Text = "Company Name";
            // 
            // txtCompanyEnglishName
            // 
            this.txtCompanyEnglishName.Location = new System.Drawing.Point(115, 36);
            this.txtCompanyEnglishName.Name = "txtCompanyEnglishName";
            this.txtCompanyEnglishName.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishName.TabIndex = 26;
            this.txtCompanyEnglishName.TextChanged += new System.EventHandler(this.txtCompanyEnglishName_TextChanged);
            // 
            // txtCompanyEnglishCRNo
            // 
            this.txtCompanyEnglishCRNo.AccessibleDescription = "txtCompanyEnglishCRNo";
            this.txtCompanyEnglishCRNo.AccessibleName = "txtCompanyEnglishCRNo";
            this.txtCompanyEnglishCRNo.Location = new System.Drawing.Point(115, 94);
            this.txtCompanyEnglishCRNo.Name = "txtCompanyEnglishCRNo";
            this.txtCompanyEnglishCRNo.Size = new System.Drawing.Size(264, 22);
            this.txtCompanyEnglishCRNo.TabIndex = 2;
            this.txtCompanyEnglishCRNo.TextChanged += new System.EventHandler(this.txtCompanyEnglishCRNo_TextChanged);
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.BackColor = System.Drawing.Color.LightGray;
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 344);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(823, 22);
            this.stsInfo.TabIndex = 158;
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(130, 17);
            this.stsLblInfo.Text = "Company Informations";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(321, 289);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 159;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.LightGreen;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.ForeColor = System.Drawing.Color.White;
            this.btnSubmit.Location = new System.Drawing.Point(421, 289);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 32);
            this.btnSubmit.TabIndex = 160;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Visible = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel1.Controls.Add(this.txtCompanyEnglishActivity);
            this.panel1.Controls.Add(this.txtCompanyEnglishName);
            this.panel1.Controls.Add(this.lblCompanyEnglishName);
            this.panel1.Controls.Add(this.lblEnglishActivity);
            this.panel1.Controls.Add(this.lblEnglishCRNo);
            this.panel1.Controls.Add(this.cboCompanyId);
            this.panel1.Controls.Add(this.lblCompanyIdNo);
            this.panel1.Controls.Add(this.txtCompanyEnglishCRNo);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(14, 131);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(382, 128);
            this.panel1.TabIndex = 188;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel3.Controls.Add(this.txtCompanyEnglishPhoneNo);
            this.panel3.Controls.Add(this.txtCompanyEmail);
            this.panel3.Controls.Add(this.lblEnglishCompanyEmail);
            this.panel3.Controls.Add(this.txtCompanyEnglishAddress);
            this.panel3.Controls.Add(this.txtCompanyWebSite);
            this.panel3.Controls.Add(this.lblEnglishFaxNo);
            this.panel3.Controls.Add(this.lblEnglishAddress);
            this.panel3.Controls.Add(this.txtCompanyEnglishFaxNo);
            this.panel3.Controls.Add(this.lblEnglishCompanyWebSite);
            this.panel3.Controls.Add(this.lblEnglishPhoneNo);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(421, 131);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(381, 128);
            this.panel3.TabIndex = 190;
            // 
            // frmCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.Salmon;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(823, 366);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.TStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmCompany";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Company Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCompany_FormClosing);
            this.Load += new System.EventHandler(this.frmCompany_Load);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label lblEnglishFaxNo;
        internal System.Windows.Forms.TextBox txtCompanyEnglishFaxNo;
        internal System.Windows.Forms.Label lblCompanyIdNo;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.Label lblEnglishCRNo;
        internal System.Windows.Forms.ComboBox cboCompanyId;
        internal System.Windows.Forms.Label lblEnglishCompanyWebSite;
        internal System.Windows.Forms.TextBox txtCompanyWebSite;
        internal System.Windows.Forms.Label lblEnglishCompanyEmail;
        internal System.Windows.Forms.TextBox txtCompanyEmail;
        internal System.Windows.Forms.Label lblEnglishPhoneNo;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.TextBox txtCompanyEnglishPhoneNo;
        internal System.Windows.Forms.Label lblEnglishAddress;
        internal System.Windows.Forms.TextBox txtCompanyEnglishAddress;
        internal System.Windows.Forms.Label lblEnglishActivity;
        internal System.Windows.Forms.TextBox txtCompanyEnglishActivity;
        internal System.Windows.Forms.Label lblCompanyEnglishName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton imgSave;
        private System.Windows.Forms.TextBox txtCompanyEnglishName;
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.TextBox txtCompanyEnglishCRNo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
    }
}