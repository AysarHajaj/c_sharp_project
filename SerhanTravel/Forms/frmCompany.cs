﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Objects;
using System.Collections;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmCompany : Form
    {
        List<clsCompany> companyList;
        Company companyConnection;
        bool DataChanged = false;
        bool iamAdding = false;
        int BranchId=0;
        int BranchSavedID = 0;
        clsAccessRights right;
        bool canAddOnly = false;

        internal frmCompany(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void ClearFields()
        {
            txtCompanyEnglishName.Clear();
            txtCompanyEnglishActivity.Clear();
            txtCompanyEnglishCRNo.Clear();
            txtCompanyEnglishAddress.Clear();
            txtCompanyEnglishPhoneNo.Clear();
            txtCompanyEnglishFaxNo.Clear();
            txtCompanyEmail.Clear();
            txtCompanyWebSite.Clear();
            stsLblInfo.Text = "";

        }
        private void txtCompanyEnglishName_TextChanged(object sender, EventArgs e)
        {   
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void txtCompanyEnglishActivity_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }
        private void txtCompanyEnglishCRNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void txtCompanyEnglishAddress_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void txtCompanyEnglishPhoneNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void txtCompanyEnglishFaxNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void txtCompanyEmail_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }
        private void txtCompanyWebSite_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
            if (iamAdding)
            {
                btnSubmit.Visible = true;
            }
        }

        private void DisplayData(int branch)
        {
            cboCompanyId.ValueMember = "BranchID";
            cboCompanyId.DataSource = companyList;
            cboCompanyId.DisplayMember = "BranchID";
            cboCompanyId.SelectedIndex = -1;
            cboCompanyId.SelectedValue = branch;

            DataChanged = false;

        }


        private void frmCompany_Load(object sender, EventArgs e)
        {
            accessRight();
            btnCancel.Visible = false;
            btnSubmit.Visible = false;
            companyConnection = new Company();

            companyList = companyConnection.CompanyInfoArrayList();
            cboCompanyId.ValueMember = "BranchID";
            cboCompanyId.DataSource = companyList;
            cboCompanyId.DisplayMember = "BranchID";
         
            DataChanged = false;
        }

        private void cboCompanyId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!iamAdding && cboCompanyId.SelectedIndex!=-1)
            {       
                int SelectedValue = cboCompanyId.SelectedIndex;
                BranchId= int.Parse(cboCompanyId.SelectedValue.ToString());

                txtCompanyEnglishName.Text = companyList[SelectedValue].CompanyName;
                txtCompanyEnglishActivity.Text = companyList[SelectedValue].CompanyActivity;
                txtCompanyEnglishCRNo.Text = companyList[SelectedValue].CRNumber.ToString();
                txtCompanyEnglishAddress.Text = companyList[SelectedValue].Address;
                txtCompanyEnglishPhoneNo.Text = companyList[SelectedValue].PhoneNumber.ToString();
                txtCompanyEnglishFaxNo.Text = companyList[SelectedValue].FaxNumber.ToString();
                txtCompanyEmail.Text = companyList[SelectedValue].EmailAddress;
                txtCompanyWebSite.Text = companyList[SelectedValue].WebSite;
            }

           
                DataChanged = false;
        }

        private void SaveDataInfo()
        {
            bool RecExists;
            try
            {
                
                clsCompany company = new clsCompany();
                company.BranchID =BranchId ;

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishName.Text))
                {
                    company.CompanyName = " ";
                }
                else
                {
                    company.CompanyName = txtCompanyEnglishName.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishActivity.Text))
                {
                    company.CompanyActivity = string.Empty;
                }
                else
                {
                    company.CompanyActivity = txtCompanyEnglishActivity.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishCRNo.Text))
                {
                    company.CRNumber = string.Empty;
                }
                else
                {
                    company.CRNumber = txtCompanyEnglishCRNo.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishAddress.Text))
                {
                    company.Address = " ";
                }
                else
                {
                    company.Address = txtCompanyEnglishAddress.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishPhoneNo.Text))
                {
                    company.PhoneNumber = " "; 
                }
                else
                {
                    company.PhoneNumber = txtCompanyEnglishPhoneNo.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyEnglishFaxNo.Text))
                {
                    company.FaxNumber = string.Empty;
                }
                else
                {
                    company.FaxNumber = txtCompanyEnglishFaxNo.Text;
                }


                if (string.IsNullOrWhiteSpace(txtCompanyEmail.Text))
                {
                    company.EmailAddress = string.Empty;
                }
                else
                {
                    company.EmailAddress= txtCompanyEmail.Text;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyWebSite.Text))
                {
                    company.WebSite = string.Empty;
                }
                else
                {
                    company.WebSite = txtCompanyWebSite.Text;
                }


                Company companyConnection = new Company();
                RecExists = companyConnection.BranchIDExists(company.BranchID);
                if (RecExists == true)
                {
                    companyConnection.UpdateCompanyInfo(company);
                    stsLblInfo.Text = "Changes has been saved successfully";
                  
                }
                else
                {
                    companyConnection.AddNewCompany(company);
                    stsLblInfo.Text = "New Company has been added successfully";
                    BranchId = companyConnection.ReadLastNo();
                    BranchSavedID = BranchId;
                }


                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
               
                
                companyList = companyConnection.CompanyInfoArrayList();
                DisplayData(BranchId);
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

        private void imgSave_Click(object sender, EventArgs e)
        {
            if (DataChanged)
            {


                btnSubmit.Visible = false;
                btnCancel.Visible = false;
                cboCompanyId.Enabled = true;
                SaveDataInfo();
                if (canAddOnly)
                {
                    imgSave.Enabled = false;
                    readOnlyEveryThing();
                }


            }
        }

     

    

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnSubmit.Visible = false;
            BranchId = BranchSavedID;
            btnCancel.Visible = false;
            iamAdding = false;
            cboCompanyId.Enabled = true;
            DisplayData(BranchId);
            if (canAddOnly)
            {
                imgSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            cboCompanyId.Enabled = true;
            
            SaveDataInfo();
      
            btnSubmit.Visible = false;
            btnCancel.Visible = false;
            iamAdding = false;
            if (canAddOnly)
            {
                imgSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripNew_Click_1(object sender, EventArgs e)
        {
            stsLblInfo.Text ="Add Company";
            if (canAddOnly)
            {
                imgSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            iamAdding = true;

            
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }

            btnCancel.Visible = true;

            BranchSavedID = BranchId;
            BranchId = 0;
            cboCompanyId.Enabled = false;
            cboCompanyId.DataSource = null;
            ClearFields();
            DataChanged = false;

        }
            //prevent the user from enter letters and space(8)
      

        private void txtCompanyEnglishPhoneNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void txtCompanyEnglishFaxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }
        

        private void frmCompany_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            /*if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }*/
            if (!right.Updating)
            {
                imgSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
           
        }
        private void readOnlyEveryThing()
        {
            cboCompanyId.DropDownStyle = ComboBoxStyle.DropDownList;
            //cbCountry.Enabled = false;
            txtCompanyEnglishName.ReadOnly = true;
            txtCompanyEnglishActivity.ReadOnly = true;
            txtCompanyEnglishCRNo.ReadOnly = true;
            txtCompanyEnglishAddress.ReadOnly = true;
            txtCompanyEnglishPhoneNo.ReadOnly = true;
            txtCompanyEnglishFaxNo.ReadOnly = true;
            txtCompanyEmail.ReadOnly = true;
            txtCompanyWebSite.ReadOnly = true;
        }

        private void unReadOnlyEveryThing()
        {
            cboCompanyId.DropDownStyle = ComboBoxStyle.DropDown;
            //cbCountry.Enabled = false;
            txtCompanyEnglishName.ReadOnly = false;
            txtCompanyEnglishActivity.ReadOnly = false;
            txtCompanyEnglishCRNo.ReadOnly = false;
            txtCompanyEnglishAddress.ReadOnly = false;
            txtCompanyEnglishPhoneNo.ReadOnly = false;
            txtCompanyEnglishFaxNo.ReadOnly = false;
            txtCompanyEmail.ReadOnly = false;
            txtCompanyWebSite.ReadOnly = false;
        }
    }
}
