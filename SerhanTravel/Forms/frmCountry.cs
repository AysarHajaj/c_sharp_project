﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmCountry : Form
    {
        int CountryId = 0;
        int SavedID;
        List<clsCountry> countryList;
        bool iamAdding = false;
        bool DataChange = false;
        clsCountry countryData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        Country countryConnnection;
        internal frmCountry(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmCountry_Load(object sender, EventArgs e)
        {
            accessRight();
            countryConnnection = new Country();
            countryList = countryConnnection.CountryArrayList();
            cmbCountry.ValueMember = "countryId";
            cmbCountry.DataSource = countryList;
            cmbCountry.DisplayMember = "englishName";
            ResetFeilds();
            this.CountryId = 0;
            DataChange = false;
        }



        public void Display(int ctId)
        {
            iamAdding = false;
            cmbCountry.DataSource = null;
            cmbCountry.ValueMember = "countryId";
            cmbCountry.DataSource = countryList;
            cmbCountry.DisplayMember = "englishName";
            cmbCountry.SelectedValue = ctId;
            
            this.CountryId = ctId;
            DataChange = false;


        }

        public void ResetFeilds()
        {
            cmbCountry.SelectedIndex = -1;
            tvArabicName.Text = "";
            stsLblInfo.Text = "";

        }

        private void cmbCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbCountry.SelectedIndex != -1 && countryList.Count > 0)
            {
                this.CountryId = int.Parse(cmbCountry.SelectedValue.ToString());
                countryData = countryList[cmbCountry.SelectedIndex];
            
                tvArabicName.Text = countryData.ArabicName;
         
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsCountry country = new clsCountry();
                country.CountryId = this.CountryId;

                if(!string.IsNullOrWhiteSpace(cmbCountry.Text))
                {
                    country.EnglishName = cmbCountry.Text;
                }
                else
                {
                    country.EnglishName = string.Empty;
                }

                if (string.IsNullOrWhiteSpace(tvArabicName.Text))
                {
                    country.ArabicName = "";
                }
                else
                {
                    country.ArabicName = tvArabicName.Text;
                }

               
                IdExists = countryConnnection.CountryIDExists(country.CountryId);

                if (IdExists)
                {
                    countryConnnection.UpdateCoutry(country);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    countryConnnection.AddCountry(country);
                    stsLblInfo.Text = "New Country has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                countryList = countryConnnection.CountryArrayList();
                if (iamAdding && countryList.Count > 0)
                {
                    clsCountry con = (clsCountry)countryList[countryList.Count - 1];
                    this.CountryId = con.CountryId;
                }
                Display(this.CountryId);

                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }

       
        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Country";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = this.CountryId;
            this.CountryId = 0;
            ResetFeilds();
            cmbCountry.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void tvArabicName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }


        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.CountryId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.CountryId = SavedID;
            Display(this.CountryId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.CountryId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Country", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (countryConnnection.CountryUsed(CountryId))
                        {
                            MessageBox.Show("This Country Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = countryConnnection.DeleteCountry(this.CountryId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        countryList = countryConnnection.CountryArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }



        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbCountry.Items.Count > 0)
            {
                if (cmbCountry.SelectedIndex == 0 || cmbCountry.SelectedIndex == -1)
                {
                    int Cities = cmbCountry.Items.Count;
                    cmbCountry.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbCountry.SelectedIndex = cmbCountry.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbCountry.Items.Count > 0)
            {
                if (cmbCountry.SelectedIndex == cmbCountry.Items.Count - 1 || cmbCountry.SelectedIndex == -1)
                {

                    cmbCountry.SelectedIndex = 0;
                }
                else
                {
                    cmbCountry.SelectedIndex = cmbCountry.SelectedIndex + 1;
                }
            }
        }



        

        private void cmbCountry_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbCountry.DropDownStyle = ComboBoxStyle.DropDownList;

            tvArabicName.ReadOnly = true;


        }

        private void unReadOnlyEveryThing()
        {
            cmbCountry.DropDownStyle = ComboBoxStyle.DropDown;

            tvArabicName.ReadOnly = false;
        }
    }
}
