﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmCurrency : Form
    {
        bool canUpdateOnly;
        int CurrencyId = 0;
        int SavedID;
        List<clsCurrency> currencyList;
        bool iamAdding = false;
        bool DataChange = false;
        clsCurrency currencyData;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection = new Currency();
        internal frmCurrency(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmCurrency_Load(object sender, EventArgs e)
        {
            accessRight();
            currencyList = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "currencyId";
            cmbCurrency.DataSource = currencyList;
            cmbCurrency.DisplayMember = "currencyName";
            ResetFeilds();
            this.CurrencyId = 0;
            DataChange = false;


        }


       
        public void Display(int crId)
        {
            iamAdding = false;
            cmbCurrency.DataSource = null;
            cmbCurrency.ValueMember = "currencyId";
            cmbCurrency.DataSource = currencyList;
            cmbCurrency.DisplayMember = "currencyName";
            cmbCurrency.SelectedValue = crId;
            this.CurrencyId = crId;
            DataChange = false;
        }

        public void ResetFeilds()
        {
            cmbCurrency.SelectedIndex = -1;
         
            txtRate.Text = "";
            txtSymbol.Text = "";
            stsLblInfo.Text = "";
        }

        private void cmbCurrency_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1 && currencyList.Count > 0)
            {
                this.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                currencyData = currencyList[cmbCurrency.SelectedIndex];
                txtRate.Text = currencyData.CurrencyRate.ToString();
                txtSymbol.Text = currencyData.CurrencySymbol;
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsCurrency currency = new clsCurrency();
                currency.CurrencyId = this.CurrencyId;

                if (!string.IsNullOrWhiteSpace(cmbCurrency.Text))
                {
                    currency.CurrencyName = cmbCurrency.Text;
                }
                else
                {
                    currency.CurrencyName = string.Empty;
                }

                if (string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    currency.CurrencyRate = 0;
                }
                else
                {
                    currency.CurrencyRate = decimal.Parse(txtRate.Text.ToString());
                }

                if (string.IsNullOrWhiteSpace(txtSymbol.Text))
                {
                    currency.CurrencySymbol = "";
                }
                else
                {
                    currency.CurrencySymbol = txtSymbol.Text;
                }

                IdExists = currencyConnection.CurrencyIDExists(currency.CurrencyId);

                if (IdExists)
                {
                    currencyConnection.UpdateCurrency(currency);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    currencyConnection.AddCurrency(currency);
                    stsLblInfo.Text = "New Currency has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                currencyList = currencyConnection.CurrencyArrayList();
                if (iamAdding && currencyList.Count > 0)
                {
                    clsCurrency cur = (clsCurrency)currencyList[currencyList.Count - 1];
                    this.CurrencyId = cur.CurrencyId;
                }
                Display(this.CurrencyId);
                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }

      

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Currency";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = this.CurrencyId;
            this.CurrencyId = 0;
            ResetFeilds();
            cmbCurrency.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

   
    

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.CurrencyId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();

            Display(this.SavedID);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.CurrencyId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Currecny", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if(currencyConnection.CurrencyUsed(CurrencyId))
                        {
                            MessageBox.Show("This Currency Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = currencyConnection.DeleteCurrency(this.CurrencyId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        currencyList = currencyConnection.CurrencyArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }



        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.Items.Count > 0)
            {
                if (cmbCurrency.SelectedIndex == 0 || cmbCurrency.SelectedIndex == -1)
                {
                    int currencies = cmbCurrency.Items.Count;
                    cmbCurrency.SelectedIndex = currencies - 1;
                }
                else
                {
                    cmbCurrency.SelectedIndex = cmbCurrency.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.Items.Count > 0)
            {
                if (cmbCurrency.SelectedIndex == cmbCurrency.Items.Count - 1 || cmbCurrency.SelectedIndex == -1)
                {

                    cmbCurrency.SelectedIndex = 0;
                }
                else
                {
                    cmbCurrency.SelectedIndex = cmbCurrency.SelectedIndex + 1;
                }
            }
        }



     

        private void txtSymbol_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void cmbCurrency_TextChanged(object sender, EventArgs e)
        {

            DataChange = true;
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {

          char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character!='.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtRate.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }

            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbCurrency.DropDownStyle = ComboBoxStyle.DropDownList;

            txtSymbol.ReadOnly = true;
            txtRate.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbCurrency.DropDownStyle = ComboBoxStyle.DropDown;

            txtSymbol.ReadOnly = false;
            txtRate.ReadOnly = false;
        }
    }
}
