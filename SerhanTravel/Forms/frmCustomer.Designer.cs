﻿namespace SerhanTravel.Forms
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomer));
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.Label13 = new System.Windows.Forms.Label();
            this.CboCustStatus = new System.Windows.Forms.ComboBox();
            this.TxtCustWebSite = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.TxtCustEmail = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.TxtCustFaxNo = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.TxtCustAddress = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.TxtCustLandPhoneNo = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.TxtCustMobileNo = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.CboCustCity = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.CboCustCountry = new System.Windows.Forms.ComboBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.CboCustType = new System.Windows.Forms.ComboBox();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.Label4 = new System.Windows.Forms.Label();
            this.TxtCustBirthPlace = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.CboCustNationality = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TxtCustomerId = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.dtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.cmbCustomerName = new System.Windows.Forms.ComboBox();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Location = new System.Drawing.Point(0, 431);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(747, 22);
            this.stsInfo.TabIndex = 182;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(66, 17);
            this.stsLblInfo.Text = "عرض بيانات";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(9, 139);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(42, 15);
            this.Label13.TabIndex = 180;
            this.Label13.Text = "Status";
            // 
            // CboCustStatus
            // 
            this.CboCustStatus.AutoCompleteCustomSource.AddRange(new string[] {
            "Single",
            "Married",
            "Divorced"});
            this.CboCustStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustStatus.FormattingEnabled = true;
            this.CboCustStatus.Location = new System.Drawing.Point(101, 139);
            this.CboCustStatus.Name = "CboCustStatus";
            this.CboCustStatus.Size = new System.Drawing.Size(195, 23);
            this.CboCustStatus.TabIndex = 181;
            this.CboCustStatus.SelectedIndexChanged += new System.EventHandler(this.CboCustStatus_SelectedIndexChanged);
            // 
            // TxtCustWebSite
            // 
            this.TxtCustWebSite.AccessibleDescription = "txtSerNo";
            this.TxtCustWebSite.AccessibleName = "txtSerNo";
            this.TxtCustWebSite.Location = new System.Drawing.Point(114, 120);
            this.TxtCustWebSite.Name = "TxtCustWebSite";
            this.TxtCustWebSite.Size = new System.Drawing.Size(190, 22);
            this.TxtCustWebSite.TabIndex = 164;
            this.TxtCustWebSite.TextChanged += new System.EventHandler(this.TxtCustWebSite_TextChanged);
            // 
            // Label12
            // 
            this.Label12.AccessibleDescription = "lblSerNo";
            this.Label12.AccessibleName = "lblSerNo";
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(11, 120);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(56, 15);
            this.Label12.TabIndex = 179;
            this.Label12.Text = "Web Site";
            // 
            // TxtCustEmail
            // 
            this.TxtCustEmail.AccessibleDescription = "txtSerNo";
            this.TxtCustEmail.AccessibleName = "txtSerNo";
            this.TxtCustEmail.Location = new System.Drawing.Point(114, 94);
            this.TxtCustEmail.Name = "TxtCustEmail";
            this.TxtCustEmail.Size = new System.Drawing.Size(190, 22);
            this.TxtCustEmail.TabIndex = 163;
            this.TxtCustEmail.TextChanged += new System.EventHandler(this.TxtCustEmail_TextChanged);
            // 
            // Label11
            // 
            this.Label11.AccessibleDescription = "lblSerNo";
            this.Label11.AccessibleName = "lblSerNo";
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(11, 97);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(85, 15);
            this.Label11.TabIndex = 178;
            this.Label11.Text = "Email Address";
            // 
            // TxtCustFaxNo
            // 
            this.TxtCustFaxNo.AccessibleDescription = "txtSerNo";
            this.TxtCustFaxNo.AccessibleName = "txtSerNo";
            this.TxtCustFaxNo.Location = new System.Drawing.Point(114, 67);
            this.TxtCustFaxNo.Name = "TxtCustFaxNo";
            this.TxtCustFaxNo.Size = new System.Drawing.Size(190, 22);
            this.TxtCustFaxNo.TabIndex = 162;
            this.TxtCustFaxNo.TextChanged += new System.EventHandler(this.TxtCustFaxNo_TextChanged);
            this.TxtCustFaxNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCustFaxNo_KeyPress);
            // 
            // Label10
            // 
            this.Label10.AccessibleDescription = "lblSerNo";
            this.Label10.AccessibleName = "lblSerNo";
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(11, 70);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(47, 15);
            this.Label10.TabIndex = 177;
            this.Label10.Text = "Fax No.";
            // 
            // TxtCustAddress
            // 
            this.TxtCustAddress.AccessibleDescription = "txtSerNo";
            this.TxtCustAddress.AccessibleName = "txtSerNo";
            this.TxtCustAddress.Location = new System.Drawing.Point(85, 3);
            this.TxtCustAddress.Multiline = true;
            this.TxtCustAddress.Name = "TxtCustAddress";
            this.TxtCustAddress.Size = new System.Drawing.Size(208, 55);
            this.TxtCustAddress.TabIndex = 157;
            this.TxtCustAddress.TextChanged += new System.EventHandler(this.TxtCustAddress_TextChanged);
            // 
            // Label9
            // 
            this.Label9.AccessibleDescription = "lblSerNo";
            this.Label9.AccessibleName = "lblSerNo";
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(11, 6);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(52, 15);
            this.Label9.TabIndex = 176;
            this.Label9.Text = "Address";
            // 
            // TxtCustLandPhoneNo
            // 
            this.TxtCustLandPhoneNo.AccessibleDescription = "txtSerNo";
            this.TxtCustLandPhoneNo.AccessibleName = "txtSerNo";
            this.TxtCustLandPhoneNo.Location = new System.Drawing.Point(114, 39);
            this.TxtCustLandPhoneNo.Name = "TxtCustLandPhoneNo";
            this.TxtCustLandPhoneNo.Size = new System.Drawing.Size(190, 22);
            this.TxtCustLandPhoneNo.TabIndex = 161;
            this.TxtCustLandPhoneNo.TextChanged += new System.EventHandler(this.TxtCustLandPhoneNo_TextChanged);
            this.TxtCustLandPhoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCustLandPhoneNo_KeyPress);
            // 
            // Label8
            // 
            this.Label8.AccessibleDescription = "lblSerNo";
            this.Label8.AccessibleName = "lblSerNo";
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(11, 42);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(99, 15);
            this.Label8.TabIndex = 175;
            this.Label8.Text = "Land Line Phone";
            // 
            // TxtCustMobileNo
            // 
            this.TxtCustMobileNo.AccessibleDescription = "txtSerNo";
            this.TxtCustMobileNo.AccessibleName = "txtSerNo";
            this.TxtCustMobileNo.Location = new System.Drawing.Point(114, 13);
            this.TxtCustMobileNo.Name = "TxtCustMobileNo";
            this.TxtCustMobileNo.Size = new System.Drawing.Size(190, 22);
            this.TxtCustMobileNo.TabIndex = 160;
            this.TxtCustMobileNo.TextChanged += new System.EventHandler(this.TxtCustMobileNo_TextChanged);
            this.TxtCustMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtCustMobileNo_KeyPress);
            // 
            // Label7
            // 
            this.Label7.AccessibleDescription = "lblSerNo";
            this.Label7.AccessibleName = "lblSerNo";
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(11, 13);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(81, 15);
            this.Label7.TabIndex = 174;
            this.Label7.Text = "Mobile Phone";
            // 
            // CboCustCity
            // 
            this.CboCustCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustCity.FormattingEnabled = true;
            this.CboCustCity.Location = new System.Drawing.Point(102, 53);
            this.CboCustCity.Name = "CboCustCity";
            this.CboCustCity.Size = new System.Drawing.Size(194, 23);
            this.CboCustCity.TabIndex = 156;
            this.CboCustCity.SelectedIndexChanged += new System.EventHandler(this.CboCustCity_SelectedIndexChanged);
            this.CboCustCity.Click += new System.EventHandler(this.CboCustCity_Click);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(9, 56);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(30, 15);
            this.Label6.TabIndex = 173;
            this.Label6.Text = "City";
            // 
            // CboCustCountry
            // 
            this.CboCustCountry.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustCountry.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustCountry.FormattingEnabled = true;
            this.CboCustCountry.Location = new System.Drawing.Point(102, 26);
            this.CboCustCountry.Name = "CboCustCountry";
            this.CboCustCountry.Size = new System.Drawing.Size(194, 23);
            this.CboCustCountry.TabIndex = 155;
            this.CboCustCountry.SelectedIndexChanged += new System.EventHandler(this.CboCustCountry_SelectedIndexChanged);
            this.CboCustCountry.Click += new System.EventHandler(this.CboCustCountry_Click);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(9, 29);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(52, 15);
            this.Label5.TabIndex = 172;
            this.Label5.Text = "Country";
            // 
            // CboCustType
            // 
            this.CboCustType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustType.FormattingEnabled = true;
            this.CboCustType.Location = new System.Drawing.Point(102, 112);
            this.CboCustType.Name = "CboCustType";
            this.CboCustType.Size = new System.Drawing.Size(194, 23);
            this.CboCustType.TabIndex = 154;
            this.CboCustType.SelectedIndexChanged += new System.EventHandler(this.CboCustType_SelectedIndexChanged);
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = ((System.Drawing.Image)(resources.GetObject("TStripNew.Image")));
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.Text = "New";
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.Text = "Save";
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.Text = "Delete";
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.Text = "Previous";
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "Next";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(9, 113);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(86, 15);
            this.Label4.TabIndex = 171;
            this.Label4.Text = "Customer type";
            // 
            // TxtCustBirthPlace
            // 
            this.TxtCustBirthPlace.AccessibleDescription = "txtSerNo";
            this.TxtCustBirthPlace.AccessibleName = "txtSerNo";
            this.TxtCustBirthPlace.Location = new System.Drawing.Point(85, 90);
            this.TxtCustBirthPlace.Name = "TxtCustBirthPlace";
            this.TxtCustBirthPlace.Size = new System.Drawing.Size(208, 22);
            this.TxtCustBirthPlace.TabIndex = 159;
            this.TxtCustBirthPlace.TextChanged += new System.EventHandler(this.TxtCustBirthPlace_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AccessibleDescription = "lblSerNo";
            this.Label3.AccessibleName = "lblSerNo";
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(11, 96);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(70, 15);
            this.Label3.TabIndex = 170;
            this.Label3.Text = "Birth Place";
            // 
            // Label2
            // 
            this.Label2.AccessibleDescription = "lblSerNo";
            this.Label2.AccessibleName = "lblSerNo";
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(11, 71);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(65, 15);
            this.Label2.TabIndex = 169;
            this.Label2.Text = "Birth Date";
            // 
            // CboCustNationality
            // 
            this.CboCustNationality.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboCustNationality.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboCustNationality.FormattingEnabled = true;
            this.CboCustNationality.Location = new System.Drawing.Point(102, 85);
            this.CboCustNationality.Name = "CboCustNationality";
            this.CboCustNationality.Size = new System.Drawing.Size(194, 23);
            this.CboCustNationality.TabIndex = 153;
            this.CboCustNationality.SelectedIndexChanged += new System.EventHandler(this.CboCustNationality_SelectedIndexChanged);
            this.CboCustNationality.Click += new System.EventHandler(this.CboCustNationality_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(9, 88);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(67, 15);
            this.Label1.TabIndex = 168;
            this.Label1.Text = "Nationality";
            // 
            // TxtCustomerId
            // 
            this.TxtCustomerId.AccessibleDescription = "txtSerNo";
            this.TxtCustomerId.AccessibleName = "txtSerNo";
            this.TxtCustomerId.Location = new System.Drawing.Point(101, 93);
            this.TxtCustomerId.Name = "TxtCustomerId";
            this.TxtCustomerId.ReadOnly = true;
            this.TxtCustomerId.Size = new System.Drawing.Size(194, 22);
            this.TxtCustomerId.TabIndex = 152;
            this.TxtCustomerId.TextChanged += new System.EventHandler(this.TxtCustomerId_TextChanged);
            // 
            // lblName
            // 
            this.lblName.AccessibleDescription = "lblName";
            this.lblName.AccessibleName = "lblName";
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 97);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(20, 15);
            this.lblName.TabIndex = 167;
            this.lblName.Text = "ID";
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Location = new System.Drawing.Point(12, 17);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(64, 15);
            this.lblSerNo.TabIndex = 166;
            this.lblSerNo.Text = "Customer ";
            // 
            // dtpBirthDate
            // 
            this.dtpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBirthDate.Location = new System.Drawing.Point(85, 65);
            this.dtpBirthDate.Name = "dtpBirthDate";
            this.dtpBirthDate.Size = new System.Drawing.Size(208, 22);
            this.dtpBirthDate.TabIndex = 184;
            this.dtpBirthDate.ValueChanged += new System.EventHandler(this.dtpBirthDate_ValueChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.btnCancel.Location = new System.Drawing.Point(307, 387);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(99, 37);
            this.btnCancel.TabIndex = 186;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(101, 36);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(194, 34);
            this.lbResult.TabIndex = 188;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // cmbCustomerName
            // 
            this.cmbCustomerName.FormattingEnabled = true;
            this.cmbCustomerName.Location = new System.Drawing.Point(101, 14);
            this.cmbCustomerName.Name = "cmbCustomerName";
            this.cmbCustomerName.Size = new System.Drawing.Size(194, 23);
            this.cmbCustomerName.TabIndex = 189;
            this.cmbCustomerName.SelectedIndexChanged += new System.EventHandler(this.cmbCustomerName_SelectedIndexChanged);
            this.cmbCustomerName.TextChanged += new System.EventHandler(this.cmbCustomerName_TextChanged);
            this.cmbCustomerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbCustomerName_KeyDown);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator1,
            this.btnSave,
            this.toolStripSeparator2,
            this.btnDelete,
            this.toolStripSeparator6,
            this.toolStripButton4,
            this.toolStripButton5});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(747, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 190;
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleDescription = "TStripNew";
            this.btnAdd.AccessibleName = "TStripNew";
            this.btnAdd.AutoSize = false;
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::SerhanTravel.Properties.Resources.New021;
            this.btnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 38);
            this.btnAdd.Text = "New";
            this.btnAdd.ToolTipText = "Add New Package";
            this.btnAdd.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 58);
            // 
            // btnSave
            // 
            this.btnSave.AccessibleDescription = "TStripSave";
            this.btnSave.AccessibleName = "TStripSave";
            this.btnSave.AutoSize = false;
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(40, 38);
            this.btnSave.Text = "Save";
            this.btnSave.ToolTipText = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 58);
            // 
            // btnDelete
            // 
            this.btnDelete.AccessibleDescription = "TStripDelete";
            this.btnDelete.AccessibleName = "TStripDelete";
            this.btnDelete.AutoSize = false;
            this.btnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(40, 38);
            this.btnDelete.Text = "Delete";
            this.btnDelete.ToolTipText = "Delete Package";
            this.btnDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 58);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.AccessibleDescription = "TStripBackWard";
            this.toolStripButton4.AccessibleName = "TStripBackWard";
            this.toolStripButton4.AutoSize = false;
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(40, 38);
            this.toolStripButton4.Text = "Previous";
            this.toolStripButton4.ToolTipText = "Backword";
            this.toolStripButton4.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.AccessibleDescription = "TStripForWard";
            this.toolStripButton5.AccessibleName = "TStripForWard";
            this.toolStripButton5.AutoSize = false;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(40, 38);
            this.toolStripButton5.Text = "Next";
            this.toolStripButton5.ToolTipText = "Forword";
            this.toolStripButton5.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel1.Controls.Add(this.lbResult);
            this.panel1.Controls.Add(this.lblSerNo);
            this.panel1.Controls.Add(this.cmbCustomerName);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.TxtCustomerId);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(12, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 121);
            this.panel1.TabIndex = 191;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel2.Controls.Add(this.CboCustCity);
            this.panel2.Controls.Add(this.Label5);
            this.panel2.Controls.Add(this.CboCustCountry);
            this.panel2.Controls.Add(this.Label6);
            this.panel2.Controls.Add(this.CboCustType);
            this.panel2.Controls.Add(this.Label1);
            this.panel2.Controls.Add(this.Label13);
            this.panel2.Controls.Add(this.CboCustNationality);
            this.panel2.Controls.Add(this.CboCustStatus);
            this.panel2.Controls.Add(this.Label4);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(12, 203);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(299, 165);
            this.panel2.TabIndex = 192;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel3.Controls.Add(this.TxtCustAddress);
            this.panel3.Controls.Add(this.Label2);
            this.panel3.Controls.Add(this.Label3);
            this.panel3.Controls.Add(this.TxtCustBirthPlace);
            this.panel3.Controls.Add(this.Label9);
            this.panel3.Controls.Add(this.dtpBirthDate);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(403, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(307, 121);
            this.panel3.TabIndex = 193;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel5.Controls.Add(this.TxtCustFaxNo);
            this.panel5.Controls.Add(this.Label7);
            this.panel5.Controls.Add(this.TxtCustMobileNo);
            this.panel5.Controls.Add(this.Label8);
            this.panel5.Controls.Add(this.TxtCustLandPhoneNo);
            this.panel5.Controls.Add(this.Label10);
            this.panel5.Controls.Add(this.Label11);
            this.panel5.Controls.Add(this.TxtCustWebSite);
            this.panel5.Controls.Add(this.TxtCustEmail);
            this.panel5.Controls.Add(this.Label12);
            this.panel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.ForeColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(403, 203);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(307, 165);
            this.panel5.TabIndex = 194;
            // 
            // frmCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(747, 453);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TStrip);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.stsInfo);
            this.Name = "frmCustomer";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomer_FormClosing);
            this.Load += new System.EventHandler(this.frmCustomer_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmCustomer_MouseClick);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.ComboBox CboCustStatus;
        internal System.Windows.Forms.TextBox TxtCustWebSite;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox TxtCustEmail;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox TxtCustFaxNo;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox TxtCustAddress;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox TxtCustLandPhoneNo;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TxtCustMobileNo;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.ComboBox CboCustCity;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.ComboBox CboCustCountry;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.ComboBox CboCustType;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TxtCustBirthPlace;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox CboCustNationality;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox TxtCustomerId;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.Label lblSerNo;
        private System.Windows.Forms.DateTimePicker dtpBirthDate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.ComboBox cmbCustomerName;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton btnSave;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton btnDelete;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        internal System.Windows.Forms.ToolStripButton toolStripButton4;
        internal System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
    }
}