﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Objects;
using System.Collections;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmCustomer : Form
    {
        DateTime DefaultDate = new DateTime(1990, 1, 1);
        List<clsCustomer> customerList;
        Customer customerConnection;
        clsCustomer customerData;
        bool skip = false;
        bool DataChanged = false;
        bool iamUpdate = false;
        bool iamAdding = false;
        //int CustomerId = 0;
        int CustomerID = 0;
        private int saveID;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        Country countryConnection;
        Nationality nationalityConnection;
        City cityConnection;
        internal frmCustomer(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

  
        private void ResetFields()
        {
            // cmbCustomerName.Items.Clear();
            cmbCustomerName.Text="";
            CboCustNationality.Text = "";
            CboCustCity.Text = "";
            CboCustCountry.Text = "";
            CboCustType.Text = "";
            CboCustStatus.Text = "";

            TxtCustomerId.Clear();
            CboCustNationality.SelectedIndex = -1;
            CboCustCity.SelectedIndex = -1;
            CboCustCountry.SelectedIndex = -1;
            CboCustType.SelectedIndex = -1;
            CboCustStatus.SelectedIndex = -1;
            TxtCustAddress.Clear();
            dtpBirthDate.Value = DefaultDate;
            TxtCustBirthPlace.Clear();
            TxtCustMobileNo.Clear();
            TxtCustLandPhoneNo.Clear();
            TxtCustFaxNo.Clear();
            TxtCustEmail.Clear();
            TxtCustWebSite.Clear();
            stsLblInfo.Text = "";
            DataChanged = false;
        }


        private void frmCustomer_Load(object sender, EventArgs e)
        {
            accessRight();
            cityConnection = new City();
            countryConnection = new Country();
            nationalityConnection = new Nationality();
            dtpBirthDate.Value = DefaultDate;
            customerConnection = new Customer();
            customerList = customerConnection.CustomersArrayList();
            dtpBirthDate.Format = DateTimePickerFormat.Custom;
            dtpBirthDate.CustomFormat = "dd-MM-yyyy";
            FillCombobox();
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            ResetFields();
            this.CustomerID = 0;
            DataChanged = false;
        }

        private void DisplayData(int custmId)
        {
        cmbCustomerName.DataSource = null;
        cmbCustomerName.ValueMember = "CustomerId";
        cmbCustomerName.DataSource = customerList;
        cmbCustomerName.DisplayMember = "CustomerName";
        skip = true;
        cmbCustomerName.SelectedValue = custmId;
        CustomerID = custmId;
        iamAdding = false;
        iamUpdate = false;
        DataChanged = false;

        }

        private void FillCombobox()
        {


            ArrayList Nationalities = nationalityConnection.NationalitiesArrayList();
            CboCustNationality.ValueMember = "NationalityId";
            CboCustNationality.DataSource = Nationalities;
            CboCustNationality.DisplayMember = "EnglishName";
            
            CboCustNationality.SelectedIndex = -1;

           

            List<clsCountry> Countries = countryConnection.CountryArrayList();
            CboCustCountry.ValueMember = "countryId";
            CboCustCountry.DataSource = Countries;
            CboCustCountry.DisplayMember = "EnglishName";
            CboCustCountry.SelectedIndex = -1;


            List<clsType> customerType = new List<clsType>();
            customerType.Add(new clsType(0, "Public"));
            customerType.Add(new clsType(1, "Guest"));
            customerType.Add(new clsType(2, "Group"));

            CboCustType.ValueMember = "TypeId";
            CboCustType.DataSource = customerType;
            CboCustType.DisplayMember = "TypeName";
            CboCustType.SelectedIndex = -1;

            List<clsStatus> customerStatus = new List<clsStatus>();
            customerStatus.Add(new clsStatus(0, "Available"));
            customerStatus.Add(new clsStatus(1, "un Available"));

            CboCustStatus.ValueMember = "StatusId";
            CboCustStatus.DataSource = customerStatus;
            CboCustStatus.DisplayMember = "StatusName";
            CboCustStatus.SelectedIndex = -1;

           

            DataChanged = false;

        }

        private void cmbCustomerName_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.KeyCode != Keys.Enter || e.KeyCode != Keys.Tab)
            {
                
                DataChanged = true;
            }
        }
        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (iamAdding)
            {
                DataChanged = true;
                return;
            }
              
        if (skip)
            {
              skip = false;
               
                return;
            }
           
            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                DataChanged = true;
                return;
            }

       
            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                DataChanged = true;
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {


               

                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
            
          
    

        }
        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
         

            if (cmbCustomerName.SelectedIndex != -1&&customerList.Count>0)
            {
                lbResult.Visible = false;
                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                customerData = customerList[cmbCustomerName.SelectedIndex];
                TxtCustomerId.Text = customerData.CustomerId.ToString();
                CboCustNationality.SelectedValue = customerData.NationalityId;
              
                CboCustCountry.SelectedValue = customerData.CountryId;
                if (CboCustCountry.SelectedIndex != -1)
                {

                    List<clsCity> Cities = cityConnection.GetCitiesByCountryId(int.Parse(CboCustCountry.SelectedValue.ToString()));
                    CboCustCity.ValueMember = "CityId";
                    CboCustCity.DataSource = Cities;
                    CboCustCity.DisplayMember = "EnglishName";


                }
                CboCustCity.SelectedValue = customerData.CityId;


                CboCustType.SelectedValue = customerData.CustomerType;
                CboCustStatus.SelectedValue = customerData.Status;
                TxtCustAddress.Text = customerData.Address;
                dtpBirthDate.Value = customerData.BirthDate;
                TxtCustBirthPlace.Text = customerData.BirthPlace;
                TxtCustMobileNo.Text = customerData.MobilePhone.ToString();
                TxtCustLandPhoneNo.Text = customerData.LandPhone;
                TxtCustFaxNo.Text = customerData.FaxNo.ToString();
                TxtCustEmail.Text = customerData.Email;
                TxtCustWebSite.Text = customerData.WebSite;
                skip = true;
                DataChanged = false;
            
            }
            else
            {
                skip = false;
            }
         
          


           

        }
        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }
        private void SaveDataInfo()
        {
           
           
            
            bool RecExists;
            try
            {
                //int selectedvalue;
                clsCustomer customer = new clsCustomer();
                customer.CustomerId = CustomerID;

                if (string.IsNullOrWhiteSpace(cmbCustomerName.Text))
                {
                    customer.CustomerName = string.Empty;
                }
                else
                {
                    customer.CustomerName = cmbCustomerName.Text;
                }

                if (CboCustNationality.SelectedIndex==-1)
                {
                    customer.NationalityId = 0;
                }
                else
                {
                    customer.NationalityId = int.Parse(CboCustNationality.SelectedValue.ToString());
                }

                if (CboCustCity.SelectedIndex == -1)
                {
                    customer.CityId = 0;
                }
                else
                {
                    customer.CityId = int.Parse(CboCustCity.SelectedValue.ToString());
                }

                if (CboCustCountry.SelectedIndex == -1)
                {
                    customer.CountryId = 0;
                }
                else
                {
                    customer.CountryId = int.Parse(CboCustCountry.SelectedValue.ToString());
                }

                if (CboCustType.SelectedIndex == -1)
                {
                    customer.CustomerType = 0;
                }
                else
                {
                    customer.CustomerType  = int.Parse(CboCustType.SelectedValue.ToString());
                }


                if (CboCustStatus.SelectedIndex == -1)
                {
                    customer.Status = 0;
                }
                else
                {
                    customer.Status = int.Parse(CboCustStatus.SelectedValue.ToString());
                }

                if (string.IsNullOrWhiteSpace(TxtCustAddress.Text))
                {
                  customer.Address = string.Empty;
                }
                else
                {
                    customer.Address = TxtCustAddress.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustBirthPlace.Text))
                {
                    customer.BirthPlace = string.Empty;
                }
                else
                {
                    customer.BirthPlace = TxtCustBirthPlace.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustMobileNo.Text))
                {
                    customer.MobilePhone = string.Empty;
                }
                else
                {
                    customer.MobilePhone = TxtCustMobileNo.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustLandPhoneNo.Text))
                {
                    customer.LandPhone = string.Empty;
                }
                else
                {
                    customer.LandPhone = TxtCustLandPhoneNo.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustFaxNo.Text))
                {
                    customer.FaxNo = string.Empty;
                }
                else
                {
                    customer.FaxNo = TxtCustFaxNo.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustEmail.Text))
                {
                    customer.Email = string.Empty;
                }
                else
                {
                    customer.Email = TxtCustEmail.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtCustWebSite.Text))
                {
                    customer.WebSite = string.Empty;
                }
                else
                {
                    customer.WebSite = TxtCustWebSite.Text;
                }


                customer.BirthDate = dtpBirthDate.Value;




                Customer customerConnection = new Customer();

                RecExists = customerConnection.CusomterIDExists(customer.CustomerId);
              
                   
                if (RecExists == true)
                {
                    customerConnection.UpdateCustomerInfo(customer);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    iamUpdate = true;

                }
                else
                {
                   
                    iamAdding = true;
                    customerConnection.AddNewCustomer(customer);
                    stsLblInfo.Text = "New Customer has been added successfully";

                }
                

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
               

                customerList = customerConnection.CustomersArrayList();
      

                if (iamUpdate)
                {
                    DisplayData(customer.CustomerId);
                }
               if(iamAdding)
                {
                    iamAdding = false;
                    DisplayData(customerList[customerList.Count - 1].CustomerId);
                }
          
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }


        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Customer";

            if (canAddOnly)
            {
                btnSave.Enabled = true;
                unReadOnlyEveryThing();
            }

            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
            iamAdding = true;
            saveID = CustomerID;
            CustomerID = 0;
            ResetFields();
            cmbCustomerName.DataSource = null;
            btnCancel.Visible = true;
                  DataChanged = false;

        }

       

        private void TxtCustAddress_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
         
        }

        private void dtpBirthDate_ValueChanged(object sender, EventArgs e)
        {
            DataChanged = true;
         
        }

        private void TxtCustBirthPlace_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
          
        }

        private void TxtCustMobileNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void TxtCustLandPhoneNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
          
        }

        private void TxtCustFaxNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void TxtCustEmail_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void TxtCustWebSite_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {

            if (DataChanged)
            {
                 
              
               btnCancel.Visible = false;
                if (canUpdateOnly && this.CustomerID == 0)
                {
                    return;
                }
                SaveDataInfo();

                if (canAddOnly)
                {
                    btnSave.Enabled = false;
                    readOnlyEveryThing();
                }

            }


        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ResetFields();
            btnCancel.Visible = false;
            iamAdding = false;
            CustomerID = saveID;
            DisplayData(CustomerID);
            DataChanged = false;
            if (canAddOnly)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }

        }

        private void CboCustNationality_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void CboCustType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void CboCustCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CboCustCountry.SelectedIndex != -1)
            {

                List<clsCity> Cities = cityConnection.GetCitiesByCountryId(int.Parse(CboCustCountry.SelectedValue.ToString()));
                CboCustCity.ValueMember = "CityId";
                CboCustCity.DataSource = Cities;
                CboCustCity.DisplayMember = "EnglishName";
                if (customerData != null)
                    CboCustCity.SelectedValue = customerData.CityId;
            }
            DataChanged = true;
        
          
        }

        private void CboCustCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
           
        }

        private void CboCustStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
          
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (CustomerID != 0)
            {


                try
                {
                    bool myResult;
                    SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReply = MessageBox.Show("Are you sure ?", "Delete Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        if (customerConnection.CustomerUsed(CustomerID))
                        {
                            MessageBox.Show("This Customer Is Used In Some Operations Can't be Deleted");
                            return;
                        }

                        myResult = customerConnection.DeleteCustomer(CustomerID);
                        if (myResult == false)
                        {
                            return;
                        }
                        notifySound.Play();
                        ResetFields();
               
                        customerList = customerConnection.CustomersArrayList();

                        if (customerList.Count > 0)
                        {
                            DisplayData(customerList[customerList.Count - 1].CustomerId);
                        }
                        else
                            CustomerID = 0;

                        lbResult.Visible = false;
                        
                        DataChanged = false;
                        stsLblInfo.Text = "Successfully Deleted";
                    }
                }
                catch (Exception ex)
                {
                    
                    DataChanged = false;
                    stsLblInfo.Text = "ERROR: Problem Deleting the Customer profile";
                    MessageBox.Show(ex.Message);
                }
              

            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }

        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if ((cmbCustomerName.Items.Count > 0))
            {
                if (cmbCustomerName.SelectedIndex ==0 || cmbCustomerName.SelectedIndex == -1)
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.Items.Count-1;
                }
                else
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.SelectedIndex-1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if((cmbCustomerName.Items.Count > 0))
            {
                if (cmbCustomerName.SelectedIndex == cmbCustomerName.Items.Count - 1 || cmbCustomerName.SelectedIndex == -1)
                {
                    cmbCustomerName.SelectedIndex = 0;
                }
                else
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.SelectedIndex + 1;
                }
            }
           
        }

        private void TxtCustMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void TxtCustLandPhoneNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void TxtCustFaxNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }

        }

        private void frmCustomer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbCustomerName.SelectedIndex != -1)
            {
                CustomerID = Convert.ToInt32(cmbCustomerName.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void CboCustNationality_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (CboCustNationality.SelectedIndex != -1)
                id = int.Parse(CboCustNationality.SelectedValue.ToString());
            ArrayList Nationalities = nationalityConnection.NationalitiesArrayList();
            CboCustNationality.ValueMember = "NationalityId";
            CboCustNationality.DataSource = Nationalities;
            CboCustNationality.DisplayMember = "EnglishName";
            CboCustNationality.SelectedValue = id;
            DataChanged = false;
        }

        private void CboCustCountry_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (CboCustCountry.SelectedIndex != -1)
                id = int.Parse(CboCustCountry.SelectedValue.ToString());
            List<clsCountry> Countries = countryConnection.CountryArrayList();
            CboCustCountry.DataSource = Countries;
            CboCustCountry.DisplayMember = "EnglishName";
            CboCustCountry.ValueMember = "countryId";
            CboCustCountry.SelectedValue = id;
            DataChanged = false;

        }

        private void CboCustCity_Click(object sender, EventArgs e)
        {

            int countryid = 0;
            if (CboCustCountry.SelectedIndex != -1)
            {
                countryid = int.Parse(CboCustCountry.SelectedValue.ToString());
            }
            int id = 0;
            if (CboCustCity.SelectedIndex != -1)
                id = int.Parse(CboCustCity.SelectedValue.ToString());
            List<clsCity> Cities = cityConnection.GetCitiesByCountryId(countryid);
            CboCustCity.ValueMember = "CityId";
            CboCustCity.DataSource = Cities;
            CboCustCity.DisplayMember = "EnglishName";
            CboCustCity.SelectedValue = id;
            DataChanged = false;


        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                btnAdd.Enabled = false;
            }
            if (!right.Deleting)
            {
                btnDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbCustomerName.DropDownStyle = ComboBoxStyle.DropDownList;
            CboCustNationality.Enabled = false;
            CboCustType.Enabled = false;
            CboCustCountry.Enabled = false;
            CboCustCity.Enabled = false;
            CboCustStatus.Enabled = false;
            dtpBirthDate.Enabled = false;
            TxtCustomerId.ReadOnly = true;
            TxtCustAddress.ReadOnly = true;
            TxtCustBirthPlace.ReadOnly = true;
            TxtCustMobileNo.ReadOnly = true;
            TxtCustLandPhoneNo.ReadOnly = true;
            TxtCustFaxNo.ReadOnly = true;
            TxtCustEmail.ReadOnly = true;
            TxtCustWebSite.ReadOnly = true;



        }

        private void unReadOnlyEveryThing()
        {
            cmbCustomerName.DropDownStyle = ComboBoxStyle.DropDown;
            CboCustNationality.Enabled = true;
            CboCustType.Enabled = true;
            CboCustCountry.Enabled = true;
            CboCustCity.Enabled = true;
            CboCustStatus.Enabled = true;
            dtpBirthDate.Enabled = true;
            //TxtCustomerId.ReadOnly = false;
            TxtCustAddress.ReadOnly = false;
            TxtCustBirthPlace.ReadOnly = false;
            TxtCustMobileNo.ReadOnly = false;
            TxtCustLandPhoneNo.ReadOnly = false;
            TxtCustFaxNo.ReadOnly = false;
            TxtCustEmail.ReadOnly = false;
            TxtCustWebSite.ReadOnly = false;
        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void TxtCustomerId_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmCustomer_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
