﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using SerhanTravel.Print;
using SerhanTravel.Connections;
using System.Reflection;

namespace SerhanTravel.Forms
{
    public partial class frmCustomerReturnedPackage : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList;
        Customer customerConnection;
        List<clsPackageInvoice> CustomerInvoiceList;
        List<clsPackage> packageList;
        List<int> packagesId = new List<int>();
        List<int> itemsId = new List<int>();
        PackageInvoice invoiceConnection;
        clsPackageInvoice invoiceData;
        List<clsReturnedCustomerPackages> returnedPackages;
        ReturnedCustomerPackage returnedPackageConnection;
        int InvoiceId = 0;
        private List<clsCurrency> currencies;
        decimal netPrice = 0;
        private bool allowRemove = false;
        string currencyFormat;
        clsAccessRights right;
        Currency currencyConnection;
        internal frmCustomerReturnedPackage(clsAccessRights right)
        {
            InitializeComponent();
            dgvPackages.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPackages.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPackages.MultiSelect = false;
            chLbPackageItems.HorizontalScrollbar = true;
            //chLbCustomerTickets.ScrollAlwaysVisible = true;
            dgvPackages.ScrollBars = ScrollBars.Both;
             this.right = right;

        }

        private void ResetFields()
        {
            InvoiceId = 0;
            
            dgvPackages.DataSource = null;
            chLbPackageItems.DataSource = null;
            lbPackages.DataSource = null;
            dgvPackages.Rows.Clear();
            txtReturnedAmount.Clear();
            txtInvoicePrice.Clear();
            txtnetPrice.Clear();
            dtpReturnDate.Value = DateTime.Today;
            lblNetPriceSymbol.Text = "";
            lblInvoicePriceSymbol.Text = "";
            lblReturnSymbol.Text = "";
            stsLblInfo.Text = "";
            packageList.Clear();
            returnedPackages.Clear();
            packagesId.Clear();
            itemsId.Clear();
            netPrice = 0;
           
        }

        private void frmCustomerReturnedPackage_Load(object sender, EventArgs e)
        {
             accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            currencyConnection = new Currency();
            dtpReturnDate.Value = DateTime.Today;
            packageList = new List<clsPackage>();
            invoiceConnection = new PackageInvoice();
            returnedPackages = new List<clsReturnedCustomerPackages>();
            customerConnection = new Customer();
            returnedPackageConnection = new ReturnedCustomerPackage();
            dgvPackages.AutoGenerateColumns = false;
            FillCombobox();
            ResetFields();
            InvoiceId = 0;
         

        }

        private void DisplayData(int invoiceId)
        {
            cmbInvoceNo.DataSource = null;
            cmbInvoceNo.ValueMember = "InvoiceId";
            cmbInvoceNo.DataSource = CustomerInvoiceList;
            cmbInvoceNo.DisplayMember = "InvoiceId";
          
        }

        private void FillCombobox()
        {
            customerList = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);
            cmbCustomer.ValueMember = "CustomerId";
            cmbCustomer.DataSource = customerList;
            cmbCustomer.DisplayMember = "CustomerName";
            cmbCustomer.SelectedIndex = -1;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
           
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbInvoceNo.DataSource = null;
            ResetFields();
            if (cmbCustomer.SelectedIndex != -1 && customerList.Count > 0)
            {
                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(int.Parse(cmbCustomer.SelectedValue.ToString()));
                cmbInvoceNo.ValueMember = "InvoiceId";
                cmbInvoceNo.DataSource = CustomerInvoiceList;
                cmbInvoceNo.DisplayMember = "InvoiceId";
                cmbInvoceNo.SelectedIndex = -1;
              
            }
        }
        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            ResetFields();
            if (cmbInvoceNo.SelectedIndex != -1 && CustomerInvoiceList.Count > 0)
            {

                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                invoiceData = CustomerInvoiceList[cmbInvoceNo.SelectedIndex];
                packageList = invoiceConnection.PackagesInvoicesArrayList(InvoiceId);
                netPrice = (invoiceData.NetPrice-(invoiceData.Discount*invoiceData.NetPrice*Convert.ToDecimal(0.01)));
                lbPackages.ValueMember = "packageID";
                lbPackages.DataSource = packageList;
                lbPackages.DisplayMember = "PackageDescription";
                returnedPackages = returnedPackageConnection.getReturnedPackage(InvoiceId);
               
                for (int i = 0; i < returnedPackages.Count; i++)
                {
                   
                    netPrice -= (returnedPackages[i].ReturnAmount * returnedPackages[i].CurrencyRate);
                    if (returnedPackages[i].ItemId == 0)
                    {
                        clsPackage package = (clsPackage)returnedPackages[i].Item;
                        packagesId.Add(package.packageID);
                        dgvPackages.Rows.Add(returnedPackages[i].ReturnedItem, returnedPackages[i].Customer, package.packageName, (package.Price*package.CurrencyRate/returnedPackages[i].CurrencyRate).ToString(currencyFormat) + " " + returnedPackages[i].CurrecnySymbol, returnedPackages[i].ReturnAmount.ToString(currencyFormat)+" "+ returnedPackages[i].CurrecnySymbol);
                        dgvPackages.Refresh();
                        dgvPackages.ClearSelection();
                    }
                    else
                    {
                        clsItem item = (clsItem)returnedPackages[i].Item;
                        itemsId.Add(returnedPackages[i].ItemId);
                        dgvPackages.Rows.Add(returnedPackages[i].ReturnedItem, returnedPackages[i].Customer, item.itemName, "-", returnedPackages[i].ReturnAmount.ToString(currencyFormat) + " "+item.CurrecnySymbol);
                        dgvPackages.Refresh();
                        dgvPackages.ClearSelection();
                    }
                 
                }

            
               

                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;

                    txtInvoicePrice.Text = ((invoiceData.NetPrice - (invoiceData.Discount * invoiceData.NetPrice * Convert.ToDecimal(0.01)))/ currency.CurrencyRate).ToString(currencyFormat);
                    txtnetPrice.Text = (netPrice / Convert.ToDecimal(currency.CurrencyRate)).ToString(currencyFormat);
                    lblInvoicePriceSymbol.Text = currency.CurrencySymbol;
                    lblNetPriceSymbol.Text = currency.CurrencySymbol;
                    lblReturnSymbol.Text = currency.CurrencySymbol;
                }
                else
                {

                    txtInvoicePrice.Text = (invoiceData.NetPrice - (invoiceData.Discount * invoiceData.NetPrice * Convert.ToDecimal(0.01))).ToString(currencyFormat);
                    txtnetPrice.Text = netPrice.ToString(currencyFormat);
                    lblInvoicePriceSymbol.Text = "L.L";
                    lblNetPriceSymbol.Text = "L.L";
                    lblReturnSymbol.Text = "L.L";
                }
            }
           

        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCurrency.SelectedIndex!=-1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
            }
            if (cmbCurrency.SelectedIndex != -1 && invoiceData != null)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                txtnetPrice.Text = (netPrice / Convert.ToDecimal(currency.CurrencyRate)).ToString(currencyFormat);
                txtInvoicePrice.Text = ((invoiceData.NetPrice - (invoiceData.Discount * invoiceData.NetPrice * Convert.ToDecimal(0.01))) / currency.CurrencyRate).ToString(currencyFormat);
                lblNetPriceSymbol.Text = currency.CurrencySymbol;
                lblInvoicePriceSymbol.Text = currency.CurrencySymbol;
                lblReturnSymbol.Text = currency.CurrencySymbol;

            }
            else if (invoiceData != null)
            {
             
                txtnetPrice.Text = (netPrice).ToString(currencyFormat);
                txtInvoicePrice.Text = (invoiceData.NetPrice - (invoiceData.Discount * invoiceData.NetPrice * Convert.ToDecimal(0.01))).ToString(currencyFormat);
                lblNetPriceSymbol.Text = "L.L";
                lblInvoicePriceSymbol.Text = "L.L";
                lblReturnSymbol.Text = "L.L";

            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(lbPackages.SelectedIndex==-1)
            {
                return;
            }
            if(string.IsNullOrWhiteSpace(txtReturnedAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount to be Return to Continue...");
                return;
            }

            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency...");
                return;
            }
            clsReturnedCustomerPackages returnedItem = new clsReturnedCustomerPackages();
            clsPackage package = (clsPackage)lbPackages.SelectedItem;
            clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
            decimal returnAmount = Convert.ToDecimal(txtReturnedAmount.Text);
            returnedItem.InvoiceId = InvoiceId;
            returnedItem.Date =dtpReturnDate.Value ;
            returnedItem.Package = package;
            returnedItem.PackageId = package.packageID;
            returnedItem.Customer = package.CustomerName;
            if (chLbPackageItems.CheckedItems.Count<=0)
            {
                try
                {

                    returnedItem.CurrencyId = package.CurrencyId;
                    returnedItem.CurrencyRate = package.CurrencyRate;
                    returnedItem.ItemId = 0;
                    if (currency.CurrencyId != package.CurrencyId)
                    {
                        returnAmount *= currency.CurrencyRate;
                        returnAmount /= package.CurrencyRate;
                    }
                    returnedItem.ReturnAmount = returnAmount;
                    returnedPackageConnection.AddReturnedItem(returnedItem);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    simpleSound.Play();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                    simpleSound.Play();
                }
            }
            else
            {
                foreach (clsItem item in chLbPackageItems.CheckedItems)
                {

                    try
                    {
                         returnAmount = Convert.ToDecimal(txtReturnedAmount.Text);
                        if (currency.CurrencyId != item.CurrencyId)
                        {
                            returnAmount *= currency.CurrencyRate;
                            returnAmount /= item.CurrencyRate;
                        }
                        returnedItem.CurrencyRate = item.CurrencyRate;
                        returnedItem.CurrencyId = item.CurrencyId;
                        returnedItem.ReturnAmount = returnAmount;
                        returnedItem.ItemId = item.itemID;
                        returnedPackageConnection.AddReturnedItem(returnedItem);
                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                        simpleSound.Play();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                        simpleSound.Play();
                    }

                }
            }
           
            DisplayData(InvoiceId);
            txtReturnedAmount.Clear();
            btnAdd.Visible = false;

        }
 

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvPackages.RowCount > 0 && returnedPackages.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvPackages.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                int index = dgvPackages.SelectedRows[0].Index;
                if (index >= returnedPackages.Count || index < 0)
                {
                    return;
                }

                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    try
                    {
                        clsReturnedCustomerPackages returnItem = returnedPackages[index];
                        returnedPackageConnection.DeleteReturnedTickets(returnItem.Id);
                        DisplayData(InvoiceId);
                    }
                    catch (Exception eex)
                    {
                        MessageBox.Show(eex.Message);
                    }
                }

            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }


        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == 0 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.Items.Count - 1;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {

            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == cmbInvoceNo.Items.Count - 1 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = 0;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex + 1;
                }
            }
        }





        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

          private void accessRight()
          {
              if (!right.Adding)
              {
                  btnAdd.Enabled = false;
              }
              if (!right.Deleting)
              {
                  btnRemove.Enabled = false;
              }
             
          }

        private void chLbCustomerTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chLbPackageItems.SelectedIndex != -1)
            {
                btnAdd.Visible = true;
            }
        }

        private void txtReturnedAmount_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtReturnedAmount.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }


        private void dgvTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPackages.RowCount > 0)
            {

                if (e.RowIndex >= 0)
                {

                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void lbPackages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbPackages.SelectedIndex != -1)
            {
               
                btnRemove.Visible = false;
                btnAdd.Visible = true;
           
                dgvPackages.ClearSelection();
                clsPackage package = (clsPackage)lbPackages.SelectedItem;
                chLbPackageItems.ValueMember = "itemId";
                chLbPackageItems.DataSource = package.packageItems;
                chLbPackageItems.DisplayMember = "ToStrings";
                chLbPackageItems.ClearSelected();
            }
        }
    }
}
