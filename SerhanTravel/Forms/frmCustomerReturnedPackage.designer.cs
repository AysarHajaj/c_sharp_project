﻿namespace SerhanTravel.Forms
{
    partial class frmCustomerReturnedPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomerReturnedPackage));
            this.lblNetPriceSymbol = new System.Windows.Forms.Label();
            this.txtnetPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblInvoicePriceSymbol = new System.Windows.Forms.Label();
            this.txtInvoicePrice = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.cmbInvoceNo = new System.Windows.Forms.ComboBox();
            this.dtpReturnDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.chLbPackageItems = new System.Windows.Forms.CheckedListBox();
            this.dgvPackages = new System.Windows.Forms.DataGridView();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnedAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.stsLblInfo = new System.Windows.Forms.Label();
            this.txtReturnedAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblReturnSymbol = new System.Windows.Forms.Label();
            this.lbPackages = new System.Windows.Forms.ListBox();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPackages)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNetPriceSymbol
            // 
            this.lblNetPriceSymbol.AutoSize = true;
            this.lblNetPriceSymbol.Location = new System.Drawing.Point(265, 87);
            this.lblNetPriceSymbol.Name = "lblNetPriceSymbol";
            this.lblNetPriceSymbol.Size = new System.Drawing.Size(0, 15);
            this.lblNetPriceSymbol.TabIndex = 97;
            // 
            // txtnetPrice
            // 
            this.txtnetPrice.AccessibleDescription = "txtSerNo";
            this.txtnetPrice.AccessibleName = "txtSerNo";
            this.txtnetPrice.Location = new System.Drawing.Point(116, 82);
            this.txtnetPrice.Name = "txtnetPrice";
            this.txtnetPrice.ReadOnly = true;
            this.txtnetPrice.Size = new System.Drawing.Size(143, 22);
            this.txtnetPrice.TabIndex = 95;
            // 
            // label6
            // 
            this.label6.AccessibleDescription = "lblSerNo";
            this.label6.AccessibleName = "lblSerNo";
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 15);
            this.label6.TabIndex = 94;
            this.label6.Text = "Remained Amount";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel2.Controls.Add(this.lblNetPriceSymbol);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cmbCurrency);
            this.panel2.Controls.Add(this.txtnetPrice);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.Label12);
            this.panel2.Controls.Add(this.txtRate);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(380, 65);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(287, 132);
            this.panel2.TabIndex = 117;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 90;
            this.label8.Text = "Rate";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(116, 21);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(143, 23);
            this.cmbCurrency.TabIndex = 60;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(3, 22);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 59;
            this.Label12.Text = "Currency";
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(116, 52);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(143, 22);
            this.txtRate.TabIndex = 61;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel3.Controls.Add(this.lblInvoicePriceSymbol);
            this.panel3.Controls.Add(this.txtInvoicePrice);
            this.panel3.Controls.Add(this.Label1);
            this.panel3.Controls.Add(this.Label7);
            this.panel3.Controls.Add(this.lblInvoiceNo);
            this.panel3.Controls.Add(this.cmbInvoceNo);
            this.panel3.Controls.Add(this.dtpReturnDate);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cmbCustomer);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(50, 65);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(324, 132);
            this.panel3.TabIndex = 116;
            // 
            // lblInvoicePriceSymbol
            // 
            this.lblInvoicePriceSymbol.AutoSize = true;
            this.lblInvoicePriceSymbol.Location = new System.Drawing.Point(261, 101);
            this.lblInvoicePriceSymbol.Name = "lblInvoicePriceSymbol";
            this.lblInvoicePriceSymbol.Size = new System.Drawing.Size(0, 15);
            this.lblInvoicePriceSymbol.TabIndex = 98;
            // 
            // txtInvoicePrice
            // 
            this.txtInvoicePrice.Location = new System.Drawing.Point(105, 98);
            this.txtInvoicePrice.Name = "txtInvoicePrice";
            this.txtInvoicePrice.Size = new System.Drawing.Size(135, 22);
            this.txtInvoicePrice.TabIndex = 73;
            // 
            // Label1
            // 
            this.Label1.AccessibleDescription = "lblSerNo";
            this.Label1.AccessibleName = "lblSerNo";
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(3, 76);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(77, 15);
            this.Label1.TabIndex = 53;
            this.Label1.Text = " Return Date";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(5, 17);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 15);
            this.Label7.TabIndex = 55;
            this.Label7.Text = "Customer";
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AccessibleDescription = "lblSerNo";
            this.lblInvoiceNo.AccessibleName = "lblSerNo";
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(5, 46);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(66, 15);
            this.lblInvoiceNo.TabIndex = 51;
            this.lblInvoiceNo.Text = "Invoice No.";
            // 
            // cmbInvoceNo
            // 
            this.cmbInvoceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbInvoceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbInvoceNo.FormattingEnabled = true;
            this.cmbInvoceNo.Location = new System.Drawing.Point(105, 43);
            this.cmbInvoceNo.Name = "cmbInvoceNo";
            this.cmbInvoceNo.Size = new System.Drawing.Size(199, 23);
            this.cmbInvoceNo.TabIndex = 70;
            this.cmbInvoceNo.SelectedIndexChanged += new System.EventHandler(this.cmbInvoceNo_SelectedIndexChanged);
            // 
            // dtpReturnDate
            // 
            this.dtpReturnDate.Location = new System.Drawing.Point(104, 70);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new System.Drawing.Size(200, 22);
            this.dtpReturnDate.TabIndex = 71;
            // 
            // label3
            // 
            this.label3.AccessibleDescription = "lblSerNo";
            this.label3.AccessibleName = "lblSerNo";
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 15);
            this.label3.TabIndex = 74;
            this.label3.Text = "Invoice Price";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(105, 14);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(199, 23);
            this.cmbCustomer.TabIndex = 77;
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(800, 483);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(99, 32);
            this.btnAdd.TabIndex = 115;
            this.btnAdd.Text = "Add ";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Red;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(122, 483);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(112, 32);
            this.btnRemove.TabIndex = 114;
            this.btnRemove.Text = "Remove ";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // chLbPackageItems
            // 
            this.chLbPackageItems.BackColor = System.Drawing.Color.Gray;
            this.chLbPackageItems.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLbPackageItems.ForeColor = System.Drawing.Color.White;
            this.chLbPackageItems.FormattingEnabled = true;
            this.chLbPackageItems.Location = new System.Drawing.Point(676, 65);
            this.chLbPackageItems.Name = "chLbPackageItems";
            this.chLbPackageItems.Size = new System.Drawing.Size(260, 361);
            this.chLbPackageItems.TabIndex = 113;
            this.chLbPackageItems.SelectedIndexChanged += new System.EventHandler(this.chLbCustomerTickets_SelectedIndexChanged);
            // 
            // dgvPackages
            // 
            this.dgvPackages.AllowUserToAddRows = false;
            this.dgvPackages.AllowUserToDeleteRows = false;
            this.dgvPackages.AllowUserToOrderColumns = true;
            this.dgvPackages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPackages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Type,
            this.Customer,
            this.ItemName,
            this.Price,
            this.ReturnedAmount});
            this.dgvPackages.Location = new System.Drawing.Point(45, 273);
            this.dgvPackages.MultiSelect = false;
            this.dgvPackages.Name = "dgvPackages";
            this.dgvPackages.ReadOnly = true;
            this.dgvPackages.Size = new System.Drawing.Size(556, 144);
            this.dgvPackages.TabIndex = 112;
            this.dgvPackages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTickets_CellClick);
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Customer
            // 
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            // 
            // ItemName
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemName.DefaultCellStyle = dataGridViewCellStyle4;
            this.ItemName.HeaderText = "Name";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // Price
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Price.DefaultCellStyle = dataGridViewCellStyle5;
            this.Price.HeaderText = "Price ";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // ReturnedAmount
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReturnedAmount.DefaultCellStyle = dataGridViewCellStyle6;
            this.ReturnedAmount.HeaderText = "Returned Amount";
            this.ReturnedAmount.Name = "ReturnedAmount";
            this.ReturnedAmount.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AccessibleDescription = "TStrip";
            this.toolStrip1.AccessibleName = "TStrip";
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.NavajoWhite;
            this.toolStrip1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripBackword,
            this.TStripForword});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1212, 58);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 120;
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.stsLblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stsLblInfo.Location = new System.Drawing.Point(0, 526);
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(1212, 21);
            this.stsLblInfo.TabIndex = 121;
            this.stsLblInfo.Text = "Returned Info";
            this.stsLblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtReturnedAmount
            // 
            this.txtReturnedAmount.Location = new System.Drawing.Point(676, 444);
            this.txtReturnedAmount.Name = "txtReturnedAmount";
            this.txtReturnedAmount.Size = new System.Drawing.Size(135, 20);
            this.txtReturnedAmount.TabIndex = 123;
            this.txtReturnedAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReturnedAmount_KeyPress);
            // 
            // label2
            // 
            this.label2.AccessibleDescription = "lblSerNo";
            this.label2.AccessibleName = "lblSerNo";
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(506, 445);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 15);
            this.label2.TabIndex = 124;
            this.label2.Text = "Amount to be Returned";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(46, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 22);
            this.label4.TabIndex = 113;
            this.label4.Text = "Returned Packages /Items";
            // 
            // lblReturnSymbol
            // 
            this.lblReturnSymbol.AutoSize = true;
            this.lblReturnSymbol.Location = new System.Drawing.Point(832, 447);
            this.lblReturnSymbol.Name = "lblReturnSymbol";
            this.lblReturnSymbol.Size = new System.Drawing.Size(0, 13);
            this.lblReturnSymbol.TabIndex = 125;
            // 
            // lbPackages
            // 
            this.lbPackages.BackColor = System.Drawing.Color.Gray;
            this.lbPackages.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.lbPackages.ForeColor = System.Drawing.Color.White;
            this.lbPackages.FormattingEnabled = true;
            this.lbPackages.ItemHeight = 15;
            this.lbPackages.Location = new System.Drawing.Point(942, 66);
            this.lbPackages.Name = "lbPackages";
            this.lbPackages.Size = new System.Drawing.Size(260, 349);
            this.lbPackages.TabIndex = 126;
            this.lbPackages.SelectedIndexChanged += new System.EventHandler(this.lbPackages_SelectedIndexChanged);
            // 
            // frmCustomerReturnedPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(1212, 547);
            this.Controls.Add(this.lbPackages);
            this.Controls.Add(this.lblReturnSymbol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvPackages);
            this.Controls.Add(this.txtReturnedAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.stsLblInfo);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.chLbPackageItems);
            this.Name = "frmCustomerReturnedPackage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Returned Package";
            this.Load += new System.EventHandler(this.frmCustomerReturnedPackage_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPackages)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNetPriceSymbol;
        internal System.Windows.Forms.TextBox txtnetPrice;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtInvoicePrice;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label lblInvoiceNo;
        private System.Windows.Forms.ComboBox cmbInvoceNo;
        private System.Windows.Forms.DateTimePicker dtpReturnDate;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.CheckedListBox chLbPackageItems;
        internal System.Windows.Forms.DataGridView dgvPackages;
        internal System.Windows.Forms.ToolStrip toolStrip1;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripButton TStripForword;
        internal System.Windows.Forms.Label stsLblInfo;
        private System.Windows.Forms.TextBox txtReturnedAmount;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblInvoicePriceSymbol;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblReturnSymbol;
        private System.Windows.Forms.ListBox lbPackages;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnedAmount;
    }
}