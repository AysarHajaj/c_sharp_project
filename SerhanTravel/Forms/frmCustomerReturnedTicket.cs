﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using SerhanTravel.Print;
using SerhanTravel.Connections;
using System.Reflection;

namespace SerhanTravel.Forms
{
    public partial class frmCustomerReturnedTicket : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList;
        Customer customerConnection;
        List<clsTickets> ticketList;
        List<clsReturnedCustomerTicket> returnedTickets;
        ReturnedCustomerTicket returnedTicketConnection;
        private List<clsCurrency> currencies;
        private bool allowRemove = false;
        string currencyFormat;
        clsAccessRights right;
        private bool skip;
        Currency currencyConnection;
        internal frmCustomerReturnedTicket(clsAccessRights right)
        {
            InitializeComponent();
            dgvTickets.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvTickets.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvTickets.MultiSelect = false;
            chLbCustomerTickets.HorizontalScrollbar = true;
            dgvTickets.ScrollBars = ScrollBars.Both;
            this.right = right;

        }

        private void ResetFields()
        {
          
            dgvTickets.DataSource = null;
            chLbCustomerTickets.DataSource = null;
            dgvTickets.Rows.Clear();
            txtReturnedAmount.Clear();
            stsLblInfo.Text = "";
            ticketList.Clear();
            returnedTickets.Clear();
        }
        private void frmCustomerReturnedTicket_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
           accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            ticketList = new List<clsTickets>();
            returnedTickets = new List<clsReturnedCustomerTicket>();
            customerConnection = new Customer();
            returnedTicketConnection = new ReturnedCustomerTicket();
            dgvTickets.AutoGenerateColumns = false;
            dgvTickets.Columns[0].DataPropertyName = "Ticket.TicketNo";
            dgvTickets.Columns[1].DataPropertyName = "Ticket.Destination";
            dgvTickets.Columns[2].DataPropertyName = "Ticket.Price";
            dgvTickets.Columns[3].DataPropertyName = "ReturnAmount";
            dgvTickets.Columns[4].DataPropertyName = "Remained";
            dgvTickets.Columns[5].DataPropertyName = "Ticket.CurrecnySymbol";
            dgvTickets.Columns[6].DataPropertyName = "Ticket.TravellerName";
            FillCombobox();
            skip = true;
            ResetFields();

        }

        private void DisplayData(int customId)
        {
            cmbCustomer.DataSource = null;
            cmbCustomer.ValueMember = "CustomerId";
            cmbCustomer.DataSource = customerList;
            cmbCustomer.DisplayMember = "CustomerName";
            cmbCustomer.SelectedValue = customId;
            skip = true;

        }

        private void FillCombobox()
        {
            customerList = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);
            cmbCustomer.ValueMember = "CustomerId";
            cmbCustomer.DataSource = customerList;
            cmbCustomer.DisplayMember = "CustomerName";
            cmbCustomer.SelectedIndex = -1;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
           
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            ResetFields();
            if (cmbCustomer.SelectedIndex != -1 && customerList.Count > 0)
            {
                ticketList = customerConnection.getUnReturnedTicekts(int.Parse(cmbCustomer.SelectedValue.ToString()));
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "ReturnString";
                returnedTickets = returnedTicketConnection.getReturnedTickets(int.Parse(cmbCustomer.SelectedValue.ToString()));
                dgvTickets.DataSource = returnedTickets;
                dgvTickets.Refresh();
                dgvTickets.ClearSelection();
                skip = true;
            }
        }
    

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                lblReturnSymbol.Text = currency.CurrencySymbol;
            }
            else 
            {
                lblReturnSymbol.Text = "";
                txtRate.Clear();
            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(chLbCustomerTickets.SelectedIndex==-1)
            {
                return;
            }
            if(string.IsNullOrWhiteSpace(txtReturnedAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount to be Return to Continue...");
                return;
            }
            if(cmbCurrency.SelectedIndex==-1)
            {
                MessageBox.Show("Please Select Currency...");
                return;
            }
         
            foreach (clsTickets tickets in chLbCustomerTickets.CheckedItems)
            {

                try
                {
                    clsTickets ticket = (clsTickets)tickets;
                  
                    clsReturnedCustomerTicket returnedTicket = new clsReturnedCustomerTicket();
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    decimal returnAmount= Convert.ToDecimal(txtReturnedAmount.Text);
                    if (currency.CurrencyId != ticket.CurrencyId)
                    {
                        returnAmount *= currency.CurrencyRate;
                        decimal ticketRate = ticket.CurrencyRate;
                        returnAmount /= ticketRate;
                    }
                    returnedTicket.CurrencyId = ticket.CurrencyId;
                    returnedTicket.Date = dtpReturnDate.Value;
                    returnedTicket.ReturnAmount = returnAmount;
                    returnedTicket.Ticket = ticket;
                    returnedTicket.CurrencyRate = ticket.CurrencyRate;
                    returnedTicketConnection.AddReturnedTicket(returnedTicket);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    simpleSound.Play();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                    simpleSound.Play();
                }
                
            }
            DisplayData(int.Parse(cmbCustomer.SelectedValue.ToString()));
            txtReturnedAmount.Clear();
            btnAdd.Visible = false;

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvTickets.RowCount > 0 && returnedTickets.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvTickets.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                int index = dgvTickets.SelectedRows[0].Index;
                if (index >= returnedTickets.Count || index < 0)
                {
                    return;
                }

                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    try
                    {
                        clsReturnedCustomerTicket ticket = returnedTickets[index];
                        returnedTicketConnection.DeleteReturnedTickets(ticket.Id);
                        DisplayData(int.Parse(cmbCustomer.SelectedValue.ToString()));
                    }
                    catch (Exception eex)
                    {
                        MessageBox.Show(eex.Message);
                    }
                }

            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex != -1)
            {
                ticketList = customerConnection.getUnReturnedTicekts(int.Parse(cmbCustomer.SelectedValue.ToString()));
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "ReturnString";
            }
        }






        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbCustomer.Items.Count > 0))
            {
                if (cmbCustomer.SelectedIndex == 0 || cmbCustomer.SelectedIndex == -1)
                {
                    cmbCustomer.SelectedIndex = cmbCustomer.Items.Count - 1;
                }
                else
                {
                    cmbCustomer.SelectedIndex = cmbCustomer.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {

            if ((cmbCustomer.Items.Count > 0))
            {
                if (cmbCustomer.SelectedIndex == cmbCustomer.Items.Count - 1 || cmbCustomer.SelectedIndex == -1)
                {
                    cmbCustomer.SelectedIndex = 0;
                }
                else
                {
                    cmbCustomer.SelectedIndex = cmbCustomer.SelectedIndex + 1;
                }
            }
        }





        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

         private void accessRight()
          {
              if (!right.Adding)
              {
                  btnAdd.Enabled = false;
              }
              if (!right.Deleting)
              {
                  btnRemove.Enabled = false;
              }
             
          }


        private void chLbCustomerTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chLbCustomerTickets.SelectedIndex != -1)
            {
                btnAdd.Visible = true;
            }
        }

        private void txtReturnedAmount_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtReturnedAmount.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }

         }


        private void dgvTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTickets.RowCount > 0)
            {

                if (e.RowIndex >= 0)
                {

                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void dgvTickets_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((dgvTickets.Rows[e.RowIndex].DataBoundItem != null) && (dgvTickets.Columns[e.ColumnIndex].DataPropertyName.Contains(".")))
                e.Value = BindProperty(dgvTickets.Rows[e.RowIndex].DataBoundItem, dgvTickets.Columns[e.ColumnIndex].DataPropertyName);
        }

        private string BindProperty(object property, string propertyName)
        {
            string retValue;

            retValue = "";

            if (propertyName.Contains("."))
            {
                PropertyInfo[] arrayProperties;
                string leftPropertyName;

                leftPropertyName = propertyName.Substring(0, propertyName.IndexOf("."));
                arrayProperties = property.GetType().GetProperties();

                foreach (PropertyInfo propertyInfo in arrayProperties)
                {
                    if (propertyInfo.Name == leftPropertyName)
                    {
                        retValue = BindProperty(propertyInfo.GetValue(property, null), propertyName.Substring(propertyName.IndexOf(".") + 1));
                        break;
                    }
                }
            }
            else
            {
                Type propertyType;
                PropertyInfo propertyInfo;

                propertyType = property.GetType();
                propertyInfo = propertyType.GetProperty(propertyName);
                retValue = propertyInfo.GetValue(property, null).ToString();
            }

            return retValue;
        }

        private void cmbCustomer_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbCustomer.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
             
                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
             
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }

        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbResult.SelectedIndex != -1)
            {
                cmbCustomer.SelectedItem = lbResult.SelectedItem;
            }
            lbResult.Visible = false;
        }

        private void txtTicketNo_TextChanged(object sender, EventArgs e)
        {
        
            string textToSearch = txtTicketNo.Text.ToLower();
            clsTickets[] result = (from i in ticketList
                                   where i.TicketString.ToLower().Contains(textToSearch)
                                   select i).ToArray();
            if (result.Length == 0)
            {
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "ReturnString";
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = result;
                chLbCustomerTickets.DisplayMember = "ReturnString";
            }

        }
    }
}
