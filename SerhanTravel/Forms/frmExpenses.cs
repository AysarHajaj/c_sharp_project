﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using System.Threading;
using SerhanTravel.Objects;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmExpenses : Form
    {

        int ExpensesId = 0;



        List<clsExpenses> expensesList;
        DateTime DefaultDate = new DateTime(1900, 1, 1);

        bool DataChange = false;
        clsExpenses expensesData;

        ExpensesType typeConnection;
        List<clsCurrency> currecncyList;
        List<clsExpensesType> typeList;
        Expenses expensesConnection;
        bool canUpdateOnly;
        Currency currencyConnection;
        int ExpensesSavedId;
        bool canAddOnly = false;
        clsAccessRights rights;
        private bool iamAdding;

        internal frmExpenses(clsAccessRights rights)
        {
            InitializeComponent();

              this.rights = rights;
        }

        private void frmExpenses_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            accessRight();
            currecncyList = currencyConnection.CurrencyArrayList();
            expensesConnection = new Expenses();
            typeConnection = new ExpensesType();
            expensesData = new clsExpenses();
            expensesData.ExpensesDate = DateTime.Today;
            dtpExpensesDate.Value = DateTime.Today;
   
            FillComboBox();
            expensesList = expensesConnection.ExpensesArrayList(dtpExpensesDate.Value);
            if (expensesList.Count <= 0)
            {
                cmbExpensesId.Enabled = false;
            }
            else
            {
                cmbExpensesId.Enabled = true;
            }
            cmbExpensesId.ValueMember = "ExpensesId";
            cmbExpensesId.DataSource = expensesList;
            cmbExpensesId.DisplayMember = "ExpensesId";
            DataChange = false;
        }


        public void FillComboBox()
        {
            
                cmbCurrency.ValueMember = "currencyId";
                cmbCurrency.DataSource = currecncyList;
                cmbCurrency.DisplayMember = "currencyName";
                typeList = typeConnection.ExpensesTypeArrayList();
                cmbExpensesType.ValueMember = "ExpensesTypeId";
                cmbExpensesType.DataSource = typeList;
                cmbExpensesType.DisplayMember = "ExpensesTypeEngName";
               
        }
        internal void Display(List<clsExpenses> myList, int expensesId)
        {
            iamAdding = false;

            cmbExpensesId.DataSource = null;
            cmbExpensesId.ValueMember = "ExpensesId";
            cmbExpensesId.DataSource = myList;
            cmbExpensesId.DisplayMember = "ExpensesId";
            if (expensesList.Count <= 0)
            {
                cmbExpensesId.Enabled = false;
            }
            else
            {
                cmbExpensesId.Enabled = true;
            }
            if (expensesId != 0)
            {
                cmbExpensesId.SelectedValue = expensesId;

            }

            DataChange = false;


        }

        public void ResetFeilds()
        {
            txtDescription.Text = "";
            txtExpensesAmount.Text = "";
            txtExpensesInvoiceNo.Text = "";
            cmbExpensesType.SelectedIndex = -1;
            stsLblInfo.Text = "";
        }

  

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsExpenses expenses = new clsExpenses();
                expenses.ExpensesId = this.ExpensesId;

                if (!string.IsNullOrWhiteSpace(txtExpensesAmount.Text))
                {
                    expenses.ExpensesAmount = decimal.Parse(txtExpensesAmount.Text.ToString());
                }
                else
                {
                    expenses.ExpensesAmount = 0;
                }
                if (!string.IsNullOrWhiteSpace(txtExpensesInvoiceNo.Text))
                {
                    expenses.ExpensesInvoiceId = int.Parse(txtExpensesInvoiceNo.Text.ToString());
                }
                else
                {
                    expenses.ExpensesInvoiceId = 0;
                }
                if (string.IsNullOrWhiteSpace(txtDescription.Text))
                {
                    expenses.ExpensesRemarks = " ";
                }
                else
                {
                    expenses.ExpensesRemarks = txtDescription.Text;
                }
                if (cmbCurrency.SelectedIndex == -1)
                {
                    expenses.ExpensesCurrencyId = 0;
                }
                else
                {
                    expenses.ExpensesCurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                if (cmbExpensesType.SelectedIndex == -1)
                {
                    expenses.ExpensesType = 0;
                }
                else
                {
                    expenses.ExpensesType = int.Parse(cmbExpensesType.SelectedValue.ToString());
                }
                expenses.ExpensesDate = dtpExpensesDate.Value;
                if(!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    expenses.CurrencyRate = decimal.Parse(txtRate.Text);
                }
                IdExists = expensesConnection.ExpensesIDExists(expenses.ExpensesId);

                if (IdExists)
                {
                    expensesConnection.UpdateExpenses(expenses);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    
                }
                else
                {

                    expensesConnection.AddExpenses(expenses);
                    stsLblInfo.Text = "New Expenses has been added successfully";
                    this.ExpensesId = expensesConnection.ReadLastNO();
                    expenses.ExpensesId = this.ExpensesId;
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                expensesList = expensesConnection.ExpensesArrayList(expenses.ExpensesDate);
                cmbExpensesId.DataSource = null;

                Display(expensesList, expenses.ExpensesId);

                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check"; 
                MessageBox.Show(ex.Message);
            }

        }




        private void TStripNew_Click(object sender, EventArgs e)
        {
           if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }

            if (DataChange)
            {
           

                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();

                }
            }
            iamAdding = true;
            this.ExpensesSavedId = this.ExpensesId;
            this.ExpensesId = 0;
            ResetFeilds();
            cmbExpensesId.DataSource = null;
            btnCancel.Visible = true;
            cmbExpensesId.Enabled = false;
            DataChange = false;
        }




        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                cmbExpensesId.Enabled = true;
                if (canUpdateOnly && this.ExpensesId == 0)
                {
                    return;
                }
                SaveDataInfo();

                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.ExpensesId = this.ExpensesSavedId;
            cmbExpensesId.Enabled = true;
            Display(expensesList, this.ExpensesId);

            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.ExpensesId != 0)
            {
                try
                {
                    bool MyResult;
              
                
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Expenses", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        MyResult = expensesConnection.DeleteExpenses(this.ExpensesId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        int index = expensesList.IndexOf(expensesData);
                        expensesList = expensesConnection.ExpensesArrayList(dtpExpensesDate.Value);
                        index -= 1;
                        this.ExpensesId = 0;
                        if (index >= 0)
                        {
                            this.ExpensesId = expensesList[index].ExpensesId;
                        }
                        else if (index == -1 && expensesList.Count > 0)
                        {
                            this.ExpensesId = expensesList[0].ExpensesId;
                        }
                       
                            stsLblInfo.Text = "Successfully deleted";
                        

                        Display(expensesList, this.ExpensesId);
                        DataChange = false;

                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }



        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbExpensesId.Items.Count > 0)
            {
                if (cmbExpensesId.SelectedIndex == 0 || cmbExpensesId.SelectedIndex == -1)
                {
                    int currencies = cmbExpensesId.Items.Count;
                    cmbExpensesId.SelectedIndex = currencies - 1;
                }
                else
                {
                    cmbExpensesId.SelectedIndex = cmbExpensesId.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbExpensesId.Items.Count > 0)
            {
                if (cmbExpensesId.SelectedIndex == cmbExpensesId.Items.Count - 1 || cmbExpensesId.SelectedIndex == -1)
                {

                    cmbExpensesId.SelectedIndex = 0;
                }
                else
                {
                    cmbExpensesId.SelectedIndex = cmbExpensesId.SelectedIndex + 1;
                }
            }
        }






        private void cmbCurrency_TextChanged(object sender, EventArgs e)
        {

            DataChange = true;
        }


        private void accessRight()
        {
          if (!rights.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!rights.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!rights.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!rights.Updating && rights.Adding)
            {
                canAddOnly = true;
            }
            if (!rights.Adding && rights.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbCurrency.Enabled = false;
            cmbExpensesType.Enabled = false;
            txtExpensesInvoiceNo.ReadOnly = true;
            txtDescription.ReadOnly = true;
            txtExpensesAmount.ReadOnly = true;
        }

        private void unReadOnlyEveryThing()
        {
            cmbCurrency.Enabled = true;
            cmbExpensesType.Enabled = true;
            txtExpensesInvoiceNo.ReadOnly = false;
            txtDescription.ReadOnly = false;
            txtExpensesAmount.ReadOnly = false; ;
        }


        private void frmCurrency_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();

                }
            }

        }
       
       



        private void dtpExpensesDate_ValueChanged(object sender, EventArgs e)
        {
            if (iamAdding )
            {
             
                return;
            }
            ResetFeilds();
            expensesList = expensesConnection.ExpensesArrayList(dtpExpensesDate.Value);
            if (expensesList.Count <= 0)
            {
                cmbExpensesId.Enabled = false;
            }
            else
            {
                cmbExpensesId.Enabled = true;
            }
            this.ExpensesId = 0;
            Display(expensesList, this.ExpensesId);
        }

        private void cmbExpensesId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbExpensesId.SelectedIndex != -1)
            {

                this.ExpensesId = int.Parse(cmbExpensesId.SelectedValue.ToString());
                expensesData = (clsExpenses)cmbExpensesId.SelectedItem;
                txtExpensesAmount.Text = expensesData.ExpensesAmount.ToString();
                int currencyId =0;
                if(cmbCurrency.SelectedIndex!=-1)
                {
                    currencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                cmbCurrency.SelectedIndex = -1;
                cmbCurrency.SelectedValue = expensesData.ExpensesCurrencyId;
                txtRate.Text = expensesData.CurrencyRate.ToString();
                txtDescription.Text = expensesData.ExpensesRemarks;
                txtExpensesInvoiceNo.Text = expensesData.ExpensesInvoiceId.ToString();
                cmbExpensesType.SelectedValue = expensesData.ExpensesType;
                DataChange = false;
               
            }
        }

        private void txtExpensesAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }


        private void cmbExpensesType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtExpensesInvoiceNo_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtExpensesAmount_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtDescription_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtExpensesInvoiceNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbCurrency.SelectedIndex != -1)
            {

                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
            DataChange = true;
        }

        private void cmbExpensesType_Click(object sender, EventArgs e)
        {
            int id = 0;
            if(cmbExpensesType.SelectedIndex!=-1)
            {
                id = int.Parse(cmbExpensesType.SelectedValue.ToString());
            }
            typeList = typeConnection.ExpensesTypeArrayList();
            cmbExpensesType.ValueMember = "ExpensesTypeId";
            cmbExpensesType.DataSource = typeList;
            cmbExpensesType.DisplayMember = "ExpensesTypeEngName";
            cmbExpensesType.SelectedValue = id; 
        }
    }
}
