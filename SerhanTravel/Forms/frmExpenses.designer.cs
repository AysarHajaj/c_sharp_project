﻿namespace SerhanTravel.Forms
{
    partial class frmExpenses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExpenses));
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSpace02 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.cmbExpensesId = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtExpensesAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtExpensesInvoiceNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbExpensesType = new System.Windows.Forms.ComboBox();
            this.dtpExpensesDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TStrip.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripSpace02,
            this.TStripBackWard,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(624, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 214;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.btnSave_Click_1);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSpace02
            // 
            this.TStripSpace02.AccessibleDescription = "TStripSpace02";
            this.TStripSpace02.AccessibleName = "TStripSpace02";
            this.TStripSpace02.AutoSize = false;
            this.TStripSpace02.Name = "TStripSpace02";
            this.TStripSpace02.Size = new System.Drawing.Size(10, 25);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 528);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(624, 22);
            this.stsInfo.TabIndex = 228;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(54, 17);
            this.stsLblInfo.Text = "Expenses";
            // 
            // cmbExpensesId
            // 
            this.cmbExpensesId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbExpensesId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbExpensesId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.cmbExpensesId.FormattingEnabled = true;
            this.cmbExpensesId.Location = new System.Drawing.Point(234, 144);
            this.cmbExpensesId.Name = "cmbExpensesId";
            this.cmbExpensesId.Size = new System.Drawing.Size(273, 24);
            this.cmbExpensesId.TabIndex = 274;
            this.cmbExpensesId.SelectedIndexChanged += new System.EventHandler(this.cmbExpensesId_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(113, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 19);
            this.label6.TabIndex = 273;
            this.label6.Text = "Amount";
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.Description.ForeColor = System.Drawing.Color.White;
            this.Description.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Description.Location = new System.Drawing.Point(113, 395);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(86, 19);
            this.Description.TabIndex = 272;
            this.Description.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.AccessibleDescription = "txtDescription";
            this.txtDescription.AccessibleName = "txtDescription";
            this.txtDescription.Location = new System.Drawing.Point(234, 382);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(4);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(273, 86);
            this.txtDescription.TabIndex = 271;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtExpensesAmount
            // 
            this.txtExpensesAmount.AccessibleDescription = "txtSerNo";
            this.txtExpensesAmount.AccessibleName = "txtSerNo";
            this.txtExpensesAmount.Location = new System.Drawing.Point(234, 271);
            this.txtExpensesAmount.Name = "txtExpensesAmount";
            this.txtExpensesAmount.Size = new System.Drawing.Size(273, 20);
            this.txtExpensesAmount.TabIndex = 270;
            this.txtExpensesAmount.TextChanged += new System.EventHandler(this.txtExpensesAmount_TextChanged);
            this.txtExpensesAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExpensesAmount_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(113, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 19);
            this.label4.TabIndex = 269;
            this.label4.Text = "Invoice No";
            // 
            // txtExpensesInvoiceNo
            // 
            this.txtExpensesInvoiceNo.AccessibleDescription = "txtSerNo";
            this.txtExpensesInvoiceNo.AccessibleName = "txtSerNo";
            this.txtExpensesInvoiceNo.Location = new System.Drawing.Point(234, 230);
            this.txtExpensesInvoiceNo.Name = "txtExpensesInvoiceNo";
            this.txtExpensesInvoiceNo.Size = new System.Drawing.Size(273, 20);
            this.txtExpensesInvoiceNo.TabIndex = 268;
            this.txtExpensesInvoiceNo.TextChanged += new System.EventHandler(this.txtExpensesInvoiceNo_TextChanged);
            this.txtExpensesInvoiceNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExpensesInvoiceNo_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(113, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 19);
            this.label3.TabIndex = 267;
            this.label3.Text = "Expenses Type";
            // 
            // cmbExpensesType
            // 
            this.cmbExpensesType.FormattingEnabled = true;
            this.cmbExpensesType.Location = new System.Drawing.Point(234, 193);
            this.cmbExpensesType.Name = "cmbExpensesType";
            this.cmbExpensesType.Size = new System.Drawing.Size(273, 21);
            this.cmbExpensesType.TabIndex = 266;
            this.cmbExpensesType.SelectedIndexChanged += new System.EventHandler(this.cmbExpensesType_SelectedIndexChanged);
            this.cmbExpensesType.Click += new System.EventHandler(this.cmbExpensesType_Click);
            // 
            // dtpExpensesDate
            // 
            this.dtpExpensesDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpensesDate.Location = new System.Drawing.Point(234, 97);
            this.dtpExpensesDate.Name = "dtpExpensesDate";
            this.dtpExpensesDate.Size = new System.Drawing.Size(273, 20);
            this.dtpExpensesDate.TabIndex = 265;
            this.dtpExpensesDate.ValueChanged += new System.EventHandler(this.dtpExpensesDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(113, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 19);
            this.label2.TabIndex = 264;
            this.label2.Text = "Expenses ID";
            // 
            // lblDate
            // 
            this.lblDate.AccessibleDescription = "lblDate";
            this.lblDate.AccessibleName = "lblDate";
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDate.Location = new System.Drawing.Point(113, 97);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(110, 19);
            this.lblDate.TabIndex = 263;
            this.lblDate.Text = "Expenses Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(441, 516);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 262;
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCurrency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Location = new System.Drawing.Point(234, 317);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(273, 24);
            this.cmbCurrency.TabIndex = 260;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.TextChanged += new System.EventHandler(this.cmbCurrency_TextChanged);
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtRate.Location = new System.Drawing.Point(234, 353);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(273, 22);
            this.txtRate.TabIndex = 259;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(327, 492);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 29);
            this.btnCancel.TabIndex = 261;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblSerNo.ForeColor = System.Drawing.Color.White;
            this.lblSerNo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSerNo.Location = new System.Drawing.Point(113, 318);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(115, 19);
            this.lblSerNo.TabIndex = 257;
            this.lblSerNo.Text = "Currency Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(113, 354);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 19);
            this.label5.TabIndex = 258;
            this.label5.Text = "Rate";
            // 
            // frmExpenses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(624, 550);
            this.Controls.Add(this.cmbExpensesId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtExpensesAmount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtExpensesInvoiceNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbExpensesType);
            this.Controls.Add(this.dtpExpensesDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbCurrency);
            this.Controls.Add(this.txtRate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblSerNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TStrip);
            this.Controls.Add(this.stsInfo);
            this.Name = "frmExpenses";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expenses";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCurrency_FormClosing);
            this.Load += new System.EventHandler(this.frmExpenses_Load);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace02;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.ComboBox cmbExpensesId;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label Description;
        internal System.Windows.Forms.TextBox txtDescription;
        internal System.Windows.Forms.TextBox txtExpensesAmount;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.TextBox txtExpensesInvoiceNo;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbExpensesType;
        private System.Windows.Forms.DateTimePicker dtpExpensesDate;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.Label label5;
    }
}