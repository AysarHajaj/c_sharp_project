﻿
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmExpensesType : Form
    {
        int ExpensesTypeId = 0;
        List<clsExpensesType> expensesTypeList;
        bool canUpdateOnly;
        bool DataChange = false;
        clsExpensesType expensesTypeData;
        ExpensesType expensesTypeConnection;
        bool canAddOnly = false;
        clsAccessRights rights;
        private int SavedId;

        internal frmExpensesType(clsAccessRights rights)
        {
            InitializeComponent();
            this.rights = rights;

        }

        private void frmExpensesType_Load(object sender, EventArgs e)
        {
            accessRight();
            expensesTypeConnection = new ExpensesType();
            expensesTypeList = expensesTypeConnection.ExpensesTypeArrayList();
            cmbExpensesType.ValueMember = "ExpensesTypeId";
            cmbExpensesType.DataSource = expensesTypeList;
            cmbExpensesType.DisplayMember = "ExpensesTypeEngName";
            cmbExpensesType.SelectedIndex = -1;
            ResetFeilds();
            this.ExpensesTypeId = 0;
            DataChange = false;
           


        }

        public void Display(int ExpensesTypeId)
        {


            this.ExpensesTypeId = ExpensesTypeId;
            cmbExpensesType.ValueMember = "ExpensesTypeId";
            cmbExpensesType.DataSource = expensesTypeList;
            cmbExpensesType.DisplayMember = "ExpensesTypeEngName";
            
            if (ExpensesTypeId != 0)
            {
                cmbExpensesType.SelectedValue = ExpensesTypeId;
            }
            else
            {
                cmbExpensesType.SelectedValue = ExpensesTypeId;
            }



            DataChange = false;


        }

        public void ResetFeilds()
        {
            cmbExpensesType.SelectedIndex = -1;
            txtExpensesTypeName.Clear();
            stsLblInfo.Text = "";
        }

        private void cmbExpensesType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbExpensesType.SelectedIndex != -1 && expensesTypeList.Count > 0)
            {
                this.ExpensesTypeId = int.Parse(cmbExpensesType.SelectedValue.ToString());
                expensesTypeData = expensesTypeList[cmbExpensesType.SelectedIndex];
                txtExpensesTypeName.Text = expensesTypeData.ExpensesTypeArabName;
            
                 
            }
           
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsExpensesType bank = new clsExpensesType();
                bank.ExpensesTypeId = this.ExpensesTypeId;
             
               

                    if (!string.IsNullOrWhiteSpace(txtExpensesTypeName.Text))
                    {
                        bank.ExpensesTypeArabName = txtExpensesTypeName.Text;
                    }
                    else
                    {
                        bank.ExpensesTypeArabName = " ";
                    }
                    if (!string.IsNullOrWhiteSpace(cmbExpensesType.Text))
                    {
                        bank.ExpensesTypeEngName = cmbExpensesType.Text;
                    }
                    else
                    {

                        bank.ExpensesTypeEngName = " ";
                    }
                

                IdExists = expensesTypeConnection.ExpensesTypeIDExists(bank.ExpensesTypeId);

                if (IdExists)
                {
                    expensesTypeConnection.UpdateExpensesType(bank);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    
                }
                else
                {

                    expensesTypeConnection.AddExpensesType(bank);
                    this.ExpensesTypeId = expensesTypeConnection.ReadLastNo();
                    stsLblInfo.Text = "New Expenses Type has been added successfully";
                    
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                expensesTypeList = expensesTypeConnection.ExpensesTypeArrayList();
                Display(this.ExpensesTypeId);

                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                
                MessageBox.Show(ex.Message);
            }

        }



        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add ExpensesType";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?",  "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedId = ExpensesTypeId;
            this.ExpensesTypeId = 0;
            ResetFeilds();
            cmbExpensesType.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.ExpensesTypeId == 0)
                {
                    return;
                }

                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;

            ResetFeilds();
            this.ExpensesTypeId = SavedId;
            Display(this.ExpensesTypeId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.ExpensesTypeId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Expenses Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (expensesTypeConnection.TypeUsed(ExpensesTypeId))
                        {
                            MessageBox.Show("This Type Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = expensesTypeConnection.DeleteExpensesType(this.ExpensesTypeId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        int index = expensesTypeList.IndexOf(expensesTypeData);
                        expensesTypeList = expensesTypeConnection.ExpensesTypeArrayList();

                        index -= 1;
                        this.ExpensesTypeId = 0;
                        if (index >= 0)
                        {
                            this.ExpensesTypeId = expensesTypeList[index].ExpensesTypeId;
                        }
                        else if (index == -1 && expensesTypeList.Count > 0)
                        {
                            index = 0;
                            this.ExpensesTypeId = expensesTypeList[index].ExpensesTypeId;
                        }

                        Display(ExpensesTypeId);
                        DataChange = false;
                      
                            stsLblInfo.Text = "Successfully deleted";
                        
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                        stsLblInfo.Text = "Error: Not deleted";
                    
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }


        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbExpensesType.Items.Count > 0)
            {
                if (cmbExpensesType.SelectedIndex == 0 || cmbExpensesType.SelectedIndex == -1)
                {
                    int Cities = cmbExpensesType.Items.Count;
                    cmbExpensesType.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbExpensesType.SelectedIndex = cmbExpensesType.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbExpensesType.Items.Count > 0)
            {
                if (cmbExpensesType.SelectedIndex == cmbExpensesType.Items.Count - 1 || cmbExpensesType.SelectedIndex == -1)
                {

                    cmbExpensesType.SelectedIndex = 0;
                }
                else
                {
                    cmbExpensesType.SelectedIndex = cmbExpensesType.SelectedIndex + 1;
                }
            }
        }



        private void accessRight()
        {
             if (!rights.Adding)
             {
                 TStripNew.Enabled = false;
             }
             if (!rights.Deleting)
             {
                 TStripDelete.Enabled = false;
             }
             if (!rights.Updating)
             {
                 TStripSave.Enabled = false;
                 readOnlyEveryThing();
             }
             if (!rights.Updating && rights.Adding)
             {
                 canAddOnly = true;
             }
            if (!rights.Adding && rights.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbExpensesType.DropDownStyle = ComboBoxStyle.DropDownList;
            txtExpensesTypeName.ReadOnly = true;
        }

        private void unReadOnlyEveryThing()
        {
            cmbExpensesType.DropDownStyle = ComboBoxStyle.DropDown;
            txtExpensesTypeName.ReadOnly = false;
        }

  

        private void frmBanks_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DataChange)
            {
              
             

                DialogResult myReply = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();

                }
            }
        }

        private void txtExpensesTypeName_TextChanged(object sender, EventArgs e)
        {
            DataChange=true;
        }

        private void cmbExpensesType_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }
    }
}
