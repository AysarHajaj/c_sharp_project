﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmGroup : Form
    {
        bool canUpdateOnly;
        private int SavedID;
        int GroupId = 0;
        ArrayList groupList;
        bool iamAdding = false;
        bool DataChange = false;
        clsGroup groupData;
        clsAccessRights right;
        bool canAddOnly = false;
        Groups groupsConnnection;
        internal frmGroup(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmGroup_Load(object sender, EventArgs e)
        {
            accessRight();
            groupsConnnection = new Groups();
            groupList = groupsConnnection.GroupArrayList();
            cmbAirLine.ValueMember = "groupId";
            cmbAirLine.DataSource = groupList;
            cmbAirLine.DisplayMember = "groupName";
            cmbAirLine.SelectedIndex = -1;
            this.GroupId = 0;
            DataChange = false;
        }

        public void Display(int groupId)
        {
            iamAdding = false;
            cmbAirLine.DataSource = null;
            cmbAirLine.ValueMember = "groupId";
            cmbAirLine.DataSource = groupList;
            cmbAirLine.DisplayMember = "groupName";
            cmbAirLine.SelectedIndex = - 1;
            this.GroupId = groupId;
            DataChange = false;
        }
        public void ResetFeilds()
        {
            cmbAirLine.SelectedIndex = -1;
            stsLblInfo.Text = "";
        }

        private void cmbAirLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAirLine.SelectedIndex != -1 && groupList.Count > 0)
            {
                this.GroupId = int.Parse(cmbAirLine.SelectedValue.ToString());
                groupData = (clsGroup)groupList[cmbAirLine.SelectedIndex];
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsGroup group = new clsGroup();
                group.GroupId = this.GroupId;

                if (!string.IsNullOrWhiteSpace(cmbAirLine.Text))
                {
                    group.GroupName = cmbAirLine.Text;
                }
                else
                {
                    group.GroupName = string.Empty;
                }

                IdExists = groupsConnnection.GroupIDExists(group.GroupId);

                if (IdExists)
                {
                    groupsConnnection.UpdateGroup(group);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    groupsConnnection.AddGroup(group);
                    stsLblInfo.Text = "New Group has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                groupList = groupsConnnection.GroupArrayList();

                if (iamAdding && groupList.Count > 0)
                {
                    clsGroup grp = (clsGroup)groupList[groupList.Count - 1];
                    this.GroupId = grp.GroupId;
                }
                Display(this.GroupId);
                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }


   

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Group";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            this.SavedID = this.GroupId;
            this.GroupId = 0;
            ResetFeilds();
            cmbAirLine.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.GroupId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.GroupId = SavedID;
            Display(this.GroupId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {

            if (this.GroupId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Group", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (groupsConnnection.GroupUsed(GroupId))
                        {
                            MessageBox.Show("This Group Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = groupsConnnection.DeleteGroup(this.GroupId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        groupList = groupsConnnection.GroupArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }


     
        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbAirLine.Items.Count > 0)
            {
                if (cmbAirLine.SelectedIndex == 0 || cmbAirLine.SelectedIndex == -1)
                {
                    int Cities = cmbAirLine.Items.Count;
                    cmbAirLine.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbAirLine.SelectedIndex = cmbAirLine.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbAirLine.Items.Count > 0)
            {
                if (cmbAirLine.SelectedIndex == cmbAirLine.Items.Count - 1 || cmbAirLine.SelectedIndex == -1)
                {

                    cmbAirLine.SelectedIndex = 0;
                }
                else
                {
                    cmbAirLine.SelectedIndex = cmbAirLine.SelectedIndex + 1;
                }
            }
        }

        private void cmbAirLine_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbAirLine.DropDownStyle = ComboBoxStyle.DropDownList;


        }

        private void unReadOnlyEveryThing()
        {
            cmbAirLine.DropDownStyle = ComboBoxStyle.DropDown;
        }
    }
}
