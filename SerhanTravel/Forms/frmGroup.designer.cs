﻿namespace SerhanTravel.Forms
{
    partial class frmGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroup));
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.cmbAirLine = new System.Windows.Forms.ComboBox();
            this.TStrip.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripBackWard,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(312, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 216;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 152);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(312, 22);
            this.stsInfo.TabIndex = 232;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(45, 17);
            this.stsLblInfo.Text = "Groups";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(145, 104);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 235;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerNo.Location = new System.Drawing.Point(23, 79);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(75, 15);
            this.lblSerNo.TabIndex = 233;
            this.lblSerNo.Text = "Group Name";
            // 
            // cmbAirLine
            // 
            this.cmbAirLine.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAirLine.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAirLine.FormattingEnabled = true;
            this.cmbAirLine.Location = new System.Drawing.Point(104, 77);
            this.cmbAirLine.Name = "cmbAirLine";
            this.cmbAirLine.Size = new System.Drawing.Size(194, 21);
            this.cmbAirLine.TabIndex = 234;
            this.cmbAirLine.SelectedIndexChanged += new System.EventHandler(this.cmbAirLine_SelectedIndexChanged);
            this.cmbAirLine.TextChanged += new System.EventHandler(this.cmbAirLine_TextChanged);
            // 
            // frmGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(312, 174);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblSerNo);
            this.Controls.Add(this.cmbAirLine);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.TStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "frmGroup";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Group";
            this.Load += new System.EventHandler(this.frmGroup_Load);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.ComboBox cmbAirLine;
    }
}