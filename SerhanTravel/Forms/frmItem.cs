﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmItem : Form
    {
        bool canUpdateOnly;
        List<clsItem> itemList;
        Item itemConnection;
        clsItem itemData;
        bool skip = false;
        bool DataChanged = false;
        bool iamUpdate = false;
        bool iamAdding = false;
        private int saveID;
        int ItemID = 0;
        private List<clsCurrency> currencies;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection;
        internal frmItem(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }
        private void ResetFields()
        {
            txtItemCost.Clear();
            txtItemDescription.Clear();
            cbItemSupplier.SelectedIndex = -1;
            cbItem.SelectedIndex = -1;
            cmbCurrency.SelectedIndex = -1;
            stsLblInfo.Text = "";
            skip = true;
            DataChanged = false;
        }

        private void frmItem_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            accessRight();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedIndex = -1;

            itemConnection = new Item();
            itemList = itemConnection.getItemList();
            FillComboBox();
            cbItem.ValueMember = "ItemID";
            cbItem.DataSource = itemList;
            cbItem.DisplayMember = "ItemName";
            skip = true;
            ResetFields();
            this.ItemID = 0;
            DataChanged = false;
        }

        private void DisplayData(int itemId)
        {
            iamAdding = false;
            iamUpdate = false;
            cbItem.DataSource = null;
            cbItem.ValueMember = "ItemID";
            cbItem.DataSource = itemList;
            cbItem.DisplayMember = "ItemName";
            skip = true;
            cbItem.SelectedValue = itemId;
            this.ItemID = itemId;
            DataChanged = false;

        }

        private void FillComboBox()
        {
            Supplier supplierConnection;
            supplierConnection = new Supplier();
            List<clsSupplier> supplierList = supplierConnection.SupplierArrayList();
            cbItemSupplier.ValueMember = "SupplierId";
            cbItemSupplier.DataSource = supplierList;
            cbItemSupplier.DisplayMember = "supplierName";
            cbItemSupplier.SelectedIndex = -1;


            List<clsCategory> itemCategory = new List<clsCategory>();
            itemCategory.Add(new clsCategory(1, "Item"));
            itemCategory.Add(new clsCategory(2, "Ticket"));

            cmbCategory.ValueMember = "categoryId";
            cmbCategory.DataSource = itemCategory;
            cmbCategory.DisplayMember = "categoryName";
          

            DataChanged = false;
        }

        private void cbItem_TextChanged(object sender, EventArgs e)
        {
            if (iamAdding)
            {
                DataChanged = true;
                return;
            }

            if (skip)
            {
                skip = false;
                return;
            }

            lbResult.Visible = false;
            string textToSearch = cbItem.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                DataChanged = true;
                return;
            }

            clsItem[] result = (from i in itemList
                                    where i.itemName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                DataChanged = true;
                return; // return with listbox's Visible set to false if nothing found
            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "ItemID";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "ItemName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void cbItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbItem.SelectedIndex != -1 && itemList.Count > 0)
            {
                ItemID = int.Parse(cbItem.SelectedValue.ToString());
                itemData = itemList[cbItem.SelectedIndex];
              
                txtItemDescription.Text = itemData.itemDescription.ToString();
                txtItemCost.Text = itemData.Cost.ToString();
                cbItemSupplier.SelectedValue = itemData.SupplierId;
                cmbCurrency.SelectedIndex = -1;
                cmbCurrency.SelectedValue = itemData.CurrencyId;
                txtRate.Text = itemData.CurrencyRate.ToString();
                cmbCategory.SelectedValue = itemData.CategoryId;
                skip = true;
                DataChanged = false;
            }
            else
            {
                skip = false;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cbItem.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void SaveDataInfo()
        {
            bool RecExists;
            try
            {
                clsItem item = new clsItem();
                item.itemID = this.ItemID;
                

                if (string.IsNullOrWhiteSpace(cbItem.Text))
                {
                    item.itemName= string.Empty;
                }
                else
                {
                    item.itemName = cbItem.Text;
                }

                if (string.IsNullOrWhiteSpace(txtItemDescription.Text))
                {
                    item.itemDescription= string.Empty;
                }
                else
                {
                    item.itemDescription = txtItemDescription.Text;
                }
                if (cmbCategory.SelectedIndex == -1)
                {
                    item.CategoryId = 0;
                }
                else
                {
                    item.CategoryId = int.Parse(cmbCategory.SelectedValue.ToString());
                }

                if (cbItemSupplier.SelectedIndex != -1)
                {
                    item.SupplierId= int.Parse(cbItemSupplier.SelectedValue.ToString());
                }
                else
                {
                    item.SupplierId = 0;
                }


                if (!string.IsNullOrWhiteSpace(txtItemCost.Text))
                {
                    item.Cost= decimal.Parse(txtItemCost.Text.ToString());
                }
                else
                {
                    item.Cost = 0;
                }
                if(cmbCurrency.SelectedIndex==-1)
                {
                    item.CurrencyId = 0;
                }
                else
                {
                    item.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                if (!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    item.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }

                Item itemConnection = new Item();
            
                RecExists = itemConnection.ItemIDExists(item.itemID);

                if (RecExists == true)
                {
                    itemConnection.UpdateItemInfo(item);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    iamUpdate = true;
                }
                else
                {
                    iamAdding = true;
                    itemConnection.AddNewItem(item);
                    stsLblInfo.Text = "New Item has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
                itemList = itemConnection.getItemList();
                if (iamUpdate)
                {
                    DisplayData(item.itemID);
                }
                if (iamAdding)
                {
                    iamAdding = false;
                    DisplayData(itemList[itemList.Count - 1].itemID);
                }

                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Item";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }

            iamAdding = true;
            saveID = ItemID;
            ItemID = 0;
            ResetFields();
            cbItem.DataSource = null;
            btnCancel.Visible = true;
            DataChanged = false;
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtItemDescription_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtItemCost_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtItemPrice_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cmbItemSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChanged)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.ItemID == 0)
                {
                    return;
                }

                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ResetFields();
            btnCancel.Visible = false;
            iamAdding = false;
            ItemID = saveID;
            DisplayData(ItemID);
            DataChanged = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (ItemID != 0)
            {


                try
                {
                    bool myResult;
                    SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReply = MessageBox.Show("Are you sure ?", "Delete Item Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        if (itemConnection.ItemUsed(ItemID))
                        {
                            MessageBox.Show("This Item Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        myResult = itemConnection.DeleteItem(this.ItemID);
                        if (myResult == false)
                        {
                            return;
                        }
                        notifySound.Play();
                        ResetFields();
                        itemList = itemConnection.getItemList();
                        if (itemList.Count > 0)
                        {
                            DisplayData(itemList[itemList.Count - 1].itemID);
                        }
                        else
                        {
                            ItemID = 0;
                        }
                        DataChanged = false;
                        stsLblInfo.Text = "Successfully Deleted";
                    }
                }
                catch (Exception ex)
                {

                    DataChanged = false;
                    stsLblInfo.Text = "ERROR: Problem Deleting the Item profile";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if ((cbItem.Items.Count > 0))
            {
                if (cbItem.SelectedIndex == cbItem.Items.Count - 1 || cbItem.SelectedIndex == -1)
                {
                    cbItem.SelectedIndex = 0;
                }
                else
                {
                    cbItem.SelectedIndex = cbItem.SelectedIndex + 1;
                }
            }
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if ((cbItem.Items.Count > 0))
            {
                if (cbItem.SelectedIndex == 0 || cbItem.SelectedIndex == -1)
                {
                    cbItem.SelectedIndex = cbItem.Items.Count - 1;
                }
                else
                {
                    cbItem.SelectedIndex = cbItem.SelectedIndex - 1;
                }
            }
        }

        private void txtItemCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character!='.')
            {
                e.Handled = true;
            }
            else
            {
                if(character=='.' && txtItemCost.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }

        private void txtItemPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void frmItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cbItem.SelectedIndex != -1)
            {
                ItemID = Convert.ToInt32(cbItem.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void cmbItemSupplier_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cbItemSupplier.SelectedIndex != -1)
                id = int.Parse(cbItemSupplier.SelectedValue.ToString());
            Supplier supplierConnection;
            supplierConnection = new Supplier();
            List<clsSupplier> Supplier = supplierConnection.SupplierArrayList();
            cbItemSupplier.ValueMember = "supplierId";
            cbItemSupplier.DataSource = Supplier;
            cbItemSupplier.DisplayMember = "supplierName";
            cbItemSupplier.SelectedValue = id;
            DataChanged = false;
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();
                lblCost.Text = currency.CurrencySymbol;
            }
            else
            {
                txtRate.Text = "";
                lblCost.Text = "";
            }
            DataChanged = true;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cbItem.DropDownStyle = ComboBoxStyle.DropDownList;
            cbItemSupplier.Enabled = false;
            cmbCategory.Enabled = false;
            txtItemDescription.ReadOnly = true;
            txtItemCost.ReadOnly = true;


        }

        private void unReadOnlyEveryThing()
        {
            cbItem.DropDownStyle = ComboBoxStyle.DropDown;
            cbItemSupplier.Enabled = true;
            cmbCategory.Enabled = true;
            txtItemDescription.ReadOnly = false;
            txtItemCost.ReadOnly = false;
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
