﻿namespace SerhanTravel.Forms
{
    partial class frmItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmItem));
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSpace02 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtItemDescription = new System.Windows.Forms.TextBox();
            this.txtItemCost = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbItemSupplier = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbItem = new System.Windows.Forms.ComboBox();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.stsInfo.SuspendLayout();
            this.TStrip.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 326);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(659, 22);
            this.stsInfo.TabIndex = 217;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(60, 17);
            this.stsLblInfo.Text = "Items Info";
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripSpace02,
            this.TStripBackWard,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(659, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 213;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSpace02
            // 
            this.TStripSpace02.AccessibleDescription = "TStripSpace02";
            this.TStripSpace02.AccessibleName = "TStripSpace02";
            this.TStripSpace02.AutoSize = false;
            this.TStripSpace02.Name = "TStripSpace02";
            this.TStripSpace02.Size = new System.Drawing.Size(10, 25);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Location = new System.Drawing.Point(33, 21);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(31, 15);
            this.lblSerNo.TabIndex = 214;
            this.lblSerNo.Text = "Item";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(33, 87);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 15);
            this.Label1.TabIndex = 216;
            this.Label1.Text = "Description";
            // 
            // txtItemDescription
            // 
            this.txtItemDescription.AccessibleDescription = "txtSerNo";
            this.txtItemDescription.AccessibleName = "txtSerNo";
            this.txtItemDescription.Location = new System.Drawing.Point(117, 84);
            this.txtItemDescription.Name = "txtItemDescription";
            this.txtItemDescription.Size = new System.Drawing.Size(145, 22);
            this.txtItemDescription.TabIndex = 218;
            this.txtItemDescription.TextChanged += new System.EventHandler(this.txtItemDescription_TextChanged);
            // 
            // txtItemCost
            // 
            this.txtItemCost.AccessibleDescription = "txtSerNo";
            this.txtItemCost.AccessibleName = "txtSerNo";
            this.txtItemCost.Location = new System.Drawing.Point(96, 77);
            this.txtItemCost.Name = "txtItemCost";
            this.txtItemCost.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtItemCost.Size = new System.Drawing.Size(145, 22);
            this.txtItemCost.TabIndex = 224;
            this.txtItemCost.TextChanged += new System.EventHandler(this.txtItemCost_TextChanged);
            this.txtItemCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtItemCost_KeyPress);
            // 
            // label3
            // 
            this.label3.AccessibleDescription = "lblName";
            this.label3.AccessibleName = "lblName";
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 15);
            this.label3.TabIndex = 225;
            this.label3.Text = "Cost";
            // 
            // cbItemSupplier
            // 
            this.cbItemSupplier.FormattingEnabled = true;
            this.cbItemSupplier.Location = new System.Drawing.Point(117, 110);
            this.cbItemSupplier.Name = "cbItemSupplier";
            this.cbItemSupplier.Size = new System.Drawing.Size(145, 23);
            this.cbItemSupplier.TabIndex = 228;
            this.cbItemSupplier.SelectedIndexChanged += new System.EventHandler(this.cmbItemSupplier_SelectedIndexChanged);
            this.cbItemSupplier.Click += new System.EventHandler(this.cmbItemSupplier_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 229;
            this.label4.Text = "Supplier";
            // 
            // cbItem
            // 
            this.cbItem.FormattingEnabled = true;
            this.cbItem.Location = new System.Drawing.Point(117, 18);
            this.cbItem.Name = "cbItem";
            this.cbItem.Size = new System.Drawing.Size(145, 23);
            this.cbItem.TabIndex = 230;
            this.cbItem.SelectedIndexChanged += new System.EventHandler(this.cbItem_SelectedIndexChanged);
            this.cbItem.TextChanged += new System.EventHandler(this.cbItem_TextChanged);
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(117, 41);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(145, 34);
            this.lbResult.TabIndex = 231;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(272, 288);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 32);
            this.btnCancel.TabIndex = 232;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(96, 39);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(145, 22);
            this.txtRate.TabIndex = 235;
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(96, 8);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(145, 23);
            this.cmbCurrency.TabIndex = 234;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 15);
            this.label15.TabIndex = 233;
            this.label15.Text = "Currency";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 15);
            this.label5.TabIndex = 236;
            this.label5.Text = "Rate";
            // 
            // lblCost
            // 
            this.lblCost.AccessibleDescription = "lblName";
            this.lblCost.AccessibleName = "lblName";
            this.lblCost.AutoSize = true;
            this.lblCost.Location = new System.Drawing.Point(253, 80);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(0, 15);
            this.lblCost.TabIndex = 237;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 15);
            this.label6.TabIndex = 240;
            this.label6.Text = "Category";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(96, 111);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(145, 23);
            this.cmbCategory.TabIndex = 239;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel3.Controls.Add(this.lbResult);
            this.panel3.Controls.Add(this.txtItemDescription);
            this.panel3.Controls.Add(this.Label1);
            this.panel3.Controls.Add(this.lblSerNo);
            this.panel3.Controls.Add(this.cbItemSupplier);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.cbItem);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(12, 124);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(277, 142);
            this.panel3.TabIndex = 241;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel2.Controls.Add(this.txtRate);
            this.panel2.Controls.Add(this.lblCost);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cmbCategory);
            this.panel2.Controls.Add(this.txtItemCost);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cmbCurrency);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(337, 124);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(288, 142);
            this.panel2.TabIndex = 242;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // frmItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tomato;
            this.ClientSize = new System.Drawing.Size(659, 348);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.TStrip);
            this.Name = "frmItem";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Item";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmItem_FormClosing);
            this.Load += new System.EventHandler(this.frmItem_Load);
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace02;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtItemDescription;
        internal System.Windows.Forms.TextBox txtItemCost;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbItemSupplier;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbItem;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label lblCost;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
    }
}