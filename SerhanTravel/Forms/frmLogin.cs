﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{

    public partial class frmLogin : Form
    {

        clsUser user;
        User userConnection;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            userConnection = new User();
            user = new clsUser();
            FillCombobox();     
        }

        private void FillCombobox()
        {
            cbLogInUsers.ValueMember = "UserName";
            cbLogInUsers.DataSource = userConnection.UsersArrayList();
            cbLogInUsers.DisplayMember = "UserName";
            cbLogInUsers.SelectedIndex = -1;
       }

        private void btnOk_Click(object sender, EventArgs e)
        {
         
           login();         
        }

        public void login()
        {
            if (!string.IsNullOrWhiteSpace(txtPassword.Text) && cbLogInUsers.SelectedIndex != -1)
            {            
              try
                {
                    clsUser user = (clsUser)cbLogInUsers.SelectedItem;
                    string password = txtPassword.Text;
                    if (password.Equals(user.Password))
                    {
                        mdiForm frm = new mdiForm(user);
                        this.Hide();
                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                        simpleSound.Play();
                        frm.Show();
                    }
                    else
                    {
                       
                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                        simpleSound.Play();
                        label1.ForeColor = Color.Red;
                        label1.Text = "Invalid Username or Password Try again...";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void cbLogInUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        private void cbLogInUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPassword.Clear();

            if(cbLogInUsers.SelectedIndex!=-1)
            {
                clsUser user = (clsUser)cbLogInUsers.SelectedItem;
              
              /*  txtPassword.Text = user.Password;
                btnOk.PerformClick();*/
            }
        
        }

        internal  clsUser getUser()
        {

            return this.user;
        }

        private void cbLogInUsers_Click(object sender, EventArgs e)
        {
            string userName = "";
            if(cbLogInUsers.SelectedIndex!=-1)
            {
                userName = cbLogInUsers.SelectedValue.ToString();
            }

            cbLogInUsers.ValueMember = "UserName";
            cbLogInUsers.DataSource = userConnection.UsersArrayList();
            cbLogInUsers.DisplayMember = "UserName";
            cbLogInUsers.SelectedValue = userName;
        }

        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
        private void chShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (chShowPassword.Checked)
            {
                txtPassword.PasswordChar = char.MinValue;
            }
            else
            {
                txtPassword.PasswordChar = '*';
            }
        }

       
    }
}
