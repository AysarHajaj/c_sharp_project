﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmNationality : Form
    {
        private int SavedID;
        int NationalityId = 0;
        ArrayList nationalityList;
        bool iamAdding = false;
        bool iamUpdate = false;
        bool DataChange = false;
        clsNationality nationalityData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        Nationality nationalityConnection;
        internal frmNationality(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmNationality_Load(object sender, EventArgs e)
        {
            nationalityConnection = new Nationality();
            accessRight();
            nationalityList = nationalityConnection.NationalitiesArrayList();
            cmbNationality.ValueMember = "nationalityId";
            cmbNationality.DataSource = nationalityList;
            cmbNationality.DisplayMember = "englishName";
            cmbNationality.SelectedIndex = -1;
            ResetFeilds();
            this.NationalityId = 0;
            DataChange = false;
        }

        public void Display(int nId)
            {
            cmbNationality.DataSource = null;
            cmbNationality.ValueMember = "nationalityId";
            cmbNationality.DataSource = nationalityList;
            cmbNationality.DisplayMember = "englishName";
            cmbNationality.SelectedValue = nId;
            this.NationalityId = nId;
            iamAdding = false;
            iamUpdate = false;
            DataChange = false;


        }

        public void ResetFeilds()
        {
            cmbNationality.SelectedIndex = -1;
            tvArabicName.Text = "";

        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbNationality.SelectedIndex != -1 && nationalityList.Count > 0)
            {
                this.NationalityId = int.Parse(cmbNationality.SelectedValue.ToString());
                nationalityData =(clsNationality) nationalityList[cmbNationality.SelectedIndex];

                tvArabicName.Text = nationalityData.ArabicName;

            }
            DataChange = false;
        }


        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsNationality nationality = new clsNationality();
                nationality.NationalityId = this.NationalityId;

                if (!string.IsNullOrWhiteSpace(cmbNationality.Text))
                {
                    nationality.EnglishName = cmbNationality.Text;
                }
                else
                {
                    nationality.EnglishName = string.Empty;
                }

                if (string.IsNullOrWhiteSpace(tvArabicName.Text))
                {
                    nationality.ArabicName = "";
                }
                else
                {
                    nationality.ArabicName = tvArabicName.Text;
                }


                IdExists = nationalityConnection.NationalityIDExists(nationality.NationalityId);

                if (IdExists)
                {
                    nationalityConnection.UpdateNationality(nationality);
                    iamUpdate = true;
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    nationalityConnection.AddNationality(nationality);
                    stsLblInfo.Text = "New Nationality has been added successfully";
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                nationalityList = nationalityConnection.NationalitiesArrayList();
                if (iamAdding)
                {
                    Display(0);
                }

                if (iamUpdate)
                {
                    Display(nationality.NationalityId);
                }

                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }

      

      

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Nationality";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            this.SavedID = this.NationalityId;
            this.NationalityId = 0;
            ResetFeilds();
            cmbNationality.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void tvArabicName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.NationalityId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.NationalityId = SavedID;
            Display(this.NationalityId);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.NationalityId != 0)
            {
                try
                {
                    bool MyResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete Nationality", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (myReplay == DialogResult.Yes)
                    {
                        if (nationalityConnection.NationalityUsed(NationalityId))
                        {
                            MessageBox.Show("This Nationality Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        MyResult = nationalityConnection.DeleteNationality(this.NationalityId);
                        if (!MyResult)
                        {
                            return;
                        }
                        simpleSound.Play();
                        ResetFeilds();
                        nationalityList = nationalityConnection.NationalitiesArrayList();
                        Display(0);
                        DataChange = false;
                        stsLblInfo.Text = "Successfully deleted";
                    }


                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Not deleted";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }


        private void cmbCountry_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cmbNationality.Items.Count > 0)
            {
                if (cmbNationality.SelectedIndex == 0 || cmbNationality.SelectedIndex == -1)
                {
                    int Cities = cmbNationality.Items.Count;
                    cmbNationality.SelectedIndex = Cities - 1;
                }
                else
                {
                    cmbNationality.SelectedIndex = cmbNationality.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cmbNationality.Items.Count > 0)
            {
                if (cmbNationality.SelectedIndex == cmbNationality.Items.Count - 1 || cmbNationality.SelectedIndex == -1)
                {

                    cmbNationality.SelectedIndex = 0;
                }
                else
                {
                    cmbNationality.SelectedIndex = cmbNationality.SelectedIndex + 1;
                }
            }
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbNationality.DropDownStyle = ComboBoxStyle.DropDownList;
            tvArabicName.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbNationality.DropDownStyle = ComboBoxStyle.DropDown;
            tvArabicName.ReadOnly = false;
        }

    }
}
