﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using SerhanTravel.Objects;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmPackage : Form
    {
        List<clsPackage> packageList;
        List<clsItem> itemList;

        List<int> packageItemsIDS;
        PackageItemDetails packageItemConnection;
        Package packageConnection;
        clsPackage packageData;
        bool Datachange = false;
        Country countryConnection;
        public int PackageSavedID { get; private set; }
        bool canUpdateOnly;
        Currency currencyConnection;
        int PackageID = 0;
        decimal TotalPrice = 0;
        decimal TotalCost = 0;
        List<clsCurrency> Currencies;
        private bool allowRemove;
        clsAccessRights right;
        bool canAddOnly = false;
        string currencyFormat;
        internal frmPackage(clsAccessRights right)
        {
            InitializeComponent();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            clbItems.HorizontalScrollbar = true;
            this.right = right;
        }

        private void frmPackage_Load(object sender, EventArgs e)
        {
            countryConnection = new Country();
            currencyConnection = new Currency();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            accessRight();
            packageConnection = new Package();
            packageItemConnection = new PackageItemDetails();
            packageList = packageConnection.PackageArrayList();
            packageItemsIDS = new List<int>();
            cbPackage.ValueMember = "PackageID";
            cbPackage.DisplayMember = "PackageName";
            cbPackage.DataSource = packageList;
            cbPackage.SelectedIndex = -1;
            FillBox();
            packageData = new clsPackage();
            if (cbCurrency.SelectedIndex!=-1)
            {
                clsCurrency currency = (clsCurrency)cbCurrency.SelectedItem;
                packageData.CurrencyId = currency.CurrencyId;
                packageData.CurrencyRate = currency.CurrencyRate;
            }
           
            ResetFields();
            this.PackageID = 0;
            Datachange = false;
        }

        private void DisplayData(int pkId)
        {
            cbPackage.DataSource = null;
            cbPackage.ValueMember = "PackageID";
            cbPackage.DisplayMember = "PackageName";
            cbPackage.DataSource = packageList;
            cbPackage.SelectedIndex = -1;
            cbPackage.SelectedValue = pkId;
            this.PackageID = pkId;
            Datachange = false;
        }

        private void FillBox()
        {
            List<clsCountry> Countries = countryConnection.CountryArrayList();
            cbCountry.ValueMember = "countryId";
            cbCountry.DisplayMember = "englishName";
            cbCountry.DataSource = Countries;
            cbCountry.SelectedIndex = -1;
            Currencies = currencyConnection.CurrencyArrayList();

        
            cbCurrency.ValueMember = "currencyId";
            cbCurrency.DisplayMember = "currencyName";
            cbCurrency.DataSource = Currencies;

            Item itemConnection = new Item();
            itemList = itemConnection.getItemList();
            clbItems.ValueMember = "ItemID";
            clbItems.DataSource = itemList;
            clbItems.DisplayMember = "ToToString";



            Datachange = false;
        }

        public void ResetFields()
        {
            packageItemsIDS.Clear();
            cbCountry.SelectedIndex = -1;
            cbCountry.Text = "";
            txtDay.Text = "";
            txtPrice.Text = "";
            txtTotalCost.Text = "";
            txtProfit.Text = "";
            TotalPrice = 0;
            TotalCost = 0;
            stsLblInfo.Text = "";
            dgv.Rows.Clear();
            dgv.Refresh();
            stsLblInfo.Text = "";
            foreach (int i in clbItems.CheckedIndices)
            {
                clbItems.SetItemCheckState(i, CheckState.Unchecked);
            }
            Datachange = false;
        }

        private void cbPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            ResetFields();
            if (cbPackage.SelectedIndex != -1 && packageList.Count > 0)
            {
                dgv.Rows.Clear();
                dgv.Refresh();
                PackageID = int.Parse(cbPackage.SelectedValue.ToString());
                packageData = packageList[cbPackage.SelectedIndex];
                txtDay.Text = packageData.packageDays.ToString();
                cbCountry.SelectedValue = packageData.countryID;
                packageItemsIDS.Clear();
                for (int i = 0; i <= packageData.packageItems.Count - 1; i++)
                {
                    packageItemsIDS.Add(packageData.packageItems[i].itemID);
                    dgv.Rows.Add(packageData.packageItems[i].itemName.ToString(), packageData.packageItems[i].itemDescription.ToString(), packageData.packageItems[i].Cost.ToString(currencyFormat)+" "+packageData.packageItems[i].CurrecnySymbol);
                }
                TotalPrice = packageData.Price*packageData.CurrencyRate;
                TotalCost = packageData.Cost * packageData.CurrencyRate;
                cbCurrency.SelectedIndex = -1;
                cbCurrency.SelectedValue = packageData.CurrencyId;
                txtRate.Text = packageData.CurrencyRate.ToString();
            }
            Datachange = false;

        }

        public void SaveDataInfo()
        {
            bool idExsist;
            if(cbCountry.SelectedIndex==-1)
            {
                MessageBox.Show("Please Choose a Country to Continue...");
                return;
            }
            try
            {
                clsPackage package = new clsPackage();
                package.packageID = this.PackageID;
                if (cbPackage.SelectedIndex == -1)
                {
                    package.packageID = 0;

                }
                else
                {
                    package.packageID = int.Parse(cbPackage.SelectedValue.ToString());
                }
                if (string.IsNullOrWhiteSpace(cbPackage.Text))
                {
                    package.packageName = " ";
                }
                else
                {
                    package.packageName = cbPackage.Text.ToString();
                }
                if (string.IsNullOrWhiteSpace(txtDay.Text))
                {
                    package.packageDays = 0;
                }
                else
                {
                    package.packageDays = int.Parse(txtDay.Text);
                }
                if (cbCountry.SelectedIndex == -1)
                {
                    package.countryID = 0;
                }
                else
                {
                    package.countryID = int.Parse(cbCountry.SelectedValue.ToString());
                }
                if(!string.IsNullOrWhiteSpace(txtPrice.Text))
                {
                    package.Price = Convert.ToDecimal(txtPrice.Text);
                }
                else
                {
                    package.Price = 0;
                }
                if (cbCurrency.SelectedIndex == -1)
                {
                    package.CurrencyId = 0;
                }
                else
                {
                    package.CurrencyId = int.Parse(cbCurrency.SelectedValue.ToString());
                }
                if (!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    package.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }
                else
                {
                    package.CurrencyRate = 0;
                }
               
                package.packageItems = packageData.packageItems;
                package.Profit = package.Price - package.Cost;
                idExsist = packageConnection.PackageIDExists(package.packageID);
                if (idExsist)
                {
                    packageConnection.UpdatePackageInfo(package);
                    packageItemConnection.UpdatePackageInfo(package.packageID, package.packageItems);
                    stsLblInfo.Text = "Change has been saved successfully";
                }
                else
                {
                    packageConnection.AddNewPackage(package);
                    this.PackageID= packageConnection.ReadLastNo();
                    packageItemConnection.AddPackageItem(this.PackageID, package.packageItems);
                    stsLblInfo.Text = "New Package has been Added successfully";
                }
                
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
                packageList = packageConnection.PackageArrayList();
                DisplayData(this.PackageID);
                Datachange = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                Datachange = false;
                stsLblInfo.Text = "Error: Not saved,Please Check";
                MessageBox.Show(ex.Message);
            }
        }
        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Package";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            
            if (Datachange)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                  
                }
            }
            PackageSavedID = PackageID;
            ResetFields();
            cbPackage.DataSource = null;
            PackageID = 0;
            packageData = new clsPackage();
            if (cbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cbCurrency.SelectedItem;
                packageData.CurrencyId = currency.CurrencyId;
                packageData.CurrencyRate = currency.CurrencyRate;
            }
            packageItemsIDS.Clear();
            btnCancel.Visible = true;
            Datachange = false;

        }

     



       


        private void cbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                txtPrice.Text = (TotalPrice / currency.CurrencyRate).ToString(currencyFormat);
                lCurrencyName.Text = currency.CurrencySymbol;
                lblCost.Text= currency.CurrencySymbol;
                lblProfit.Text = currency.CurrencySymbol;
                txtTotalCost.Text = (TotalCost / currency.CurrencyRate).ToString(currencyFormat);
                txtProfit.Text = ((TotalPrice - TotalCost) / currency.CurrencyRate).ToString(currencyFormat);
            }
            else
            {
                txtRate.Text = "";
                txtPrice.Text = TotalPrice.ToString(currencyFormat);
                txtTotalCost.Text = TotalCost.ToString(currencyFormat);
                lCurrencyName.Text = "L.L";
                lblCost.Text = "L.L";
                lblProfit.Text = "L.L";
            }
            Datachange = true;
        }





        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (var item in clbItems.CheckedItems)
            {
                var itemdata = (clsItem)item;
                if (packageItemsIDS.Contains(itemdata.itemID))
                {
                    MessageBox.Show("You choose a selected Item");
                    break;
                }
                else
                {
                    packageItemsIDS.Add(itemdata.itemID);
                    packageData.packageItems.Add(itemdata);
                    dgv.Rows.Add(itemdata.itemName.ToString(), itemdata.itemDescription.ToString(), itemdata.Cost.ToString(currencyFormat)+" "+itemdata.CurrecnySymbol);
                    dgv.Refresh();
                    Datachange = true;
                }

            }
            if(!string.IsNullOrWhiteSpace(txtPrice.Text))
            {
                packageData.Price = Convert.ToDecimal(txtPrice.Text);
            }
            else
            {
                packageData.Price = 0;
            }
           
          
            foreach (int i in clbItems.CheckedIndices)
            {
                clbItems.SetItemCheckState(i, CheckState.Unchecked);
            }

            TotalPrice = packageData.Price*packageData.CurrencyRate;
            TotalCost = packageData.Cost * packageData.CurrencyRate;
            cbCurrency.SelectedIndex = -1;
            cbCurrency.SelectedValue = packageData.CurrencyId;
            btnAdd.Visible = false;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

            if (dgv.RowCount > 0 && packageData.packageItems.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgv.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    
                    int index = dgv.SelectedRows[0].Index;
                    dgv.Rows.RemoveAt(index);
                    packageItemsIDS.RemoveAt(index);
                    packageData.packageItems.RemoveAt(index);
                    if (!string.IsNullOrWhiteSpace(txtPrice.Text))
                    {
                        packageData.Price = Convert.ToDecimal(txtPrice.Text);
                    }
                    else
                    {
                        packageData.Price = 0;
                    }

                    TotalPrice = packageData.Price*packageData.CurrencyRate;
                    TotalCost = packageData.Cost*packageData.CurrencyRate;
                    cbCurrency.SelectedIndex = -1;
                    cbCurrency.SelectedValue = packageData.CurrencyId;
                    Datachange = true;

                }
            }
            btnRemove.Visible = false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            Datachange = true;
        }

        private void cbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Datachange = true;
        }

        private void txtDay_TextChanged(object sender, EventArgs e)
        {
            Datachange = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (Datachange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.PackageID == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            PackageID = PackageSavedID;
            btnCancel.Visible = false;
            ResetFields();
            DisplayData(PackageID);
            Datachange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }

        }

        private void txtDay_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.PackageID != 0)
            {
                try
                {
                    bool myResult;
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Package", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {

                        if (packageConnection.PackageUsed(PackageID))
                        {
                            MessageBox.Show("This Package Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        bool myResult1 = packageItemConnection.DeletePackageItems(PackageID);
                        if (!myResult1)
                        {
                            return;
                        }
                        else
                        {
                            myResult = packageConnection.DeletePackage(PackageID);
                            if (!myResult)
                            {
                                return;
                            }
                            simpleSound.Play();
                            ResetFields();
                            packageList = packageConnection.PackageArrayList();
                            packageItemsIDS.Clear();
                            DisplayData(0);
                            Datachange = false;
                            stsLblInfo.Text = "Successfully Deleted";
                        }
                    }

                }
                catch (Exception ex)
                {
                    Datachange = false;
                    stsLblInfo.Text = "Error: Deleting the package";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cbPackage.Items.Count > 0)
            {
                if (cbPackage.SelectedIndex == 0 || cbPackage.SelectedIndex == -1)
                {
                    int index = cbPackage.Items.Count;
                    cbPackage.SelectedIndex = index - 1;
                }
                else
                {
                    cbPackage.SelectedIndex = cbPackage.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cbPackage.Items.Count > 0)
            {
                if (cbPackage.SelectedIndex == cbPackage.Items.Count - 1 || cbPackage.SelectedIndex == -1)
                {

                    cbPackage.SelectedIndex = 0;
                }
                else
                {
                    cbPackage.SelectedIndex = cbPackage.SelectedIndex + 1;
                }
            }
        }

        private void frmPackage_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (cbPackage.SelectedIndex != -1)
            {
                PackageID = Convert.ToInt32(cbPackage.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (Datachange == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void cbCountry_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Datachange = true;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {

            Item itemConnection = new Item();
            itemList = itemConnection.getItemList();
            clbItems.ValueMember = "ItemID";
            clbItems.DataSource = itemList;
            clbItems.DisplayMember = "ToToString";

        }

        private void cbCountry_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cbCountry.SelectedIndex != -1)
                 id = int.Parse(cbCountry.SelectedValue.ToString());


            List<clsCountry> Countries = countryConnection.CountryArrayList();

            cbCountry.ValueMember = "countryId";
            cbCountry.DataSource = Countries;
            cbCountry.DisplayMember = "EnglishName";
            cbCountry.SelectedValue = id;
            Datachange = false;

        }

        private void cbCurrency_Click(object sender, EventArgs e)
        {

            int id = 0;
            if(cbCurrency.SelectedIndex!=-1)
            {
                id = int.Parse(cbCurrency.SelectedValue.ToString());
            }
            Currencies = currencyConnection.CurrencyArrayList();
            cbCurrency.ValueMember = "currencyId";
            cbCurrency.DisplayMember = "currencyName";
            cbCurrency.DataSource = Currencies;
            cbCurrency.SelectedValue = id;
            Datachange = false;

        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.RowCount > 0)
            {
                if (e.RowIndex >= 0)
                {
                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cbPackage.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCountry.Enabled = false;
            //cbCurrency.Enabled = false;
            //clbItems.Enabled = false;
            //dgv.Enabled = false;
            btnRemove.Enabled = false;
            btnAdd.Enabled = false;
            //btnLRefresh.Enabled = false;
          
            txtDay.ReadOnly = true;
            txtPrice.ReadOnly = true;
            txtRate.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cbPackage.DropDownStyle = ComboBoxStyle.DropDown;
            cbCountry.Enabled = true;
            cbCurrency.Enabled = true;
            clbItems.Enabled = true;
            dgv.Enabled = true;
            btnRemove.Enabled = true;
            btnAdd.Enabled = true;
            btnLRefresh.Enabled = true;
          
            txtDay.ReadOnly = false;
            txtPrice.ReadOnly = true;
            txtRate.ReadOnly = true;
        }

        private void TStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void clbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(clbItems.SelectedIndex!=-1)
            {
                btnAdd.Visible = true;
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            Datachange = true;
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character!='.')
            {
                e.Handled = true;
            }
            else
            {
                if(character=='.' && txtPrice.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }

                decimal price = 0;
                decimal cost = 0;
                if (!string.IsNullOrWhiteSpace(txtTotalCost.Text))
                {
                    cost = decimal.Parse(txtTotalCost.Text.ToString());
                }
                if (character != 8)
                {
                    price = decimal.Parse(txtPrice.Text + character.ToString());

                }
                else
                {
                    if (txtPrice.Text.Length - 1 > 0)
                    {
                        price = decimal.Parse(txtPrice.Text.Substring(0, txtPrice.Text.Length - 1));
                    }


                }
                txtProfit.Text = (price - cost).ToString();
            }
        }

     
        private void txtPrice_KeyUp(object sender, KeyEventArgs e)
        {
            decimal cost = 0, price = 0;
            if (!string.IsNullOrWhiteSpace(txtTotalCost.Text))
            {
                cost = Convert.ToDecimal(txtTotalCost.Text);
            }
            if (!string.IsNullOrWhiteSpace(txtPrice.Text))
            {
                price = Convert.ToDecimal(txtPrice.Text);
            }
            txtProfit.Text = (price - cost).ToString();
        }
    }
}
