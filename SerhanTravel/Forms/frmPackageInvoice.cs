﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using SerhanTravel.Print;

namespace SerhanTravel.Forms
{
    public partial class frmPackageInvoice : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList=new List<clsCustomer>();
        List<clsPackageInvoice> CustomerInvoiceList=new List<clsPackageInvoice>();
        List<clsPackage> packageList= new List<clsPackage>();
        List<clsPackage> AllPackages=new List<clsPackage>();
        List<clsItem> itemList=new List<clsItem>();
        List<clsItem> selectedItemList=new List<clsItem>();
        PackageInvoice invoiceConnection=new PackageInvoice();
        Customer customerConnection=new Customer();
        clsPackageInvoice invoiceData=new clsPackageInvoice();
        clsCustomer customerData=new clsCustomer();
        bool skip = false;
        bool DataChanged = false;
        bool iamAdding = false;
        int InvoiceId = 0;
        int InvoiceSavedID=0;
        private int CustomerSavedID;
        int CustomerID = 0;
        private List<clsCurrency>  currencies = new List<clsCurrency>();
        decimal totalPrice = 0,packagePrice=0,packageRate=0;
        bool add = false;
        bool update = false;
        private decimal netPrice;
        private bool allowRemove;
        string currencyFormat;
        clsAccessRights right;
        bool canUpdateOnly;
        bool canAddOnly = false;
        Currency currencyConnection;
        internal frmPackageInvoice(clsAccessRights right)
        {
            InitializeComponent();
            dgvPackages.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPackages.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPackages.MultiSelect = false;
            cmbCustomerName.SelectedIndex = -1;
            lbPackage.HorizontalScrollbar = true;
            clbItem.HorizontalScrollbar = true;
            dgvPackages.ScrollBars = ScrollBars.Both;
            dgvPackages.AutoGenerateColumns = false;
            this.right = right;
        }

        private void ResetFields()
        {
            cmbInvoceNo.DataSource = null;
            clbItem.DataSource = null;
            dgvPackages.DataSource = null;
            cmbInvoceNo.Text = "";
            dgvPackages.Rows.Clear();
            txtTotalPrice.Clear();
            txtCustomerPackage.Clear();
            txtCustomerName.Clear();
            nmDiscount.Value = 0;
            netPrice = 0;
            txtNetPrice.Clear();
            dtpInvoiceDate.Value = DateTime.Today;
            cmbInvoceNo.SelectedIndex = -1;
            clbItem.SelectedIndex = -1;
            packageList.Clear();
            totalPrice = 0;
            stsLblInfo.Text = "";
            packagePrice = 0;
            packageRate = 0;
            txtPacagePrice.Clear();
            lbPackage.ClearSelected();
            lbResult.Visible = false;
            DataChanged = false;

        }

        private void FillCombobox()
        {
         Package packageConnection = new Package();
            AllPackages = packageConnection.PackageArrayList();
            lbPackage.ValueMember = "PackageID";
            lbPackage.DataSource = AllPackages;
            lbPackage.DisplayMember = "ToStrings";
            lbPackage.ClearSelected();

        clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            DataChanged = false;
        }

        private void frmPackageInvoice_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            lbResult.Visible = false;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            dtpInvoiceDate.Value = DateTime.Today;
            customerConnection = new Customer();
            invoiceConnection = new PackageInvoice();
            customerList = customerConnection.CustomersArrayList();
            dgvPackages.Columns[0].DataPropertyName = "CustomerName";
            dgvPackages.Columns[1].DataPropertyName = "PackageName";
            dgvPackages.Columns[2].DataPropertyName = "CountryName";
            dgvPackages.Columns[3].DataPropertyName = "PackageDays";
            dgvPackages.Columns[4].DataPropertyName = "PriceString";
            dgvPackages.Columns[5].DataPropertyName = "ProfitString";
            FillCombobox();
            InvoiceId = 0;
            ResetFields();
            DataChanged = false;
            
        }

        private void DisplayData(int custmId,int invoiceId)
        {
            iamAdding = false;
            cmbCustomerName.DataSource = null;
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedValue = custmId;
            cmbInvoceNo.SelectedValue = invoiceId;
            skip = true;
            DataChanged = false;
            
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            lbResult.Visible = false;
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                ResetFields();
                InvoiceId = 0;
              
                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                if (CustomerID != 0)
                {
                    customerData = customerList[cmbCustomerName.SelectedIndex];
                    txtCustomerName.Text = customerData.CustomerName;
                }

                if (iamAdding)
                {

                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.SelectedIndex = -1;
                    CustomerInvoiceList.Clear();
                    packageList.Clear();
                    return;
                }
                

            
               
             
                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(CustomerID);
                
               if (CustomerInvoiceList.Count > 0)
                {
                    cmbInvoceNo.Visible = true;
                    lblInvoiceNo.Visible = true;
                    
                    cmbInvoceNo.ValueMember = "invoiceId";
                    cmbInvoceNo.DataSource = CustomerInvoiceList;
                    cmbInvoceNo.DisplayMember = "invoiceId";
                    cmbInvoceNo.SelectedValue =InvoiceId;
                    
                }
                else
                {
                 
                    cmbInvoceNo.Visible = false;
                    lblInvoiceNo.Visible = false;
                    ResetFields();
                    if (CustomerID != 0)
                    {
                        
                        txtCustomerName.Text = customerData.CustomerName;
                    }
                }
                
                
                DataChanged = false;

                skip = true;
                
            }
            else
            {
                skip = false;
            }
     
            DataChanged = false;
           

        }


        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            if (cmbInvoceNo.SelectedIndex != -1 && CustomerInvoiceList.Count > 0)
            {
                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                invoiceData = CustomerInvoiceList[cmbInvoceNo.SelectedIndex];
                dtpInvoiceDate.Value = invoiceData.InvoiceDate;
                txtCustomerPackage.Clear();
                txtCustomerName.Text = invoiceData.CustomerName;
                packageList.Clear();
                packageList = invoiceConnection.PackagesInvoicesArrayList(InvoiceId);
                nmDiscount.Value = Convert.ToDecimal(invoiceData.Discount);

                dgvPackages.DataSource = null;
                dgvPackages.DataSource = packageList;
                dgvPackages.ClearSelection();
                totalPrice = 0;
                totalPrice = invoiceData.NetPrice;
                netPrice = totalPrice - totalPrice * (invoiceData.Discount * Convert.ToDecimal(0.01));
                int currencyId = 0;
                if(cmbCurrency.SelectedIndex!=-1)
                {
                    currencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                cmbCurrency.SelectedIndex = -1;
                cmbCurrency.SelectedValue = currencyId;
            }
            DataChanged = false;
        }


        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                txtNetPrice.Text = (netPrice / currency.CurrencyRate).ToString(currencyFormat);
                txtPacagePrice.Text = (packagePrice * packageRate / currency.CurrencyRate).ToString(currencyFormat);
                lblPackagePrice.Text = currency.CurrencySymbol;
                lblTotalPrice.Text = currency.CurrencySymbol;
                lblNetPrice.Text = currency.CurrencySymbol;
            }
            else
            {
                txtNetPrice.Text = (netPrice).ToString(currencyFormat);
                txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                lblNetPrice.Text = "";
                lblTotalPrice.Text = "";
                lblPackagePrice.Text = "";
            }
        }


        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                return;
            }
            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                return; // return with listbox's Visible set to false if nothing found
            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;
            }
            lbResult.Visible = false;
        }


        private void SaveDataInfo()
        {

            if (cmbCustomerName.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a customer");
                if (iamAdding)
                {
                    btnCancel.Visible = true;
                }

                return;
            }
            else if(string.IsNullOrWhiteSpace(txtCustomerName.Text))
            {
                MessageBox.Show("Please Enter the Customer Name.");
                return;
            }
            bool RecExists;
            try
            {
                clsPackageInvoice invoice = new clsPackageInvoice();
                if (cmbCustomerName.SelectedIndex != -1)
                {
                    invoice.CustomerId = int.Parse(cmbCustomerName.SelectedValue.ToString());
                }
                else
                {
                    invoice.CustomerId = 0;
                }

                if (cmbInvoceNo.SelectedIndex == -1)
                {
                    invoice.InvoiceId = 0;
                }
                else
                {
                    invoice.InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                }
              
                if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                {
                    invoice.CustomerName = string.Empty;
                }
                else
                {
                    invoice.CustomerName = txtCustomerName.Text;
                }
                
               
       
                invoice.Discount = decimal.Parse(nmDiscount.Value.ToString());
                invoice.NetPrice = netPrice;
                invoice.InvoiceDate = dtpInvoiceDate.Value;
                RecExists = invoiceConnection.InvoiceIDExists(invoice.InvoiceId);
                
                if (RecExists == true)
                {
                    invoiceConnection.UpdateInvoiceInfo(invoice);
                    invoiceConnection.UpdateInvoiceDetails(invoice.InvoiceId, packageList);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    invoiceConnection.AddNewInvoice(invoice);
                    int invoicenum = invoiceConnection.ReadLastNo();
                    invoiceConnection.AddInvoicePackage(invoicenum, packageList);
                    invoiceConnection.AddFinal(invoicenum, packageList);
                    invoice.InvoiceId = invoicenum;
                    stsLblInfo.Text = "New Invoice has been added successfully";
                }
                InvoiceSavedID = invoice.InvoiceId;
                InvoiceId = InvoiceSavedID;
                CustomerInvoiceList.Clear();
               
                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(invoice.CustomerId);

                if (CustomerInvoiceList.Count > 0)
                {
                    lblInvoiceNo.Visible = true;
                    cmbInvoceNo.Visible = true;
                }
                DisplayData(CustomerID, InvoiceId);
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

 

        private void TStripSave_Click(object sender, EventArgs e)
        {
        

           if (DataChanged)
            {
              
                btnCancel.Visible = false;
                if (canUpdateOnly && this.InvoiceId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void dtpInvoiceDate_ValueChanged(object sender, EventArgs e)
        {
            if (!iamAdding)
                DataChanged = true;
        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            if (clbItem.CheckedItems.Count == 0)
            {
                MessageBox.Show("You should choose some items for your package");
            }
            else
            {
                List<clsItem> items = new List<clsItem>();
                foreach (clsItem item in clbItem.CheckedItems)
                {
                    items.Add(item);
                }
              
                if (add  && !update )
                {
              

                    clsPackage Selectedpackage = (clsPackage)lbPackage.SelectedItem;
                    clsPackage package = new clsPackage();
       
                    package.packageID = Selectedpackage.packageID;
                    package.packageName = Selectedpackage.packageName;
                    package.countryID = Selectedpackage.countryID;
                    package.packageDays = Selectedpackage.packageDays;
                    package.packageItems = items;
                    package.CurrencyRate = Selectedpackage.CurrencyRate;
                    package.Price = Selectedpackage.Price;
                    package.CurrencyId = Selectedpackage.CurrencyId;
                    if(cmbCurrency.SelectedIndex!=-1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        package.CurrencyId = currency.CurrencyId;
                        package.CurrencyRate = currency.CurrencyRate;
                    }
                    if(!string.IsNullOrWhiteSpace(txtPacagePrice.Text))
                    {
                        package.Price = Convert.ToDecimal(txtPacagePrice.Text);
                    }
                    package.Profit = package.Price - package.Cost;
                    if(!string.IsNullOrWhiteSpace(txtCustomerPackage.Text))
                    {
                        package.CustomerName = txtCustomerPackage.Text;
                    }
                    else
                    {
                        package.CustomerName = " ";
                    }
                    packageList.Add(package);
                }
                else
                {
                    int index = dgvPackages.SelectedRows[0].Index;
                    packageList[index].packageItems = items;
                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        packageList[index].CurrencyId = currency.CurrencyId;
                        packageList[index].CurrencyRate = currency.CurrencyRate;
                    }
                    if (!string.IsNullOrWhiteSpace(txtPacagePrice.Text))
                    {
                        packageList[index].Price = Convert.ToDecimal(txtPacagePrice.Text);
                    }
                    packageList[index].Profit = packageList[index].Price - packageList[index].Cost;
                    if (!string.IsNullOrWhiteSpace(txtCustomerPackage.Text))
                    {
                        packageList[index].CustomerName = txtCustomerPackage.Text;
                    }
                    else
                    {
                        packageList[index].CustomerName = " ";
                    }
                }

                dgvPackages.DataSource = null;
                dgvPackages.DataSource = packageList;
                totalPrice = 0;
                   for (int i = 0; i < packageList.Count; i++)
                    {
                        totalPrice += packageList[i].Price* packageList[i].CurrencyRate;
                    }
                
                netPrice = totalPrice - (totalPrice * decimal.Parse(nmDiscount.Value.ToString()) *Convert.ToDecimal( 0.01));
                int currencyId = 0;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    currencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                cmbCurrency.SelectedIndex = -1;
                cmbCurrency.SelectedValue = currencyId;
                dgvPackages.Refresh();
                dgvPackages.ClearSelection();
                lbPackage.SelectedIndex = -1;
                clbItem.DataSource = null;
                DataChanged = true;
                add = false;
                update = false;
                txtCustomerPackage.Clear();
            }
            btnAdd.Visible = false;
        }

        private void btnRemove_Click_1(object sender, EventArgs e)
        {
            if (dgvPackages.RowCount > 0&&packageList.Count>0&&allowRemove)
            {
                allowRemove = false;
                if (dgvPackages.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        int index = dgvPackages.SelectedRows[0].Index;
                        packageList.RemoveAt(index);
                        dgvPackages.DataSource = null;
                        dgvPackages.Rows.Clear();
                        dgvPackages.DataSource = packageList;
                        totalPrice = 0;
                      for (int i = 0; i < packageList.Count; i++)
                        {
                            totalPrice += packageList[i].Price* packageList[i].CurrencyRate;
                        }
                    

                        netPrice = totalPrice - (totalPrice * decimal.Parse(nmDiscount.Value.ToString()) * Convert.ToDecimal( 0.01));
                        int currencyId = 0;
                        if(cmbCurrency.SelectedIndex!=-1)
                        {
                            currencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                        }
                         cmbCurrency.SelectedIndex = -1;
                         cmbCurrency.SelectedValue = currencyId;
                         dgvPackages.ClearSelection();
                         clbItem.DataSource = null;
                         DataChanged = true;

                    }

                
            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
          
            Package packageConnection = new Package();
            AllPackages = packageConnection.PackageArrayList();
            lbPackage.ValueMember = "PackageID";
            lbPackage.DataSource = AllPackages;
            lbPackage.DisplayMember = "ToStrings";
            lbPackage.ClearSelected();
            clbItem.DataSource = null;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblInvoiceNo.Visible = true;
            cmbInvoceNo.Visible = true;
            btnCancel.Visible = false;
            iamAdding = false;
            InvoiceId = InvoiceSavedID;
            CustomerID = CustomerSavedID; ;
            ResetFields();
            DisplayData(CustomerID, InvoiceId);
            DataChanged = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
           
            if (this.InvoiceId != 0)
            {
                try
                {
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {

                        if (invoiceConnection.InvoiceUsed(InvoiceId))
                        {
                            MessageBox.Show("This Invoice Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        bool myResult1 = invoiceConnection.DeleteInvoicePackageDetails(InvoiceId);
                        if (!myResult1)
                        {
                            return;
                        }
                        else
                        {
                            bool myResult2 = invoiceConnection.DeleteInvoicePackages(InvoiceId);
                            if (!myResult2)
                            {
                                return;
                            }
                            else
                            {
                                bool myResult3 = invoiceConnection.DeleteInvoice(InvoiceId);
                                if (myResult3)
                                {
                                    ResetFields();
                                    int index = CustomerInvoiceList.IndexOf(invoiceData);
                                    CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(CustomerID);
                                    index -= 1;
                                    InvoiceId = 0;
                                    if (index >= 0)
                                    {
                                        InvoiceId = CustomerInvoiceList[index].InvoiceId;
                                    }
                                    else if (index == -1 && CustomerInvoiceList.Count > 0)
                                    {
                                        index = 0;
                                        InvoiceId = CustomerInvoiceList[index].InvoiceId;
                                    }

                                    DisplayData(CustomerID, InvoiceId);
                                    DataChanged = false;
                                    stsLblInfo.Text = "Successfully Deleted";
                                    simpleSound.Play(); 
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DataChanged = false;
                    stsLblInfo.Text = "Error: Deleting the Invoice";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }


        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == 0 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.Items.Count - 1;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == cmbInvoceNo.Items.Count - 1 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = 0;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex + 1;
                }
            }
        }


        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();
        
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue=id;
        }


       




        private void TStripNew_Click(object sender, EventArgs e)
        {
            add = true;
            update = false;
            iamAdding = true;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;

            stsLblInfo.Text = "Add Invoice";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }

            InvoiceSavedID = InvoiceId;
            CustomerSavedID = CustomerID;
            InvoiceId = 0;
            cmbCustomerName.SelectedIndex = -1;
            ResetFields();
            btnCancel.Visible = true;
            DataChanged = false;
           
        }

        
        


        

        

        
        

        private void lbPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
      
            if (lbPackage.SelectedIndex != -1)
            {
                add = true;
                update = false;
                btnRemove.Visible = false;
                btnAdd.Visible = true;
                btnAdd.Text = "Add ";
                dgvPackages.ClearSelection();
                clsPackage package = (clsPackage)lbPackage.SelectedItem;
                clbItem.ValueMember = "itemId";
                clbItem.DataSource = package.packageItems;
                clbItem.DisplayMember = "ToStrings";
                for (int i = 0; i < clbItem.Items.Count; i++)
                {
                    clbItem.SetItemChecked(i, true);
                }
                clbItem.ClearSelected();
                packagePrice = package.Price;
                packageRate = package.CurrencyRate;
                decimal Rate = 1;
                if(cmbCurrency.SelectedIndex!=-1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    Rate = currency.CurrencyRate;
                }
                txtPacagePrice.Text = ((packagePrice * packageRate) / Rate).ToString(currencyFormat);
            }
        }

        

        private void dgvPackages_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
            if (dgvPackages.RowCount > 0)
            {
                if (e.RowIndex >= 0)
                {
                    add = false;
                    update = true;
                    allowRemove = true;
                    
                    btnRemove.Visible = true;
                    List<clsItem> items = new List<clsItem>();
                    clbItem.DataSource = null;
                    lbPackage.ClearSelected();
                    clsPackage package = (clsPackage)dgvPackages.SelectedRows[0].DataBoundItem;
                    items = dgvCellClick(package.packageID);
                    clbItem.ValueMember = "itemId";
                    clbItem.DataSource = items;
                    clbItem.DisplayMember = "ToStrings";
                    clbItem.ClearSelected();
                    List<int> indexList = GetIndexList(package, items);
                    checkItems(indexList);
                    packagePrice = package.Price;
                    packageRate = package.CurrencyRate;
                    decimal Rate = 1;
                    if(cmbCurrency.SelectedIndex!=-1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        Rate = currency.CurrencyRate;
                    }
                    txtPacagePrice.Text = ((packagePrice * packageRate) / Rate).ToString(currencyFormat);
                    txtCustomerPackage.Text = package.CustomerName;
                }
            }
        }

        

        internal List<clsItem> dgvCellClick(int package)
        {
            clsPackage pack = new clsPackage();
            List<clsItem> items = new List<clsItem>();
            btnAdd.Text = "Update";
            btnAdd.Visible = true;
            for (int i = 0; i < AllPackages.Count; i++)
            {
                if (package == AllPackages[i].packageID)
                {
                    pack = AllPackages[i];
                    break;
                }
            }
            items = pack.packageItems;
            return items;

        }

        internal List<int> GetIndexList(clsPackage package, List<clsItem> items)
        {

            List<int> indexList = new List<int>();
            for (int i = 0; i < package.packageItems.Count; i++)
            {
                for (int j = 0; j < items.Count; j++)
                {
                    if (package.packageItems[i].itemID == items[j].itemID)
                    {
                        indexList.Add(j);
                    }
                }
            }
            return indexList;
        }

        internal void checkItems(List<int> indexList)
        {
            for (int i = 0; i < indexList.Count; i++)
            {
                clbItem.SetItemChecked(indexList[i], true);
            }
        }

        private void frmPackageInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbInvoceNo.SelectedIndex != -1)
            {
                InvoiceId = Convert.ToInt32(cmbInvoceNo.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void nmDiscount_ValueChanged(object sender, EventArgs e)
        {
            if (totalPrice != 0)
            {
                netPrice = totalPrice - totalPrice * decimal.Parse(nmDiscount.Value.ToString()) *Convert.ToDecimal( 0.01);
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    txtNetPrice.Text = (netPrice / currency.CurrencyRate).ToString(currencyFormat);
                }
                else
                {
                    txtNetPrice.Text = netPrice.ToString(currencyFormat);
                }

                DataChanged = true;
            }
        
        }

        private void TStripPrint_Click(object sender, EventArgs e)
        {

           
            if (cmbInvoceNo.SelectedIndex != -1 && CustomerInvoiceList.Count > 0 && packageList.Count > 0)
            {

                dgvPackages.ClearSelection();
                if (string.IsNullOrWhiteSpace(txtCustomerName.Text) )
                {
                    MessageBox.Show("Please enter Customer Name ");
                    return;
                }
              
                clsCurrency currency;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    currency = (clsCurrency)cmbCurrency.SelectedItem;

                }
                else
                {
                    currency = new clsCurrency();
                    currency.CurrencyRate = 1;
                    currency.CurrencyId = 0;
                    currency.CurrencySymbol = "L.L";
                }
                if (DataChanged == true)
                {

                    DialogResult myReply = MessageBox.Show("Do you want to save changes before printing ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        SaveDataInfo();
                        cmbCurrency.SelectedValue = currency.CurrencyId;

                    }
                }
                clsPackageInvoice invoice = (clsPackageInvoice)cmbInvoceNo.SelectedItem;
                clsCustomer customer = (clsCustomer)cmbCustomerName.SelectedItem;
                invoice.CustomerName = txtCustomerName.Text;
                List<clsPackage> printedList = new List<clsPackage>();
                for (int i=0;i<packageList.Count;i++)
                {
                    clsPackage package = new clsPackage();

                    package.packageName =(i+1)+". "+ packageList[i].packageName+"\n"+ packageList[i].CustomerName;
                    package.packageID =  packageList[i].packageID;
                    package.countryID =  packageList[i].countryID;
                    package.packageDays =  packageList[i].packageDays;
                    package.packageItems = packageList[i].packageItems;
                    package.Price = packageList[i].Price;
                    package.CurrencyId = packageList[i].CurrencyId;
                    printedList.Add(package);

                }
                
                printPackageInvoice frm = new printPackageInvoice(printedList, invoice,currency,double.Parse(txtTotalPrice.Text),double.Parse(txtNetPrice.Text));
                frm.Show();
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void dgvPackages_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            add = false;
            update = true;
        

        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Printing)
            {
                TStripPrint.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }

        }

        private void readOnlyEveryThing()
        {
            //cmbInvoceNo.Enabled = false;
            //cmbCustomerName.Enabled = false;
            //cmbCurrency.Enabled = false;
            //clbItem.Enabled = false;
            //lbPackage.Enabled = false;
            dtpInvoiceDate.Enabled = false;
            //dgvPackages.Enabled = false;
            btnAdd.Enabled = false;
            btnRemove.Enabled = false;
            //btnLRefresh.Enabled = false;
            nmDiscount.Enabled = false;
            txtCustomerPackage.ReadOnly = true;


        }

        private void unReadOnlyEveryThing()
        {
            cmbInvoceNo.Enabled = true;
            cmbCustomerName.Enabled = true;
            cmbCurrency.Enabled = true;
            clbItem.Enabled = true;
            lbPackage.Enabled = true;
            dtpInvoiceDate.Enabled = true;
            dgvPackages.Enabled = true;
            btnAdd.Enabled = true;
            btnRemove.Enabled = true;
            btnLRefresh.Enabled = true;
            nmDiscount.Enabled = true;
            txtCustomerPackage.ReadOnly = false;
        }

        private void clbItem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvPackages_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (!iamAdding)
                DataChanged = true;
        }

        private void frmPackageInvoice_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }

        private void txtPacagePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtPacagePrice.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }

            }
        }

        }
}
