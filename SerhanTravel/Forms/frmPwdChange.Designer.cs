﻿namespace SerhanTravel.Forms
{
    partial class frmPwdChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblPasswordConfirm = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtLogInUserName = new System.Windows.Forms.TextBox();
            this.txtLogInPassword = new System.Windows.Forms.TextBox();
            this.txtLogInPasswordConfirmed = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleDescription = "btnCancel";
            this.btnCancel.AccessibleName = "btnCancel";
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(310, 161);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(82, 32);
            this.btnCancel.TabIndex = 143;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AccessibleDescription = "btnOk";
            this.btnOk.AccessibleName = "btnOk";
            this.btnOk.BackColor = System.Drawing.Color.LightGreen;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatAppearance.BorderSize = 0;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(170, 161);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(82, 32);
            this.btnOk.TabIndex = 142;
            this.btnOk.Text = "Update";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblPasswordConfirm
            // 
            this.lblPasswordConfirm.AccessibleDescription = "lblPasswordConfirm";
            this.lblPasswordConfirm.AccessibleName = "lblPasswordConfirm";
            this.lblPasswordConfirm.AutoSize = true;
            this.lblPasswordConfirm.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPasswordConfirm.ForeColor = System.Drawing.Color.White;
            this.lblPasswordConfirm.Location = new System.Drawing.Point(75, 117);
            this.lblPasswordConfirm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPasswordConfirm.Name = "lblPasswordConfirm";
            this.lblPasswordConfirm.Size = new System.Drawing.Size(53, 15);
            this.lblPasswordConfirm.TabIndex = 147;
            this.lblPasswordConfirm.Text = "Confirm";
            this.lblPasswordConfirm.Click += new System.EventHandler(this.lblPasswordConfirm_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AccessibleDescription = "lblPassword";
            this.lblPassword.AccessibleName = "lblPassword";
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.Color.White;
            this.lblPassword.Location = new System.Drawing.Point(44, 89);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(85, 15);
            this.lblPassword.TabIndex = 146;
            this.lblPassword.Text = "New Password";
            this.lblPassword.Click += new System.EventHandler(this.lblPassword_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AccessibleDescription = "lblUserName";
            this.lblUserName.AccessibleName = "lblUserName";
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(67, 61);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(62, 15);
            this.lblUserName.TabIndex = 145;
            this.lblUserName.Text = "Username";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblUserName.Click += new System.EventHandler(this.lblUserName_Click);
            // 
            // txtLogInUserName
            // 
            this.txtLogInUserName.AccessibleDescription = "txtLogInUserName";
            this.txtLogInUserName.AccessibleName = "txtLogInUserName";
            this.txtLogInUserName.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtLogInUserName.Location = new System.Drawing.Point(170, 61);
            this.txtLogInUserName.Margin = new System.Windows.Forms.Padding(2);
            this.txtLogInUserName.Name = "txtLogInUserName";
            this.txtLogInUserName.ReadOnly = true;
            this.txtLogInUserName.Size = new System.Drawing.Size(222, 20);
            this.txtLogInUserName.TabIndex = 139;
            this.txtLogInUserName.TabStop = false;
            // 
            // txtLogInPassword
            // 
            this.txtLogInPassword.AccessibleDescription = "txtLogInPassword";
            this.txtLogInPassword.AccessibleName = "txtLogInPassword";
            this.txtLogInPassword.Location = new System.Drawing.Point(170, 89);
            this.txtLogInPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtLogInPassword.Name = "txtLogInPassword";
            this.txtLogInPassword.PasswordChar = '*';
            this.txtLogInPassword.Size = new System.Drawing.Size(222, 20);
            this.txtLogInPassword.TabIndex = 140;
            this.txtLogInPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtLogInPasswordConfirmed
            // 
            this.txtLogInPasswordConfirmed.AccessibleDescription = "txtLogInPasswordConfirmed";
            this.txtLogInPasswordConfirmed.AccessibleName = "txtLogInPasswordConfirmed";
            this.txtLogInPasswordConfirmed.Location = new System.Drawing.Point(170, 117);
            this.txtLogInPasswordConfirmed.Margin = new System.Windows.Forms.Padding(2);
            this.txtLogInPasswordConfirmed.Name = "txtLogInPasswordConfirmed";
            this.txtLogInPasswordConfirmed.PasswordChar = '*';
            this.txtLogInPasswordConfirmed.Size = new System.Drawing.Size(222, 20);
            this.txtLogInPasswordConfirmed.TabIndex = 141;
            this.txtLogInPasswordConfirmed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblStatus
            // 
            this.lblStatus.AccessibleDescription = "lblStatus";
            this.lblStatus.AccessibleName = "lblStatus";
            this.lblStatus.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.Location = new System.Drawing.Point(0, 210);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(464, 26);
            this.lblStatus.TabIndex = 144;
            this.lblStatus.Text = "Update Password";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(166, 37);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 22);
            this.lblInfo.TabIndex = 148;
            // 
            // frmPwdChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(464, 236);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblPasswordConfirm);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtLogInUserName);
            this.Controls.Add(this.txtLogInPassword);
            this.Controls.Add(this.txtLogInPasswordConfirmed);
            this.Controls.Add(this.lblStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmPwdChange";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Password";
            this.Load += new System.EventHandler(this.frmPwdChange_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Button btnOk;
        internal System.Windows.Forms.Label lblPasswordConfirm;
        internal System.Windows.Forms.Label lblPassword;
        internal System.Windows.Forms.Label lblUserName;
        internal System.Windows.Forms.TextBox txtLogInUserName;
        internal System.Windows.Forms.TextBox txtLogInPassword;
        internal System.Windows.Forms.TextBox txtLogInPasswordConfirmed;
        internal System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblInfo;
    }
}