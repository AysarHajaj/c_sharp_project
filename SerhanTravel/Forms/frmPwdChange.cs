﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Objects;
using System.Data.SqlClient;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmPwdChange : Form
    {
        clsUser user;
        public frmPwdChange()
        {
            InitializeComponent();
        }
        internal frmPwdChange(clsUser user)
        {
            InitializeComponent();
            this.user = user;
        }
        private void lblUserName_Click(object sender, EventArgs e)
        {

        }

        private void lblPasswordConfirm_Click(object sender, EventArgs e)
        {

        }

        private void lblPassword_Click(object sender, EventArgs e)
        {

        }

        private void frmPwdChange_Load(object sender, EventArgs e)
        {
            
            txtLogInUserName.Text = user.UserName.ToString();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(txtLogInPassword.Text)&&!string.IsNullOrWhiteSpace(txtLogInPasswordConfirmed.Text))
            {
                string NewPassword = txtLogInPassword.Text;
                string confirmedPassword = txtLogInPasswordConfirmed.Text;
                if(NewPassword.Equals(confirmedPassword))
                {
                    user.Password = NewPassword;
                    
                    if(UpdatePassword())
                    {
                        MessageBox.Show("Your password has been updated");
                        this.Hide();
                    }
                    
                   
                 
                }
                else
                {
                
                    lblInfo.ForeColor = Color.Red;
                    lblInfo.Text = "Your passwords do not match...";
                    txtLogInPasswordConfirmed.Clear();
                }
            }
        }

        private bool UpdatePassword()
        {
            int cmdResult;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }

                string sqlQuery = "update Users set " +

                        "Password = @Password " +
                        "WHERE (UserId  = @UserId);";

                SqlTransaction myTrans = sqlCon.BeginTransaction();
                SqlCommand myCommand = new SqlCommand(sqlQuery, sqlCon, myTrans);

                myCommand.Parameters.Add("@UserId", SqlDbType.Int).Value = user.UserId;
                myCommand.Parameters.Add("@Password", SqlDbType.NVarChar).Value =user.Password;

                cmdResult = myCommand.ExecuteNonQuery();
                myTrans.Commit();
               
                sqlCon.Close();
                sqlCon.Dispose();
                myCommand.Dispose();
                myTrans.Dispose();
                return true;
            }
            catch (Exception ex)
            {   
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
