﻿namespace SerhanTravel.Forms
{
    partial class frmRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistration));
            this.label3 = new System.Windows.Forms.Label();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbUser = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbUserRole = new System.Windows.Forms.ComboBox();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.TStrip.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(51, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 237;
            this.label3.Text = "User Role";
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerNo.Location = new System.Drawing.Point(51, 95);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(49, 15);
            this.lblSerNo.TabIndex = 227;
            this.lblSerNo.Text = "User ID";
            // 
            // label2
            // 
            this.label2.AccessibleDescription = "lblSerNo";
            this.label2.AccessibleName = "lblSerNo";
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 234;
            this.label2.Text = "Name";
            // 
            // cbUser
            // 
            this.cbUser.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbUser.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbUser.FormattingEnabled = true;
            this.cbUser.Location = new System.Drawing.Point(130, 92);
            this.cbUser.Name = "cbUser";
            this.cbUser.Size = new System.Drawing.Size(194, 21);
            this.cbUser.TabIndex = 232;
            this.cbUser.SelectedIndexChanged += new System.EventHandler(this.cbUser_SelectedIndexChanged);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(51, 174);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(59, 15);
            this.Label1.TabIndex = 229;
            this.Label1.Text = "Password";
            // 
            // txtName
            // 
            this.txtName.AccessibleDescription = "txtSerNo";
            this.txtName.AccessibleName = "txtSerNo";
            this.txtName.Location = new System.Drawing.Point(130, 119);
            this.txtName.Name = "txtName";
            this.txtName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtName.Size = new System.Drawing.Size(194, 20);
            this.txtName.TabIndex = 225;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // lblName
            // 
            this.lblName.AccessibleDescription = "lblName";
            this.lblName.AccessibleName = "lblName";
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(51, 148);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(64, 15);
            this.lblName.TabIndex = 228;
            this.lblName.Text = "UserName";
            // 
            // txtUserName
            // 
            this.txtUserName.AccessibleDescription = "txtSerNo";
            this.txtUserName.AccessibleName = "txtSerNo";
            this.txtUserName.Location = new System.Drawing.Point(130, 145);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(194, 20);
            this.txtUserName.TabIndex = 231;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripBackWard,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(350, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 226;
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(55, 17);
            this.stsLblInfo.Text = "Add User";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(130, 171);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(194, 20);
            this.txtPassword.TabIndex = 236;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(177, 224);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(86, 29);
            this.btnCancel.TabIndex = 235;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbUserRole
            // 
            this.cbUserRole.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbUserRole.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbUserRole.FormattingEnabled = true;
            this.cbUserRole.Location = new System.Drawing.Point(130, 197);
            this.cbUserRole.Name = "cbUserRole";
            this.cbUserRole.Size = new System.Drawing.Size(194, 21);
            this.cbUserRole.TabIndex = 233;
            this.cbUserRole.SelectedIndexChanged += new System.EventHandler(this.cbUserRole_SelectedIndexChanged);
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 281);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(350, 22);
            this.stsInfo.TabIndex = 230;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // frmRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(350, 303);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSerNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbUser);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.TStrip);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cbUserRole);
            this.Controls.Add(this.stsInfo);
            this.Name = "frmRegistration";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.frmRegistration_Load);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.ComboBox cbUser;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.TextBox txtUserName;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.ComboBox cbUserRole;
        internal System.Windows.Forms.StatusStrip stsInfo;
    }
}