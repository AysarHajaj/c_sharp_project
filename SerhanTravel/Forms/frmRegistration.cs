﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmRegistration : Form
    {
        int UserID = 0;
        List<clsUser> UserList;
        List<string> UserNameList;
        bool iamAdding = false;
        bool DataChange = false;
        clsUser UserData;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;
        private int SavedID;
        User userConnection;
        internal frmRegistration(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
            
        }

        private void frmRegistration_Load(object sender, EventArgs e)
        {
            userConnection = new User();
            accessRight();
            UserList = userConnection.UsersArrayList();
            fillComboBox();
            cbUser.ValueMember = "UserId";
            cbUser.DataSource = UserList;
            cbUser.DisplayMember = "Name";
            cbUser.SelectedIndex = -1;
            ResetFeilds();
            this.UserID = 0;
            DataChange = false;

        }

        public void fillComboBox()
        {
            ArrayList RoleList = new ArrayList();
            clsRole Manager = new clsRole();
            Manager.RoleId = 1;
            Manager.RoleType = "Manager";

            clsRole employee = new clsRole();
            employee.RoleId = 2;
            employee.RoleType = "Employee";

            RoleList.Add(Manager);
            RoleList.Add(employee);
            cbUserRole.ValueMember = "roleId";
            cbUserRole.DataSource = RoleList;
            cbUserRole.DisplayMember = "roleType";
            cbUserRole.SelectedIndex = -1;
        }

        public void Display(int uId)
        {
            iamAdding = false;
            cbUser.DataSource = null;
            cbUser.ValueMember = "UserId";
            cbUser.DataSource = UserList;
            cbUser.DisplayMember = "Name";
            cbUser.SelectedIndex = - 1;
            this.UserID = uId;
            DataChange = false;


        }

        public void ResetFeilds()
        {
            cbUser.SelectedIndex = -1;
            cbUserRole.SelectedIndex = -1;
            txtName.Text = "";
            stsLblInfo.Text = "";
            txtPassword.Text = "";
            txtUserName.Text = "";
        }

        private void cbUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUser.SelectedIndex != -1 && UserList.Count > 0)
            {
                this.UserID = int.Parse(cbUser.SelectedValue.ToString());
                UserData = UserList[cbUser.SelectedIndex];
                cbUserRole.SelectedValue = UserData.RoleId;
                txtName.Text = UserData.Name;
                txtUserName.Text = UserData.UserName;
                txtPassword.Text = UserData.Password;
            }
            DataChange = false;
        }

        public void SaveDataInfo()
        {
            bool IdExists;
            try
            {
                clsUser user = new clsUser();
                user.UserId = this.UserID;

                if (cbUser.SelectedIndex == -1)
                {
                    user.UserId = 0;
                }
                else
                {
                    user.UserId = int.Parse(cbUser.SelectedValue.ToString());
                }

                if (cbUserRole.SelectedIndex == -1)
                {
                    user.RoleId = 0;
                }
                else
                {
                    user.RoleId = int.Parse(cbUserRole.SelectedValue.ToString());
                }

                if (string.IsNullOrWhiteSpace(txtName.Text))
                {
                    user.Name = "";
                }
                else
                {
                    user.Name = txtName.Text;
                }

                if (string.IsNullOrWhiteSpace(txtPassword.Text))
                {
                    user.Password = "";
                }
                else
                {
                    user.Password = txtPassword.Text;
                }

                if (string.IsNullOrWhiteSpace(txtUserName.Text))
                {
                    user.UserName = "";
                }
                else
                {
                    user.UserName = txtUserName.Text;
                }

                IdExists = userConnection.UserIDExists(user.UserId);

                if (IdExists)
                {
                    UserNameList = userConnection.UserNameArrayList();
                    
                    if (UserNameList.Contains(txtUserName.Text.ToString()) && !UserData.UserName.Equals(txtUserName.Text.ToString()))
                    {
                        MessageBox.Show("This username is inused", "Error");
                        txtUserName.Text = "";
                        DataChange = false;
                        return;
                    }
                    else
                    {
                        userConnection.UpdateUser(user);
                        stsLblInfo.Text = "Changes has been saved successfully";
                    }
                    
                }
                else
                {
                    UserNameList = userConnection.UserNameArrayList();
                    if (UserNameList.Contains(txtUserName.Text.ToString()))
                    {
                        MessageBox.Show("This username is inused", "Error");
                        txtUserName.Text = "";
                        DataChange = false;
                        return;
                    }
                    else
                    {
                        iamAdding = true;
                        userConnection.AddUser(user);
                        stsLblInfo.Text = "New user has been added successfully";
                    }
                    
                }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                UserList = userConnection.UsersArrayList();
                if (iamAdding && UserList.Count > 0)
                {
                    clsUser us = (clsUser)UserList[UserList.Count - 1];
                    this.UserID = us.UserId;
                }
                Display(this.UserID);

                DataChange = false;

            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "Error: Not saved, please check";
                MessageBox.Show(ex.Message);
            }

        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add User";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myReplay = MessageBox.Show("Do you want to save changes?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReplay == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            SavedID = this.UserID;
            this.UserID = 0;
            ResetFeilds();
            cbUser.DataSource = null;
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void cbUserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.UserID == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFeilds();
            this.UserID = SavedID;
            Display(this.UserID);
            DataChange = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.UserID != 0)
            {
                if (this.UserID != right.UserId)
                {
                    try
                    {
                        bool MyResult;
                        SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                        DialogResult myReplay = MessageBox.Show("Are you sure?", "Delete City", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (myReplay == DialogResult.Yes)
                        {
                            MyResult = userConnection.DeleteUser(this.UserID);
                            if (!MyResult)
                            {
                                return;
                            }
                            simpleSound.Play();
                            ResetFeilds();
                            UserList = userConnection.UsersArrayList();
                            Display(0);
                            DataChange = false;
                            stsLblInfo.Text = "Successfully deleted";
                        }


                    }
                    catch (Exception ex)
                    {
                        DataChange = false;
                        stsLblInfo.Text = "Error: Not deleted";
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Cannot delete this user because you are using it.");
                    return;
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if (cbUser.Items.Count > 0)
            {
                if (cbUser.SelectedIndex == 0 || cbUser.SelectedIndex == -1)
                {
                    int Users = cbUser.Items.Count;
                    cbUser.SelectedIndex = Users - 1;
                }
                else
                {
                    cbUser.SelectedIndex = cbUser.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if (cbUser.Items.Count > 0)
            {
                if (cbUser.SelectedIndex == cbUser.Items.Count - 1 || cbUser.SelectedIndex == -1)
                {

                    cbUser.SelectedIndex = 0;
                }
                else
                {
                    cbUser.SelectedIndex = cbUser.SelectedIndex + 1;
                }
            }
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cbUser.Enabled = false;
            cbUserRole.Enabled = false;
            txtName.ReadOnly = true;
            txtUserName.ReadOnly = true;
            txtPassword.ReadOnly = true;
        }

        private void unReadOnlyEveryThing()
        {
            cbUser.Enabled = true;
            cbUserRole.Enabled = true;
            txtName.ReadOnly = false;
            txtUserName.ReadOnly = false;
            txtPassword.ReadOnly = false;
        }
    }
}
