﻿namespace SerhanTravel.Forms
{
    partial class frmSupplierBalanceInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSupplierBalanceInventory));
            this.btnCancel = new System.Windows.Forms.Button();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.cmbInventoryId = new System.Windows.Forms.ComboBox();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCurrencySymbol = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSpace02 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.panel3.SuspendLayout();
            this.stsInfo.SuspendLayout();
            this.TStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(139, 253);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(81, 32);
            this.btnCancel.TabIndex = 245;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(117, 41);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(220, 34);
            this.lbResult.TabIndex = 231;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // cmbInventoryId
            // 
            this.cmbInventoryId.FormattingEnabled = true;
            this.cmbInventoryId.Location = new System.Drawing.Point(117, 82);
            this.cmbInventoryId.Name = "cmbInventoryId";
            this.cmbInventoryId.Size = new System.Drawing.Size(220, 23);
            this.cmbInventoryId.TabIndex = 228;
            this.cmbInventoryId.SelectedIndexChanged += new System.EventHandler(this.cmbInventoryId_SelectedIndexChanged);
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(117, 18);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(220, 23);
            this.cmbSupplier.TabIndex = 230;
            this.cmbSupplier.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            this.cmbSupplier.TextChanged += new System.EventHandler(this.cmbCustomer_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel3.Controls.Add(this.lbResult);
            this.panel3.Controls.Add(this.lblCurrencySymbol);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.cmbCurrency);
            this.panel3.Controls.Add(this.Label12);
            this.panel3.Controls.Add(this.txtRate);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.dtpDate);
            this.panel3.Controls.Add(this.btnCancel);
            this.panel3.Controls.Add(this.txtAmount);
            this.panel3.Controls.Add(this.Label1);
            this.panel3.Controls.Add(this.lblSerNo);
            this.panel3.Controls.Add(this.cmbInventoryId);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.cmbSupplier);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(626, 298);
            this.panel3.TabIndex = 246;
            // 
            // lblCurrencySymbol
            // 
            this.lblCurrencySymbol.AutoSize = true;
            this.lblCurrencySymbol.Location = new System.Drawing.Point(349, 202);
            this.lblCurrencySymbol.Name = "lblCurrencySymbol";
            this.lblCurrencySymbol.Size = new System.Drawing.Size(0, 15);
            this.lblCurrencySymbol.TabIndex = 252;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 171);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 251;
            this.label8.Text = "Rate";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(117, 139);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(220, 23);
            this.cmbCurrency.TabIndex = 249;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            this.cmbCurrency.MouseCaptureChanged += new System.EventHandler(this.cmbCurrency_MouseCaptureChanged);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(23, 142);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 248;
            this.Label12.Text = "Currency";
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(117, 168);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(220, 22);
            this.txtRate.TabIndex = 250;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 15);
            this.label2.TabIndex = 247;
            this.label2.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(117, 111);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(220, 22);
            this.dtpDate.TabIndex = 246;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.AccessibleDescription = "txtSerNo";
            this.txtAmount.AccessibleName = "txtSerNo";
            this.txtAmount.Location = new System.Drawing.Point(117, 196);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(220, 22);
            this.txtAmount.TabIndex = 218;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(23, 203);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(50, 15);
            this.Label1.TabIndex = 216;
            this.Label1.Text = "Amount";
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Location = new System.Drawing.Point(23, 26);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(57, 15);
            this.lblSerNo.TabIndex = 214;
            this.lblSerNo.Text = "Supplier ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 229;
            this.label4.Text = "Balance ID";
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 356);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(626, 22);
            this.stsInfo.TabIndex = 244;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(611, 17);
            this.stsLblInfo.Spring = true;
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripSpace02,
            this.TStripBackWard,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(626, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 243;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSpace02
            // 
            this.TStripSpace02.AccessibleDescription = "TStripSpace02";
            this.TStripSpace02.AccessibleName = "TStripSpace02";
            this.TStripSpace02.AutoSize = false;
            this.TStripSpace02.Name = "TStripSpace02";
            this.TStripSpace02.Size = new System.Drawing.Size(10, 25);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // frmSupplierBalanceInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 378);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.TStrip);
            this.Name = "frmSupplierBalanceInventory";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supplier Balance Inventory";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomerBalanceInventory_FormClosing);
            this.Load += new System.EventHandler(this.frmCustomerBalanceInventory_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.ComboBox cmbInventoryId;
        private System.Windows.Forms.ComboBox cmbSupplier;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDate;
        internal System.Windows.Forms.TextBox txtAmount;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace02;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.Label lblCurrencySymbol;
    }
}