﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SerhanTravel.Objects;

using System.Windows.Forms;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierBalanceInventory : Form
    {
        int SupplierId = 0;
        int InventoryId;
        bool canUpdateOnly;

        public int SupplierSavedID { get; private set; }

        int InventorySavedId;
        bool iamAdding = false;
        bool DataChange = false;
        List<clsSupplierInventory> inventoryList;
        SupplierBalanceInventory inventoryConnection;
        clsSupplierInventory inventoryData;
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        clsSupplier supplierData;
        List<clsCurrency> currencylist;
        string currencyFormat;
        private bool skip;
        clsAccessRights rights;
        bool canAddOnly = false;
        Currency currencyConnection;
        internal frmSupplierBalanceInventory(clsAccessRights rights)
        {
            InitializeComponent();
            this.rights = rights;
        }

        private void frmCustomerBalanceInventory_Load(object sender, EventArgs e)
        {
            accessRight();
            currencyConnection = new Currency();
            currencyFormat = "#,##0.00;-#,##0.00;Zero";
            inventoryConnection = new SupplierBalanceInventory();
            supplierConnection = new Supplier();
            supplierList = supplierConnection.SupplierArrayList();
            currencylist = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencylist;
            cmbCurrency.DisplayMember = "CurrencyName";

            cmbSupplier.ValueMember = "SupplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "SupplierName";
            cmbSupplier.SelectedIndex = -1;
        
            ResetFileds();
            this.SupplierId = 0;
            this.InventoryId = 0;
            DataChange = false;


        }
        private void accessRight()
        {
            if (!rights.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!rights.Deleting)
            {
                TStripDelete.Enabled = false;
            }

            if (!rights.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!rights.Updating && rights.Adding)
            {
                canAddOnly = true;
            }
            if (!rights.Adding && rights.Updating)
            {
                canUpdateOnly = true;
            }

        }
        private void readOnlyEveryThing()
        {


            dtpDate.Enabled = false;
            txtAmount.ReadOnly = true;
            cmbCurrency.Enabled = false;

        }

        private void unReadOnlyEveryThing()
        {
            cmbCurrency.Enabled = true;
            dtpDate.Enabled = true;
            txtAmount.ReadOnly = false;
        }


        private void ResetFileds()
        {
             cmbInventoryId.DataSource = null;
            cmbInventoryId.SelectedIndex = -1;
            dtpDate.Value = DateTime.Today;
            txtAmount.Clear();
        }
        public void DisplayData(int customerId,int inventoryid)
        {
            iamAdding = false;
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "SupplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "SupplierName";
            cmbSupplier.SelectedValue = customerId;
            cmbInventoryId.SelectedValue = inventoryid;


            skip = true;



            DataChange = false;
        }
        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                lblCurrencySymbol.Text = currency.CurrencySymbol;

            }
            else
            {
               
                lblCurrencySymbol.Text = "L.L";

            }
            DataChange = true;
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
            {
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            }

            currencylist = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencylist;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
            DataChange = false;
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbResult.Visible = false;
         

            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {

               ResetFileds();
                this.InventoryId = 0;

              
                supplierData = (clsSupplier)cmbSupplier.SelectedItem;
                this.SupplierId = supplierData.SupplierId;
                if (iamAdding)
                {

                    cmbInventoryId.DataSource = null;
                    cmbInventoryId.SelectedIndex = -1;
                
                    return;
                }
                inventoryList = inventoryConnection.getSuppierInventoryList(this.SupplierId);
               
                cmbInventoryId.ValueMember = "InventoryId";
                cmbInventoryId.DataSource = inventoryList;
                cmbInventoryId.DisplayMember = "DateString";
                skip = true;
            }
           
            DataChange = false;
          
        }

        private void cmbCustomer_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "SupplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "SupplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChange = false;
                cmbSupplier.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void cmbInventoryId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbInventoryId.SelectedIndex!=-1 && inventoryList.Count>0)
            {
                inventoryData = (clsSupplierInventory)cmbInventoryId.SelectedItem;
                this.InventoryId = inventoryData.InventoryId;
                dtpDate.Value = inventoryData.Date;
                txtAmount.Text = inventoryData.Balance.ToString(currencyFormat);
                cmbCurrency.SelectedValue = inventoryData.CurrencyId;
                txtRate.Text = inventoryData.CurrencyRate.ToString();
                DataChange = false;

            }
        }

        private void SaveDataInfo()
        {

            if (cmbSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Supplier");
                if (iamAdding)
                {
                    btnCancel.Visible = true;
                }
                return;
            }
            


            bool RecExists;
            try
            {
    
                clsSupplierInventory inventory = new clsSupplierInventory();

                inventory.InventoryId = this.InventoryId;
                if (cmbSupplier.SelectedIndex != -1)
                {
                    inventory.SupplierId = int.Parse(cmbSupplier.SelectedValue.ToString());
                }
                else
                {
                    inventory.SupplierId = 0;
                }

              

                if (string.IsNullOrWhiteSpace(txtAmount .Text))
                {
                    inventory.Balance = 0;
                }
                else
                {
                    inventory.Balance = decimal.Parse(txtAmount.Text);
                }
                inventory.Date = dtpDate.Value;


              if(cmbCurrency.SelectedIndex==-1)
                {
                    inventory.CurrencyId = 0;
                }
              else
                {
                    inventory.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                if (!string.IsNullOrEmpty(txtRate.Text))
                {
                    inventory.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }


                RecExists = inventoryConnection.InventoryIDExists(inventory.InventoryId);


                if (RecExists == true)
                {
                    inventoryConnection.UpdateInventoryInfo(inventory);
                 
                    stsLblInfo.Text = "Changes has been saved successfully";
                    iamAdding = true;

                }
                else
                {
                    iamAdding = true;
                    inventoryConnection.AddNewInventory(inventory);
                    int invoicenum = inventoryConnection.ReadLastNo();
             
                    inventory.InventoryId = invoicenum;
                    this.InventoryId = invoicenum;

                    stsLblInfo.Text = "New Balance has been added successfully";

                }

                InventorySavedId =this.InventoryId;
           
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

           
                inventoryList = inventoryConnection.getSuppierInventoryList(inventory.SupplierId);



      
                DisplayData(inventory.SupplierId, inventory.InventoryId);


                DataChange = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChange = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }



        private void TStripNew_Click(object sender, EventArgs e)
        {
            iamAdding = true;
         
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChange)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }

            InventorySavedId = this.InventoryId;
            this.InventoryId = 0;
            SupplierSavedID = this.SupplierId;
            this.SupplierId = 0;
    
            cmbSupplier.SelectedIndex = -1;
            ResetFileds();
           
            btnCancel.Visible = true;
            DataChange = false;
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            DataChange = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChange)
            {
                btnCancel.Visible = false;

                if (canUpdateOnly && this.InventoryId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Visible = false;
            iamAdding = false;
            this.InventoryId = InventorySavedId;
            this.SupplierId = SupplierSavedID;
            ResetFileds();
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            DisplayData(this.SupplierId, this.InventoryId);

        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.InventoryId != 0)
            {
                try
                {

                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Balance", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {
                        bool myResult1 = inventoryConnection.DeleteInventory(this.InventoryId);
                    
                        
                            if (myResult1)
                            {
                                ResetFileds();
                                int index = inventoryList.IndexOf(inventoryData);
                                inventoryList = inventoryConnection.getSuppierInventoryList(this.SupplierId);
                                index -= 1;
                                this.InventoryId = 0;
                                if (index >= 0)
                                {
                                InventoryId = inventoryList[index].InventoryId;
                                }
                                else if (index == -1 && inventoryList.Count > 0)
                                {
                                    index = 0;
                                InventoryId = inventoryList[index].InventoryId;
                                }

                                DisplayData(SupplierId, InventoryId);
                                DataChange = false;
                                stsLblInfo.Text = "Successfully Deleted";
                                simpleSound.Play();
                            }

                        
                    }

                }
                catch (Exception ex)
                {
                    DataChange = false;
                    stsLblInfo.Text = "Error: Deleting the Invoice";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if ((cmbInventoryId.Items.Count > 0))
            {
                if (cmbInventoryId.SelectedIndex == 0 || cmbInventoryId.SelectedIndex == -1)
                {
                    cmbInventoryId.SelectedIndex = cmbInventoryId.Items.Count - 1;
                }
                else
                {
                    cmbInventoryId.SelectedIndex = cmbInventoryId.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if ((cmbInventoryId.Items.Count > 0))
            {
                if (cmbInventoryId.SelectedIndex == cmbInventoryId.Items.Count - 1 || cmbInventoryId.SelectedIndex == -1)
                {
                    cmbInventoryId.SelectedIndex = 0;
                }
                else
                {
                    cmbInventoryId.SelectedIndex = cmbInventoryId.SelectedIndex + 1;
                }
            }
        }

        private void frmCustomerBalanceInventory_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbInventoryId.SelectedIndex != -1)
            {
                InventoryId = Convert.ToInt32(cmbInventoryId.SelectedValue);
            }
            if (DataChange == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                 
                }
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 45 && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtAmount.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
                else if (character == 45 && txtAmount.Text.Contains("-"))
                {
                    e.Handled = true;
                    return;
                }
            }
        }

        private void cmbCurrency_MouseCaptureChanged(object sender, EventArgs e)
        {

        }
    }
}
