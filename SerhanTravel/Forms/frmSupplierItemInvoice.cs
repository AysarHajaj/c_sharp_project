﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using SerhanTravel.Print;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierItemInvoice : Form
    {
        /*DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        List<clsSupplierItemInvoice> supplierInvoiceList;
        List<clsItem> itemList;
        List<int> itemListIDS;
        List<clsItem> SupplierItemList;
        SupplierItemInvoice invoiceConnection;
        Supplier supplierConnection;
        clsSupplierItemInvoice invoiceData;
        clsSupplier supplierData;
        bool skip = false;
        bool DataChanged = false;
        bool iamAdding = false;
        int InvoiceId = 0;
        int InvoiceSavedID = 0;
        int SupplierID = 0;
        private int SuppliersaveID;
        private List<clsCurrency> currencies;
        decimal totalPrice = 0;
        private bool allowRemove;

        bool add = true;
        bool update = false;
        string currencyFormat;
        clsAccessRights right;
        bool canAddOnly = false;
        bool canUpdateOnly;*/
        internal frmSupplierItemInvoice(clsAccessRights right)
        {
            InitializeComponent();
            /*this.right = right;
            dgvItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvItems.MultiSelect = false;
            cmbSupplier.SelectedIndex = -1;
            cLbSupplierItems.HorizontalScrollbar = true;
            dgvItems.ScrollBars = ScrollBars.Both;
            dgvItems.AutoGenerateColumns = false;
            dgvItems.Columns[0].DataPropertyName = "ItemName";
            dgvItems.Columns[1].DataPropertyName = "ItemDescription";
            dgvItems.Columns[2].DataPropertyName = "cost";
            dgvItems.Columns[3].DataPropertyName = "quantity";
            dgvItems.Columns[4].DataPropertyName = "totalCost";
            dgvItems.Columns[5].DataPropertyName = "CurrecnySymbol";*/
        }

        private void ResetFields()
        {
           /* cmbInvoceNo.DataSource = null;
            cLbSupplierItems.DataSource = null;
            dgvItems.DataSource = null;
            cmbInvoceNo.Text = "";
            dgvItems.Rows.Clear();
            lblTotalPrice.Text = "";
            txtTotalPrice.Clear();
            lblDescription.Text = "";
            dtpInvoiceDate.Value = DateTime.Today;
            cmbInvoceNo.SelectedIndex = -1;
            cLbSupplierItems.SelectedIndex = -1;
            itemList.Clear();
            itemListIDS.Clear();
            totalPrice = 0;
            DataChanged = false;*/
        }

        private void FillCombobox()
        {
            /*currencies = Modules.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            DataChanged = false;*/
        }

        private void frmSupplierInvoice_Load(object sender, EventArgs e)
        {
          /*  currencyFormat = "#,##0.00;-#,##0.00;0";
            accessRight();
            lbResult.Visible = false;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            dtpInvoiceDate.Value = DateTime.Today;
            supplierConnection = new Supplier();
            itemList = new List<clsItem>();
            itemListIDS = new List<int>();
            invoiceConnection = new SupplierItemInvoice();
            supplierList = supplierConnection.SupplierArrayList();
            supplierInvoiceList = new List<clsSupplierItemInvoice>();
            FillCombobox();
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedIndex = -1;
            ResetFields();
            InvoiceId = 0;
            DataChanged = false;*/
        }

        private void DisplayData(int supId,int invId)
        {
         /*   iamAdding = false;
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedValue = supId;
            cmbInvoceNo.SelectedValue = invId;
            skip = true;
            SupplierID = supId;
           
            DataChanged = false;*/
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
           /* cLbSupplierItems.DataSource = null;
            lbResult.Visible = false;
            btnAdd.Visible = false;
            btnRemove.Visible = false;
            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                InvoiceId = 0;
                ResetFields();
                SupplierID = int.Parse(cmbSupplier.SelectedValue.ToString());
                SupplierItemList = invoiceConnection.SupplierItemsArrayList(SupplierID);
                
                cLbSupplierItems.ValueMember = "ItemID";
                cLbSupplierItems.DataSource = SupplierItemList;
                cLbSupplierItems.DisplayMember = "ToToString";

                if (iamAdding)
                {
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.SelectedIndex = -1;
                    SupplierItemList.Clear();
                    itemList.Clear();
                    itemListIDS.Clear();
                    return;
                }


                supplierData = supplierList[cmbSupplier.SelectedIndex];
                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);

                if (supplierInvoiceList.Count > 0)
                {
                    cmbInvoceNo.Visible = true;
                    lblInvoiceNo.Visible = true;
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.ValueMember = "invoiceId";
                    cmbInvoceNo.DataSource = supplierInvoiceList;
                    cmbInvoceNo.DisplayMember = "invoiceId";
                    cmbInvoceNo.SelectedValue = InvoiceId;


                }
                else
                {
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.Visible = false;
                    lblInvoiceNo.Visible = false;
                    dgvItems.DataSource = null;
                    dgvItems.Rows.Clear();
                    totalPrice = 0;
                    txtTotalPrice.Text = "";
                    dtpInvoiceDate.Value = DateTime.Today;
                }

                DataChanged = false;
                skip = true;
            }
            else
            {
                skip = false;
            }

            DataChanged = false;*/
        }

        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
           /* btnRemove.Visible = false;
            btnAdd.Visible = false;
            if (cmbInvoceNo.SelectedIndex != -1 && supplierInvoiceList.Count > 0)
            {
                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                invoiceData = supplierInvoiceList[cmbInvoceNo.SelectedIndex];
                dtpInvoiceDate.Value = invoiceData.Date;
                itemListIDS.Clear();
                itemList.Clear();
                itemList = invoiceConnection.ItemsInvoicesArrayList(InvoiceId);
                for (int i = 0; i < itemList.Count; i++)
                {
                    itemListIDS.Add(itemList[i].itemID);
                }
                dgvItems.DataSource = null;
                totalPrice = 0;

                if (itemList.Count > 0)
                {
                    dgvItems.DataSource = itemList;
                    for (int i = 0; i < itemList.Count; i++)
                    {
                        totalPrice +=itemList[i].totalCost* itemList[i].CurrencyRate ;
                    }
                    dgvItems.Refresh();
                    dgvItems.ClearSelection();
                }

                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;

                    txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                }
                else
                {
                    txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                }
            }
            DataChanged = false;*/
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
           /* if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                lblTotalPrice.Text = currency.CurrencySymbol;
            }
          */
        }

        private void cmbSupplier_TextChanged(object sender, EventArgs e)
        {/*
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbSupplier.SelectedItem = lbResult.SelectedItem;

            }
            lbResult.Visible = false;
        }

        private void SaveDataInfo()
        {

            if (cmbSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a supplier");
                if(iamAdding)
                {
                    btnCancel.Visible = true;
                }
                return;
            }

            bool RecExists;
            try
            {
                //int selectedvalue;
                clsSupplierItemInvoice invoice = new clsSupplierItemInvoice();


                if (cmbSupplier.SelectedIndex != -1)
                {
                    invoice.SupplierId = int.Parse(cmbSupplier.SelectedValue.ToString());
                }
                else
                {
                    invoice.SupplierId = 0;
                }

                if (cmbInvoceNo.SelectedIndex == -1)
                {
                    invoice.InvoiceId = 0;
                }
                else
                {
                    invoice.InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                }


                invoice.NetPrice =totalPrice;

                invoice.Date = dtpInvoiceDate.Value;

                RecExists = invoiceConnection.InvoiceIDExists(invoice.InvoiceId);


                if (RecExists == true)
                {
                    invoiceConnection.UpdateInvoiceInfo(invoice);
                    invoiceConnection.UpdateInvoiceDetails(invoice.InvoiceId, itemList);
                    stsLblInfo.Text = "Changes has been saved successfully";

                }
                else
                {
                    iamAdding = true;
                    invoiceConnection.AddNewSupplierInvoice(invoice);
                    int invoicenum = invoiceConnection.ReadLastNo();
                    invoiceConnection.AddInvoiceItems(invoicenum, itemList);
                    invoice.InvoiceId = invoicenum;
                    InvoiceId = invoicenum;
                    InvoiceSavedID = invoicenum;
                    stsLblInfo.Text = "New Invoice has been added successfully";

                }

        

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(invoice.SupplierId);
                DisplayData(invoice.SupplierId, invoice.InvoiceId) ;
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }*/
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
          /*  if (DataChanged)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.InvoiceId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }*/
        }

        private void dtpInvoiceDate_ValueChanged(object sender, EventArgs e)
        {
           /* if (!iamAdding)
                DataChanged = true;*/
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           /* if (add && !update)
            {
                if (cLbSupplierItems.SelectedIndex == -1)
                {
                    MessageBox.Show("Please choose an item");
                    return;
                }
                if (string.IsNullOrWhiteSpace(txtQuantity.Text))
                {
                    MessageBox.Show("Please enter the quantity of item");
                    return;
                }
                clsItem item = (clsItem)cLbSupplierItems.SelectedItem;

                if (itemListIDS.Contains(item.itemID))
                {
                    for (int i = 0; i < itemList.Count; i++)
                    {
                        if (itemList[i].itemID == item.itemID)
                        {
                            itemList[i].Quantity = itemList[i].Quantity + int.Parse(txtQuantity.Text);
                        }
                    }
                    
                }
                else
                {
                    item.Quantity = int.Parse(txtQuantity.Text);
                    itemList.Add(item);
                    itemListIDS.Add(item.itemID);
                }
            }

            if (update && !add)
            {
                if (string.IsNullOrWhiteSpace(txtQuantity.Text))
                {
                    MessageBox.Show("Please enter the quantity of item");
                    return;
                }
                if (dgvItems.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                int index = dgvItems.SelectedRows[0].Index;
                itemList[index].Quantity= int.Parse(txtQuantity.Text);
            }

            dgvItems.DataSource = null;
            dgvItems.DataSource = itemList;
            dgvItems.Refresh();
            totalPrice = 0;
            for (int i = 0; i < itemList.Count; i++)
            {
                totalPrice += itemList[i].totalCost * itemList[i].CurrencyRate;
            }
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
            }
            else
            {
                txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
            }
            dgvItems.Refresh();
            dgvItems.ClearSelection();
            cLbSupplierItems.SelectedIndex = -1;
            txtQuantity.Clear();
            DataChanged = true;
            update = false;
            add = false;
            btnAdd.Visible = false;*/
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            /*if (dgvItems.RowCount > 0&&itemList.Count>0&&allowRemove)
            {
                allowRemove = false;
                if(dgvItems.SelectedRows.Count<=0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    int index = dgvItems.SelectedRows[0].Index;
                    itemList.RemoveAt(index);
                    itemListIDS.RemoveAt(index);
                    dgvItems.DataSource = null;
                    dgvItems.Rows.Clear();
                    dgvItems.DataSource = itemList;
                    totalPrice = 0;
                    for (int i = 0; i < itemList.Count; i++)
                    {
                        totalPrice += itemList[i].totalCost * itemList[i].CurrencyRate;

                    }

                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                    }
                    else
                    {
                        txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                    }
                   
                    DataChanged = true;
                    dgvItems.ClearSelection();

                }
                
            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnAdd.Visible = false;
            add = false;
            update = false;
            btnRemove.Visible = false;*/
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            /*if (cmbSupplier.SelectedIndex != -1)
            {
                SupplierItemList = invoiceConnection.SupplierItemsArrayList(SupplierID);
                cLbSupplierItems.DataSource = null;
                cLbSupplierItems.ValueMember = "ItemID";
                cLbSupplierItems.DataSource = SupplierItemList;
                cLbSupplierItems.DisplayMember = "ToToString";
            }*/
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            /*lblInvoiceNo.Visible = true;
            cmbInvoceNo.Visible = true;
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFields();
            InvoiceId = InvoiceSavedID;
            SupplierID = SuppliersaveID;
            DisplayData(SupplierID, InvoiceId);
            DataChanged = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }*/
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            /*iamAdding = true;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;

            stsLblInfo.Text = "Add Invoice";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            if (DataChanged)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                
                }
            }

            InvoiceSavedID = InvoiceId;
            SuppliersaveID = SupplierID;
            cmbSupplier.SelectedIndex = -1;
            ResetFields();
            InvoiceId = 0;
            btnCancel.Visible = true;
            DataChanged = false;*/
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
           /* if (this.InvoiceId != 0)
            {
                try
                {
                   
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {
                        bool myResult1 = invoiceConnection.DeleteInvoiceItems(InvoiceId);
                        if (!myResult1)
                        {
                            return;
                        }
                        else
                        {

                            bool myResult2 = invoiceConnection.DeleteInVoice(InvoiceId);
                            if (myResult2)
                            {

                                ResetFields();
                                int index = supplierInvoiceList.IndexOf(invoiceData);
                                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
                                index -= 1;
                                InvoiceId = 0;
                                if (index >= 0)
                                {
                                    InvoiceId = supplierInvoiceList[index].InvoiceId;
                                }
                                else if (index == -1 && supplierInvoiceList.Count > 0)
                                {
                                    index = 0;
                                    InvoiceId = supplierInvoiceList[index].InvoiceId;
                                }
                                DisplayData(SupplierID, InvoiceId);
                                DataChanged = false;
                                stsLblInfo.Text = "Successfully Deleted";
                                simpleSound.Play();
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    DataChanged = false;
                    stsLblInfo.Text = "Error: Deleting the Invoice";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }*/
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
           /* if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == 0 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.Items.Count - 1;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex - 1;
                }
            }*/
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
           /* if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == cmbInvoceNo.Items.Count - 1 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = 0;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex + 1;
                }
            }*/
        }

        private void dgvItems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           /* lblDescription.Text = "";
            if (dgvItems.RowCount > 0)
            {

                if (e.RowIndex >= 0)
                {
                    btnRemove.Visible = true;
                    update = true;
                    add = false;
                    int index = dgvItems.SelectedRows[0].Index;
                    txtQuantity.Text = itemList[index].Quantity.ToString();
                    allowRemove = true;
                    btnAdd.Text = "Update";
                    cLbSupplierItems.SelectedIndex = -1;
                }
            }*/
        }

        private void frmSupplierInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
           /* if (cmbInvoceNo.SelectedIndex != -1)
            {
                InvoiceId = Convert.ToInt32(cmbInvoceNo.SelectedValue);
            }
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    
                }
            }*/
        }


        private void TStripPrint_Click(object sender, EventArgs e)
        {/*
            if (cmbInvoceNo.SelectedIndex != -1 && supplierInvoiceList.Count > 0 && itemList.Count > 0)
            {

                clsCurrency currency;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    currency = (clsCurrency)cmbCurrency.SelectedItem;

                }
                else
                {
                    currency = new clsCurrency();
                    currency.CurrencyRate = 1;
                    currency.CurrencyId = 0;
                    currency.CurrencySymbol = "L.L";
                }
                if (DataChanged == true)
                {

                    DialogResult myReply = MessageBox.Show("Do you want to save changes before printing ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        SaveDataInfo();
                        cmbCurrency.SelectedValue = currency.CurrencyId;

                    }
                }
                clsSupplierItemInvoice invoice = (clsSupplierItemInvoice)cmbInvoceNo.SelectedItem;

                invoice.SupplierName = supplierData.SupplierName;
                frmPrintSupplierItemInvoice frm = new frmPrintSupplierItemInvoice(itemList, invoice, double.Parse(txtTotalPrice.Text),currency);
                frm.Show();

            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }

            */
        }

        private void cmbInvoceNo_Click(object sender, EventArgs e)
        {/*
            supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
            int id = 0;
            if (cmbInvoceNo.SelectedIndex != -1)
            {
                id = int.Parse(cmbInvoceNo.SelectedValue.ToString());
            }
            if (supplierInvoiceList.Count > 0)
            {
                cmbInvoceNo.ValueMember = "invoiceId";
                cmbInvoceNo.DataSource = supplierInvoiceList;
                cmbInvoceNo.DisplayMember = "invoiceId";
                cmbInvoceNo.SelectedValue = id;
            }*/
        }

        private void accessRight()
        {
           /* if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Printing)
            {
                TStripPrint.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }*/
        }

        private void readOnlyEveryThing()
        {
           /* dtpInvoiceDate.Enabled = false;
            btnAdd.Enabled = false;
            btnRemove.Enabled = false;
            txtQuantity.ReadOnly = true;*/
        }

        private void unReadOnlyEveryThing()
        {
            /*cmbCurrency.Enabled = true;
            cmbInvoceNo.Enabled = true;
            cmbSupplier.Enabled = true;
            cLbSupplierItems.Enabled = true;
            dtpInvoiceDate.Enabled = true;
            btnAdd.Enabled = true;
            btnRemove.Enabled = true;
            btnLRefresh.Enabled = true;
            txtQuantity.ReadOnly = false;*/
        }

        private void cLbSupplierItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cLbSupplierItems.SelectedIndex != -1)
            {
              /*  btnAdd.Visible = true;
                dgvItems.ClearSelection();
                add = true;
                update = false;
                txtQuantity.Text = "";
                btnAdd.Text = "Add";
                clsItem item = (clsItem)cLbSupplierItems.SelectedItem;
                int itemIssued =invoiceConnection.NumberOfItemIssued(item.itemID);
                int itemPayed = invoiceConnection.NumberOfInvoiced(item.itemID);
                string description = item.itemName +" ( "+ item.CurrecnySymbol + " )\nunit Cost = " + item.Cost.ToString(currencyFormat)+
                "\nUn Invoiced quantity = " + (itemIssued - itemPayed).ToString(currencyFormat) + 
                "\nTotal Cost = " + ((item.Cost) * (itemIssued - itemPayed)).ToString(currencyFormat);
                lblDescription.Text = description;*/
            }


        }

        private void txtQuantity_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
           /* int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = Modules.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;*/

        }

        private void frmSupplierItemInvoice_MouseClick(object sender, MouseEventArgs e)
        {
            //lbResult.Visible = false;
        }
    }
}
