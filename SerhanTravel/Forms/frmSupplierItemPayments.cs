﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using SerhanTravel.Print;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierItemPayments : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        List<clsSupplierItemInvoice> SupplierInvoiceList;
        List<clsSuppliersItemPayment> invoicepaymentList;
        SupplierItemInvoice invoiceConnection;
        SuppliersItemPayment paymentConnection;
        Supplier supplierConnection;
        clsSupplierItemInvoice invoiceData;
        ReturnedSupplierItem returnedSupplierItem;
        bool skip = false;
        bool DataChanged = false;
        int InvoiceId = 0;
        int SupplierID = 0;
        string currencyFormat;
        decimal totalPrice = 0;
        decimal paidAmount = 0;
        decimal remainingAmount = 0;
        int PaymentID = 0;
        int savedInvoiceId = 0;
        private bool allowRemove = false;
        private List<clsCurrency> currencies;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection;
        Bank bankConnection;
        internal frmSupplierItemPayments(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
            dgvPayments.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvPayments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPayments.MultiSelect = false;
            dgvPayments.ScrollBars = ScrollBars.Both;
            dgvPayments.AutoGenerateColumns = false;
            dgvPayments.Columns[0].DataPropertyName = "Date";
            dgvPayments.Columns[1].DataPropertyName = "Payment";
            dgvPayments.Columns[2].DataPropertyName = "CurrecnySymbol";
            dgvPayments.Columns[3].DataPropertyName = "ChequeNo";
            dgvPayments.Columns[4].DataPropertyName = "BankName";
            dgvPayments.Columns[5].DataPropertyName = "ChequeDate";
            dgvPayments.Columns[6].DataPropertyName = "CollectedDate";
        }

        private void ResetFields()
        {
            dgvPayments.DataSource = null;
            dgvPayments.Rows.Clear();
            txtTotalPrice.Clear();
            txtRemainingAmount.Clear();
            txtPaidAmount.Clear();
            totalPrice = 0;
            paidAmount = 0;
            remainingAmount = 0;
            panelPayment.Visible = false;
            DataChanged = false;
            btnRemove.Visible = false;
        }

        private void frmSupplierPayments_Load(object sender, EventArgs e)
        {
            accessRight();
            bankConnection = new Bank();
            currencyConnection = new Currency();
            lbResult.Visible = false;
            supplierConnection = new Supplier();
            currencyFormat = "#,##0.00;-#,##0.00;Zero";
            invoiceConnection = new SupplierItemInvoice();
            paymentConnection = new SuppliersItemPayment();
            supplierList= supplierConnection.SupplierArrayList();
            SupplierInvoiceList= new List<clsSupplierItemInvoice>();
            returnedSupplierItem = new ReturnedSupplierItem();
            currencies = currencyConnection.CurrencyArrayList();

            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedIndex = -1;

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
           

            cbBank.ValueMember = "BankId";
            cbBank.DataSource = bankConnection.BankArrayList();
            cbBank.DisplayMember = "BankName";
            cbBank.SelectedIndex = -1;
            ResetFields();
            panelPayment.Visible = false;
            btnRemove.Visible = false;
            DataChanged = false;

        }

        private void DisplayData(int supplierId)
        {

            panelPayment.Visible = false;
            cmbSupplierName.DataSource = null;
            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedValue = supplierId;
            skip = true;
            SupplierID = supplierId;
            DataChanged = false;

        }

        private void cmbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            lbResult.Visible = false;
            panelPayment.Visible = false;
            if (cmbSupplierName.SelectedIndex != -1 && supplierList.Count > 0)
            {

                SupplierID = int.Parse(cmbSupplierName.SelectedValue.ToString());
                SupplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
                ResetFields();
                totalPrice = invoiceConnection.TotalPrice(SupplierID);
                foreach (clsSupplierItemInvoice invoice in SupplierInvoiceList)
                {
                    if (returnedSupplierItem.InvoiceExist(invoice.InvoiceId))
                    {

                        totalPrice -= returnedSupplierItem.getReturnAmount(invoice.InvoiceId);

                    }
                }
                remainingAmount = 0;
                paidAmount = 0;
                invoicepaymentList = new List<clsSuppliersItemPayment>();
                invoicepaymentList = paymentConnection.SupplierPayment(SupplierID);
                if (invoicepaymentList.Count > 0)
                {
                    dgvPayments.DataSource = invoicepaymentList;
                    for (int i = 0; i < invoicepaymentList.Count; i++)
                    {
                        paidAmount += invoicepaymentList[i].Payment * invoicepaymentList[i].CurrencyRate;
                    }
                }

                remainingAmount = totalPrice - paidAmount;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                    txtPaidAmount.Text = (paidAmount / currency.CurrencyRate).ToString(currencyFormat);
                    txtRemainingAmount.Text = (remainingAmount / currency.CurrencyRate).ToString(currencyFormat);
                }

                dgvPayments.Refresh();
                dgvPayments.ClearSelection();
                DataChanged = false; 
                skip = true;
            }
            else
            {
                skip = false;
            }

            DataChanged = false;
        }

      
        private void cmbSupplierName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplierName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbSupplierName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void SaveDataInfo()
        {

            savedInvoiceId = InvoiceId;
            if (cmbSupplierName.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a supplier");
                return;
            }
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Currency to Continue");
                return;
            }
            bool RecExists;
            try
            {
                clsSuppliersItemPayment payment = new clsSuppliersItemPayment();

                if (!string.IsNullOrWhiteSpace(txtPayment.Text))
                {
                    payment.Payment = Convert.ToDecimal(txtPayment.Text);
                }
                else
                {
                    payment.Payment = 0;
                }

                if (!string.IsNullOrWhiteSpace(txtChequeNumber.Text))
                {
                    payment.ChequeNo = txtChequeNumber.Text;
                    payment.PaymentType = 1;
                    if (cbBank.SelectedIndex == -1)
                    {
                        MessageBox.Show("Please choose a bank");
                        return;
                    }
                    else
                    {
                        payment.BankId = int.Parse(cbBank.SelectedValue.ToString());
                    }

                    if (!string.IsNullOrWhiteSpace(dtbChequeDate.Text))
                    {
                        if (IsDateTime(dtbChequeDate.Text))
                        {
                            payment.ChequeDate = dtbChequeDate.Text;
                        }
                        else
                        {
                            MessageBox.Show("Your cheque date is not in the correct format");
                            return;
                        }

                    }
                    else
                    {
                        payment.ChequeDate = null;
                    }

                    if (!string.IsNullOrWhiteSpace(dtbCollectedDate.Text))
                    {
                        if (IsDateTime(dtbCollectedDate.Text))
                        {
                            payment.CollectedDate = dtbCollectedDate.Text;
                        }
                        else
                        {
                            MessageBox.Show("Your colllected date is not in the correct format");
                            return;
                        }

                    }
                    else
                    {
                        payment.CollectedDate = null;
                    }

                }
                else
                {
                    payment.ChequeDate = null;
                    payment.CollectedDate =null;
                    payment.PaymentType = 0;
                    payment.ChequeNo = string.Empty;

                }
                if (cmbCurrency.SelectedIndex == -1)
                {
                    payment.CurrencyId = 0;
                }
                else
                {
                    payment.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                if (!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    payment.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }
                payment.SupplierId = SupplierID;
                payment.Date = dtpDate.Value;
                RecExists = paymentConnection.invoiceIDExists(payment.PaymentId);
                if (RecExists == true)
                {
                    paymentConnection.UpdatePaymentDetails(payment);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    paymentConnection.AddPaymentDetails(payment);
                    stsLblInfo.Text = "New Payment has been added successfully";
                }
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                DisplayData(SupplierID);
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }


        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (canAddOnly)
            {
                btnSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            panelPayment.Visible = true;
            dtpDate.Value = DateTime.Today;
            dtbChequeDate.Text = "";
            dtbCollectedDate.Text = "";
            txtPayment.Clear();
            txtPaymentRate.Clear();
            txtChequeNumber.Clear();
            cbBank.SelectedIndex = -1;
            PaymentID = 0;
            DataChanged = false;
            btnRemove.Visible = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            panelPayment.Visible = false;
            btnRemove.Visible = false;
            if (canAddOnly)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            btnRemove.Visible = false;

            if (DataChanged)
            {

                SaveDataInfo();
                if (canAddOnly)
                {
                    btnSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtPayment_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtChequeNumber_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            savedInvoiceId = InvoiceId;
            if (dgvPayments.RowCount > 0 && invoicepaymentList.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvPayments.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    clsSuppliersItemPayment payment = (clsSuppliersItemPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                    try
                    {
                        if (paymentConnection.DeleteInvoicePayments(payment.PaymentId))
                        {
                            dgvPayments.DataSource = null;
                            dgvPayments.Rows.Clear();
                            DisplayData(SupplierID);
                            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                            simpleSound.Play();
                            stsLblInfo.Text = "Payment has been deleted";
                            DataChanged = false;
                            btnRemove.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        stsLblInfo.Text = "Error while deleting";
                    }
                }


            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void dgvPayments_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!canAddOnly)
            {
                if (dgvPayments.RowCount > 0)
                {
                    if (e.RowIndex >= 0)
                    {
                        allowRemove = true;
                        btnRemove.Visible = true;
                        panelPayment.Visible = true;
                        clsSuppliersItemPayment payment = (clsSuppliersItemPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                        if (!string.IsNullOrWhiteSpace(payment.Date.ToShortDateString()))
                        {
                            dtpDate.Value = payment.Date;
                        }
                        else
                        {
                            dtpDate.Value = DateTime.Today;
                        }

                        if (!string.IsNullOrWhiteSpace(payment.Payment.ToString()))
                        {
                            txtPayment.Text = payment.Payment.ToString();
                        }
                        else
                        {
                            txtPayment.Text = "";
                        }

                        if (!string.IsNullOrWhiteSpace(payment.ChequeNo))
                        {
                            txtChequeNumber.Text = payment.ChequeNo;
                        }
                        else
                        {
                            txtChequeNumber.Text = "";
                        }

                        cmbCurrency.SelectedValue = payment.CurrencyId;
                        cbBank.SelectedValue = payment.BankId;

                        if (!string.IsNullOrWhiteSpace(payment.ChequeDate))
                        {
                            dtbChequeDate.Text = payment.ChequeDate;
                        }
                        else
                        {
                            dtbChequeDate.Text = "";
                        }

                        if (!string.IsNullOrWhiteSpace(payment.CollectedDate))
                        {
                            dtbCollectedDate.Text = payment.CollectedDate;
                        }
                        else
                        {
                            dtbCollectedDate.Text = "";
                        }
                        PaymentID = payment.PaymentId;
                        txtPaymentRate.Text = payment.CurrencyRate.ToString(currencyFormat);
                    }
                }
            }

        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbSupplierName.Items.Count > 0))
            {
                if (cmbSupplierName.SelectedIndex == 0 || cmbSupplierName.SelectedIndex == -1)
                {
                    cmbSupplierName.SelectedIndex = cmbSupplierName.Items.Count - 1;
                }
                else
                {
                    cmbSupplierName.SelectedIndex = cmbSupplierName.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if ((cmbSupplierName.Items.Count > 0))
            {
                if (cmbSupplierName.SelectedIndex == cmbSupplierName.Items.Count - 1 || cmbSupplierName.SelectedIndex == -1)
                {
                    cmbSupplierName.SelectedIndex = 0;
                }
                else
                {
                    cmbSupplierName.SelectedIndex = cmbSupplierName.SelectedIndex + 1;
                }
            }
        }

        private void dtbChequeDate_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void dtbCollectedDate_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cbBank_Click(object sender, EventArgs e)
        {
            int id = 0;
            if(cbBank.SelectedIndex!=-1)
            {
                id = int.Parse(cbBank.SelectedValue.ToString());
            }


            cbBank.ValueMember = "BankId";
            cbBank.DataSource = bankConnection.BankArrayList();
            cbBank.DisplayMember = "BankName";
            cbBank.SelectedValue =id;
        }

        private void TStripPrint_Click(object sender, EventArgs e)
        {
            if (dgvPayments.SelectedRows.Count > 0)
            {

                if (cmbSupplierName.SelectedIndex == -1)
                {
                    MessageBox.Show("Please Select Supplier ");
                    return;
                }



                clsSuppliersItemPayment payment = (clsSuppliersItemPayment)dgvPayments.SelectedRows[0].DataBoundItem;

                clsSupplier supplier = (clsSupplier)cmbSupplierName.SelectedItem;

                int paymetNo = dgvPayments.SelectedRows[0].Index + 1;

                printSupplierItemPayment frm = new printSupplierItemPayment( payment, paymetNo, supplier);
                frm.Show();
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }


        private void accessRight()
        {
            if (!right.Adding)
            {
                btnAdd.Enabled = false;
            }
            if (!right.Deleting)
            {
                btnRemove.Enabled = false;
            }
            if (!right.Printing)
            {
                TStripPrint.Enabled = false;
            }
            if (!right.Updating)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            //cmbSupplierName.Enabled = false;
            //cmbInvoiceNo.Enabled = false;
            cbBank.Enabled = false;
            dtpDate.Enabled = false;
            txtPayment.ReadOnly = true;
            txtChequeNumber.ReadOnly = true;
            dtbChequeDate.ReadOnly = true;
            dtbCollectedDate.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbSupplierName.Enabled = true;
            cbBank.Enabled = true;
            dtpDate.Enabled = true;
            txtPayment.ReadOnly = false;
            txtChequeNumber.ReadOnly = false;
            dtbChequeDate.ReadOnly = false;
            dtbCollectedDate.ReadOnly = false;
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();
                lblPaidAmount.Text = currency.CurrencySymbol;
                lblPrice.Text = currency.CurrencySymbol;
                lblRemaining.Text = currency.CurrencySymbol;
                lblPayment.Text = currency.CurrencySymbol;
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                txtPaidAmount.Text = (paidAmount / currency.CurrencyRate).ToString(currencyFormat);
                txtRemainingAmount.Text = (remainingAmount / currency.CurrencyRate).ToString(currencyFormat);
            }
            else
            {
                txtRate.Text = "";
                lblRemaining.Text = "";
                lblPrice.Text = "";
                lblPaidAmount.Text = "";
                lblPayment.Text = "";
            }
            DataChanged = true;
        }

        private void frmSupplierItemPayments_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }

        private void txtPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtPayment.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }
    }
}
