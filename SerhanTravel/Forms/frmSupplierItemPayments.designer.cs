﻿namespace SerhanTravel.Forms
{
    partial class frmSupplierItemPayments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSupplierItemPayments));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.lblRemaining = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblPaidAmount = new System.Windows.Forms.Label();
            this.txtPaidAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRemainingAmount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbSupplierName = new System.Windows.Forms.ComboBox();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripPrint = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.Label3 = new System.Windows.Forms.Label();
            this.stsLblInfo = new System.Windows.Forms.Label();
            this.dgvPayments = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Payment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BankId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChequeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CollectedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.txtChequeNumber = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cbBank = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dtbChequeDate = new System.Windows.Forms.TextBox();
            this.dtbCollectedDate = new System.Windows.Forms.TextBox();
            this.panelPayment = new System.Windows.Forms.Panel();
            this.lblPayment = new System.Windows.Forms.Label();
            this.txtPaymentRate = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.TStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayments)).BeginInit();
            this.panelPayment.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(316, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Paid Amount";
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.Location = new System.Drawing.Point(429, 17);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.ReadOnly = true;
            this.txtTotalPrice.Size = new System.Drawing.Size(100, 20);
            this.txtTotalPrice.TabIndex = 38;
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.Location = new System.Drawing.Point(67, 37);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(227, 30);
            this.lbResult.TabIndex = 43;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Red;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(57, 422);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(160, 36);
            this.btnRemove.TabIndex = 86;
            this.btnRemove.Text = "Remove Payment";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(316, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Total Price";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Coral;
            this.panel2.Controls.Add(this.lbResult);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtRate);
            this.panel2.Controls.Add(this.lblRemaining);
            this.panel2.Controls.Add(this.cmbCurrency);
            this.panel2.Controls.Add(this.lblPrice);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.lblPaidAmount);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtTotalPrice);
            this.panel2.Controls.Add(this.txtPaidAmount);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtRemainingAmount);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.cmbSupplierName);
            this.panel2.Location = new System.Drawing.Point(12, 69);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(583, 140);
            this.panel2.TabIndex = 88;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 92;
            this.label4.Text = "Rate";
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(67, 113);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(227, 20);
            this.txtRate.TabIndex = 91;
            // 
            // lblRemaining
            // 
            this.lblRemaining.AutoSize = true;
            this.lblRemaining.Location = new System.Drawing.Point(535, 87);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(0, 13);
            this.lblRemaining.TabIndex = 90;
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(67, 81);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(227, 21);
            this.cmbCurrency.TabIndex = 90;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(535, 19);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(0, 13);
            this.lblPrice.TabIndex = 89;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 84);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 89;
            this.label15.Text = "Currency";
            // 
            // lblPaidAmount
            // 
            this.lblPaidAmount.AutoSize = true;
            this.lblPaidAmount.Location = new System.Drawing.Point(535, 59);
            this.lblPaidAmount.Name = "lblPaidAmount";
            this.lblPaidAmount.Size = new System.Drawing.Size(0, 13);
            this.lblPaidAmount.TabIndex = 46;
            // 
            // txtPaidAmount
            // 
            this.txtPaidAmount.Location = new System.Drawing.Point(429, 56);
            this.txtPaidAmount.Name = "txtPaidAmount";
            this.txtPaidAmount.ReadOnly = true;
            this.txtPaidAmount.Size = new System.Drawing.Size(100, 20);
            this.txtPaidAmount.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Supplier";
            // 
            // txtRemainingAmount
            // 
            this.txtRemainingAmount.Location = new System.Drawing.Point(429, 84);
            this.txtRemainingAmount.Name = "txtRemainingAmount";
            this.txtRemainingAmount.ReadOnly = true;
            this.txtRemainingAmount.Size = new System.Drawing.Size(100, 20);
            this.txtRemainingAmount.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(316, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Remaining Amount";
            // 
            // cmbSupplierName
            // 
            this.cmbSupplierName.FormattingEnabled = true;
            this.cmbSupplierName.Location = new System.Drawing.Point(67, 17);
            this.cmbSupplierName.Name = "cmbSupplierName";
            this.cmbSupplierName.Size = new System.Drawing.Size(227, 21);
            this.cmbSupplierName.TabIndex = 34;
            this.cmbSupplierName.SelectedIndexChanged += new System.EventHandler(this.cmbSupplierName_SelectedIndexChanged);
            this.cmbSupplierName.TextChanged += new System.EventHandler(this.cmbSupplierName_TextChanged);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.ToolStripSeparator3,
            this.TStripPrint,
            this.ToolStripSeparator5,
            this.TStripBackword,
            this.toolStripSeparator1,
            this.TStripForword});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(1113, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 84;
            // 
            // btnAdd
            // 
            this.btnAdd.AccessibleDescription = "TStripForword";
            this.btnAdd.AccessibleName = "TStripForword";
            this.btnAdd.AutoSize = false;
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = global::SerhanTravel.Properties.Resources.New021;
            this.btnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 38);
            this.btnAdd.Text = "Add";
            this.btnAdd.ToolTipText = "Add";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripPrint
            // 
            this.TStripPrint.AccessibleDescription = "TStripPrint";
            this.TStripPrint.AccessibleName = "TStripPrint";
            this.TStripPrint.AutoSize = false;
            this.TStripPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripPrint.Image = global::SerhanTravel.Properties.Resources.Printer011;
            this.TStripPrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripPrint.Name = "TStripPrint";
            this.TStripPrint.Size = new System.Drawing.Size(40, 38);
            this.TStripPrint.Text = "Print";
            this.TStripPrint.ToolTipText = "Print Invoice";
            this.TStripPrint.Click += new System.EventHandler(this.TStripPrint_Click);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(6, 37);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(51, 13);
            this.Label3.TabIndex = 82;
            this.Label3.Text = "Customer";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.stsLblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stsLblInfo.Location = new System.Drawing.Point(0, 472);
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(1113, 21);
            this.stsLblInfo.TabIndex = 85;
            this.stsLblInfo.Text = "Supplier Items Payments Info";
            this.stsLblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvPayments
            // 
            this.dgvPayments.AllowUserToAddRows = false;
            this.dgvPayments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPayments.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.Payment,
            this.Currency,
            this.ChequeNo,
            this.BankId,
            this.ChequeDate,
            this.CollectedDate});
            this.dgvPayments.Location = new System.Drawing.Point(12, 217);
            this.dgvPayments.Name = "dgvPayments";
            this.dgvPayments.ReadOnly = true;
            this.dgvPayments.Size = new System.Drawing.Size(583, 199);
            this.dgvPayments.TabIndex = 83;
            this.dgvPayments.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPayments_CellClick);
            // 
            // Date
            // 
            this.Date.FillWeight = 80F;
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 80;
            // 
            // Payment
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0";
            this.Payment.DefaultCellStyle = dataGridViewCellStyle3;
            this.Payment.FillWeight = 80F;
            this.Payment.HeaderText = "Payment";
            this.Payment.Name = "Payment";
            this.Payment.ReadOnly = true;
            this.Payment.Width = 80;
            // 
            // Currency
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currency.DefaultCellStyle = dataGridViewCellStyle4;
            this.Currency.HeaderText = "Currency";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            // 
            // ChequeNo
            // 
            this.ChequeNo.FillWeight = 80F;
            this.ChequeNo.HeaderText = "Cheque No.";
            this.ChequeNo.Name = "ChequeNo";
            this.ChequeNo.ReadOnly = true;
            this.ChequeNo.Width = 80;
            // 
            // BankId
            // 
            this.BankId.HeaderText = "Bank";
            this.BankId.Name = "BankId";
            this.BankId.ReadOnly = true;
            // 
            // ChequeDate
            // 
            this.ChequeDate.FillWeight = 80F;
            this.ChequeDate.HeaderText = "Cheque Date";
            this.ChequeDate.Name = "ChequeDate";
            this.ChequeDate.ReadOnly = true;
            this.ChequeDate.Width = 80;
            // 
            // CollectedDate
            // 
            this.CollectedDate.FillWeight = 80F;
            this.CollectedDate.HeaderText = "Collected Date";
            this.CollectedDate.Name = "CollectedDate";
            this.CollectedDate.ReadOnly = true;
            this.CollectedDate.Width = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Cheque Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Bank";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Cheque Number";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Payment";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 210);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Collected Date";
            // 
            // txtPayment
            // 
            this.txtPayment.Location = new System.Drawing.Point(107, 63);
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.Size = new System.Drawing.Size(200, 20);
            this.txtPayment.TabIndex = 6;
            this.txtPayment.TextChanged += new System.EventHandler(this.txtPayment_TextChanged);
            this.txtPayment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayment_KeyPress);
            // 
            // txtChequeNumber
            // 
            this.txtChequeNumber.Location = new System.Drawing.Point(107, 120);
            this.txtChequeNumber.Name = "txtChequeNumber";
            this.txtChequeNumber.Size = new System.Drawing.Size(200, 20);
            this.txtChequeNumber.TabIndex = 7;
            this.txtChequeNumber.TextChanged += new System.EventHandler(this.txtChequeNumber_TextChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(107, 30);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 10;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // cbBank
            // 
            this.cbBank.FormattingEnabled = true;
            this.cbBank.Location = new System.Drawing.Point(107, 146);
            this.cbBank.Name = "cbBank";
            this.cbBank.Size = new System.Drawing.Size(200, 21);
            this.cbBank.TabIndex = 11;
            this.cbBank.SelectedIndexChanged += new System.EventHandler(this.cbBank_SelectedIndexChanged);
            this.cbBank.Click += new System.EventHandler(this.cbBank_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(232, 254);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dtbChequeDate
            // 
            this.dtbChequeDate.Location = new System.Drawing.Point(107, 176);
            this.dtbChequeDate.Name = "dtbChequeDate";
            this.dtbChequeDate.Size = new System.Drawing.Size(200, 20);
            this.dtbChequeDate.TabIndex = 15;
            this.dtbChequeDate.TextChanged += new System.EventHandler(this.dtbChequeDate_TextChanged);
            // 
            // dtbCollectedDate
            // 
            this.dtbCollectedDate.Location = new System.Drawing.Point(107, 207);
            this.dtbCollectedDate.Name = "dtbCollectedDate";
            this.dtbCollectedDate.Size = new System.Drawing.Size(200, 20);
            this.dtbCollectedDate.TabIndex = 16;
            this.dtbCollectedDate.TextChanged += new System.EventHandler(this.dtbCollectedDate_TextChanged);
            // 
            // panelPayment
            // 
            this.panelPayment.BackColor = System.Drawing.Color.LightGreen;
            this.panelPayment.Controls.Add(this.lblPayment);
            this.panelPayment.Controls.Add(this.txtPaymentRate);
            this.panelPayment.Controls.Add(this.label19);
            this.panelPayment.Controls.Add(this.dtbCollectedDate);
            this.panelPayment.Controls.Add(this.dtbChequeDate);
            this.panelPayment.Controls.Add(this.btnSave);
            this.panelPayment.Controls.Add(this.btnCancel);
            this.panelPayment.Controls.Add(this.cbBank);
            this.panelPayment.Controls.Add(this.dtpDate);
            this.panelPayment.Controls.Add(this.txtChequeNumber);
            this.panelPayment.Controls.Add(this.txtPayment);
            this.panelPayment.Controls.Add(this.label12);
            this.panelPayment.Controls.Add(this.label11);
            this.panelPayment.Controls.Add(this.label10);
            this.panelPayment.Controls.Add(this.label9);
            this.panelPayment.Controls.Add(this.label8);
            this.panelPayment.Controls.Add(this.label1);
            this.panelPayment.Location = new System.Drawing.Point(635, 69);
            this.panelPayment.Name = "panelPayment";
            this.panelPayment.Size = new System.Drawing.Size(421, 347);
            this.panelPayment.TabIndex = 87;
            // 
            // lblPayment
            // 
            this.lblPayment.AutoSize = true;
            this.lblPayment.Location = new System.Drawing.Point(313, 66);
            this.lblPayment.Name = "lblPayment";
            this.lblPayment.Size = new System.Drawing.Size(0, 13);
            this.lblPayment.TabIndex = 50;
            this.lblPayment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPaymentRate
            // 
            this.txtPaymentRate.Location = new System.Drawing.Point(170, 90);
            this.txtPaymentRate.Name = "txtPaymentRate";
            this.txtPaymentRate.ReadOnly = true;
            this.txtPaymentRate.Size = new System.Drawing.Size(137, 20);
            this.txtPaymentRate.TabIndex = 49;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 90);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 13);
            this.label19.TabIndex = 48;
            this.label19.Text = "Payment Rate";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(107, 254);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 32);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmSupplierItemPayments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(1113, 493);
            this.Controls.Add(this.panelPayment);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.TStrip);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.stsLblInfo);
            this.Controls.Add(this.dgvPayments);
            this.Name = "frmSupplierItemPayments";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Supplier Item Payments";
            this.Load += new System.EventHandler(this.frmSupplierPayments_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmSupplierItemPayments_MouseClick);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayments)).EndInit();
            this.panelPayment.ResumeLayout(false);
            this.panelPayment.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotalPrice;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtPaidAmount;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRemainingAmount;
        private System.Windows.Forms.Label label7;
        internal System.Windows.Forms.ComboBox cmbSupplierName;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton btnAdd;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton TStripForword;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label stsLblInfo;
        internal System.Windows.Forms.DataGridView dgvPayments;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPayment;
        private System.Windows.Forms.TextBox txtChequeNumber;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.ComboBox cbBank;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox dtbChequeDate;
        private System.Windows.Forms.TextBox dtbCollectedDate;
        private System.Windows.Forms.Panel panelPayment;
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblRemaining;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblPaidAmount;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtPaymentRate;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblPayment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Payment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn BankId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChequeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollectedDate;
        internal System.Windows.Forms.Label label4;
    }
}