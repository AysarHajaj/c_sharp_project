﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using SerhanTravel.Print;
using SerhanTravel.Connections;
using System.Reflection;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierReturnedItem : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        List<clsSupplierItemInvoice> supplierInvoiceList;
        List<clsItem> itemList;
        List<int> itemsId = new List<int>();
        SupplierItemInvoice invoiceConnection;
        clsSupplierItemInvoice invoiceData;
        List<clsReturnedSupplierItem> returnedItems;
        ReturnedSupplierItem returnedItemConnection;
        int InvoiceId = 0;
        private List<clsCurrency> currencies;
        decimal netPrice = 0;
        private bool allowRemove = false;
        string currencyFormat;
        clsAccessRights right;
        Currency currencyConnection;
        internal frmSupplierReturnedItem(clsAccessRights right)
        {
            InitializeComponent();
            dgvItems.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvItems.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvItems.MultiSelect = false;
            chLbCustomerItems.HorizontalScrollbar = true;
            dgvItems.ScrollBars = ScrollBars.Both;
            this.right = right;

        }

        private void ResetFields()
        {
           
            InvoiceId = 0;
            dgvItems.DataSource = null;
            chLbCustomerItems.DataSource = null;
            dgvItems.Rows.Clear();
            txtReturnedAmount.Clear();
            dtpReturnDate.Value = DateTime.Today;
            stsLblInfo.Text = "";
            itemList.Clear();
            returnedItems.Clear();
            itemsId.Clear();
            netPrice = 0;
           
        }
        private void frmSupplierReturnedItem_Load(object sender, EventArgs e)
        {
            accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            currencyConnection = new Currency();
            dtpReturnDate.Value = DateTime.Today;
            itemList = new List<clsItem>();
            invoiceConnection = new SupplierItemInvoice();
            returnedItems = new List<clsReturnedSupplierItem>();
            supplierConnection = new Supplier();
            returnedItemConnection = new ReturnedSupplierItem();
            dgvItems.AutoGenerateColumns = false;
            dgvItems.Columns[0].DataPropertyName = "Item.itemName";
            dgvItems.Columns[1].DataPropertyName = "Item.CustomerName";
            dgvItems.Columns[2].DataPropertyName = "Item.CostString";
            dgvItems.Columns[3].DataPropertyName = "ReturnedAmountString";
            dgvItems.Columns[4].DataPropertyName = "Remained";
            FillCombobox();
            ResetFields();
            InvoiceId = 0;
         

        }

        private void DisplayData(int invoiceId)
        {
            cmbInvoceNo.DataSource = null;
            cmbInvoceNo.ValueMember = "InvoiceId";
            cmbInvoceNo.DataSource = supplierInvoiceList;
            cmbInvoceNo.DisplayMember = "InvoiceId";
          
        }

        private void FillCombobox()
        {
            supplierList = supplierConnection.SupplierArrayList();
         
            cmbSupplier.ValueMember = "SupplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "SupplierName";
            cmbSupplier.SelectedIndex = -1;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
           
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbInvoceNo.DataSource = null;
            ResetFields();
            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(int.Parse(cmbSupplier.SelectedValue.ToString()));
                cmbInvoceNo.ValueMember = "InvoiceId";
                cmbInvoceNo.DataSource = supplierInvoiceList;
                cmbInvoceNo.DisplayMember = "InvoiceId";
                cmbInvoceNo.SelectedIndex = -1;
              
            }
        }
        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            ResetFields();
            if (cmbInvoceNo.SelectedIndex != -1 && supplierInvoiceList.Count > 0)
            {

                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                invoiceData = supplierInvoiceList[cmbInvoceNo.SelectedIndex];
                itemList = new List<clsItem>(invoiceData.Items);
                chLbCustomerItems.ValueMember = "itemID";
                chLbCustomerItems.DataSource = itemList;
                chLbCustomerItems.DisplayMember = "SupplierReturn";
                returnedItems = returnedItemConnection.getReturnedItems(InvoiceId);
                foreach(clsReturnedSupplierItem item in returnedItems)
                {
                    itemsId.Add(item.Item.itemID);
                }
                dgvItems.DataSource = returnedItems;
                dgvItems.Refresh();
                dgvItems.ClearSelection();

            }
           

        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
            }
            if (cmbCurrency.SelectedIndex != -1 && invoiceData != null)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                lblReturnSymbol.Text = currency.CurrencySymbol;

            }
            else if (invoiceData != null)
            {
                lblReturnSymbol.Text = "";
                txtRate.Clear();
            }
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(chLbCustomerItems.SelectedIndex==-1)
            {
                return;
            }
            if(string.IsNullOrWhiteSpace(txtReturnedAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount to be Return to Continue...");
                return;
            }
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency...");
                return;
            }
            foreach (clsItem items in chLbCustomerItems.CheckedItems)
            {

                try
                {
                    clsItem item = (clsItem)items;
                    if(itemsId.Contains(item.itemID))
                    {
                        MessageBox.Show("This Item is Returned Please Select Another Item...");
                        return;
                    }
                    clsReturnedSupplierItem returnedItem = new clsReturnedSupplierItem();
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    decimal returnAmount= Convert.ToDecimal(txtReturnedAmount.Text);

                    if (currency.CurrencyId != item.CurrencyId)
                    {
                        returnAmount *= currency.CurrencyRate;
                        returnAmount /= item.CurrencyRate;
                    }
                   
                    returnedItem.CurrencyId = item.CurrencyId;
                    returnedItem.CurrencyRate = item.CurrencyRate;
                    returnedItem.Date = dtpReturnDate.Value;
                    returnedItem.InvoiceId = InvoiceId;
                    returnedItem.ReturnAmount = returnAmount;
                    returnedItem.Item = item;
                    returnedItemConnection.AddReturnedItem(returnedItem);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    simpleSound.Play();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                    simpleSound.Play();
                }
                
            }
            DisplayData(InvoiceId);
            txtReturnedAmount.Clear();
            btnAdd.Visible = false;

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvItems.RowCount > 0 && returnedItems.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvItems.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                int index = dgvItems.SelectedRows[0].Index;
                if (index >= returnedItems.Count || index < 0)
                {
                    return;
                }

                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    try
                    {
                        clsReturnedSupplierItem item = returnedItems[index];
                        returnedItemConnection.DeleteReturnedItem(item.Id);
                        DisplayData(InvoiceId);
                    }
                    catch (Exception eex)
                    {
                        MessageBox.Show(eex.Message);
                    }
                }

            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            if (cmbSupplier.SelectedIndex != -1 && cmbInvoceNo.SelectedIndex!=-1)
            {
                itemList = invoiceData.Items;
                chLbCustomerItems.ValueMember = "itemID";
                chLbCustomerItems.DataSource = itemList;
                chLbCustomerItems.DisplayMember = "SupplierReturn";
            }
        }






        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == 0 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.Items.Count - 1;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {

            if ((cmbInvoceNo.Items.Count > 0))
            {
                if (cmbInvoceNo.SelectedIndex == cmbInvoceNo.Items.Count - 1 || cmbInvoceNo.SelectedIndex == -1)
                {
                    cmbInvoceNo.SelectedIndex = 0;
                }
                else
                {
                    cmbInvoceNo.SelectedIndex = cmbInvoceNo.SelectedIndex + 1;
                }
            }
        }





        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

          private void accessRight()
          {
              if (!right.Adding)
              {
                  btnAdd.Enabled = false;
              }
              if (!right.Deleting)
              {
                  btnRemove.Enabled = false;
              }
           
              
          }



        private void chLbCustomerTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chLbCustomerItems.SelectedIndex != -1)
            {
                btnAdd.Visible = true;
            }
        }

        private void txtReturnedAmount_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if(character=='.' && txtReturnedAmount.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }


        private void dgvTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvItems.RowCount > 0)
            {

                if (e.RowIndex >= 0)
                {

                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void dgvTickets_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((dgvItems.Rows[e.RowIndex].DataBoundItem != null) && (dgvItems.Columns[e.ColumnIndex].DataPropertyName.Contains(".")))
                e.Value = BindProperty(dgvItems.Rows[e.RowIndex].DataBoundItem, dgvItems.Columns[e.ColumnIndex].DataPropertyName);
        }

        private string BindProperty(object property, string propertyName)
        {
            string retValue;

            retValue = "";

            if (propertyName.Contains("."))
            {
                PropertyInfo[] arrayProperties;
                string leftPropertyName;

                leftPropertyName = propertyName.Substring(0, propertyName.IndexOf("."));
                arrayProperties = property.GetType().GetProperties();

                foreach (PropertyInfo propertyInfo in arrayProperties)
                {
                    if (propertyInfo.Name == leftPropertyName)
                    {
                        retValue = BindProperty(propertyInfo.GetValue(property, null), propertyName.Substring(propertyName.IndexOf(".") + 1));
                        break;
                    }
                }
            }
            else
            {
                Type propertyType;
                PropertyInfo propertyInfo;

                propertyType = property.GetType();
                propertyInfo = propertyType.GetProperty(propertyName);
                retValue = propertyInfo.GetValue(property, null).ToString();
            }

            return retValue;
        }

 

      
    }
}
