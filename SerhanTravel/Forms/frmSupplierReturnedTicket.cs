﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using SerhanTravel.Print;
using SerhanTravel.Connections;
using System.Reflection;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierReturnedTicket : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        List<clsTickets> ticketList;
        List<int> ticketsId = new List<int>();
        List<clsReturnedSupplierTicket> returnedTickets;
        ReturnedSupplierTicket returnedTicketConnection;
        private List<clsCurrency> currencies;
        private bool allowRemove = false;
        string currencyFormat;
        clsAccessRights right;
        private bool skip;
        Currency currencyConnection;
        internal frmSupplierReturnedTicket(clsAccessRights right)
        {
            InitializeComponent();
            dgvTickets.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvTickets.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvTickets.MultiSelect = false;
            chLbCustomerTickets.HorizontalScrollbar = true;
            //chLbCustomerTickets.ScrollAlwaysVisible = true;
            dgvTickets.ScrollBars = ScrollBars.Both;
             this.right = right;

        }

        private void ResetFields()
        {
            dgvTickets.DataSource = null;
            chLbCustomerTickets.DataSource = null;
            dgvTickets.Rows.Clear();
            txtReturnedAmount.Clear();
            stsLblInfo.Text = "";
            ticketList.Clear();
            returnedTickets.Clear();
           
        }
        private void frmSupplierReturnedTicket_Load(object sender, EventArgs e)
        {
             accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            currencyConnection = new Currency();
            ticketList = new List<clsTickets>();
            returnedTickets = new List<clsReturnedSupplierTicket>();
            supplierConnection = new Supplier();
            returnedTicketConnection = new ReturnedSupplierTicket();
            dgvTickets.AutoGenerateColumns = false;
            dgvTickets.Columns[0].DataPropertyName = "Ticket.TicketNo";
            dgvTickets.Columns[1].DataPropertyName = "Ticket.Destination";
            dgvTickets.Columns[2].DataPropertyName = "Ticket.Cost";
            dgvTickets.Columns[3].DataPropertyName = "ReturnAmount";
            dgvTickets.Columns[4].DataPropertyName = "Remained";
            dgvTickets.Columns[5].DataPropertyName = "Ticket.CurrecnySymbol";
            dgvTickets.Columns[6].DataPropertyName = "Ticket.TravellerName";
            FillCombobox();
            skip = true;
            ResetFields();
 
         

        }

        private void DisplayData(int suppId)
        {
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "SupplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "SupplierName";
            cmbSupplier.SelectedValue = suppId;
            skip = true;
        }

        private void FillCombobox()
        {
            supplierList = supplierConnection.SupplierArrayList();
         
            cmbSupplier.ValueMember = "SupplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "SupplierName";
            cmbSupplier.SelectedIndex = -1;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
           
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {

            ResetFields();
            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                ticketList = supplierConnection.getUnReturnedTicekts(int.Parse(cmbSupplier.SelectedValue.ToString()));
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "SupplierReturn";
                returnedTickets = returnedTicketConnection.getReturnedTickets(int.Parse(cmbSupplier.SelectedValue.ToString()));
                dgvTickets.DataSource = returnedTickets;
                dgvTickets.Refresh();
                dgvTickets.ClearSelection();
                skip = true;

            }
        }
   
        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                lblReturnSymbol.Text = currency.CurrencySymbol;
            }
            else
            {
                lblReturnSymbol.Text = "";
                txtRate.Clear();
            }


        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(chLbCustomerTickets.SelectedIndex==-1)
            {
                return;
            }
            if(string.IsNullOrWhiteSpace(txtReturnedAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount to be Return to Continue...");
                return;
            }
         
            foreach (clsTickets tickets in chLbCustomerTickets.CheckedItems)
            {

                try
                {
                    clsTickets ticket = (clsTickets)tickets;
                    if(ticketsId.Contains(ticket.TicketId))
                    {
                        MessageBox.Show("This Ticket is Returned Please Select Another Ticket...");
                        return;
                    }
                    clsReturnedSupplierTicket returnedTicket = new clsReturnedSupplierTicket();
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    decimal returnAmount= Convert.ToDecimal(txtReturnedAmount.Text);
                    if (currency.CurrencyId != ticket.CurrencyId)
                    {
                        returnAmount *= currency.CurrencyRate;
                        decimal ticketRate = ticket.CurrencyRate;
                        returnAmount /= ticketRate;
                    }
                    returnedTicket.CurrencyId = ticket.CurrencyId;
                    returnedTicket.Date = dtpReturnDate.Value;
                    returnedTicket.ReturnAmount = returnAmount;
                    returnedTicket.Ticket = ticket;
                    returnedTicket.CurrencyRate = ticket.CurrencyRate;
                    returnedTicketConnection.AddReturnedTicket(returnedTicket);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    simpleSound.Play();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                    simpleSound.Play();
                }
                
            }
            DisplayData(int.Parse(cmbSupplier.SelectedValue.ToString()));
            txtReturnedAmount.Clear();
            btnAdd.Visible = false;

        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvTickets.RowCount > 0 && returnedTickets.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvTickets.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                int index = dgvTickets.SelectedRows[0].Index;
                if (index >= returnedTickets.Count || index < 0)
                {
                    return;
                }

                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    try
                    {
                        clsReturnedSupplierTicket ticket = returnedTickets[index];
                        returnedTicketConnection.DeleteReturnedTickets(ticket.Id);
                        DisplayData(int.Parse(cmbSupplier.SelectedValue.ToString()));
                    }
                    catch (Exception eex)
                    {
                        MessageBox.Show(eex.Message);
                    }
                }

            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            if (cmbSupplier.SelectedIndex != -1)
            {
                ticketList =supplierConnection.getUnReturnedTicekts(int.Parse(cmbSupplier.SelectedValue.ToString()));

                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "SupplierReturn";
            }
        }






        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbSupplier.Items.Count > 0))
            {
                if (cmbSupplier.SelectedIndex == 0 || cmbSupplier.SelectedIndex == -1)
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.Items.Count - 1;
                }
                else
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {

            if ((cmbSupplier.Items.Count > 0))
            {
                if (cmbSupplier.SelectedIndex == cmbSupplier.Items.Count - 1 || cmbSupplier.SelectedIndex == -1)
                {
                    cmbSupplier.SelectedIndex = 0;
                }
                else
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.SelectedIndex + 1;
                }
            }
        }





        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                btnAdd.Enabled = false;
            }
            if (!right.Deleting)
            {
                btnRemove.Enabled = false;
            }


        }
        private void chLbCustomerTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chLbCustomerTickets.SelectedIndex != -1)
            {
                btnAdd.Visible = true;
            }
        }

        private void txtReturnedAmount_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtReturnedAmount.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }


        private void dgvTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTickets.RowCount > 0)
            {

                if (e.RowIndex >= 0)
                {

                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void dgvTickets_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if ((dgvTickets.Rows[e.RowIndex].DataBoundItem != null) && (dgvTickets.Columns[e.ColumnIndex].DataPropertyName.Contains(".")))
                e.Value = BindProperty(dgvTickets.Rows[e.RowIndex].DataBoundItem, dgvTickets.Columns[e.ColumnIndex].DataPropertyName);
        }

        private string BindProperty(object property, string propertyName)
        {
            string retValue;

            retValue = "";

            if (propertyName.Contains("."))
            {
                PropertyInfo[] arrayProperties;
                string leftPropertyName;

                leftPropertyName = propertyName.Substring(0, propertyName.IndexOf("."));
                arrayProperties = property.GetType().GetProperties();

                foreach (PropertyInfo propertyInfo in arrayProperties)
                {
                    if (propertyInfo.Name == leftPropertyName)
                    {
                        retValue = BindProperty(propertyInfo.GetValue(property, null), propertyName.Substring(propertyName.IndexOf(".") + 1));
                        break;
                    }
                }
            }
            else
            {
                Type propertyType;
                PropertyInfo propertyInfo;

                propertyType = property.GetType();
                propertyInfo = propertyType.GetProperty(propertyName);
                retValue = propertyInfo.GetValue(property, null).ToString();
            }

            return retValue;
        }

        private void cmbSupplier_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {



                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                cmbSupplier.SelectedItem = lbResult.SelectedItem;
            }
            lbResult.Visible = false;
        }

        private void txtTicketNo_TextChanged(object sender, EventArgs e)
        {
            string textToSearch = txtTicketNo.Text.ToLower();
            clsTickets[] result = (from i in ticketList
                                   where i.TicketString.ToLower().Contains(textToSearch)
                                   select i).ToArray();
            if (result.Length == 0)
            {
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = ticketList;
                chLbCustomerTickets.DisplayMember = "ReturnString";
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = result;
                chLbCustomerTickets.DisplayMember = "ReturnString";
            }
        }
    }
}
