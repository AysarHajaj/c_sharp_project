﻿namespace SerhanTravel.Forms
{
    partial class frmSupplierReturnedTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSupplierReturnedTicket));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtRate = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.chLbCustomerTickets = new System.Windows.Forms.CheckedListBox();
            this.dgvTickets = new System.Windows.Forms.DataGridView();
            this.TicketNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Destination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnedAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remained = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Traveller = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLRefresh = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.stsLblInfo = new System.Windows.Forms.Label();
            this.txtReturnedAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblReturnSymbol = new System.Windows.Forms.Label();
            this.txtTicketNo = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.dtpReturnDate = new System.Windows.Forms.DateTimePicker();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cmbCurrency);
            this.panel2.Controls.Add(this.Label12);
            this.panel2.Controls.Add(this.txtRate);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(380, 121);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(287, 122);
            this.panel2.TabIndex = 117;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 90;
            this.label8.Text = "Rate";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(78, 19);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(143, 23);
            this.cmbCurrency.TabIndex = 60;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(3, 22);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 59;
            this.Label12.Text = "Currency";
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(78, 48);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(143, 22);
            this.txtRate.TabIndex = 61;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel3.Controls.Add(this.Label1);
            this.panel3.Controls.Add(this.dtpReturnDate);
            this.panel3.Controls.Add(this.lbResult);
            this.panel3.Controls.Add(this.Label7);
            this.panel3.Controls.Add(this.cmbSupplier);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(50, 121);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(324, 122);
            this.panel3.TabIndex = 116;
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(105, 36);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(199, 34);
            this.lbResult.TabIndex = 190;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(5, 17);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(54, 15);
            this.Label7.TabIndex = 55;
            this.Label7.Text = "Supplier";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Location = new System.Drawing.Point(105, 14);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(199, 23);
            this.cmbSupplier.TabIndex = 77;
            this.cmbSupplier.SelectedIndexChanged += new System.EventHandler(this.cmbSupplier_SelectedIndexChanged);
            this.cmbSupplier.TextChanged += new System.EventHandler(this.cmbSupplier_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(800, 555);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(99, 32);
            this.btnAdd.TabIndex = 115;
            this.btnAdd.Text = "Add ";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Red;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(122, 555);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(112, 32);
            this.btnRemove.TabIndex = 114;
            this.btnRemove.Text = "Remove ";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // chLbCustomerTickets
            // 
            this.chLbCustomerTickets.BackColor = System.Drawing.Color.Gray;
            this.chLbCustomerTickets.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLbCustomerTickets.ForeColor = System.Drawing.Color.White;
            this.chLbCustomerTickets.FormattingEnabled = true;
            this.chLbCustomerTickets.Location = new System.Drawing.Point(728, 121);
            this.chLbCustomerTickets.Name = "chLbCustomerTickets";
            this.chLbCustomerTickets.Size = new System.Drawing.Size(322, 361);
            this.chLbCustomerTickets.TabIndex = 113;
            this.chLbCustomerTickets.SelectedIndexChanged += new System.EventHandler(this.chLbCustomerTickets_SelectedIndexChanged);
            // 
            // dgvTickets
            // 
            this.dgvTickets.AllowUserToAddRows = false;
            this.dgvTickets.AllowUserToDeleteRows = false;
            this.dgvTickets.AllowUserToOrderColumns = true;
            this.dgvTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TicketNo,
            this.Destination,
            this.Cost,
            this.ReturnedAmount,
            this.Remained,
            this.Currency,
            this.Traveller});
            this.dgvTickets.Location = new System.Drawing.Point(45, 288);
            this.dgvTickets.MultiSelect = false;
            this.dgvTickets.Name = "dgvTickets";
            this.dgvTickets.ReadOnly = true;
            this.dgvTickets.Size = new System.Drawing.Size(622, 144);
            this.dgvTickets.TabIndex = 112;
            this.dgvTickets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTickets_CellClick);
            this.dgvTickets.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvTickets_CellFormatting);
            // 
            // TicketNo
            // 
            this.TicketNo.HeaderText = "Ticket No";
            this.TicketNo.Name = "TicketNo";
            this.TicketNo.ReadOnly = true;
            // 
            // Destination
            // 
            this.Destination.HeaderText = "Destination";
            this.Destination.Name = "Destination";
            this.Destination.ReadOnly = true;
            // 
            // Cost
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = "0";
            this.Cost.DefaultCellStyle = dataGridViewCellStyle1;
            this.Cost.HeaderText = "Cost";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // ReturnedAmount
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.ReturnedAmount.DefaultCellStyle = dataGridViewCellStyle2;
            this.ReturnedAmount.HeaderText = "Returned Amount";
            this.ReturnedAmount.Name = "ReturnedAmount";
            this.ReturnedAmount.ReadOnly = true;
            // 
            // Remained
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0";
            this.Remained.DefaultCellStyle = dataGridViewCellStyle3;
            this.Remained.HeaderText = "Remained";
            this.Remained.Name = "Remained";
            this.Remained.ReadOnly = true;
            // 
            // Currency
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currency.DefaultCellStyle = dataGridViewCellStyle4;
            this.Currency.HeaderText = "Currency";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            this.Currency.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Traveller
            // 
            this.Traveller.HeaderText = "Traveller";
            this.Traveller.Name = "Traveller";
            this.Traveller.ReadOnly = true;
            // 
            // btnLRefresh
            // 
            this.btnLRefresh.BackgroundImage = global::SerhanTravel.Properties.Resources.if_view_refresh_118801__1_;
            this.btnLRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLRefresh.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnLRefresh.Location = new System.Drawing.Point(1043, 107);
            this.btnLRefresh.Name = "btnLRefresh";
            this.btnLRefresh.Size = new System.Drawing.Size(28, 25);
            this.btnLRefresh.TabIndex = 119;
            this.btnLRefresh.UseVisualStyleBackColor = true;
            this.btnLRefresh.Click += new System.EventHandler(this.btnLRefresh_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AccessibleDescription = "TStrip";
            this.toolStrip1.AccessibleName = "TStrip";
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.NavajoWhite;
            this.toolStrip1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripBackword,
            this.TStripForword});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1083, 58);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 120;
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.stsLblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stsLblInfo.Location = new System.Drawing.Point(0, 660);
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(1083, 21);
            this.stsLblInfo.TabIndex = 121;
            this.stsLblInfo.Text = "Returned Info";
            this.stsLblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtReturnedAmount
            // 
            this.txtReturnedAmount.Location = new System.Drawing.Point(728, 500);
            this.txtReturnedAmount.Name = "txtReturnedAmount";
            this.txtReturnedAmount.Size = new System.Drawing.Size(135, 20);
            this.txtReturnedAmount.TabIndex = 123;
            this.txtReturnedAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReturnedAmount_KeyPress);
            // 
            // label2
            // 
            this.label2.AccessibleDescription = "lblSerNo";
            this.label2.AccessibleName = "lblSerNo";
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(558, 501);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 15);
            this.label2.TabIndex = 124;
            this.label2.Text = "Amount to be Returned";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Trebuchet MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(46, 263);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 22);
            this.label4.TabIndex = 113;
            this.label4.Text = "Returned Tickets";
            // 
            // lblReturnSymbol
            // 
            this.lblReturnSymbol.AutoSize = true;
            this.lblReturnSymbol.Location = new System.Drawing.Point(884, 503);
            this.lblReturnSymbol.Name = "lblReturnSymbol";
            this.lblReturnSymbol.Size = new System.Drawing.Size(0, 13);
            this.lblReturnSymbol.TabIndex = 125;
            // 
            // txtTicketNo
            // 
            this.txtTicketNo.AccessibleDescription = "txtSerNo";
            this.txtTicketNo.AccessibleName = "txtSerNo";
            this.txtTicketNo.Location = new System.Drawing.Point(728, 95);
            this.txtTicketNo.Name = "txtTicketNo";
            this.txtTicketNo.Size = new System.Drawing.Size(309, 20);
            this.txtTicketNo.TabIndex = 129;
            this.txtTicketNo.TextChanged += new System.EventHandler(this.txtTicketNo_TextChanged);
            // 
            // label19
            // 
            this.label19.AccessibleDescription = "lblSerNo";
            this.label19.AccessibleName = "lblSerNo";
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(540, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(186, 15);
            this.label19.TabIndex = 128;
            this.label19.Text = "Search (Ticket No/Traveller)";
            // 
            // Label1
            // 
            this.Label1.AccessibleDescription = "lblSerNo";
            this.Label1.AccessibleName = "lblSerNo";
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(3, 82);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(77, 15);
            this.Label1.TabIndex = 191;
            this.Label1.Text = " Return Date";
            // 
            // dtpReturnDate
            // 
            this.dtpReturnDate.Location = new System.Drawing.Point(104, 76);
            this.dtpReturnDate.Name = "dtpReturnDate";
            this.dtpReturnDate.Size = new System.Drawing.Size(200, 22);
            this.dtpReturnDate.TabIndex = 192;
            // 
            // frmSupplierReturnedTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(1083, 681);
            this.Controls.Add(this.txtTicketNo);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lblReturnSymbol);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvTickets);
            this.Controls.Add(this.txtReturnedAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.stsLblInfo);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnLRefresh);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.chLbCustomerTickets);
            this.Name = "frmSupplierReturnedTicket";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Returned Ticket";
            this.Load += new System.EventHandler(this.frmSupplierReturnedTicket_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label Label12;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label Label7;
        private System.Windows.Forms.ComboBox cmbSupplier;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.CheckedListBox chLbCustomerTickets;
        internal System.Windows.Forms.DataGridView dgvTickets;
        private System.Windows.Forms.Button btnLRefresh;
        internal System.Windows.Forms.ToolStrip toolStrip1;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripButton TStripForword;
        internal System.Windows.Forms.Label stsLblInfo;
        private System.Windows.Forms.TextBox txtReturnedAmount;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblReturnSymbol;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txtRate;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn TicketNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Destination;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnedAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remained;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Traveller;
        internal System.Windows.Forms.TextBox txtTicketNo;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.DateTimePicker dtpReturnDate;
    }
}