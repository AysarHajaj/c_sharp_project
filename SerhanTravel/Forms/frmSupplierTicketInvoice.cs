﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using SerhanTravel.Print;

namespace SerhanTravel.Forms
{
    public partial class frmSupplierTicketInvoice : Form
    {
        bool canUpdateOnly;

        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        List<clsSupplierTicketInvoice> supplierInvoiceList;
        List<clsTickets> ticketList;
        List<int> ticketListIDS;
        List<clsTickets> SupplierTicketList;
        SupplierTicketInvoice invoiceConnection;
        Supplier supplierConnection;
        clsSupplierTicketInvoice invoiceData;
        clsSupplier supplierData;
        bool skip = false;
        bool DataChanged = false;
        bool iamAdding = false;
        int InvoiceId = 0;
        int InvoiceSavedID = 0;
        int SupplierID = 0;
        private int SuppliersaveID;
        private List<clsCurrency> currencies;
        decimal totalPrice = 0;
        private bool allowRemove;
        string currencyFormat;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection;
        internal frmSupplierTicketInvoice(clsAccessRights right)
        {
            InitializeComponent();
            dgvTickets.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvTickets.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvTickets.MultiSelect = false;
            cmbSupplier.SelectedIndex = -1;
            cLbSupplierTickets.HorizontalScrollbar = true;
            dgvTickets.ScrollBars = ScrollBars.Both;
            dgvTickets.AutoGenerateColumns = false;
            dgvTickets.Columns[0].DataPropertyName = "TicketNo";
            dgvTickets.Columns[1].DataPropertyName = "Destination";
            dgvTickets.Columns[2].DataPropertyName = "Cost";
            dgvTickets.Columns[3].DataPropertyName = "CurrecnySymbol";
            dgvTickets.Columns[4].DataPropertyName = "TravellerName";
            dgvTickets.Columns[5].DataPropertyName = "flightNo";
            this.right = right;
        }

        private void ResetFields()
        {
            currencyFormat = "#,##0.00;-#,##0.00;Zero";
            currencyConnection = new Connections.Currency();
            cmbInvoceNo.DataSource = null;
            cLbSupplierTickets.DataSource = null;
            dgvTickets.DataSource = null;
            cmbCurrency.Text = "";
            cmbInvoceNo.Text = "";
            dgvTickets.Rows.Clear();
            txtRate.Clear();
            stsLblInfo.Text = "";
            txtTotalPrice.Clear();
            dtpInvoiceDate.Value = DateTime.Today;
            cmbCurrency.SelectedIndex = -1;
            cmbInvoceNo.SelectedIndex = -1;
            cLbSupplierTickets.SelectedIndex = -1;
            ticketList.Clear();
            ticketListIDS.Clear();
            totalPrice = 0;
            lblTotalPrice.Text = "";
           DataChanged = false;
        }

        private void FillCombobox()
        {
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = 0;
            DataChanged = false;
        }

        private void frmSupplierInvoice_Load(object sender, EventArgs e)
        {
            accessRight();
            lbResult.Visible = false;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            dtpInvoiceDate.Value = DateTime.Today;
            supplierConnection = new Supplier();
            ticketList = new List<clsTickets>();
            ticketListIDS = new List<int>();
            invoiceConnection = new SupplierTicketInvoice();
            supplierList = supplierConnection.SupplierArrayList();
            supplierInvoiceList = new List<clsSupplierTicketInvoice>();
            currencyConnection = new Currency();
            FillCombobox();
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedIndex = -1;
            ResetFields();
            InvoiceId = 0;
            DataChanged = false;
        }

        private void DisplayData(int supId,int invId)
        {
            iamAdding = false;
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedValue = supId;
            cmbInvoceNo.SelectedValue = invId;
            skip = true;
            SupplierID = supId;
            
            DataChanged = false;
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            cLbSupplierTickets.DataSource = null;
            lbResult.Visible = false;
            btnAdd.Visible = false;
            btnRemove.Visible = false;
            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                InvoiceId = 0;
                ResetFields();
                SupplierID = int.Parse(cmbSupplier.SelectedValue.ToString());
                SupplierTicketList = invoiceConnection.SupplierTicketsArrayList(SupplierID);
                cLbSupplierTickets.ValueMember = "ticketId";
                cLbSupplierTickets.DataSource = SupplierTicketList;
                cLbSupplierTickets.DisplayMember = "SupplierToString";
                if (iamAdding)
                {
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.SelectedIndex = -1;
                    SupplierTicketList.Clear();
                    ticketList.Clear();
                    ticketListIDS.Clear();
                    return;
                }
                supplierData = supplierList[cmbSupplier.SelectedIndex];
                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
                if (supplierInvoiceList.Count > 0)
                {
                    cmbInvoceNo.Visible = true;
                    lblInvoiceNo.Visible = true;
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.ValueMember = "invoiceId";
                    cmbInvoceNo.DataSource = supplierInvoiceList;
                    cmbInvoceNo.DisplayMember = "invoiceId";
                    cmbInvoceNo.SelectedIndex = supplierInvoiceList.Count - 1;
                }
                else
                {
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.Visible = false;
                    lblInvoiceNo.Visible = false;
                    dgvTickets.DataSource = null;
                    dgvTickets.Rows.Clear();
                    totalPrice = 0;
                    txtTotalPrice.Text = "";
                    dtpInvoiceDate.Value = DateTime.Today;
                }
                DataChanged = false;
                skip = true;
            }
            else
            {
                skip = false;
            }
            DataChanged = false;
        }

        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbInvoceNo.SelectedIndex != -1 && supplierInvoiceList.Count > 0)
            {
                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                invoiceData = supplierInvoiceList[cmbInvoceNo.SelectedIndex];
                dtpInvoiceDate.Value = invoiceData.Date;
                ticketListIDS.Clear();
                ticketList.Clear();
                ticketList = invoiceConnection.TicketsInvoicesArrayList(InvoiceId);
                for (int i = 0; i < ticketList.Count; i++)
                {
                    ticketListIDS.Add(ticketList[i].TicketId);
                }
                dgvTickets.DataSource = null;
                totalPrice = 0;
                if (ticketList.Count > 0)
                {
                    dgvTickets.DataSource = ticketList;
                    for (int i = 0; i < dgvTickets.Rows.Count; i++)
                    {
                        int currencyId = ticketList.ElementAt(i).CurrencyId;
                        decimal Rate = 1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if (currency != null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }

                        totalPrice += (decimal)dgvTickets.Rows[i].Cells[2].Value * Rate;
                    }
                    dgvTickets.Refresh();
                    dgvTickets.ClearSelection();
                }
               
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                }
                else
                {
                    txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                }
            }
            DataChanged = false;
            btnRemove.Visible = false;
            btnAdd.Visible = false;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                lblTotalPrice.Text = currency.CurrencySymbol;
            }
            else
            {
                txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                lblTotalPrice.Text = "L.L";
            }
        }

        private void cmbSupplier_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                return;
            }
            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                return; // return with listbox's Visible set to false if nothing found
            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbSupplier.SelectedItem = lbResult.SelectedItem;
            }
            lbResult.Visible = false;
        }

        private void SaveDataInfo()
        {
         
            if (cmbSupplier.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a supplier");
                if(iamAdding)
                {
                    btnCancel.Visible = true;
                }
                return;
            }
            bool RecExists;
            try
            {
                clsSupplierTicketInvoice invoice = new clsSupplierTicketInvoice();
                if (cmbSupplier.SelectedIndex != -1)
                {
                    invoice.SupplierId = int.Parse(cmbSupplier.SelectedValue.ToString());
                  
                }
                else
                {
                    invoice.SupplierId = 0;
                 
                }
                if (cmbInvoceNo.SelectedIndex == -1)
                {
                    invoice.InvoiceId = 0;
                }
                else
                {
                    invoice.InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                }


                invoice.TotalPrice = totalPrice;
               
               
                invoice.Date = dtpInvoiceDate.Value;
                RecExists = invoiceConnection.InvoiceIDExists(invoice.InvoiceId);
             
                if (RecExists == true)
                {
                    invoiceConnection.UpdateInvoiceInfo(invoice);
                    invoiceConnection.UpdateInvoiceDetails(invoice.InvoiceId, ticketList);
                    stsLblInfo.Text = "Changes has been saved successfully";
                }
                else
                {
                    iamAdding = true;
                    invoiceConnection.AddNewSupplierInvoice(invoice);
                    int invoicenum = invoiceConnection.ReadLastNo();
                    invoiceConnection.AddInvoiceTickets(invoicenum, ticketList);
                    invoice.InvoiceId = invoicenum;
                    stsLblInfo.Text = "New Invoice has been added successfully";
                }
                InvoiceSavedID = invoice.InvoiceId;
                InvoiceId = InvoiceSavedID;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(invoice.SupplierId);
                DisplayData(SupplierID, InvoiceId);
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChanged)
            {
                btnCancel.Visible = false;

                if (canUpdateOnly && this.InvoiceId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void dtpInvoiceDate_ValueChanged(object sender, EventArgs e)
        {
            if (!iamAdding)
                DataChanged = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (var ticket in cLbSupplierTickets.CheckedItems)
            {
                clsTickets ticketdata = (clsTickets)ticket;
                if (ticketListIDS.Contains(ticketdata.TicketId))
                {
                    MessageBox.Show("You choose a selected Item");
                    break;
                }
                else
                {
                    dgvTickets.DataSource = null;
                    ticketList.Add(ticketdata);
                    ticketListIDS.Add(ticketdata.TicketId);
                    dgvTickets.DataSource = ticketList;
                    dgvTickets.Refresh();
                  
                    totalPrice = 0;

                    for (int m = 0; m < dgvTickets.Rows.Count; m++)
                    {
                        int currencyId = ticketList.ElementAt(m).CurrencyId;
                        decimal Rate = 1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if (currency != null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }


                        totalPrice += (decimal)dgvTickets.Rows[m].Cells[2].Value * Rate;


                    }
                    decimal rate;
                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        rate = currency.CurrencyRate;
                      
                    }
                    else
                    {
                        rate = 1;
                      
                    }

                    txtTotalPrice.Text = (totalPrice / rate).ToString(currencyFormat);
                    DataChanged = true;
                  
                }
            }
            btnAdd.Visible = false;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvTickets.RowCount > 0&&ticketList.Count>0&&allowRemove)
            {
                allowRemove = false;
                if (dgvTickets.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    int index = dgvTickets.SelectedRows[0].Index;
                    ticketList.RemoveAt(index);
                    ticketListIDS.RemoveAt(index);
                    dgvTickets.DataSource = null;
                    dgvTickets.Rows.Clear();
                    dgvTickets.DataSource = ticketList;
                    totalPrice = 0;

                    for (int m = 0; m < dgvTickets.Rows.Count; m++)
                    {
                        int currencyId = ticketList.ElementAt(m).CurrencyId;
                        decimal Rate = 1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if (currency != null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }


                        totalPrice += (decimal)dgvTickets.Rows[m].Cells[2].Value * Rate;


                    }
                    decimal rate;
                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        rate = currency.CurrencyRate;

                    }
                    else
                    {
                        rate = 1;

                    }

                    txtTotalPrice.Text = (totalPrice / rate).ToString(currencyFormat);

                    DataChanged = true;
                    dgvTickets.ClearSelection();
                }
            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            if (cmbSupplier.SelectedIndex != -1)
            {
                SupplierTicketList = invoiceConnection.SupplierTicketsArrayList(SupplierID);
                cLbSupplierTickets.DataSource = null;
                cLbSupplierTickets.ValueMember = "ticketId";
                cLbSupplierTickets.DataSource = SupplierTicketList;
                cLbSupplierTickets.DisplayMember = "SupplierToString";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblInvoiceNo.Visible = true;
            cmbInvoceNo.Visible = true;
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFields();
            SupplierID = SuppliersaveID;
            InvoiceId = InvoiceSavedID; 
            DisplayData(SupplierID, InvoiceId);
            DataChanged = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            iamAdding = true;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            stsLblInfo.Text = "Add Invoice";
            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }
            InvoiceSavedID = InvoiceId;
            SuppliersaveID = SupplierID; 
            InvoiceId = 0;
            cmbSupplier.SelectedIndex = -1;
            ResetFields();
            btnCancel.Visible = true;
            DataChanged = false;
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.InvoiceId != 0)
            {
                try
                {
                   SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {
                        bool myResult1 = invoiceConnection.DeleteInvoiceTickets(InvoiceId);
                        if (!myResult1)
                        {
                            return;
                        }
                        else
                        {
                            bool myResult2 = invoiceConnection.DeleteInVoice(InvoiceId);
                            if (myResult2)
                            {
                                ResetFields();
                                int index = supplierInvoiceList.IndexOf(invoiceData);
                                supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
                                index -= 1;
                                InvoiceId = 0;
                                if (index >= 0)
                                {
                                    InvoiceId = supplierInvoiceList[index].InvoiceId;
                                }
                                else if (index == -1 && supplierInvoiceList.Count > 0)
                                {
                                    index = 0;
                                    InvoiceId = supplierInvoiceList[index].InvoiceId;
                                }

                                DisplayData(SupplierID,InvoiceId);
                                DataChanged = false;
                                stsLblInfo.Text = "Successfully Deleted";
                                simpleSound.Play();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DataChanged = false;
                    stsLblInfo.Text = "Error: Deleting the Invoice";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbSupplier.Items.Count > 0))
            {
                if (cmbSupplier.SelectedIndex == 0 || cmbSupplier.SelectedIndex == -1)
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.Items.Count - 1;
                }
                else
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if ((cmbSupplier.Items.Count > 0))
            {
                if (cmbSupplier.SelectedIndex == cmbSupplier.Items.Count - 1 || cmbSupplier.SelectedIndex == -1)
                {
                    cmbSupplier.SelectedIndex = 0;
                }
                else
                {
                    cmbSupplier.SelectedIndex = cmbSupplier.SelectedIndex + 1;
                }
            }
        }

        private void dgvItems_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTickets.RowCount > 0)
            {
                if (e.RowIndex >= 0)
                {
                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void frmSupplierInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbInvoceNo.SelectedIndex != -1)
            {
                InvoiceId = Convert.ToInt32(cmbInvoceNo.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }


        private void TStripPrint_Click(object sender, EventArgs e)
        {

            if (cmbInvoceNo.SelectedIndex != -1 && supplierInvoiceList.Count > 0 && ticketList.Count > 0)
            {

                clsCurrency currency;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    currency = (clsCurrency)cmbCurrency.SelectedItem;

                }
                else
                {
                    currency = new clsCurrency();
                    currency.CurrencyRate = 1;
                    currency.CurrencyId = 0;
                    currency.CurrencySymbol = "L.L";
                }
                if (DataChanged == true)
                {

                    DialogResult myReply = MessageBox.Show("Do you want to save changes before printing ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        SaveDataInfo();
                        cmbCurrency.SelectedValue = currency.CurrencyId;

                    }
                }
                clsSupplierTicketInvoice invoice = (clsSupplierTicketInvoice)cmbInvoceNo.SelectedItem;

                invoice.SupplierName = supplierData.SupplierName;
                printSupplierTicketInvoice frm = new printSupplierTicketInvoice(ticketList, invoice, double.Parse(txtTotalPrice.Text),  currency);
              frm.Show();
             
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }


        }

        private void cmbInvoceNo_Click(object sender, EventArgs e)
        {
            supplierInvoiceList = invoiceConnection.SupplierInvoicesArrayList(SupplierID);
            int id = 0;
            if (cmbInvoceNo.SelectedIndex != -1)
            {
                id = int.Parse(cmbInvoceNo.SelectedValue.ToString());
            }
            if (supplierInvoiceList.Count > 0)
            {
                cmbInvoceNo.ValueMember = "invoiceId";
                cmbInvoceNo.DataSource = supplierInvoiceList;
                cmbInvoceNo.DisplayMember = "invoiceId";
                cmbInvoceNo.SelectedValue = id;
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if(cmbCurrency.SelectedIndex!=-1)
            {
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            }
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
            DataChanged = false;
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Printing)
            {
                TStripPrint.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            //cmbCurrency.Enabled = false;
            //cmbInvoceNo.Enabled = false;
            //cmbSupplier.Enabled = false;
            //cLbSupplierTickets.Enabled = false;
            dtpInvoiceDate.Enabled = false;
            btnAdd.Enabled = false;
            btnRemove.Enabled = false;
            //btnLRefresh.Enabled = false;


        }

        private void unReadOnlyEveryThing()
        {
            cmbCurrency.Enabled = true;
            cmbInvoceNo.Enabled = true;
            cmbSupplier.Enabled = true;
            cLbSupplierTickets.Enabled = true;
            dtpInvoiceDate.Enabled = true;
            btnAdd.Enabled = true;
            btnRemove.Enabled = true;
            btnLRefresh.Enabled = true;
        }

        private void dgvTickets_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cLbSupplierTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cLbSupplierTickets.SelectedIndex!=-1)
            {
                btnAdd.Visible = true;
            }
        }


        private void frmSupplierTicketInvoice_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }
    }
}
