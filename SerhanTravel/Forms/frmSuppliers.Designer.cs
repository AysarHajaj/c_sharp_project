﻿namespace SerhanTravel.Forms
{
    partial class frmSuppliers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSuppliers));
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.Label2 = new System.Windows.Forms.Label();
            this.CboSupStatusNo = new System.Windows.Forms.ComboBox();
            this.TxtSupAddress = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.TxtContactPerson = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TxtSupWebSite = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.TxtSupEmail = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.TxtSupFaxNo = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.TxtSupLandPhoneNo = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.TxtSupMobileNo = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.CboSupGroupNo = new System.Windows.Forms.ComboBox();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSpace02 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.Label6 = new System.Windows.Forms.Label();
            this.CboSupCountryNo = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TxtSupID = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.CboSupCityNo = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbSuppName = new System.Windows.Forms.ComboBox();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.stsInfo.SuspendLayout();
            this.TStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 419);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.stsInfo.Size = new System.Drawing.Size(791, 22);
            this.stsInfo.TabIndex = 178;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(79, 17);
            this.stsLblInfo.Text = "Suppliers Info";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(46, 118);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(42, 15);
            this.Label2.TabIndex = 176;
            this.Label2.Text = "Status";
            // 
            // CboSupStatusNo
            // 
            this.CboSupStatusNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupStatusNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupStatusNo.FormattingEnabled = true;
            this.CboSupStatusNo.Items.AddRange(new object[] {
            "Active",
            "Terminated",
            "Closed"});
            this.CboSupStatusNo.Location = new System.Drawing.Point(115, 114);
            this.CboSupStatusNo.Name = "CboSupStatusNo";
            this.CboSupStatusNo.Size = new System.Drawing.Size(226, 23);
            this.CboSupStatusNo.TabIndex = 177;
            this.CboSupStatusNo.SelectedIndexChanged += new System.EventHandler(this.CboSupStatusNo_SelectedIndexChanged);
            // 
            // TxtSupAddress
            // 
            this.TxtSupAddress.AccessibleDescription = "txtSerNo";
            this.TxtSupAddress.AccessibleName = "txtSerNo";
            this.TxtSupAddress.Location = new System.Drawing.Point(102, 17);
            this.TxtSupAddress.Multiline = true;
            this.TxtSupAddress.Name = "TxtSupAddress";
            this.TxtSupAddress.Size = new System.Drawing.Size(226, 66);
            this.TxtSupAddress.TabIndex = 162;
            this.TxtSupAddress.TextChanged += new System.EventHandler(this.TxtSupAddress_TextChanged);
            // 
            // Label9
            // 
            this.Label9.AccessibleDescription = "lblSerNo";
            this.Label9.AccessibleName = "lblSerNo";
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(49, 20);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(52, 15);
            this.Label9.TabIndex = 161;
            this.Label9.Text = "Address";
            // 
            // TxtContactPerson
            // 
            this.TxtContactPerson.AccessibleDescription = "txtSerNo";
            this.TxtContactPerson.AccessibleName = "txtSerNo";
            this.TxtContactPerson.Location = new System.Drawing.Point(102, 92);
            this.TxtContactPerson.Name = "TxtContactPerson";
            this.TxtContactPerson.Size = new System.Drawing.Size(226, 22);
            this.TxtContactPerson.TabIndex = 160;
            this.TxtContactPerson.TextChanged += new System.EventHandler(this.TxtContactPerson_TextChanged);
            // 
            // Label4
            // 
            this.Label4.AccessibleDescription = "lblSerNo";
            this.Label4.AccessibleName = "lblSerNo";
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(5, 96);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(91, 15);
            this.Label4.TabIndex = 159;
            this.Label4.Text = "Contact Person";
            // 
            // TxtSupWebSite
            // 
            this.TxtSupWebSite.AccessibleDescription = "txtSerNo";
            this.TxtSupWebSite.AccessibleName = "txtSerNo";
            this.TxtSupWebSite.Location = new System.Drawing.Point(111, 124);
            this.TxtSupWebSite.Name = "TxtSupWebSite";
            this.TxtSupWebSite.Size = new System.Drawing.Size(226, 22);
            this.TxtSupWebSite.TabIndex = 174;
            this.TxtSupWebSite.TextChanged += new System.EventHandler(this.TxtSupWebSite_TextChanged);
            // 
            // Label12
            // 
            this.Label12.AccessibleDescription = "lblSerNo";
            this.Label12.AccessibleName = "lblSerNo";
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(45, 124);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(56, 15);
            this.Label12.TabIndex = 173;
            this.Label12.Text = "Web Site";
            // 
            // TxtSupEmail
            // 
            this.TxtSupEmail.AccessibleDescription = "txtSerNo";
            this.TxtSupEmail.AccessibleName = "txtSerNo";
            this.TxtSupEmail.Location = new System.Drawing.Point(111, 96);
            this.TxtSupEmail.Name = "TxtSupEmail";
            this.TxtSupEmail.Size = new System.Drawing.Size(226, 22);
            this.TxtSupEmail.TabIndex = 172;
            this.TxtSupEmail.TextChanged += new System.EventHandler(this.TxtSupEmail_TextChanged);
            // 
            // Label11
            // 
            this.Label11.AccessibleDescription = "lblSerNo";
            this.Label11.AccessibleName = "lblSerNo";
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(16, 99);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(85, 15);
            this.Label11.TabIndex = 171;
            this.Label11.Text = "Email Address";
            // 
            // TxtSupFaxNo
            // 
            this.TxtSupFaxNo.AccessibleDescription = "txtSerNo";
            this.TxtSupFaxNo.AccessibleName = "txtSerNo";
            this.TxtSupFaxNo.Location = new System.Drawing.Point(111, 65);
            this.TxtSupFaxNo.Name = "TxtSupFaxNo";
            this.TxtSupFaxNo.Size = new System.Drawing.Size(226, 22);
            this.TxtSupFaxNo.TabIndex = 170;
            this.TxtSupFaxNo.TextChanged += new System.EventHandler(this.TxtSupFaxNo_TextChanged);
            this.TxtSupFaxNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSupFaxNo_KeyPress);
            // 
            // Label10
            // 
            this.Label10.AccessibleDescription = "lblSerNo";
            this.Label10.AccessibleName = "lblSerNo";
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(59, 71);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(47, 15);
            this.Label10.TabIndex = 169;
            this.Label10.Text = "Fax No.";
            // 
            // TxtSupLandPhoneNo
            // 
            this.TxtSupLandPhoneNo.AccessibleDescription = "txtSerNo";
            this.TxtSupLandPhoneNo.AccessibleName = "txtSerNo";
            this.TxtSupLandPhoneNo.Location = new System.Drawing.Point(111, 35);
            this.TxtSupLandPhoneNo.Name = "TxtSupLandPhoneNo";
            this.TxtSupLandPhoneNo.Size = new System.Drawing.Size(226, 22);
            this.TxtSupLandPhoneNo.TabIndex = 168;
            this.TxtSupLandPhoneNo.TextChanged += new System.EventHandler(this.TxtSupLandPhoneNo_TextChanged);
            this.TxtSupLandPhoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSupLandPhoneNo_KeyPress);
            // 
            // Label8
            // 
            this.Label8.AccessibleDescription = "lblSerNo";
            this.Label8.AccessibleName = "lblSerNo";
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(8, 39);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(99, 15);
            this.Label8.TabIndex = 167;
            this.Label8.Text = "Land Line Phone";
            // 
            // TxtSupMobileNo
            // 
            this.TxtSupMobileNo.AccessibleDescription = "txtSerNo";
            this.TxtSupMobileNo.AccessibleName = "txtSerNo";
            this.TxtSupMobileNo.Location = new System.Drawing.Point(111, 3);
            this.TxtSupMobileNo.Name = "TxtSupMobileNo";
            this.TxtSupMobileNo.Size = new System.Drawing.Size(226, 22);
            this.TxtSupMobileNo.TabIndex = 166;
            this.TxtSupMobileNo.TextChanged += new System.EventHandler(this.TxtSupMobileNo_TextChanged);
            this.TxtSupMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSupMobileNo_KeyPress);
            // 
            // Label7
            // 
            this.Label7.AccessibleDescription = "lblSerNo";
            this.Label7.AccessibleName = "lblSerNo";
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(27, 7);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(81, 15);
            this.Label7.TabIndex = 165;
            this.Label7.Text = "Mobile Phone";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(11, 87);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(75, 15);
            this.Label3.TabIndex = 163;
            this.Label3.Text = "Group Name";
            // 
            // CboSupGroupNo
            // 
            this.CboSupGroupNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupGroupNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupGroupNo.FormattingEnabled = true;
            this.CboSupGroupNo.Location = new System.Drawing.Point(115, 83);
            this.CboSupGroupNo.Name = "CboSupGroupNo";
            this.CboSupGroupNo.Size = new System.Drawing.Size(226, 23);
            this.CboSupGroupNo.TabIndex = 164;
            this.CboSupGroupNo.SelectedIndexChanged += new System.EventHandler(this.CboSupGroupNo_SelectedIndexChanged);
            this.CboSupGroupNo.Click += new System.EventHandler(this.CboSupGroupNo_Click);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripSpace02,
            this.TStripBackword,
            this.TStripForword});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(791, 67);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 175;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Supplier";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 67);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 67);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Supplier";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 67);
            // 
            // TStripSpace02
            // 
            this.TStripSpace02.AccessibleDescription = "TStripSpace02";
            this.TStripSpace02.AccessibleName = "TStripSpace02";
            this.TStripSpace02.AutoSize = false;
            this.TStripSpace02.Name = "TStripSpace02";
            this.TStripSpace02.Size = new System.Drawing.Size(10, 25);
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(61, 56);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(30, 15);
            this.Label6.TabIndex = 157;
            this.Label6.Text = "City";
            // 
            // CboSupCountryNo
            // 
            this.CboSupCountryNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupCountryNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupCountryNo.FormattingEnabled = true;
            this.CboSupCountryNo.Items.AddRange(new object[] {
            "Lebanon",
            "Syria",
            "Egypt",
            "Turkia"});
            this.CboSupCountryNo.Location = new System.Drawing.Point(115, 21);
            this.CboSupCountryNo.Name = "CboSupCountryNo";
            this.CboSupCountryNo.Size = new System.Drawing.Size(226, 23);
            this.CboSupCountryNo.TabIndex = 156;
            this.CboSupCountryNo.SelectedIndexChanged += new System.EventHandler(this.CboSupCountryNo_SelectedIndexChanged);
            this.CboSupCountryNo.Click += new System.EventHandler(this.CboSupCountryNo_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(39, 24);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 15);
            this.Label1.TabIndex = 155;
            this.Label1.Text = "Country";
            // 
            // TxtSupID
            // 
            this.TxtSupID.AccessibleDescription = "txtSerNo";
            this.TxtSupID.AccessibleName = "txtSerNo";
            this.TxtSupID.Location = new System.Drawing.Point(115, 92);
            this.TxtSupID.Name = "TxtSupID";
            this.TxtSupID.ReadOnly = true;
            this.TxtSupID.Size = new System.Drawing.Size(226, 22);
            this.TxtSupID.TabIndex = 154;
            this.TxtSupID.TextChanged += new System.EventHandler(this.TxtSupName_TextChanged);
            // 
            // lblName
            // 
            this.lblName.AccessibleDescription = "lblName";
            this.lblName.AccessibleName = "lblName";
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(68, 95);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(20, 15);
            this.lblName.TabIndex = 153;
            this.lblName.Text = "ID";
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Location = new System.Drawing.Point(1, 20);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(88, 15);
            this.lblSerNo.TabIndex = 151;
            this.lblSerNo.Text = "Supplier Name";
            // 
            // CboSupCityNo
            // 
            this.CboSupCityNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CboSupCityNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CboSupCityNo.FormattingEnabled = true;
            this.CboSupCityNo.Items.AddRange(new object[] {
            "Beirut",
            "Saida",
            "Sarafand",
            "Jounieh"});
            this.CboSupCityNo.Location = new System.Drawing.Point(115, 52);
            this.CboSupCityNo.Name = "CboSupCityNo";
            this.CboSupCityNo.Size = new System.Drawing.Size(226, 23);
            this.CboSupCityNo.TabIndex = 158;
            this.CboSupCityNo.SelectedIndexChanged += new System.EventHandler(this.CboSupCityNo_SelectedIndexChanged);
            this.CboSupCityNo.Click += new System.EventHandler(this.CboSupCityNo_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.MediumAquamarine;
            this.btnCancel.FlatAppearance.BorderSize = 20;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.btnCancel.Location = new System.Drawing.Point(332, 374);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 37);
            this.btnCancel.TabIndex = 180;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbSuppName
            // 
            this.cmbSuppName.FormattingEnabled = true;
            this.cmbSuppName.Location = new System.Drawing.Point(115, 17);
            this.cmbSuppName.Name = "cmbSuppName";
            this.cmbSuppName.Size = new System.Drawing.Size(226, 23);
            this.cmbSuppName.TabIndex = 184;
            this.cmbSuppName.SelectedIndexChanged += new System.EventHandler(this.cmbSuppName_SelectedIndexChanged_1);
            this.cmbSuppName.TextChanged += new System.EventHandler(this.cmbSuppName_TextChanged);
            this.cmbSuppName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSuppName_KeyDown);
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(115, 39);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(226, 34);
            this.lbResult.TabIndex = 185;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel1.Controls.Add(this.lbResult);
            this.panel1.Controls.Add(this.lblSerNo);
            this.panel1.Controls.Add(this.cmbSuppName);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.TxtSupID);
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(12, 81);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(346, 121);
            this.panel1.TabIndex = 186;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel2.Controls.Add(this.CboSupCountryNo);
            this.panel2.Controls.Add(this.CboSupCityNo);
            this.panel2.Controls.Add(this.Label1);
            this.panel2.Controls.Add(this.Label6);
            this.panel2.Controls.Add(this.Label2);
            this.panel2.Controls.Add(this.CboSupGroupNo);
            this.panel2.Controls.Add(this.CboSupStatusNo);
            this.panel2.Controls.Add(this.Label3);
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(12, 208);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(348, 148);
            this.panel2.TabIndex = 187;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel3.Controls.Add(this.TxtSupAddress);
            this.panel3.Controls.Add(this.Label4);
            this.panel3.Controls.Add(this.TxtContactPerson);
            this.panel3.Controls.Add(this.Label9);
            this.panel3.Location = new System.Drawing.Point(395, 81);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(340, 121);
            this.panel3.TabIndex = 188;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel4.Controls.Add(this.TxtSupLandPhoneNo);
            this.panel4.Controls.Add(this.Label7);
            this.panel4.Controls.Add(this.TxtSupMobileNo);
            this.panel4.Controls.Add(this.Label8);
            this.panel4.Controls.Add(this.Label10);
            this.panel4.Controls.Add(this.TxtSupFaxNo);
            this.panel4.Controls.Add(this.TxtSupWebSite);
            this.panel4.Controls.Add(this.Label11);
            this.panel4.Controls.Add(this.Label12);
            this.panel4.Controls.Add(this.TxtSupEmail);
            this.panel4.ForeColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(395, 208);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(340, 148);
            this.panel4.TabIndex = 189;
            // 
            // frmSuppliers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(791, 441);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.TStrip);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmSuppliers";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Suppliers";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSuppliers_FormClosed);
            this.Load += new System.EventHandler(this.frmSuppliers_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmSuppliers_MouseClick);
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.ComboBox CboSupStatusNo;
        internal System.Windows.Forms.TextBox TxtSupAddress;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox TxtContactPerson;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox TxtSupWebSite;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox TxtSupEmail;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox TxtSupFaxNo;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox TxtSupLandPhoneNo;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox TxtSupMobileNo;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.ComboBox CboSupGroupNo;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace02;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripButton TStripForword;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.ComboBox CboSupCountryNo;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox TxtSupID;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.ComboBox CboSupCityNo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbSuppName;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
    }
}