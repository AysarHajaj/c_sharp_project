﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmSuppliers : Form
    {
        List<clsSupplier> supplierList;
        Supplier SupplierConnection;
        clsSupplier supplierData;
        bool DataChanged = false;
        bool iamUpdate = false;
        bool skip = true;
        bool iamAdding = false;
        int SaveID;
        int supplierID = 0;
        clsAccessRights right;
        bool canUpdateOnly;
        bool canAddOnly = false;
        Country countryConnection;
        Groups groupsConnection;
        City cityConnection;
        internal frmSuppliers(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmSuppliers_Load(object sender, EventArgs e)
        {
            accessRight();
            cityConnection = new City();
            countryConnection = new Country();
            SupplierConnection = new Supplier();
            supplierList = SupplierConnection.SupplierArrayList();
            groupsConnection = new Groups();
            FillCombobox();
            cmbSuppName.ValueMember = "SupplierId";
            cmbSuppName.DataSource = supplierList;
            cmbSuppName.DisplayMember = "SupplierName";
            cmbSuppName.SelectedIndex = -1;
            skip = true;
            ResetFields();
            this.supplierID = 0;
            DataChanged = false;
        }
     
        private void ResetFields()
        {

            cmbSuppName.Text = "";
            CboSupCountryNo.Text = "";
            CboSupCityNo.Text = "";
            CboSupGroupNo.Text = "";
            CboSupStatusNo.Text = "";
            TxtSupID.Clear();
            stsLblInfo.Text = "";
            cmbSuppName.SelectedIndex = -1;
            CboSupCountryNo.SelectedIndex = -1;
            CboSupCityNo.SelectedIndex = -1;
            CboSupGroupNo.SelectedIndex = -1;
            CboSupStatusNo.SelectedIndex = -1;
           
            TxtContactPerson.Clear();
            TxtSupAddress.Clear();
            TxtSupMobileNo.Clear();
            TxtSupLandPhoneNo.Clear();
            TxtSupFaxNo.Clear();
            TxtSupEmail.Clear();
            TxtSupWebSite.Clear();
            DataChanged = false;
        }
        private void DisplayData(int supId)
        {
            cmbSuppName.DataSource = null;
            cmbSuppName.ValueMember = "SupplierId";
            cmbSuppName.DataSource = supplierList;
            cmbSuppName.DisplayMember = "SupplierName";
            skip = true;
            cmbSuppName.SelectedValue = supId;
            supplierID = supId;
            iamAdding = false;
            iamUpdate = false;
            DataChanged = false;

        }

        private void FillCombobox()
        {
         
           
            

           ArrayList groupList = groupsConnection.GroupArrayList();
            CboSupGroupNo.ValueMember = "GroupId";
            CboSupGroupNo.DataSource = groupList;
            CboSupGroupNo.DisplayMember = "GroupName";
        
        CboSupGroupNo.SelectedIndex = -1;

          

            List<clsCountry> Countries = countryConnection.CountryArrayList();
            CboSupCountryNo.ValueMember = "countryId";
            CboSupCountryNo.DataSource = Countries;
            CboSupCountryNo.DisplayMember = "EnglishName";
           
            CboSupCountryNo.SelectedIndex = -1;


            List<clsStatus> customerStatus = new List<clsStatus>();
            customerStatus.Add(new clsStatus(0, "Available"));
            customerStatus.Add(new clsStatus(1, "un Available"));

            CboSupStatusNo.ValueMember = "StatusId";
            CboSupStatusNo.DataSource = customerStatus;
            CboSupStatusNo.DisplayMember = "StatusName";
          
            CboSupStatusNo.SelectedIndex = -1;



            DataChanged = false;

        }


        private void CboSupStatusNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupWebSite_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupEmail_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupFaxNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupLandPhoneNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupMobileNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void CboSupGroupNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupName_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void CboSupCountryNo_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(CboSupCountryNo.SelectedIndex!=-1)
            {

                List<clsCity> Cities = cityConnection.GetCitiesByCountryId(int.Parse(CboSupCountryNo.SelectedValue.ToString()));
                CboSupCityNo.ValueMember = "CityId";
                CboSupCityNo.DataSource = Cities;
                CboSupCityNo.DisplayMember = "EnglishName";
                if(supplierData!=null)
                    CboSupCityNo.SelectedValue =supplierData.CityId;
            }
            DataChanged = true;
        }

        private void CboSupCityNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtContactPerson_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TxtSupAddress_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }


        private void cmbSuppName_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode != Keys.Enter || e.KeyCode != Keys.Tab)
            {

                DataChanged = true;
            }
        }
        private void cmbSuppName_TextChanged(object sender, EventArgs e)
        {
            if (iamAdding)
            {
                DataChanged = true;
                return;
            }

            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSuppName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                DataChanged = true;
                return;
            }

            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                DataChanged = true;
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {


                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "SupplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "SupplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }

        }
        private void cmbSuppName_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (cmbSuppName.SelectedIndex != -1&&supplierList.Count>0)
            {
                lbResult.Visible = false;
                supplierID = int.Parse(cmbSuppName.SelectedValue.ToString());
                supplierData = supplierList[cmbSuppName.SelectedIndex];
                TxtSupID.Text = supplierData.SupplierId.ToString();
                CboSupCountryNo.SelectedValue = supplierData.CountryId;
                if (CboSupCountryNo.SelectedIndex != -1)
                {
               
                    List<clsCity> Cities = cityConnection.GetCitiesByCountryId(int.Parse(CboSupCountryNo.SelectedValue.ToString()));
                    CboSupCityNo.ValueMember = "CityId";
                    CboSupCityNo.DataSource = Cities;
                    CboSupCityNo.DisplayMember = "EnglishName";

                    
                }
                CboSupCityNo.SelectedValue = supplierData.CityId;
               
                CboSupGroupNo.SelectedValue = supplierData.GroupId;
                CboSupStatusNo.SelectedValue = supplierData.Status;
                TxtSupAddress.Text = supplierData.Address;
                TxtContactPerson.Text = supplierData.ContactPerson;
                TxtSupMobileNo.Text = supplierData.MobilePhone.ToString();
                TxtSupLandPhoneNo.Text = supplierData.LandPhone;
                TxtSupFaxNo.Text = supplierData.FaxNo.ToString();
                TxtSupEmail.Text = supplierData.Email;
                TxtSupWebSite.Text = supplierData.WebSite;
                DataChanged = false;
                skip = true;
            }
            else
            {
                skip = false;
            }
        }


        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbSuppName.SelectedItem = lbResult.SelectedItem;


            }


            lbResult.Visible = false;
        }
        private void SaveDataInfo()
        {



            bool RecExists;
            try
            {
                //int selectedvalue;
                clsSupplier supplier = new clsSupplier();
                supplier.SupplierId = supplierID;

                if (string.IsNullOrWhiteSpace(cmbSuppName.Text))
                {
                    supplier.SupplierName = " ";
                }
                else
                {
                    supplier.SupplierName = cmbSuppName.Text;
                }

                if (CboSupGroupNo.SelectedIndex == -1)
                {
                    supplier.GroupId = 0;
                }
                else
                {
                    supplier.GroupId = int.Parse(CboSupGroupNo.SelectedValue.ToString());
                }

                if (CboSupCityNo.SelectedIndex == -1)
                {
                    supplier.CityId = 0;
                }
                else
                {
                    supplier.CityId = int.Parse(CboSupCityNo.SelectedValue.ToString());
                }

                if (CboSupCountryNo.SelectedIndex == -1)
                {
                    supplier.CountryId = 0;
                }
                else
                {
                    supplier.CountryId = int.Parse(CboSupCountryNo.SelectedValue.ToString());
                }

                if (CboSupStatusNo.SelectedIndex == -1)
                {
                    supplier.Status = 0;
                }
                else
                {
                    supplier.Status = int.Parse(CboSupStatusNo.SelectedValue.ToString());
                }

                if (string.IsNullOrWhiteSpace(TxtSupAddress.Text))
                {
                    supplier.Address = " ";
                }
                else
                {
                    supplier.Address = TxtSupAddress.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtContactPerson.Text))
                {
                    supplier.ContactPerson = " ";
                }
                else
                {
                    supplier.ContactPerson = TxtContactPerson.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtSupMobileNo.Text))
                {
                    supplier.MobilePhone = " ";
                }
                else
                {
                    supplier.MobilePhone = TxtSupMobileNo.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtSupLandPhoneNo.Text))
                {
                    supplier.LandPhone = string.Empty;
                }
                else
                {
                    supplier.LandPhone = TxtSupLandPhoneNo.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtSupFaxNo.Text))
                {
                    supplier.FaxNo = string.Empty;
                }
                else
                {
                    supplier.FaxNo = TxtSupFaxNo.Text.ToString();
                }
                if (string.IsNullOrWhiteSpace(TxtSupEmail.Text))
                {
                    supplier.Email = string.Empty;
                }
                else
                {
                    supplier.Email = TxtSupEmail.Text;
                }
                if (string.IsNullOrWhiteSpace(TxtSupWebSite.Text))
                {
                    supplier.WebSite = string.Empty;
                }
                else
                {
                    supplier.WebSite = TxtSupWebSite.Text;
                }


           




                Supplier supplierConnection = new Supplier();

                RecExists = supplierConnection.SupplierIDExists(supplier.SupplierId);


                if (RecExists == true)
                {
                    supplierConnection.UpdateSupplierInfo(supplier);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    iamUpdate = true;

                }
                else
                {

                    iamAdding = true;
                    supplierConnection.AddNewSupplier(supplier);
                    stsLblInfo.Text = "New Supplier has been added successfully";

                }


                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();


                supplierList = supplierConnection.SupplierArrayList();

                if (iamUpdate)
                {
                    DisplayData(supplier.SupplierId);
                }
                if (iamAdding)
                {
                    iamAdding = false;
                    DisplayData(supplierList[supplierList.Count-1].SupplierId);
                }

                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add Supplier";

            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }

            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }

            iamAdding = true;
            SaveID = supplierID;

            supplierID = 0;
            ResetFields();
            cmbSuppName.DataSource = null;
            btnCancel.Visible = true;
            DataChanged = false;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChanged)
            {


                btnCancel.Visible = false;
                if (canUpdateOnly && this.supplierID == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ResetFields();
            btnCancel.Visible = false;
            iamAdding = false;
            supplierID = SaveID;
            DisplayData(supplierID);
            DataChanged = false;
           if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (supplierID != 0)
            {


                try
                {
                    bool myResult;
                    SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReply = MessageBox.Show("Are you sure ?", "Delete Supplier Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        if (SupplierConnection.SupplierUsed(supplierID))
                        {
                            MessageBox.Show("This Supplier Is Used In Some Operations Can't be Deleted");
                            return;
                        }

                        myResult = SupplierConnection.DeleteSupplier(supplierID);
                        if (myResult == false)
                        {
                            return;
                        }
                        notifySound.Play();
                        ResetFields();
                        // ClearFields();
                        supplierList = SupplierConnection.SupplierArrayList();
                        if (supplierList.Count > 0)
                            DisplayData(supplierList[supplierList.Count - 1].SupplierId);
                        else
                            supplierID = 0;
                        lbResult.Visible = false;
                        DataChanged = false;
                        stsLblInfo.Text = "Successfully Deleted";
                    }
                }
                catch (Exception ex)
                {

                    DataChanged = false;
                    stsLblInfo.Text = "ERROR: Problem Deleting the Supplier profile";
                    MessageBox.Show(ex.Message);
                }


            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbSuppName.Items.Count > 0))
            {
                if (cmbSuppName.SelectedIndex == 0 || cmbSuppName.SelectedIndex == -1)
                {
                    cmbSuppName.SelectedIndex = cmbSuppName.Items.Count - 1;
                }
                else
                {
                    cmbSuppName.SelectedIndex = cmbSuppName.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if ((cmbSuppName.Items.Count > 0))
            {
                if (cmbSuppName.SelectedIndex == cmbSuppName.Items.Count - 1 || cmbSuppName.SelectedIndex == -1)
                {
                    cmbSuppName.SelectedIndex = 0;
                }
                else
                {
                    cmbSuppName.SelectedIndex = cmbSuppName.SelectedIndex + 1;
                }
            }

        }

      

      

        private void TxtSupMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void TxtSupLandPhoneNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void TxtSupFaxNo_KeyPress(object sender, KeyPressEventArgs e)
        {

            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8)
            {
                e.Handled = true;
            }
        }

        private void frmSuppliers_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (cmbSuppName.SelectedIndex != -1)
            {
                supplierID = Convert.ToInt32(cmbSuppName.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }


        private void CboSupCountryNo_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (CboSupCountryNo.SelectedIndex != -1)
                id = int.Parse(CboSupCountryNo.SelectedValue.ToString());


            List<clsCountry> Countries = countryConnection.CountryArrayList();
         
            CboSupCountryNo.ValueMember = "countryId";
            CboSupCountryNo.DataSource = Countries;
            CboSupCountryNo.DisplayMember = "EnglishName";
            CboSupCountryNo.SelectedValue = id;
            DataChanged = false;

        }

        private void CboSupCityNo_Click(object sender, EventArgs e)
        {

            int countryid = 0;
            if(CboSupCountryNo.SelectedIndex!=-1)
            {
                countryid = int.Parse(CboSupCountryNo.SelectedValue.ToString());
            }
            int id = 0;
            if (CboSupCityNo.SelectedIndex != -1)
                id = int.Parse(CboSupCityNo.SelectedValue.ToString());
            List<clsCity> Cities = cityConnection.GetCitiesByCountryId(countryid);
            CboSupCityNo.ValueMember = "CityId";
            CboSupCityNo.DataSource = Cities;
            CboSupCityNo.DisplayMember = "EnglishName";
            CboSupCityNo.SelectedValue = id;
            DataChanged = false;
        }

        private void CboSupGroupNo_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (CboSupGroupNo.SelectedIndex != -1)
                id = int.Parse(CboSupGroupNo.SelectedValue.ToString());

            ArrayList groupList = groupsConnection.GroupArrayList();
            CboSupGroupNo.ValueMember = "GroupId";
            CboSupGroupNo.DataSource = groupList;
            CboSupGroupNo.DisplayMember = "GroupName";
            DataChanged = false;

        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }

            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            cmbSuppName.DropDownStyle = ComboBoxStyle.DropDownList;
            CboSupCountryNo.Enabled = false;
            CboSupGroupNo.Enabled = false;
            CboSupStatusNo.Enabled = false;
            CboSupCityNo.Enabled = false;

            TxtContactPerson.ReadOnly = true;
            TxtSupAddress.ReadOnly = true;
            TxtSupMobileNo.ReadOnly = true;
            TxtSupLandPhoneNo.ReadOnly = true;
            TxtSupFaxNo.ReadOnly = true;
            TxtSupEmail.ReadOnly = true;
            TxtSupWebSite.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbSuppName.DropDownStyle = ComboBoxStyle.DropDown;
            CboSupCountryNo.Enabled = true;
            CboSupGroupNo.Enabled = true;
            CboSupStatusNo.Enabled = true;
            CboSupCityNo.Enabled = true;

            TxtContactPerson.ReadOnly = false;
            TxtSupAddress.ReadOnly = false;
            TxtSupMobileNo.ReadOnly = false;
            TxtSupLandPhoneNo.ReadOnly = false;
            TxtSupFaxNo.ReadOnly = false;
            TxtSupEmail.ReadOnly = false;
            TxtSupWebSite.ReadOnly = false;
        }

        private void frmSuppliers_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }
    }
}
