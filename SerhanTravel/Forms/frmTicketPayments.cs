﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using SerhanTravel.Print;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmTicketPayments : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        Ticket ticketConnection;
        List<clsCustomer> customerList;
        List<clsTickets> customerTicketList;
        List<clsTicketsPayment> ticketPaymentLIst;
        TicketsPayment paymentConnection;
        Customer customerConnection;
        clsTickets ticketData;
        ReturnedCustomerTicket returnConnection;
        bool skip = false;
        bool DataChanged = false;
        int TicketId = 0;
        int CustomerID = 0;
        private List<clsCurrency> currencies;
        decimal TicketPrice = 0;
        decimal paidAmount = 0;
        decimal remainingAmount = 0;
        int PaymentID = 0;
        int savedInvoiceId = 0;
        private bool allowRemove = false;
        string currencyFormat;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection;
        Bank bankConnection;
        internal frmTicketPayments(clsAccessRights right)
        {
            InitializeComponent();

            dgvPayments.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvPayments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPayments.MultiSelect = false;
            dgvPayments.ScrollBars = ScrollBars.Both;
            dgvPayments.AutoGenerateColumns = false;
            dgvPayments.Columns[0].DataPropertyName = "Date";
            dgvPayments.Columns[1].DataPropertyName = "Payment";
            dgvPayments.Columns[2].DataPropertyName = "CurrecnySymbol";
            dgvPayments.Columns[3].DataPropertyName = "ChequeNo";
            dgvPayments.Columns[4].DataPropertyName = "BankName";
            dgvPayments.Columns[5].DataPropertyName = "ChequeDate";
            dgvPayments.Columns[6].DataPropertyName = "CollectedDate";
            this.right = right;
        }

        private void ResetFields()
        {
          
            dgvPayments.DataSource = null;
            cmbInvoiceNo.Text = "";
            dgvPayments.Rows.Clear(); 
            txtPrice.Clear();
            txtRemainingAmount.Clear();
            txtCustomerName.Clear();
            txtPaidAmount.Clear();
            cmbInvoiceNo.SelectedIndex = -1;
            TicketPrice = 0;
            paidAmount = 0;
            stsLblInfo.Text = "";
            remainingAmount = 0;
            panelPayment.Visible = false;
            DataChanged = false;
            btnRemove.Visible = false;
        }

        private void frmTicketPayments_Load(object sender, EventArgs e)
        {
            bankConnection = new Bank();
            currencyConnection = new Currency();
            accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            lbResult.Visible = false;
            customerConnection = new Customer();
            returnConnection = new ReturnedCustomerTicket();
            paymentConnection = new TicketsPayment();
            customerList = customerConnection.CustomersArrayList();
            customerTicketList = new List<clsTickets>();
            ticketConnection = new Ticket();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);

            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            

            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";

            cbBank.ValueMember = "BankId";
            cbBank.DataSource = bankConnection.BankArrayList();
            cbBank.DisplayMember = "BankName";
            cbBank.SelectedIndex = -1;
            cmbInvoiceNo.DataSource = null;
            ResetFields();
            panelPayment.Visible = false;
            btnRemove.Visible = false;
            DataChanged = false;

        }

        private void DisplayData(int customerId)
        {
            panelPayment.Visible = false;
            cmbCustomerName.DataSource = null;
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedValue = customerId;
            skip = true;
            CustomerID = customerId;
            DataChanged = false;

        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbResult.Visible = false;
            btnRemove.Visible = false;
            panelPayment.Visible = false;
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            { 
                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                customerTicketList = ticketConnection.CustomerTickets(CustomerID);
                cmbInvoiceNo.DataSource = null;
                dgvPayments.DataSource = null;
                if (CustomerID!=0)
                {
                    cmbInvoiceNo.Visible = false;
                    lblTicket.Visible = false;
                    ResetFields();
                    txtCustomerName.Text = cmbCustomerName.Text;
                    TicketPrice = ticketConnection.TotalPrice(CustomerID);
                    foreach(clsTickets ticket in customerTicketList)
                    {
                        if (returnConnection.TicektIDExist(ticket.TicketId))
                        {

                            TicketPrice -=  returnConnection.getReturnAmount(ticket.TicketId);

                        }
                    }
                    remainingAmount = 0;
                    paidAmount = 0;
                    ticketPaymentLIst = new List<clsTicketsPayment>();
                    ticketPaymentLIst.Clear();
                    ticketPaymentLIst = paymentConnection.CustomerPaymment(CustomerID);
                    if (ticketPaymentLIst.Count > 0)
                    {

                        dgvPayments.DataSource = ticketPaymentLIst;

                        for (int i = 0; i < ticketPaymentLIst.Count; i++)
                        {

                            paidAmount += ticketPaymentLIst[i].Payment * ticketPaymentLIst[i].CurrencyRate;
                        }

                    }
                    remainingAmount = TicketPrice - paidAmount;
                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        txtPrice.Text = (TicketPrice / currency.CurrencyRate).ToString(currencyFormat);
                        txtPaidAmount.Text = (paidAmount / currency.CurrencyRate).ToString(currencyFormat);
                        txtRemainingAmount.Text = (remainingAmount / currency.CurrencyRate).ToString(currencyFormat);
                    }
                    dgvPayments.Refresh();
                    dgvPayments.ClearSelection();
                }
               else
                {
                    
                    txtCustomerName.Clear();
                    cmbInvoiceNo.Visible = true;
                    lblTicket.Visible = true;
                    if (customerTicketList.Count > 0)
                    {
                        cmbInvoiceNo.ValueMember = "TicketId";
                        cmbInvoiceNo.DataSource = customerTicketList;
                        cmbInvoiceNo.DisplayMember = "TicketNo";
                    }
                    ResetFields();
                }
            
                
                DataChanged = false;
                skip = true;
            }
            else
            {
                skip = false;
            }

            DataChanged = false;
        }


       

        private void cmbInvoiceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            btnRemove.Visible = false;
            panelPayment.Visible = false;
            if (cmbInvoiceNo.SelectedIndex != -1 && customerTicketList.Count > 0)
            {
                if(CustomerID!=0)
                {
                    return;
                }
                txtPrice.Clear();
                txtRemainingAmount.Clear();
                txtPaidAmount.Clear();
                TicketId = int.Parse(cmbInvoiceNo.SelectedValue.ToString());
                ticketData = customerTicketList[cmbInvoiceNo.SelectedIndex];
                if(this.CustomerID==0)
                {
                    txtCustomerName.Text = ticketData.TravellerName;
                }
                ticketPaymentLIst = new List<clsTicketsPayment>();
                ticketPaymentLIst.Clear();
                ticketPaymentLIst = paymentConnection.TicketsPayments(TicketId);
                dgvPayments.DataSource = null;
                dgvPayments.Rows.Clear();
                if (returnConnection.TicektIDExist(ticketData.TicketId))
                 {
                   
                    TicketPrice =ticketData.Price*ticketData.CurrencyRate - returnConnection.getReturnAmount(ticketData.TicketId);
                  
                 }
                 else
                 {
                     TicketPrice = Convert.ToDecimal(ticketData.Price * ticketData.CurrencyRate);

                 }

                remainingAmount = 0;
                paidAmount = 0;
         
                if (ticketPaymentLIst.Count > 0)
                {

                    dgvPayments.DataSource = ticketPaymentLIst;

                    for (int i = 0; i < ticketPaymentLIst.Count; i++)
                    {
                        
                        paidAmount += ticketPaymentLIst[i].Payment * ticketPaymentLIst[i].CurrencyRate;
                    }
                   
                }
                remainingAmount = TicketPrice - paidAmount;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    txtPrice.Text = (TicketPrice / currency.CurrencyRate).ToString(currencyFormat);
                    txtPaidAmount.Text = (paidAmount / currency.CurrencyRate).ToString(currencyFormat);
                    txtRemainingAmount.Text = (remainingAmount / currency.CurrencyRate).ToString(currencyFormat);
                }
                dgvPayments.Refresh();
                dgvPayments.ClearSelection();
            }
            DataChanged = false;

        }

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void SaveDataInfo()
        {

            savedInvoiceId = TicketId;
            if (cmbCustomerName.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a customer");
                return;
            }
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Currency to Continue");
                return;
            }

            bool RecExists;
            try
            {
                clsTicketsPayment payment = new clsTicketsPayment();
                payment.PaymentId = PaymentID;
                if (cmbInvoiceNo.SelectedIndex != -1)
                {
                    payment.TicketId = int.Parse(cmbInvoiceNo.SelectedValue.ToString());
                }
                else if (cmbInvoiceNo.SelectedIndex == -1 && this.CustomerID==0)
                {
                    MessageBox.Show("Please select a Ticket");
                    return;
                }

                 if (!string.IsNullOrWhiteSpace(txtPayment.Text))
                 {
                     payment.Payment = decimal.Parse(txtPayment.Text.ToString());
                 }
                 else
                 {
                     payment.Payment = 0;
                 }
     
                 if (!string.IsNullOrWhiteSpace(txtChequeNumber.Text))
                 {
                     payment.ChequeNo = txtChequeNumber.Text;
                     payment.PaymentType = 1;
                     if (cbBank.SelectedIndex == -1)
                     {
                         MessageBox.Show("Please choose a bank");
                         return;
                     }
                     else
                     {
                         payment.BankId = int.Parse(cbBank.SelectedValue.ToString());
                     }

                     if (!string.IsNullOrWhiteSpace(dtbChequeDate.Text))
                     {
                         if (IsDateTime(dtbChequeDate.Text))
                         {
                             payment.ChequeDate = dtbChequeDate.Text;
                         }
                         else
                         {
                             MessageBox.Show("Your cheque date is not in the correct format");
                             return;
                         }

                     }
                     else
                     {
                         payment.ChequeDate=null;
                     }

                     if (!string.IsNullOrWhiteSpace(dtbCollectedDate.Text))
                     {
                         if (IsDateTime(dtbCollectedDate.Text))
                         {
                             payment.CollectedDate = dtbCollectedDate.Text;
                         }
                         else
                         {
                             MessageBox.Show("Your colllected date is not in the correct format");
                             return;
                         }

                     }
                     else
                     {
                         payment.CollectedDate = null;
                     }

                 }
                 else
                 {
                     payment.ChequeDate=null;
                     payment.CollectedDate=null;
                     payment.PaymentType = 0;
                     payment.ChequeNo = string.Empty;

                 }
                 if(cmbCurrency.SelectedIndex==-1)
                {
                    payment.CurrencyId = 0;
                }
                 else
                {
                    payment.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }
                payment.CustomerId = this.CustomerID;
                payment.Date = dtpDate.Value;
                RecExists = paymentConnection.paymentIDExists(payment.PaymentId);

                if(!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    payment.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }
              
               

                if (RecExists == true)
                  {
                     paymentConnection.UpdatePaymentDetails(payment);
                     stsLblInfo.Text = "Changes has been saved successfully";
                  
                  }
                  else
                  {
                   
                     paymentConnection.AddPaymentDetails(payment);
                     stsLblInfo.Text = "New Payment has been added successfully";

                  }

                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();
                DisplayData(CustomerID);
                cmbInvoiceNo.SelectedValue = savedInvoiceId;
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }


        


        /*public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }*/

        /*private void TStripSave_Click(object sender, EventArgs e)
        {
           // btnCancel.Visible = false;
            SaveDataInfo();
        }*/

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (canAddOnly)
            {
                btnSave.Enabled = true;
                unReadOnlyEveryThing();
            }
            panelPayment.Visible = true;
            dtpDate.Value = DateTime.Today;
            dtbChequeDate.Text = "";
            dtbCollectedDate.Text = "";
            txtPayment.Clear();
            txtPaymentRate.Clear();
            txtChequeNumber.Clear();
            cbBank.SelectedIndex = -1;
            PaymentID = 0;
            DataChanged = false;
            btnRemove.Visible = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            panelPayment.Visible = false;
            btnRemove.Visible = false;
            if (canAddOnly)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            btnRemove.Visible = false;

            if (DataChanged)
            {
                
                SaveDataInfo();
                if (canAddOnly)
                {
                    btnSave.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            
                DataChanged = true;
        }

        private void txtChequeNumber_TextChanged(object sender, EventArgs e)
        {
            
                DataChanged = true;
        }

        private void cbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                DataChanged = true;
        }

        private void panelPayment_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtPayment_TextChanged(object sender, EventArgs e)
        {
         
                DataChanged = true;
        }

      
        private void cmbInvoiceNo_Click(object sender, EventArgs e)
        {
            customerTicketList = ticketConnection.CustomerTickets(CustomerID);
            int id = 0;
            if (cmbInvoiceNo.SelectedIndex != -1)
            {
                id = int.Parse(cmbInvoiceNo.SelectedValue.ToString());
            }
       
            if (customerTicketList.Count > 0)
            {
                cmbInvoiceNo.ValueMember = "TicketId";
                cmbInvoiceNo.DataSource = customerTicketList;
                cmbInvoiceNo.DisplayMember = "TicketNo";
                cmbInvoiceNo.SelectedValue = id;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            savedInvoiceId = TicketId;
            if (dgvPayments.RowCount > 0 && ticketPaymentLIst.Count > 0 && allowRemove)
            {
                allowRemove = false;
                if (dgvPayments.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    clsTicketsPayment payment = (clsTicketsPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                    try
                    {
                        if (paymentConnection.DeletePayment(payment.PaymentId))
                        {
                            dgvPayments.DataSource = null;
                            dgvPayments.Rows.Clear();

                            DisplayData(CustomerID);
                            cmbInvoiceNo.SelectedValue = savedInvoiceId;
                            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                            simpleSound.Play();
                            stsLblInfo.Text = "Payment has been deleted";
                            DataChanged = false;
                            btnRemove.Visible = false;
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        stsLblInfo.Text = "Error while deleting";
                    }
                }


            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void dgvPayments_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPayments.RowCount > 0)
            {
                if (e.RowIndex >= 0)
                {
                    allowRemove = true;
                    btnRemove.Visible = true;
                    panelPayment.Visible = true;
                    clsTicketsPayment payment = (clsTicketsPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                    if (!string.IsNullOrWhiteSpace(payment.Date.ToShortDateString()))
                    {
                        dtpDate.Value = payment.Date;
                    }
                    else
                    {
                        dtpDate.Value = DateTime.Today;
                    }

                    if (!string.IsNullOrWhiteSpace(payment.Payment.ToString()))
                    {
                        txtPayment.Text = payment.Payment.ToString();
                    }
                    else
                    {
                        txtPayment.Text = "";
                    }

                    if (!string.IsNullOrWhiteSpace(payment.ChequeNo))
                    {
                        txtChequeNumber.Text = payment.ChequeNo;
                    }
                    else
                    {
                        txtChequeNumber.Text = "";
                    }

                    cmbCurrency.SelectedValue = payment.CurrencyId;
                    cbBank.SelectedValue=payment.BankId;

                    if (!string.IsNullOrWhiteSpace(payment.ChequeDate))
                    {
                        dtbChequeDate.Text = payment.ChequeDate;
                    }
                    else
                    {
                        dtbChequeDate.Text = "";
                    }

                    if (!string.IsNullOrWhiteSpace(payment.CollectedDate))
                    {
                        dtbCollectedDate.Text = payment.CollectedDate;
                    }
                    else
                    {
                        dtbCollectedDate.Text = "";
                    }
                    txtPaymentRate.Text = payment.CurrencyRate.ToString();
                    PaymentID = payment.PaymentId;

                }
            }
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoiceNo.Items.Count > 0))
            {
                if (cmbInvoiceNo.SelectedIndex == 0 || cmbInvoiceNo.SelectedIndex == -1)
                {
                    cmbInvoiceNo.SelectedIndex = cmbInvoiceNo.Items.Count - 1;
                }
                else
                {
                    cmbInvoiceNo.SelectedIndex = cmbInvoiceNo.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if ((cmbInvoiceNo.Items.Count > 0))
            {
                if (cmbInvoiceNo.SelectedIndex == cmbInvoiceNo.Items.Count - 1 || cmbInvoiceNo.SelectedIndex == -1)
                {
                    cmbInvoiceNo.SelectedIndex = 0;
                }
                else
                {
                    cmbInvoiceNo.SelectedIndex = cmbInvoiceNo.SelectedIndex + 1;
                }
            }
        }

        private void dtbChequeDate_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void dtbCollectedDate_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }

        private void cbBank_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cbBank.SelectedIndex != -1)
            {
                id = int.Parse(cbBank.SelectedValue.ToString());
            }


            cbBank.ValueMember = "BankId";
            cbBank.DataSource = bankConnection.BankArrayList();
            cbBank.DisplayMember = "BankName";
            cbBank.SelectedValue = id;
        }

        private void TStripPrint_Click(object sender, EventArgs e)
        {
            if (cmbInvoiceNo.SelectedIndex != -1 && customerTicketList.Count > 0 && ticketPaymentLIst.Count > 0 && dgvPayments.SelectedRows.Count > 0)
            {

                clsTickets tickets = (clsTickets)cmbInvoiceNo.SelectedItem;

                if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                {
                    MessageBox.Show("Please Enter Customer Name");
                    return;
                }



                clsTicketsPayment payment = (clsTicketsPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                clsCustomer customer = (clsCustomer)cmbCustomerName.SelectedItem;
               
                int paymetNo = dgvPayments.SelectedRows[0].Index + 1;

                printTicketPayment frm = new printTicketPayment(tickets, payment, paymetNo,txtCustomerName.Text);
                frm.Show();
            }
            else
            {
                if (this.CustomerID!=0 && customerTicketList.Count > 0 && ticketPaymentLIst.Count > 0 && dgvPayments.SelectedRows.Count > 0)
                {
                    if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                    {
                        MessageBox.Show("Please Enter Customer Name");
                        return;
                    }



                    clsTicketsPayment payment = (clsTicketsPayment)dgvPayments.SelectedRows[0].DataBoundItem;
                    clsCustomer customer = (clsCustomer)cmbCustomerName.SelectedItem;

                    int paymetNo = dgvPayments.SelectedRows[0].Index + 1;
                    string paymentFor = "Ticket(s) Reservation ";
                    printCustTicketPayment frm = new printCustTicketPayment(payment, paymetNo, txtCustomerName.Text, paymentFor);
                    frm.Show();
                }
                else
                {
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                    simpleSound.Play();
                }
                
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();
                lblPaidAmount.Text = currency.CurrencySymbol;
                lblPriceSymbol.Text = currency.CurrencySymbol;
                lblRemainingSymbol.Text = currency.CurrencySymbol;
                lblPayment.Text = currency.CurrencySymbol;
                txtPrice.Text = (TicketPrice / currency.CurrencyRate).ToString(currencyFormat);
                txtPaidAmount.Text = (paidAmount / currency.CurrencyRate).ToString(currencyFormat);
                txtRemainingAmount.Text = (remainingAmount / currency.CurrencyRate).ToString(currencyFormat);
            }
            else
            {
                txtRate.Text = "";
                lblRemainingSymbol.Text = "";
                lblPriceSymbol.Text = "";
                lblPaidAmount.Text = "";
                lblPayment.Text = "";
            }
            DataChanged = true;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                toolStripButton1.Enabled = false;
            }
            if (!right.Deleting)
            {
                btnRemove.Enabled = false;
            }
            if (!right.Printing)
            {
                TStripPrint.Enabled = false;
            }
            if (!right.Updating)
            {
                btnSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            //cmbCustomerName.Enabled = false;
            //cmbInvoiceNo.Enabled = false;
            cbBank.Enabled = false;
            dtpDate.Enabled = false;
            txtPayment.ReadOnly = true;
            txtChequeNumber.ReadOnly = true;
            dtbChequeDate.ReadOnly = true;
            dtbCollectedDate.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbCustomerName.Enabled = true;
            cmbInvoiceNo.Enabled = true;
            cbBank.Enabled = true;
            dtpDate.Enabled = true;
            txtPayment.ReadOnly = false;
            txtChequeNumber.ReadOnly = false;
            dtbChequeDate.ReadOnly = false;
            dtbCollectedDate.ReadOnly = false;
        }

        private void frmTicketPayments_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }

        private void txtPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtPayment.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
    }

