﻿namespace SerhanTravel.Forms
{
    partial class frmTickets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTickets));
            this.txtRate = new System.Windows.Forms.TextBox();
            this.stsLblInfo = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.txtFlightNo = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.txtTravellerName = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.cmbSupplier = new System.Windows.Forms.ComboBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace00 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackWard = new System.Windows.Forms.ToolStripButton();
            this.TStripSpace02 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripForWard = new System.Windows.Forms.ToolStripButton();
            this.Label3 = new System.Windows.Forms.Label();
            this.cmbClass = new System.Windows.Forms.ComboBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txttickerId = new System.Windows.Forms.TextBox();
            this.lblSerNo = new System.Windows.Forms.Label();
            this.cmbAirLines = new System.Windows.Forms.ComboBox();
            this.dtpTicketDate = new System.Windows.Forms.DateTimePicker();
            this.cmbTicketNo = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCost = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblCostSymbol = new System.Windows.Forms.Label();
            this.txtFlightSchedual = new System.Windows.Forms.TextBox();
            this.lblPriceSymbol = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTicketNo = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtReferenceNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblProfit = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtProfit = new System.Windows.Forms.TextBox();
            this.TStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(79, 56);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(205, 22);
            this.txtRate.TabIndex = 53;
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.stsLblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stsLblInfo.Location = new System.Drawing.Point(0, 473);
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(816, 21);
            this.stsLblInfo.TabIndex = 63;
            this.stsLblInfo.Text = "Ticket Info";
            this.stsLblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRemarks
            // 
            this.txtRemarks.AccessibleDescription = "txtSerNo";
            this.txtRemarks.AccessibleName = "txtSerNo";
            this.txtRemarks.Location = new System.Drawing.Point(79, 93);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(251, 66);
            this.txtRemarks.TabIndex = 61;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // Label14
            // 
            this.Label14.AccessibleDescription = "lblSerNo";
            this.Label14.AccessibleName = "lblSerNo";
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(18, 96);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(58, 15);
            this.Label14.TabIndex = 60;
            this.Label14.Text = "Remarks";
            // 
            // Label13
            // 
            this.Label13.AccessibleDescription = "lblSerNo";
            this.Label13.AccessibleName = "lblSerNo";
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(35, 59);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(32, 15);
            this.Label13.TabIndex = 52;
            this.Label13.Text = "Rate";
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(79, 29);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(205, 23);
            this.cmbCurrency.TabIndex = 51;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(7, 32);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 50;
            this.Label12.Text = "Currency";
            // 
            // txtPrice
            // 
            this.txtPrice.AccessibleDescription = "txtSerNo";
            this.txtPrice.AccessibleName = "txtSerNo";
            this.txtPrice.Location = new System.Drawing.Point(79, 84);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(205, 22);
            this.txtPrice.TabIndex = 49;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            this.txtPrice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyUp);
            // 
            // Label11
            // 
            this.Label11.AccessibleDescription = "lblSerNo";
            this.Label11.AccessibleName = "lblSerNo";
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(30, 87);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(37, 15);
            this.Label11.TabIndex = 48;
            this.Label11.Text = "Price";
            // 
            // txtFlightNo
            // 
            this.txtFlightNo.AccessibleDescription = "txtSerNo";
            this.txtFlightNo.AccessibleName = "txtSerNo";
            this.txtFlightNo.Location = new System.Drawing.Point(109, 103);
            this.txtFlightNo.Name = "txtFlightNo";
            this.txtFlightNo.Size = new System.Drawing.Size(199, 22);
            this.txtFlightNo.TabIndex = 39;
            this.txtFlightNo.TextChanged += new System.EventHandler(this.txtFlightNo_TextChanged);
            // 
            // Label10
            // 
            this.Label10.AccessibleDescription = "lblSerNo";
            this.Label10.AccessibleName = "lblSerNo";
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(10, 106);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(61, 15);
            this.Label10.TabIndex = 38;
            this.Label10.Text = "Flight No.";
            // 
            // Label9
            // 
            this.Label9.AccessibleDescription = "lblSerNo";
            this.Label9.AccessibleName = "lblSerNo";
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(10, 134);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(93, 15);
            this.Label9.TabIndex = 44;
            this.Label9.Text = "Flight Schedual";
            // 
            // txtTravellerName
            // 
            this.txtTravellerName.AccessibleDescription = "txtSerNo";
            this.txtTravellerName.AccessibleName = "txtSerNo";
            this.txtTravellerName.Location = new System.Drawing.Point(109, 42);
            this.txtTravellerName.Name = "txtTravellerName";
            this.txtTravellerName.Size = new System.Drawing.Size(199, 22);
            this.txtTravellerName.TabIndex = 47;
            this.txtTravellerName.TextChanged += new System.EventHandler(this.txtTravellerName_TextChanged);
            // 
            // Label8
            // 
            this.Label8.AccessibleDescription = "lblSerNo";
            this.Label8.AccessibleName = "lblSerNo";
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(10, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(87, 15);
            this.Label8.TabIndex = 46;
            this.Label8.Text = "Traveler Name";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Items.AddRange(new object[] {
            "Public",
            "Mohammad Serhan",
            "Hussein Khalifeh",
            "Serhan Company",
            "Fakih Office"});
            this.cmbCustomer.Location = new System.Drawing.Point(109, 12);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(199, 23);
            this.cmbCustomer.TabIndex = 59;
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            this.cmbCustomer.Click += new System.EventHandler(this.cmbCustomer_Click);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(10, 18);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 15);
            this.Label7.TabIndex = 58;
            this.Label7.Text = "Customer";
            // 
            // Label6
            // 
            this.Label6.AccessibleDescription = "lblSerNo";
            this.Label6.AccessibleName = "lblSerNo";
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(5, 6);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(64, 15);
            this.Label6.TabIndex = 42;
            this.Label6.Text = "Ticket No.";
            // 
            // cmbSupplier
            // 
            this.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbSupplier.FormattingEnabled = true;
            this.cmbSupplier.Items.AddRange(new object[] {
            "Middle East Airlines",
            "Saudia Airlines",
            "Emarat Airlines",
            "Egypt Air"});
            this.cmbSupplier.Location = new System.Drawing.Point(79, 66);
            this.cmbSupplier.Name = "cmbSupplier";
            this.cmbSupplier.Size = new System.Drawing.Size(251, 23);
            this.cmbSupplier.TabIndex = 57;
            this.cmbSupplier.SelectedIndexChanged += new System.EventHandler(this.cmbSupplier_SelectedIndexChanged);
            this.cmbSupplier.Click += new System.EventHandler(this.cmbSupplier_Click);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(22, 69);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(54, 15);
            this.Label5.TabIndex = 56;
            this.Label5.Text = "Supplier";
            // 
            // txtDestination
            // 
            this.txtDestination.AccessibleDescription = "txtSerNo";
            this.txtDestination.AccessibleName = "txtSerNo";
            this.txtDestination.Location = new System.Drawing.Point(109, 70);
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.Size = new System.Drawing.Size(199, 22);
            this.txtDestination.TabIndex = 41;
            this.txtDestination.TextChanged += new System.EventHandler(this.txtDestination_TextChanged);
            // 
            // Label4
            // 
            this.Label4.AccessibleDescription = "lblSerNo";
            this.Label4.AccessibleName = "lblSerNo";
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(10, 72);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(70, 15);
            this.Label4.TabIndex = 40;
            this.Label4.Text = "Destination";
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripNew,
            this.ToolStripSeparator4,
            this.TStripSave,
            this.ToolStripSeparator3,
            this.TStripDelete,
            this.TStripSpace00,
            this.ToolStripSeparator5,
            this.TStripBackWard,
            this.TStripSpace02,
            this.TStripForWard});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(816, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 62;
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = global::SerhanTravel.Properties.Resources.New021;
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Customer";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Customer";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripSpace00
            // 
            this.TStripSpace00.AccessibleDescription = "TStripSpace00";
            this.TStripSpace00.AccessibleName = "TStripSpace00";
            this.TStripSpace00.AutoSize = false;
            this.TStripSpace00.Name = "TStripSpace00";
            this.TStripSpace00.Size = new System.Drawing.Size(80, 40);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripBackWard
            // 
            this.TStripBackWard.AccessibleDescription = "TStripBackWard";
            this.TStripBackWard.AccessibleName = "TStripBackWard";
            this.TStripBackWard.AutoSize = false;
            this.TStripBackWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackWard.Image")));
            this.TStripBackWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackWard.Name = "TStripBackWard";
            this.TStripBackWard.Size = new System.Drawing.Size(40, 38);
            this.TStripBackWard.ToolTipText = "Backword";
            this.TStripBackWard.Click += new System.EventHandler(this.TStripBackWard_Click);
            // 
            // TStripSpace02
            // 
            this.TStripSpace02.AccessibleDescription = "TStripSpace02";
            this.TStripSpace02.AccessibleName = "TStripSpace02";
            this.TStripSpace02.AutoSize = false;
            this.TStripSpace02.Name = "TStripSpace02";
            this.TStripSpace02.Size = new System.Drawing.Size(10, 25);
            // 
            // TStripForWard
            // 
            this.TStripForWard.AccessibleDescription = "TStripForWard";
            this.TStripForWard.AccessibleName = "TStripForWard";
            this.TStripForWard.AutoSize = false;
            this.TStripForWard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForWard.Image = ((System.Drawing.Image)(resources.GetObject("TStripForWard.Image")));
            this.TStripForWard.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForWard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForWard.Name = "TStripForWard";
            this.TStripForWard.Size = new System.Drawing.Size(40, 38);
            this.TStripForWard.Text = "ToolStripButton1";
            this.TStripForWard.ToolTipText = "Forword";
            this.TStripForWard.Click += new System.EventHandler(this.TStripForWard_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(23, 41);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(53, 15);
            this.Label3.TabIndex = 54;
            this.Label3.Text = "Airlines";
            // 
            // cmbClass
            // 
            this.cmbClass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbClass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbClass.FormattingEnabled = true;
            this.cmbClass.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbClass.Location = new System.Drawing.Point(79, 12);
            this.cmbClass.Name = "cmbClass";
            this.cmbClass.Size = new System.Drawing.Size(251, 23);
            this.cmbClass.TabIndex = 37;
            this.cmbClass.SelectedIndexChanged += new System.EventHandler(this.cmbClass_SelectedIndexChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(38, 15);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(38, 15);
            this.Label2.TabIndex = 36;
            this.Label2.Text = "Class";
            // 
            // Label1
            // 
            this.Label1.AccessibleDescription = "lblSerNo";
            this.Label1.AccessibleName = "lblSerNo";
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(5, 122);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(32, 15);
            this.Label1.TabIndex = 34;
            this.Label1.Text = "Date";
            // 
            // txttickerId
            // 
            this.txttickerId.AccessibleDescription = "txtSerNo";
            this.txttickerId.AccessibleName = "txtSerNo";
            this.txttickerId.Location = new System.Drawing.Point(96, 88);
            this.txttickerId.Name = "txttickerId";
            this.txttickerId.ReadOnly = true;
            this.txttickerId.Size = new System.Drawing.Size(199, 22);
            this.txttickerId.TabIndex = 33;
            // 
            // lblSerNo
            // 
            this.lblSerNo.AccessibleDescription = "lblSerNo";
            this.lblSerNo.AccessibleName = "lblSerNo";
            this.lblSerNo.AutoSize = true;
            this.lblSerNo.Location = new System.Drawing.Point(5, 91);
            this.lblSerNo.Name = "lblSerNo";
            this.lblSerNo.Size = new System.Drawing.Size(59, 15);
            this.lblSerNo.TabIndex = 32;
            this.lblSerNo.Text = "Ticket ID";
            // 
            // cmbAirLines
            // 
            this.cmbAirLines.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbAirLines.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbAirLines.FormattingEnabled = true;
            this.cmbAirLines.Items.AddRange(new object[] {
            "Middle East Airlines",
            "Saudia Airlines",
            "Emarat Airlines",
            "Egypt Air"});
            this.cmbAirLines.Location = new System.Drawing.Point(79, 39);
            this.cmbAirLines.Name = "cmbAirLines";
            this.cmbAirLines.Size = new System.Drawing.Size(251, 23);
            this.cmbAirLines.TabIndex = 55;
            this.cmbAirLines.SelectedIndexChanged += new System.EventHandler(this.cmbAirLines_SelectedIndexChanged);
            this.cmbAirLines.Click += new System.EventHandler(this.cmbAirLines_Click);
            // 
            // dtpTicketDate
            // 
            this.dtpTicketDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpTicketDate.Location = new System.Drawing.Point(96, 116);
            this.dtpTicketDate.Name = "dtpTicketDate";
            this.dtpTicketDate.Size = new System.Drawing.Size(199, 22);
            this.dtpTicketDate.TabIndex = 64;
            this.dtpTicketDate.ValueChanged += new System.EventHandler(this.dtpTicketDate_ValueChanged);
            // 
            // cmbTicketNo
            // 
            this.cmbTicketNo.FormattingEnabled = true;
            this.cmbTicketNo.Location = new System.Drawing.Point(96, 3);
            this.cmbTicketNo.Name = "cmbTicketNo";
            this.cmbTicketNo.Size = new System.Drawing.Size(199, 23);
            this.cmbTicketNo.TabIndex = 66;
            this.cmbTicketNo.SelectedIndexChanged += new System.EventHandler(this.cmbTicketNo_SelectedIndexChanged);
            this.cmbTicketNo.TextChanged += new System.EventHandler(this.cmbTicketNo_TextChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.btnCancel.Location = new System.Drawing.Point(328, 436);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 34);
            this.btnCancel.TabIndex = 67;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(300, 61);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(0, 15);
            this.lblCurrency.TabIndex = 68;
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(96, 25);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(199, 19);
            this.lbResult.TabIndex = 71;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(949, 251);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 74;
            // 
            // txtCost
            // 
            this.txtCost.AccessibleDescription = "txtSerNo";
            this.txtCost.AccessibleName = "txtSerNo";
            this.txtCost.Location = new System.Drawing.Point(79, 110);
            this.txtCost.Name = "txtCost";
            this.txtCost.Size = new System.Drawing.Size(205, 22);
            this.txtCost.TabIndex = 73;
            this.txtCost.TextChanged += new System.EventHandler(this.txtCost_TextChanged);
            this.txtCost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCost_KeyPress);
            this.txtCost.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtCost_KeyUp);
            // 
            // label16
            // 
            this.label16.AccessibleDescription = "lblSerNo";
            this.label16.AccessibleName = "lblSerNo";
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 113);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 15);
            this.label16.TabIndex = 72;
            this.label16.Text = "Cost";
            // 
            // lblCostSymbol
            // 
            this.lblCostSymbol.AccessibleDescription = "lblSerNo";
            this.lblCostSymbol.AccessibleName = "lblSerNo";
            this.lblCostSymbol.AutoSize = true;
            this.lblCostSymbol.Location = new System.Drawing.Point(311, 113);
            this.lblCostSymbol.Name = "lblCostSymbol";
            this.lblCostSymbol.Size = new System.Drawing.Size(0, 15);
            this.lblCostSymbol.TabIndex = 75;
            // 
            // txtFlightSchedual
            // 
            this.txtFlightSchedual.AccessibleDescription = "txtSerNo";
            this.txtFlightSchedual.AccessibleName = "txtSerNo";
            this.txtFlightSchedual.Location = new System.Drawing.Point(109, 131);
            this.txtFlightSchedual.Name = "txtFlightSchedual";
            this.txtFlightSchedual.Size = new System.Drawing.Size(199, 22);
            this.txtFlightSchedual.TabIndex = 76;
            // 
            // lblPriceSymbol
            // 
            this.lblPriceSymbol.AccessibleDescription = "lblSerNo";
            this.lblPriceSymbol.AccessibleName = "lblSerNo";
            this.lblPriceSymbol.AutoSize = true;
            this.lblPriceSymbol.Location = new System.Drawing.Point(311, 89);
            this.lblPriceSymbol.Name = "lblPriceSymbol";
            this.lblPriceSymbol.Size = new System.Drawing.Size(0, 15);
            this.lblPriceSymbol.TabIndex = 77;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel1.Controls.Add(this.lbResult);
            this.panel1.Controls.Add(this.txtTicketNo);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.txtReferenceNo);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.Label6);
            this.panel1.Controls.Add(this.cmbTicketNo);
            this.panel1.Controls.Add(this.txttickerId);
            this.panel1.Controls.Add(this.lblSerNo);
            this.panel1.Controls.Add(this.dtpTicketDate);
            this.panel1.Controls.Add(this.Label1);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(18, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(323, 173);
            this.panel1.TabIndex = 187;
            // 
            // txtTicketNo
            // 
            this.txtTicketNo.AccessibleDescription = "txtSerNo";
            this.txtTicketNo.AccessibleName = "txtSerNo";
            this.txtTicketNo.Location = new System.Drawing.Point(96, 60);
            this.txtTicketNo.Name = "txtTicketNo";
            this.txtTicketNo.Size = new System.Drawing.Size(199, 22);
            this.txtTicketNo.TabIndex = 75;
            this.txtTicketNo.TextChanged += new System.EventHandler(this.txtTicketNo_TextChanged);
            // 
            // label19
            // 
            this.label19.AccessibleDescription = "lblSerNo";
            this.label19.AccessibleName = "lblSerNo";
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 63);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 15);
            this.label19.TabIndex = 74;
            this.label19.Text = "Ticket NO";
            // 
            // txtReferenceNo
            // 
            this.txtReferenceNo.AccessibleDescription = "txtSerNo";
            this.txtReferenceNo.AccessibleName = "txtSerNo";
            this.txtReferenceNo.Location = new System.Drawing.Point(96, 144);
            this.txtReferenceNo.Name = "txtReferenceNo";
            this.txtReferenceNo.Size = new System.Drawing.Size(199, 22);
            this.txtReferenceNo.TabIndex = 73;
            this.txtReferenceNo.TextChanged += new System.EventHandler(this.txtReferenceNo_TextChanged);
            // 
            // label17
            // 
            this.label17.AccessibleDescription = "lblSerNo";
            this.label17.AccessibleName = "lblSerNo";
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 15);
            this.label17.TabIndex = 72;
            this.label17.Text = "Referance No";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel2.Controls.Add(this.cmbCustomer);
            this.panel2.Controls.Add(this.Label7);
            this.panel2.Controls.Add(this.txtTravellerName);
            this.panel2.Controls.Add(this.txtFlightSchedual);
            this.panel2.Controls.Add(this.Label8);
            this.panel2.Controls.Add(this.txtDestination);
            this.panel2.Controls.Add(this.Label4);
            this.panel2.Controls.Add(this.txtFlightNo);
            this.panel2.Controls.Add(this.Label10);
            this.panel2.Controls.Add(this.Label9);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(18, 255);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(323, 162);
            this.panel2.TabIndex = 188;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel3.Controls.Add(this.txtRemarks);
            this.panel3.Controls.Add(this.cmbAirLines);
            this.panel3.Controls.Add(this.Label2);
            this.panel3.Controls.Add(this.cmbClass);
            this.panel3.Controls.Add(this.Label3);
            this.panel3.Controls.Add(this.Label5);
            this.panel3.Controls.Add(this.cmbSupplier);
            this.panel3.Controls.Add(this.Label14);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(403, 255);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(381, 167);
            this.panel3.TabIndex = 189;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.MediumAquamarine;
            this.panel7.Controls.Add(this.lblProfit);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Controls.Add(this.txtProfit);
            this.panel7.Controls.Add(this.cmbCurrency);
            this.panel7.Controls.Add(this.Label11);
            this.panel7.Controls.Add(this.txtPrice);
            this.panel7.Controls.Add(this.Label12);
            this.panel7.Controls.Add(this.lblPriceSymbol);
            this.panel7.Controls.Add(this.lblCurrency);
            this.panel7.Controls.Add(this.Label13);
            this.panel7.Controls.Add(this.lblCostSymbol);
            this.panel7.Controls.Add(this.txtRate);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.txtCost);
            this.panel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(403, 76);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(381, 173);
            this.panel7.TabIndex = 190;
            // 
            // lblProfit
            // 
            this.lblProfit.AccessibleDescription = "lblSerNo";
            this.lblProfit.AccessibleName = "lblSerNo";
            this.lblProfit.AutoSize = true;
            this.lblProfit.Location = new System.Drawing.Point(311, 141);
            this.lblProfit.Name = "lblProfit";
            this.lblProfit.Size = new System.Drawing.Size(0, 15);
            this.lblProfit.TabIndex = 80;
            // 
            // label18
            // 
            this.label18.AccessibleDescription = "lblSerNo";
            this.label18.AccessibleName = "lblSerNo";
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 141);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 15);
            this.label18.TabIndex = 78;
            this.label18.Text = "Profit";
            // 
            // txtProfit
            // 
            this.txtProfit.AccessibleDescription = "txtSerNo";
            this.txtProfit.AccessibleName = "txtSerNo";
            this.txtProfit.Location = new System.Drawing.Point(79, 138);
            this.txtProfit.Name = "txtProfit";
            this.txtProfit.Size = new System.Drawing.Size(205, 22);
            this.txtProfit.TabIndex = 79;
            // 
            // frmTickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(816, 494);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.stsLblInfo);
            this.Controls.Add(this.TStrip);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "frmTickets";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tickets Reservation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTickets_FormClosing);
            this.Load += new System.EventHandler(this.frmTickets_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmTickets_MouseClick);
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.Label stsLblInfo;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox txtPrice;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox txtFlightNo;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox txtTravellerName;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.ComboBox cmbCustomer;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.ComboBox cmbSupplier;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox txtDestination;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace00;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripSeparator TStripSpace02;
        internal System.Windows.Forms.ToolStripButton TStripBackWard;
        internal System.Windows.Forms.ToolStripButton TStripForWard;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.ComboBox cmbClass;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txttickerId;
        internal System.Windows.Forms.Label lblSerNo;
        internal System.Windows.Forms.ComboBox cmbAirLines;
        private System.Windows.Forms.DateTimePicker dtpTicketDate;
        private System.Windows.Forms.ComboBox cmbTicketNo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblCurrency;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.Label label15;
        internal System.Windows.Forms.TextBox txtCost;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label lblCostSymbol;
        internal System.Windows.Forms.TextBox txtFlightSchedual;
        internal System.Windows.Forms.Label lblPriceSymbol;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        internal System.Windows.Forms.TextBox txtReferenceNo;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label lblProfit;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.TextBox txtProfit;
        internal System.Windows.Forms.TextBox txtTicketNo;
        internal System.Windows.Forms.Label label19;
    }
}