﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Objects;
using System.Collections;
using System.Media;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmTickets : Form
    {
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsTickets> ticketList;
        bool canUpdateOnly;

        Ticket ticketConnection;
        clsTickets ticketsData;
        List<clsCurrency> currencies;
        bool DataChanged = false;
        bool iamUpdate = false;
        bool skip = false;
        int saveID;
        bool iamAdding = false;
        int TicketID = 0;
        int CustomerID;
        clsAccessRights right;
        bool canAddOnly = false;
        Currency currencyConnection;
        AirLines airlineConnection;
        private void ResetFields()
        {

            cmbAirLines.SelectedIndex = -1;
            cmbClass.SelectedIndex = -1;
            cmbCurrency.SelectedIndex = -1;
            cmbCustomer.SelectedIndex = -1;
            cmbSupplier.SelectedIndex = -1;
            cmbTicketNo.SelectedIndex = -1;
            dtpTicketDate.Value = DateTime.Today;
            txtDestination.Text="";
            txttickerId.Text="";
            stsLblInfo.Text = "";
            txtFlightNo.Text="";
            txtPrice.Text="";
            txtCost.Text = "";
            txtRate.Text="";
            txtRemarks.Text="";
            txtTravellerName.Text="";
            txtFlightSchedual.Text = "";
            dtpTicketDate.Value.ToLocalTime();
            txtProfit.Clear();
            txtReferenceNo.Clear();
            txtTicketNo.Clear();
            DataChanged = false;
        }


        internal frmTickets(clsAccessRights right)
        {
            InitializeComponent();
            this.right = right;
        }

        private void frmTickets_Load(object sender, EventArgs e)
        {
            accessRight();
            airlineConnection = new AirLines();
            dtpTicketDate.Value.ToLocalTime();
            currencyConnection = new Currency();
            ticketConnection = new Ticket();
            ticketList = ticketConnection.TicketsArrayList();
            FillCombobox();

            if (ticketList.Count > 0)
            {
                DisplayData(ticketList[ticketList.Count - 1].TicketId);
             
            }

            DataChanged = false;

        }
        private void FillCombobox()
        {

            ArrayList Airlines = airlineConnection.AirlineArrayList();
            cmbAirLines.ValueMember = "airlineId";
            cmbAirLines.DataSource = Airlines;
            cmbAirLines.DisplayMember = "AirLineName";
            cmbAirLines.SelectedIndex = -1;

            Supplier supplierConnection = new Supplier();
            List<clsSupplier> suppliers = supplierConnection.SupplierArrayList();
            cmbSupplier.DataSource = suppliers;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.SelectedIndex = -1;

            Customer customerConnection = new Customer();
            List<clsCustomer> customers = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customers.Insert(0,cus);
            cmbCustomer.DataSource = customers;
            cmbCustomer.DisplayMember = "customerName";
            cmbCustomer.ValueMember = "customerId";
            cmbCustomer.SelectedIndex = -1;


            currencies = currencyConnection.CurrencyArrayList();

      
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            



            List<ClassSeat> classSeats = new List<ClassSeat>();
            classSeats.Add(new ClassSeat(1, "Economy Class"));
            classSeats.Add(new ClassSeat(2, "Premium Economy"));
            classSeats.Add(new ClassSeat(3, "Business Class"));
            classSeats.Add(new ClassSeat(4, "First Class"));

            cmbClass.DataSource = classSeats;
            cmbClass.DisplayMember = "ClassName";
            cmbClass.ValueMember = "classId";
            cmbClass.SelectedIndex = -1;



            DataChanged = false;

        }

        private void DisplayData(int tktId)
        {

            if ( tktId != 0)
            {
                cmbTicketNo.DataSource = null;
                cmbTicketNo.ValueMember = "TicketId";
                cmbTicketNo.DataSource = ticketList;
                cmbTicketNo.DisplayMember = "TicketString";
                skip = true;
                cmbTicketNo.SelectedValue = tktId;
                TicketID = tktId;
            }
           
         
            iamAdding = false;
            iamUpdate = false;


            DataChanged = false;

        }
  
      
        
       

        private void cmbTicketNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTicketNo.SelectedIndex != -1 && ticketList.Count > 0)
            {
                lbResult.Visible = false;
                TicketID = int.Parse(cmbTicketNo.SelectedValue.ToString());
                ticketsData = ticketList[cmbTicketNo.SelectedIndex];
                txttickerId.Text = ticketsData.TicketId.ToString();
                dtpTicketDate.Value = ticketsData.TicketDate;
                txtTravellerName.Text = ticketsData.TravellerName;
                txtDestination.Text = ticketsData.Destination;
                cmbClass.SelectedValue = ticketsData.TicketClassId;
                txtFlightNo.Text = ticketsData.FlightNo.ToString();
                cmbAirLines.SelectedValue = ticketsData.AirLineId;
                cmbSupplier.SelectedValue = ticketsData.SupplierId;
                 cmbCustomer.SelectedValue = ticketsData.CustomerId;
                txtFlightSchedual.Text = ticketsData.FlightSchedual;
                cmbCurrency.SelectedValue = ticketsData.CurrencyId;              
                txtPrice.Text = ticketsData.Price.ToString();
                txtCost.Text = ticketsData.Cost.ToString();
                txtReferenceNo.Text = ticketsData.ReferenceNo;
                txtProfit.Text = ticketsData.Profit.ToString();
                txtRemarks.Text = ticketsData.Remarks;
                txtRate.Text = ticketsData.CurrencyRate.ToString();
                txtTicketNo.Text = ticketsData.TicketNo;
                skip = true;
                DataChanged = false;
             
            }
            else
            {
                skip = false;
            }



        }
        private void SaveDataInfo()
        {



            bool RecExists;
            try
            {
               
                clsTickets ticket = new clsTickets();
                ticket.TicketId = TicketID;

                if (string.IsNullOrWhiteSpace(txtTicketNo.Text))
                {
                    ticket.TicketNo = " ";
                }
                else
                {
                    ticket.TicketNo = txtTicketNo.Text;
                }

                if (dtpTicketDate.Value ==null)
                {
                    ticket.TicketDate= dtpTicketDate.Value.ToLocalTime();
                }
                else
                {
                   ticket.TicketDate = dtpTicketDate.Value;
                }

                if (string.IsNullOrWhiteSpace(txtTravellerName.Text))
                {
                    ticket.TravellerName = " ";
                }
                else
                {
                    ticket.TravellerName = txtTravellerName.Text;
                }

                if (string.IsNullOrWhiteSpace(txtDestination.Text))
                {
                    ticket.Destination = " ";
                }
                else
                {
                    ticket.Destination = txtDestination.Text;
                }

                if (cmbClass.SelectedIndex == -1)
                {
                    ticket.TicketClassId = 0;
                }
                else
                {
                    ticket.TicketClassId = int.Parse(cmbClass.SelectedValue.ToString());
                }


                if (string.IsNullOrWhiteSpace(txtFlightNo.Text))
                {
                    ticket.FlightNo = "";
                }
                else
                {
                    ticket.FlightNo = txtFlightNo.Text;
                }

                if (cmbAirLines.SelectedIndex==-1)
                {
                    ticket.AirLineId= 0;
                }
                else
                {
                    ticket.AirLineId =int.Parse(cmbAirLines.SelectedValue.ToString());
                }
                if (cmbSupplier.SelectedIndex==-1)
                {
                    ticket.SupplierId = 0;
                }
                else
                {
                   ticket.SupplierId = int.Parse(cmbSupplier.SelectedValue.ToString());
                }
                if (cmbCustomer.SelectedIndex==-1|| cmbCustomer.SelectedIndex == 0)
                {
                    CustomerID= 0;
                }
                else
                {
                    CustomerID = int.Parse(cmbCustomer.SelectedValue.ToString());
                }
                if (string.IsNullOrWhiteSpace(txtFlightSchedual.Text))
                {
                    ticket.FlightSchedual="";
                }
                else
                {
                    ticket.FlightSchedual = txtFlightSchedual.Text;
                }
                if (string.IsNullOrWhiteSpace(txtPrice.Text))
                {
                    ticket.Price = 0;
                    
                }
                else
                {  
                        
                        ticket.Price = decimal.Parse(txtPrice.Text.ToString()) ;
                    
                }
                if(!string.IsNullOrWhiteSpace(txtRate.Text))
                {
                    ticket.CurrencyRate = Convert.ToDecimal(txtRate.Text);
                }
               
               
                if (string.IsNullOrWhiteSpace(txtCost.Text))
                {
                    ticket.Cost = 0;

                }
                else
                {

                    ticket.Cost = decimal.Parse(txtCost.Text.ToString());

                }
                if (string.IsNullOrWhiteSpace(txtReferenceNo.Text))
                {
                    ticket.ReferenceNo = " ";
                }
                else
                {
                    ticket.ReferenceNo = txtReferenceNo.Text;
                }
                ticket.Profit = ticket.Price - ticket.Cost;

                if (cmbCustomer.SelectedIndex==-1)
                {
                    ticket.CustomerId = 0;
                }
                else
                {
                    ticket.CustomerId = int.Parse(cmbCustomer.SelectedValue.ToString());
                }
                if (string.IsNullOrWhiteSpace(txtRemarks.Text))
                {
                    ticket.Remarks= string.Empty;
                }
                else
                {
                    ticket.Remarks = txtRemarks.Text;
                }
                if(cmbCurrency.SelectedIndex==-1)
                {
                    ticket.CurrencyId = 0;
                }
                else
                {
                    ticket.CurrencyId = int.Parse(cmbCurrency.SelectedValue.ToString());
                }


              




                Ticket ticketConnection = new Ticket();

                RecExists = ticketConnection.TicketIDExists(ticket.TicketId);


                if (RecExists == true)
                {
                    ticketConnection.UpdateTicketInfo(ticket);
                    stsLblInfo.Text = "Changes has been saved successfully";
                    iamUpdate = true;

                }
                else
                {

                    iamAdding = true;
                    ticketConnection.AddNewTicketReservation(ticket);
                    stsLblInfo.Text = "Reservation has been added successfully";

                }


                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();


                ticketList = ticketConnection.TicketsArrayList();

                if (iamUpdate)
                {
                    DisplayData(ticket.TicketId);
                }
                if (iamAdding)
                {
                    iamAdding = false;
                    DisplayData(ticketList[ticketList.Count-1].TicketId);
                }

                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            stsLblInfo.Text = "Add New Reservation";

            if (canAddOnly)
            {
                TStripSave.Enabled = true;
                unReadOnlyEveryThing();
            }

            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
            iamAdding = true;
            saveID = TicketID;
            TicketID = 0;
            ResetFields();
            cmbTicketNo.DataSource = null;
            btnCancel.Visible = true;
            DataChanged = false;
        }

        private void dtpTicketDate_ValueChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtTravellerName_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtDestination_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtFlightNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void dtpFlightSchedual_ValueChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCurrency.SelectedIndex!=-1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                lblCostSymbol.Text = currency.CurrencySymbol.ToString();
                lblPriceSymbol.Text = currency.CurrencySymbol.ToString();
                txtRate.Text = currency.CurrencyRate.ToString();
                lblProfit.Text= currency.CurrencySymbol.ToString();

            }
            else
            {
                lblCostSymbol.Text = "";
                lblPriceSymbol.Text = "";
                lblProfit.Text = "";
                txtRate.Clear();
            }
       

            DataChanged = true;

        }
     

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            iamAdding = false;

            if (DataChanged)
            {


                btnCancel.Visible = false;
                if (canUpdateOnly && this.TicketID == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                    TStripSave.Enabled = false;
                    readOnlyEveryThing();
                }

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ResetFields();
            btnCancel.Visible = false;
            iamAdding = false;
            TicketID = saveID;
            DisplayData(TicketID);
            DataChanged = false;
            if (canAddOnly)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }

        }

        private void cmbClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cmbAirLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (TicketID != 0)
            {


                try
                {
                    bool myResult;
                    SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myReply = MessageBox.Show("Are you sure ?", "Delete Ticket Reservation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        if (ticketConnection.TicketUsed(TicketID))
                        {
                            MessageBox.Show("This Ticket Is Used In Some Operations Can't be Deleted");
                            return;
                        }
                        myResult = ticketConnection.DeleteTicket(TicketID);
                        if (myResult == false)
                        {
                            return;
                        }
                        notifySound.Play();
                        ResetFields();
                        // ClearFields();
                        ticketList = ticketConnection.TicketsArrayList();
                        if (ticketList.Count > 0)
                        {
                            DisplayData(ticketList[ticketList.Count - 1].TicketId);
                        }
                        else
                            TicketID = 0;
                        lbResult.Visible = false;
                        DataChanged = false;
                        stsLblInfo.Text = "Successfully Deleted";
                    }
                }
                catch (Exception ex)
                {

                    DataChanged = false;
                    stsLblInfo.Text = "ERROR: Problem Deleting the Doctor's profile";
                    MessageBox.Show(ex.Message);
                }


            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackWard_Click(object sender, EventArgs e)
        {
            if ((cmbTicketNo.Items.Count > 0))
            {
                if (cmbTicketNo.SelectedIndex == 0 || cmbTicketNo.SelectedIndex == -1)
                {
                    cmbTicketNo.SelectedIndex = cmbTicketNo.Items.Count - 1;
                }
                else
                {
                    cmbTicketNo.SelectedIndex = cmbTicketNo.SelectedIndex - 1;
                }
            }
        }

        private void TStripForWard_Click(object sender, EventArgs e)
        {
            if ((cmbTicketNo.Items.Count > 0))
            {
                if (cmbTicketNo.SelectedIndex == cmbTicketNo.Items.Count - 1 || cmbTicketNo.SelectedIndex == -1)
                {
                    cmbTicketNo.SelectedIndex = 0;
                }
                else
                {
                    cmbTicketNo.SelectedIndex = cmbTicketNo.SelectedIndex + 1;
                }
            }

        }

     
        private void frmTickets_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbTicketNo.SelectedIndex != -1)
            {
                TicketID = Convert.ToInt32(cmbTicketNo.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

        private void cmbTicketNo_TextChanged(object sender, EventArgs e)
        {
    
            if (skip)
            {
                skip = false;
                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbTicketNo.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                return;
            }
            

            clsTickets [] result = (from i in ticketList
                                    where i.TicketString.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                return; // return with listbox's Visible set to false if nothing found
            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "TicketId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "TicketString";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }

        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbTicketNo.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }
        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character!='.' )
            {
                e.Handled = true;
            }
            else
            {
                if(character=='.' && txtPrice.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
                decimal price = 0;
                decimal cost = 0;
                if (!string.IsNullOrWhiteSpace(txtCost.Text))
                {
                    cost = decimal.Parse(txtCost.Text.ToString());
                }
                if (character != 8)
                {
                    price = decimal.Parse(txtPrice.Text + character.ToString());

                }
                else
                {
                    if (txtPrice.Text.Length - 1 > 0)
                    {
                        price = decimal.Parse(txtPrice.Text.Substring(0, txtPrice.Text.Length - 1));
                    }


                }
                txtProfit.Text = (price - cost).ToString();

            }



        }

        private void cmbAirLines_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbAirLines.SelectedIndex != -1)
                id = int.Parse(cmbAirLines.SelectedValue.ToString());

            ArrayList Airlines = airlineConnection.AirlineArrayList();
            cmbAirLines.ValueMember = "airlineId";
            cmbAirLines.DataSource = Airlines;
            cmbAirLines.DisplayMember = "AirLineName";
            cmbAirLines.SelectedValue = id;
            DataChanged = false;

        }

        private void cmbSupplier_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbSupplier.SelectedIndex != -1)
                id = int.Parse(cmbSupplier.SelectedValue.ToString());

            Supplier supplierConnection = new Supplier();
            List<clsSupplier> suppliers = supplierConnection.SupplierArrayList();
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = suppliers;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedValue = id;
            DataChanged = false;


    
        }

        private void cmbCustomer_Click(object sender, EventArgs e)
        {


            int id = 0;
            if (cmbCustomer.SelectedIndex != -1)
                id = int.Parse(cmbCustomer.SelectedValue.ToString());

            Customer customerConnection = new Customer();
            List<clsCustomer> customers = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customers.Insert(0, cus);
            cmbCustomer.DataSource = customers;
            cmbCustomer.DisplayMember = "customerName";
            cmbCustomer.ValueMember = "customerId";

            cmbCustomer.SelectedValue = id;
            DataChanged = false;

            
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {

            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());


            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
            DataChanged = false;

        }

        private void txtCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            char character = e.KeyChar;
            if (!Char.IsDigit(character) && character != 8 && character != '.')
            {
                e.Handled = true;
            }
            else
            {
                if (character == '.' && txtCost.Text.Contains("."))
                {
                    e.Handled = true;
                    return;
                }
                decimal price = 0;
                decimal cost = 0;
                if (!string.IsNullOrWhiteSpace(txtPrice.Text))
                {
                    price = decimal.Parse(txtPrice.Text.ToString());
                }
                if (character != 8)
                {
                    cost = decimal.Parse(txtCost.Text + character.ToString());

                }
                else
                {
                    if (txtCost.Text.Length - 1 > 0)
                    {
                        cost = decimal.Parse(txtCost.Text.Substring(0, txtCost.Text.Length - 1));
                    }


                }
                txtProfit.Text = (price - cost).ToString();

            }
        }
        private void txtCost_KeyUp(object sender, KeyEventArgs e)
        {
            decimal cost = 0, price = 0;
            if(! string.IsNullOrWhiteSpace(txtCost.Text))
            {
                cost = Convert.ToDecimal(txtCost.Text);
            }
            if (!string.IsNullOrWhiteSpace(txtPrice.Text))
            {
                price = Convert.ToDecimal(txtPrice.Text);
            }
            txtProfit.Text = (price - cost).ToString();
        }
        private void txtCost_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }
        private void accessRight()
        {
            if (!right.Adding)
            {
                TStripNew.Enabled = false;
            }
            if (!right.Deleting)
            {
                TStripDelete.Enabled = false;
            }
            if (!right.Updating)
            {
                TStripSave.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }

            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }

        }

        private void readOnlyEveryThing()
        {
            cmbAirLines.Enabled = false;
            cmbClass.Enabled = false;
            cmbCurrency.Enabled = false;
            cmbCustomer.Enabled = false;
            cmbSupplier.Enabled = false;
            cmbTicketNo.DropDownStyle = ComboBoxStyle.DropDownList;
            dtpTicketDate.Enabled = false;
            txtTravellerName.ReadOnly = true;
            txtFlightSchedual.ReadOnly = true;
            txtDestination.ReadOnly = true;
            txtFlightNo.ReadOnly = true;
            txtPrice.ReadOnly = true;
            txtCost.ReadOnly = true;
            txtRemarks.ReadOnly = true;

        }

        private void unReadOnlyEveryThing()
        {
            cmbAirLines.Enabled = true;
            cmbClass.Enabled = true;
            cmbCurrency.Enabled = true;
            cmbCustomer.Enabled = true;
            cmbSupplier.Enabled = true;
            cmbTicketNo.DropDownStyle = ComboBoxStyle.DropDown;
            dtpTicketDate.Enabled = true;
            txtTravellerName.ReadOnly = false;
            txtFlightSchedual.ReadOnly = false;
            txtDestination.ReadOnly = false;
            txtFlightNo.ReadOnly = false;
            txtPrice.ReadOnly = false;
            txtCost.ReadOnly = false;
            txtRemarks.ReadOnly = false;
        }

        private void frmTickets_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }

        private void txtPrice_KeyUp(object sender, KeyEventArgs e)
        {
            decimal cost = 0, price = 0;
            if (!string.IsNullOrWhiteSpace(txtCost.Text))
            {
                cost = Convert.ToDecimal(txtCost.Text);
            }
            if (!string.IsNullOrWhiteSpace(txtPrice.Text))
            {
                price = Convert.ToDecimal(txtPrice.Text);
            }
            txtProfit.Text = (price - cost).ToString();
        }

        private void txtReferenceNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }

        private void txtTicketNo_TextChanged(object sender, EventArgs e)
        {
            DataChanged = true;
        }
    }
}
