﻿namespace SerhanTravel.Forms
{
    partial class frmTicketsInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTicketsInvoice));
            this.txtRate = new System.Windows.Forms.TextBox();
            this.cmbCurrency = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.stsLblInfo = new System.Windows.Forms.Label();
            this.dgvTickets = new System.Windows.Forms.DataGridView();
            this.TicketNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Destination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Currency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Traveller = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FlightNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripPrint = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lblInvoiceNo = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.cmbInvoceNo = new System.Windows.Forms.ComboBox();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbResult = new System.Windows.Forms.ListBox();
            this.chLbCustomerTickets = new System.Windows.Forms.CheckedListBox();
            this.cmbCustomerName = new System.Windows.Forms.ComboBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnLRefresh = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.TStripNew = new System.Windows.Forms.ToolStripButton();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.TStripDelete = new System.Windows.Forms.ToolStripButton();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.printButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.nmDiscount = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnetPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTotalPiceSymbol = new System.Windows.Forms.Label();
            this.lblNetPriceSymbol = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiscount)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRate
            // 
            this.txtRate.AccessibleDescription = "txtSerNo";
            this.txtRate.AccessibleName = "txtSerNo";
            this.txtRate.Location = new System.Drawing.Point(78, 48);
            this.txtRate.Name = "txtRate";
            this.txtRate.ReadOnly = true;
            this.txtRate.Size = new System.Drawing.Size(143, 22);
            this.txtRate.TabIndex = 61;
            // 
            // cmbCurrency
            // 
            this.cmbCurrency.FormattingEnabled = true;
            this.cmbCurrency.Items.AddRange(new object[] {
            "Economy",
            "First Class",
            "Business Class"});
            this.cmbCurrency.Location = new System.Drawing.Point(78, 19);
            this.cmbCurrency.Name = "cmbCurrency";
            this.cmbCurrency.Size = new System.Drawing.Size(143, 23);
            this.cmbCurrency.TabIndex = 60;
            this.cmbCurrency.SelectedIndexChanged += new System.EventHandler(this.cmbCurrency_SelectedIndexChanged);
            this.cmbCurrency.Click += new System.EventHandler(this.cmbCurrency_Click);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(3, 22);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 59;
            this.Label12.Text = "Currency";
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.AccessibleDescription = "txtSerNo";
            this.txtTotalPrice.AccessibleName = "txtSerNo";
            this.txtTotalPrice.Location = new System.Drawing.Point(89, 3);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.ReadOnly = true;
            this.txtTotalPrice.Size = new System.Drawing.Size(98, 20);
            this.txtTotalPrice.TabIndex = 66;
            // 
            // Label2
            // 
            this.Label2.AccessibleDescription = "lblSerNo";
            this.Label2.AccessibleName = "lblSerNo";
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(25, 6);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(62, 13);
            this.Label2.TabIndex = 65;
            this.Label2.Text = "Total Price";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.stsLblInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.stsLblInfo.Location = new System.Drawing.Point(0, 554);
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(1082, 21);
            this.stsLblInfo.TabIndex = 69;
            this.stsLblInfo.Text = "Invoice Info";
            this.stsLblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvTickets
            // 
            this.dgvTickets.AllowUserToAddRows = false;
            this.dgvTickets.AllowUserToDeleteRows = false;
            this.dgvTickets.AllowUserToOrderColumns = true;
            this.dgvTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TicketNo,
            this.Destination,
            this.Price,
            this.Currency,
            this.Traveller,
            this.FlightNo});
            this.dgvTickets.Location = new System.Drawing.Point(46, 288);
            this.dgvTickets.MultiSelect = false;
            this.dgvTickets.Name = "dgvTickets";
            this.dgvTickets.ReadOnly = true;
            this.dgvTickets.Size = new System.Drawing.Size(617, 154);
            this.dgvTickets.TabIndex = 62;
            this.dgvTickets.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTickets_CellClick);
            // 
            // TicketNo
            // 
            this.TicketNo.HeaderText = "Ticket No";
            this.TicketNo.Name = "TicketNo";
            this.TicketNo.ReadOnly = true;
            // 
            // Destination
            // 
            this.Destination.HeaderText = "Destination";
            this.Destination.Name = "Destination";
            this.Destination.ReadOnly = true;
            // 
            // Price
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = "0";
            this.Price.DefaultCellStyle = dataGridViewCellStyle1;
            this.Price.HeaderText = "Price ";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // Currency
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Currency.DefaultCellStyle = dataGridViewCellStyle2;
            this.Currency.HeaderText = "Currency";
            this.Currency.Name = "Currency";
            this.Currency.ReadOnly = true;
            this.Currency.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Traveller
            // 
            this.Traveller.HeaderText = "Traveller";
            this.Traveller.Name = "Traveller";
            this.Traveller.ReadOnly = true;
            // 
            // FlightNo
            // 
            this.FlightNo.HeaderText = "Flight Number";
            this.FlightNo.Name = "FlightNo";
            this.FlightNo.ReadOnly = true;
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(6, 58);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 58);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripPrint
            // 
            this.TStripPrint.AccessibleDescription = "TStripPrint";
            this.TStripPrint.AccessibleName = "TStripPrint";
            this.TStripPrint.AutoSize = false;
            this.TStripPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripPrint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripPrint.Name = "TStripPrint";
            this.TStripPrint.Size = new System.Drawing.Size(40, 38);
            this.TStripPrint.ToolTipText = "Print Invoice";
            this.TStripPrint.Click += new System.EventHandler(this.TStripPrint_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.AccessibleDescription = "TStripSpace00";
            this.ToolStripSeparator2.AccessibleName = "TStripSpace00";
            this.ToolStripSeparator2.AutoSize = false;
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(80, 40);
            // 
            // lblInvoiceNo
            // 
            this.lblInvoiceNo.AccessibleDescription = "lblSerNo";
            this.lblInvoiceNo.AccessibleName = "lblSerNo";
            this.lblInvoiceNo.AutoSize = true;
            this.lblInvoiceNo.Location = new System.Drawing.Point(34, 110);
            this.lblInvoiceNo.Name = "lblInvoiceNo";
            this.lblInvoiceNo.Size = new System.Drawing.Size(66, 15);
            this.lblInvoiceNo.TabIndex = 51;
            this.lblInvoiceNo.Text = "Invoice No.";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(39, 17);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 15);
            this.Label7.TabIndex = 55;
            this.Label7.Text = "Customer";
            // 
            // Label1
            // 
            this.Label1.AccessibleDescription = "lblSerNo";
            this.Label1.AccessibleName = "lblSerNo";
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(68, 141);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(32, 15);
            this.Label1.TabIndex = 53;
            this.Label1.Text = "Date";
            // 
            // cmbInvoceNo
            // 
            this.cmbInvoceNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbInvoceNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbInvoceNo.FormattingEnabled = true;
            this.cmbInvoceNo.Location = new System.Drawing.Point(106, 107);
            this.cmbInvoceNo.Name = "cmbInvoceNo";
            this.cmbInvoceNo.Size = new System.Drawing.Size(199, 23);
            this.cmbInvoceNo.TabIndex = 70;
            this.cmbInvoceNo.SelectedIndexChanged += new System.EventHandler(this.cmbInvoceNo_SelectedIndexChanged);
            this.cmbInvoceNo.TextChanged += new System.EventHandler(this.cmbInvoceNo_TextChanged);
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.Location = new System.Drawing.Point(105, 134);
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Size = new System.Drawing.Size(200, 22);
            this.dtpInvoiceDate.TabIndex = 71;
            this.dtpInvoiceDate.ValueChanged += new System.EventHandler(this.dtpInvoiceDate_ValueChanged);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Location = new System.Drawing.Point(106, 79);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Size = new System.Drawing.Size(199, 22);
            this.txtCustomerName.TabIndex = 73;
            this.txtCustomerName.TextChanged += new System.EventHandler(this.txtCustomerName_TextChanged);
            // 
            // label3
            // 
            this.label3.AccessibleDescription = "lblSerNo";
            this.label3.AccessibleName = "lblSerNo";
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 15);
            this.label3.TabIndex = 74;
            this.label3.Text = "Customer Name";
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.ItemHeight = 15;
            this.lbResult.Location = new System.Drawing.Point(105, 33);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(199, 19);
            this.lbResult.TabIndex = 75;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // chLbCustomerTickets
            // 
            this.chLbCustomerTickets.BackColor = System.Drawing.Color.Gray;
            this.chLbCustomerTickets.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLbCustomerTickets.ForeColor = System.Drawing.Color.White;
            this.chLbCustomerTickets.FormattingEnabled = true;
            this.chLbCustomerTickets.Location = new System.Drawing.Point(724, 78);
            this.chLbCustomerTickets.Name = "chLbCustomerTickets";
            this.chLbCustomerTickets.Size = new System.Drawing.Size(322, 361);
            this.chLbCustomerTickets.TabIndex = 76;
            this.chLbCustomerTickets.SelectedIndexChanged += new System.EventHandler(this.chLbCustomerTickets_SelectedIndexChanged);
            // 
            // cmbCustomerName
            // 
            this.cmbCustomerName.FormattingEnabled = true;
            this.cmbCustomerName.Location = new System.Drawing.Point(105, 14);
            this.cmbCustomerName.Name = "cmbCustomerName";
            this.cmbCustomerName.Size = new System.Drawing.Size(199, 23);
            this.cmbCustomerName.TabIndex = 77;
            this.cmbCustomerName.SelectedIndexChanged += new System.EventHandler(this.cmbCustomerName_SelectedIndexChanged);
            this.cmbCustomerName.TextChanged += new System.EventHandler(this.cmbCustomerName_TextChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.Red;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(151, 456);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(112, 32);
            this.btnRemove.TabIndex = 78;
            this.btnRemove.Text = "Remove ";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(809, 456);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(99, 32);
            this.btnAdd.TabIndex = 79;
            this.btnAdd.Text = "Add ";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnLRefresh
            // 
            this.btnLRefresh.BackgroundImage = global::SerhanTravel.Properties.Resources.if_view_refresh_118801__1_;
            this.btnLRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnLRefresh.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.btnLRefresh.Location = new System.Drawing.Point(1040, 61);
            this.btnLRefresh.Name = "btnLRefresh";
            this.btnLRefresh.Size = new System.Drawing.Size(28, 25);
            this.btnLRefresh.TabIndex = 81;
            this.btnLRefresh.UseVisualStyleBackColor = true;
            this.btnLRefresh.Click += new System.EventHandler(this.btnLRefresh_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(487, 492);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(104, 36);
            this.btnCancel.TabIndex = 82;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // TStripNew
            // 
            this.TStripNew.AccessibleDescription = "TStripNew";
            this.TStripNew.AccessibleName = "TStripNew";
            this.TStripNew.AutoSize = false;
            this.TStripNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripNew.Image = ((System.Drawing.Image)(resources.GetObject("TStripNew.Image")));
            this.TStripNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripNew.Name = "TStripNew";
            this.TStripNew.Size = new System.Drawing.Size(40, 38);
            this.TStripNew.ToolTipText = "Add New Invoice";
            this.TStripNew.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // TStripDelete
            // 
            this.TStripDelete.AccessibleDescription = "TStripDelete";
            this.TStripDelete.AccessibleName = "TStripDelete";
            this.TStripDelete.AutoSize = false;
            this.TStripDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("TStripDelete.Image")));
            this.TStripDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripDelete.Name = "TStripDelete";
            this.TStripDelete.Size = new System.Drawing.Size(40, 38);
            this.TStripDelete.ToolTipText = "Delete Invoice";
            this.TStripDelete.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AccessibleDescription = "TStrip";
            this.toolStrip1.AccessibleName = "TStrip";
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.NavajoWhite;
            this.toolStrip1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addButton,
            this.toolStripSeparator5,
            this.saveButton,
            this.toolStripSeparator6,
            this.deleteButton,
            this.toolStripSeparator7,
            this.printButton,
            this.toolStripSeparator8,
            this.toolStripButton5,
            this.toolStripButton6});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1082, 58);
            this.toolStrip1.Stretch = true;
            this.toolStrip1.TabIndex = 90;
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // addButton
            // 
            this.addButton.AccessibleDescription = "TStripNew";
            this.addButton.AccessibleName = "TStripNew";
            this.addButton.AutoSize = false;
            this.addButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addButton.Image = global::SerhanTravel.Properties.Resources.New021;
            this.addButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.addButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(40, 38);
            this.addButton.ToolTipText = "Add New Invoice";
            this.addButton.Click += new System.EventHandler(this.TStripNew_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 58);
            // 
            // saveButton
            // 
            this.saveButton.AccessibleDescription = "TStripSave";
            this.saveButton.AccessibleName = "TStripSave";
            this.saveButton.AutoSize = false;
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(40, 38);
            this.saveButton.ToolTipText = "Save Changes";
            this.saveButton.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 58);
            // 
            // deleteButton
            // 
            this.deleteButton.AccessibleDescription = "TStripDelete";
            this.deleteButton.AccessibleName = "TStripDelete";
            this.deleteButton.AutoSize = false;
            this.deleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteButton.Image")));
            this.deleteButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.deleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(40, 38);
            this.deleteButton.ToolTipText = "Delete Invoice";
            this.deleteButton.Click += new System.EventHandler(this.TStripDelete_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 58);
            // 
            // printButton
            // 
            this.printButton.AccessibleDescription = "TStripPrint";
            this.printButton.AccessibleName = "TStripPrint";
            this.printButton.AutoSize = false;
            this.printButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printButton.Image = global::SerhanTravel.Properties.Resources.Printer011;
            this.printButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.printButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(40, 38);
            this.printButton.ToolTipText = "Print Invoice";
            this.printButton.Click += new System.EventHandler(this.TStripPrint_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.AccessibleDescription = "TStripSpace00";
            this.toolStripSeparator8.AccessibleName = "TStripSpace00";
            this.toolStripSeparator8.AutoSize = false;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(80, 40);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.AccessibleDescription = "TStripBackword";
            this.toolStripButton5.AccessibleName = "TStripBackword";
            this.toolStripButton5.AutoSize = false;
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(40, 38);
            this.toolStripButton5.ToolTipText = "Backword";
            this.toolStripButton5.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.AccessibleDescription = "TStripForword";
            this.toolStripButton6.AccessibleName = "TStripForword";
            this.toolStripButton6.AutoSize = false;
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(40, 38);
            this.toolStripButton6.Text = "ToolStripButton1";
            this.toolStripButton6.ToolTipText = "Forword";
            this.toolStripButton6.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // nmDiscount
            // 
            this.nmDiscount.Location = new System.Drawing.Point(89, 29);
            this.nmDiscount.Name = "nmDiscount";
            this.nmDiscount.Size = new System.Drawing.Size(98, 20);
            this.nmDiscount.TabIndex = 91;
            this.nmDiscount.ValueChanged += new System.EventHandler(this.nmDiscount_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 92;
            this.label4.Text = "Discount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(193, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 18);
            this.label5.TabIndex = 93;
            this.label5.Text = "%";
            // 
            // txtnetPrice
            // 
            this.txtnetPrice.AccessibleDescription = "txtSerNo";
            this.txtnetPrice.AccessibleName = "txtSerNo";
            this.txtnetPrice.Location = new System.Drawing.Point(89, 70);
            this.txtnetPrice.Name = "txtnetPrice";
            this.txtnetPrice.ReadOnly = true;
            this.txtnetPrice.Size = new System.Drawing.Size(98, 20);
            this.txtnetPrice.TabIndex = 95;
            // 
            // label6
            // 
            this.label6.AccessibleDescription = "lblSerNo";
            this.label6.AccessibleName = "lblSerNo";
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 94;
            this.label6.Text = "Net Price";
            // 
            // lblTotalPiceSymbol
            // 
            this.lblTotalPiceSymbol.AutoSize = true;
            this.lblTotalPiceSymbol.Location = new System.Drawing.Point(193, 6);
            this.lblTotalPiceSymbol.Name = "lblTotalPiceSymbol";
            this.lblTotalPiceSymbol.Size = new System.Drawing.Size(0, 13);
            this.lblTotalPiceSymbol.TabIndex = 96;
            // 
            // lblNetPriceSymbol
            // 
            this.lblNetPriceSymbol.AutoSize = true;
            this.lblNetPriceSymbol.Location = new System.Drawing.Point(190, 73);
            this.lblNetPriceSymbol.Name = "lblNetPriceSymbol";
            this.lblNetPriceSymbol.Size = new System.Drawing.Size(0, 13);
            this.lblNetPriceSymbol.TabIndex = 97;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel3.Controls.Add(this.lbResult);
            this.panel3.Controls.Add(this.txtCustomerName);
            this.panel3.Controls.Add(this.Label1);
            this.panel3.Controls.Add(this.Label7);
            this.panel3.Controls.Add(this.lblInvoiceNo);
            this.panel3.Controls.Add(this.cmbInvoceNo);
            this.panel3.Controls.Add(this.dtpInvoiceDate);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cmbCustomerName);
            this.panel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(46, 78);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(324, 192);
            this.panel3.TabIndex = 109;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.cmbCurrency);
            this.panel2.Controls.Add(this.Label12);
            this.panel2.Controls.Add(this.txtRate);
            this.panel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.Control;
            this.panel2.Location = new System.Drawing.Point(376, 78);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(287, 84);
            this.panel2.TabIndex = 110;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 90;
            this.label8.Text = "Rate";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MediumOrchid;
            this.panel1.Controls.Add(this.nmDiscount);
            this.panel1.Controls.Add(this.Label2);
            this.panel1.Controls.Add(this.txtTotalPrice);
            this.panel1.Controls.Add(this.lblNetPriceSymbol);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblTotalPiceSymbol);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtnetPrice);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(376, 168);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 102);
            this.panel1.TabIndex = 111;
            // 
            // frmTicketsInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(1082, 575);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnLRefresh);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.chLbCustomerTickets);
            this.Controls.Add(this.stsLblInfo);
            this.Controls.Add(this.dgvTickets);
            this.Name = "frmTicketsInvoice";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tickets Invoices";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTicketsInvoice_FormClosing);
            this.Load += new System.EventHandler(this.frmTicketsInvoice_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frmTicketsInvoice_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTickets)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmDiscount)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.TextBox txtRate;
        internal System.Windows.Forms.ComboBox cmbCurrency;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox txtTotalPrice;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label stsLblInfo;
        internal System.Windows.Forms.ToolStripButton TStripNew;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton TStripDelete;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton TStripPrint;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripButton TStripForword;
        internal System.Windows.Forms.Label lblInvoiceNo;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.ComboBox cmbInvoceNo;
        private System.Windows.Forms.DateTimePicker dtpInvoiceDate;
        private System.Windows.Forms.TextBox txtCustomerName;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.CheckedListBox chLbCustomerTickets;
        private System.Windows.Forms.ComboBox cmbCustomerName;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnLRefresh;
       
        private System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.ToolStrip toolStrip1;
        internal System.Windows.Forms.ToolStripButton addButton;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        internal System.Windows.Forms.ToolStripButton saveButton;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        internal System.Windows.Forms.ToolStripButton deleteButton;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        internal System.Windows.Forms.ToolStripButton printButton;
        internal System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        internal System.Windows.Forms.ToolStripButton toolStripButton5;
        internal System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.NumericUpDown nmDiscount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.TextBox txtnetPrice;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.DataGridView dgvTickets;
        private System.Windows.Forms.Label lblTotalPiceSymbol;
        private System.Windows.Forms.Label lblNetPriceSymbol;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TicketNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Destination;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Currency;
        private System.Windows.Forms.DataGridViewTextBoxColumn Traveller;
        private System.Windows.Forms.DataGridViewTextBoxColumn FlightNo;
    }
}