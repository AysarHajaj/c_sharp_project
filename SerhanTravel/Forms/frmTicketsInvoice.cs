﻿using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Media;
using SerhanTravel.Print;
using SerhanTravel.Connections;

namespace SerhanTravel.Forms
{
    public partial class frmTicketsInvoice : Form
    {
        bool canUpdateOnly;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList;
        List<clsTicketInvoice> CustomerInvoiceList;
        List<clsTickets> ticketList;
        List<int> ticketListIDS;
        List<clsTickets> CustomerticketList;
         TicketInvoice invoiceConnection;
        Customer customerConnection;
        clsTicketInvoice invoiceData;
        clsCustomer customerData;
        bool skip = false;
        bool DataChanged = false;
        bool iamAdding = false;
        int InvoiceId = 0;
        int InvoiceSavedID=0;
        private int CustomerSavedID;
        int CustomerID = 0;
        Currency currencyConnection;
        private List<clsCurrency> currencies;
        decimal totalPrice = 0;
        decimal netPrice = 0;
        private bool allowRemove=false;
        string currencyFormat;
        clsAccessRights right;
        bool canAddOnly = false;
        internal frmTicketsInvoice(clsAccessRights right)
        {
            InitializeComponent();
            dgvTickets.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvTickets.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvTickets.MultiSelect = false;
            chLbCustomerTickets.HorizontalScrollbar = true;
            //chLbCustomerTickets.ScrollAlwaysVisible = true;
            dgvTickets.ScrollBars = ScrollBars.Both;
            this.right = right;

        }

        private void ResetFields()
        {
            cmbInvoceNo.DataSource = null;
            dgvTickets.DataSource = null;

            cmbCurrency.Text = "";
            cmbInvoceNo.Text = "";
            dgvTickets.Rows.Clear();
            txtCustomerName.Clear();
            txtRate.Clear();
            txtTotalPrice.Clear();
            txtnetPrice.Clear();
            dtpInvoiceDate.Value = DateTime.Today;
            lblNetPriceSymbol.Text = "";
            lblTotalPiceSymbol.Text = "";

            cmbCurrency.SelectedIndex = -1;
            cmbInvoceNo.SelectedIndex = -1;

            stsLblInfo.Text = "";
            ticketList.Clear();
            ticketListIDS.Clear();
            totalPrice = 0;
            netPrice = 0;
            nmDiscount.Value = 0;
            DataChanged = false;
        }
        private void frmTicketsInvoice_Load(object sender, EventArgs e)
        {
            accessRight();
            currencyFormat = "#,##0.00;-#,##0.00;Zero";
            lbResult.Visible = false;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            currencyConnection = new Currency();
            dtpInvoiceDate.Value = DateTime.Today;
            customerConnection = new Customer();
            ticketList = new List<clsTickets>();
            ticketListIDS = new List<int>();
            invoiceConnection = new TicketInvoice();

            customerList = customerConnection.CustomersArrayList();
            CustomerInvoiceList = new List<clsTicketInvoice>();

         

            dgvTickets.AutoGenerateColumns = false;


            dgvTickets.Columns[0].DataPropertyName = "TicketNo";
            dgvTickets.Columns[1].DataPropertyName = "Destination";
            dgvTickets.Columns[2].DataPropertyName = "Price";
            dgvTickets.Columns[3].DataPropertyName = "CurrecnySymbol";
            dgvTickets.Columns[4].DataPropertyName = "TravellerName";
            dgvTickets.Columns[5].DataPropertyName = "flightNo";
            FillCombobox();
            ResetFields();
            chLbCustomerTickets.DataSource = null;
            InvoiceId = 0;
            DataChanged = false;
          
        }

        private void DisplayData(int custmId,int invoiceId)
        {
            iamAdding = false;
            cmbCustomerName.DataSource = null;
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedValue = custmId;
            cmbInvoceNo.SelectedValue = invoiceId;
            skip = true;
            DataChanged = false;
        }

        private void FillCombobox()
        {
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;

            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedIndex = -1;
          

            DataChanged = false;
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
          
          
            lbResult.Visible = false;
            btnRemove.Visible = false;
            btnAdd.Visible = false;

            if (cmbCustomerName.SelectedIndex!=-1&&customerList.Count>0)
            {
               
              ResetFields();
              InvoiceId = 0;

                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                CustomerticketList = invoiceConnection.CustomerTicketsArrayList(CustomerID);

                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = CustomerticketList;
                chLbCustomerTickets.DisplayMember = "ToStR";

                
                if (CustomerID != 0)
                {
                    customerData = customerList[cmbCustomerName.SelectedIndex];
                    txtCustomerName.Text = customerData.CustomerName;
                }


                if (iamAdding)
                {
                  
                    cmbInvoceNo.DataSource = null;
                    cmbInvoceNo.SelectedIndex = -1;
                    CustomerInvoiceList.Clear();
                    ticketList.Clear();
                    ticketListIDS.Clear();
                    return;
                }

                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(CustomerID);
        
                if (CustomerInvoiceList.Count>0)
                {
                    cmbInvoceNo.Visible = true;
                    lblInvoiceNo.Visible = true;
                    
                    cmbInvoceNo.ValueMember = "invoiceId";
                    cmbInvoceNo.DataSource = CustomerInvoiceList;
                    cmbInvoceNo.DisplayMember = "invoiceId";
                    cmbInvoceNo.SelectedValue = InvoiceId;


                }
                else
                {
                   
                    
                    cmbInvoceNo.Visible = false;
                    lblInvoiceNo.Visible = false;
                    ResetFields();
                    if (CustomerID != 0)
                    {
                       
                        txtCustomerName.Text = customerData.CustomerName;
                    }


                }



                DataChanged = false;
                skip = true;

            }
            else
            {
                skip = false;
            }
        
            DataChanged = false;
        }

        private void cmbInvoceNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Visible = false;
            btnAdd.Visible = false;
            if (cmbInvoceNo.SelectedIndex!=-1&& CustomerInvoiceList.Count>0)
            {
                
                InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                
                invoiceData = CustomerInvoiceList[cmbInvoceNo.SelectedIndex];

                     dtpInvoiceDate.Value = invoiceData.InvoiceDate;
                     txtCustomerName.Text = invoiceData.CustomerName;
               

                ticketListIDS.Clear();
                ticketList.Clear();
          
                ticketList = invoiceConnection.TicketsInvoicesArrayList(InvoiceId);
         
                nmDiscount.Value =Convert.ToDecimal( invoiceData.Discount);

            
                for (int i=0;i<ticketList.Count;i++)
                {
                    ticketListIDS.Add(ticketList[i].TicketId);
                }

                dgvTickets.DataSource = null;
                  totalPrice = 0;
             if (ticketList.Count>0)
                {
                  
                 dgvTickets.DataSource = ticketList;
                    

                    for (int i = 0; i < dgvTickets.Rows.Count; i++)
                    {
                        int currencyId = ticketList.ElementAt(i).CurrencyId;
                        decimal Rate=1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if(currency!=null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }
                        
                        totalPrice += (decimal)dgvTickets.Rows[i].Cells[2].Value*Rate;
                    }
                    dgvTickets.Refresh();
                    dgvTickets.ClearSelection();

                }
                

                netPrice = totalPrice - totalPrice * (invoiceData.Discount * Convert.ToDecimal(0.01));
              
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;

                    txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                    txtnetPrice.Text = (netPrice / currency.CurrencyRate).ToString(currencyFormat);
                    lblTotalPiceSymbol.Text = currency.CurrencySymbol;
                    lblNetPriceSymbol.Text = currency.CurrencySymbol;
                }
                else
                {
                    txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                    txtnetPrice.Text = netPrice.ToString(currencyFormat);
                    lblTotalPiceSymbol.Text = "L.L";
                    lblNetPriceSymbol.Text = "L.L";
                }

               


            }
            DataChanged = false;
         
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString(currencyFormat);
                txtTotalPrice.Text = (totalPrice / currency.CurrencyRate).ToString(currencyFormat);
                txtnetPrice.Text = (netPrice / currency.CurrencyRate).ToString(currencyFormat);
                lblTotalPiceSymbol.Text = currency.CurrencySymbol;
                lblNetPriceSymbol.Text = currency.CurrencySymbol;

            }
            else
            {
                txtnetPrice.Text = (netPrice).ToString(currencyFormat);
                txtTotalPrice.Text = totalPrice.ToString(currencyFormat);
                lblTotalPiceSymbol.Text = "L.L";
                lblNetPriceSymbol.Text = "L.L";

            }
        }

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
              
                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
              
                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                DataChanged = false;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }
        private void SaveDataInfo()
        {

            if(cmbCustomerName.SelectedIndex==-1)
            {
                MessageBox.Show("Please select a customer");
                if(iamAdding)
                {
                    btnCancel.Visible = true;
                }
                return;
            }
            else if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
            {
                MessageBox.Show("Please Enter the Customer Name.");
                return;
            }
            

            bool RecExists;
            try
            {
                //int selectedvalue;
                clsTicketInvoice invoice = new clsTicketInvoice();
                

                if (cmbCustomerName.SelectedIndex!=-1)
                {
                    invoice.CustomerId = int.Parse(cmbCustomerName.SelectedValue.ToString());
                }
                else
                {
                    invoice.CustomerId = 0;
                }

                if (cmbInvoceNo.SelectedIndex == -1)
                {
                    invoice.InvoiceId = 0;
                }
                else
                {
                    invoice.InvoiceId = int.Parse(cmbInvoceNo.SelectedValue.ToString());
                }

                if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                {
                    invoice.CustomerName = string.Empty;
                }
                else
                {
                    invoice.CustomerName = txtCustomerName.Text;
                }
                invoice.Discount =Convert.ToDecimal(nmDiscount.Value);
                invoice.NetPrice = netPrice;



                invoice.InvoiceDate = dtpInvoiceDate.Value;

                RecExists = invoiceConnection.InvoiceIDExists(invoice.InvoiceId);


                if (RecExists == true)
                {
                    invoiceConnection.UpdateInvoiceInfo(invoice);
                    invoiceConnection.UpdateInvoiceDetails(invoice.InvoiceId, ticketList);
                    stsLblInfo.Text = "Changes has been saved successfully";

                }
                else
                {
                    iamAdding = true;
                    invoiceConnection.AddNewTicketInvoice(invoice);
                    int invoicenum = invoiceConnection.ReadLastNo();
                    invoiceConnection.AddInvoiceTickets(invoicenum, ticketList);
                    invoice.InvoiceId = invoicenum;
                    stsLblInfo.Text = "New Invoice has been added successfully";

                }

                InvoiceSavedID= invoice.InvoiceId;
                InvoiceId = InvoiceSavedID;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                simpleSound.Play();

                CustomerInvoiceList.Clear();
                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(CustomerID);
           

             
                 if(CustomerInvoiceList.Count>0)
                {
                    lblInvoiceNo.Visible = true;
                    cmbInvoceNo.Visible = true;
                }
                    DisplayData(CustomerID, InvoiceId);
                    
         
                DataChanged = false;
            }
            catch (Exception ex)
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
                DataChanged = false;
                stsLblInfo.Text = "ERROR: Not saved, please check";
                MessageBox.Show(ex.Message);
            }
        }



        private void txtCustomerName_TextChanged(object sender, EventArgs e)
        {
            if(!iamAdding)
                DataChanged = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if (DataChanged)
            {
                btnCancel.Visible = false;
                if (canUpdateOnly && this.InvoiceId == 0)
                {
                    return;
                }
                SaveDataInfo();
                if (canAddOnly)
                {
                   saveButton.Enabled = false;
                    readOnlyEveryThing();
                }
            }
        }

        private void dtpInvoiceDate_ValueChanged(object sender, EventArgs e)
        {
            if (!iamAdding)
                DataChanged = true;
        }

        private void cmbInvoceNo_TextChanged(object sender, EventArgs e)
        {
            //DataChanged = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           

            foreach (var item in chLbCustomerTickets.CheckedItems)
            {
                clsTickets itemdata = (clsTickets)item;
                if (ticketListIDS.Contains(itemdata.TicketId))
                {
                    MessageBox.Show("You choose a selected Item");
                    break;
                }
                else
                {



                    dgvTickets.DataSource = null;
             
                    ticketList.Add(itemdata);
                    ticketListIDS.Add(itemdata.TicketId);
                  
                    
                    dgvTickets.DataSource = ticketList;
                    dgvTickets.Refresh();
                   

                    totalPrice = 0;

                    for (int m = 0; m < dgvTickets.Rows.Count; m++)
                    {
                        int currencyId = ticketList.ElementAt(m).CurrencyId;
                        decimal Rate = 1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if (currency != null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }
                      

                        totalPrice += (decimal)dgvTickets.Rows[m].Cells[2].Value * Rate;
                        

                    }
                    decimal rate;
                    if(cmbCurrency.SelectedIndex!=-1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        rate = currency.CurrencyRate;
                        lblNetPriceSymbol.Text = currency.CurrencySymbol;
                        lblTotalPiceSymbol.Text = currency.CurrencySymbol;
                    }
                    else
                    {
                        rate = 1;
                        lblNetPriceSymbol.Text = "L.L";
                        lblTotalPiceSymbol.Text ="L.L";
                    }
                    

                    txtTotalPrice.Text = (totalPrice/ rate).ToString(currencyFormat);
                    netPrice = totalPrice - totalPrice * (decimal.Parse(nmDiscount.Value.ToString()) * Convert.ToDecimal(0.01));
                    txtnetPrice.Text =( netPrice / rate).ToString(currencyFormat);
                    DataChanged = true;
                }

            }

            btnAdd.Visible = false;




            
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgvTickets.RowCount > 0&&ticketList.Count>0&&allowRemove)
            {
                allowRemove = false;

                if (dgvTickets.SelectedRows.Count <= 0)
                {
                    btnRemove.Visible = false;
                    return;
                }
                if (MessageBox.Show("Are you sure ??", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        int index = dgvTickets.SelectedRows[0].Index;
                        ticketList.RemoveAt(index);
                        ticketListIDS.RemoveAt(index);

                        dgvTickets.DataSource = null;
      
                    dgvTickets.Rows.Clear();
                        dgvTickets.DataSource = ticketList;
              
                    totalPrice = 0;

                    for (int m = 0; m < dgvTickets.Rows.Count; m++)
                    {
                        int currencyId = ticketList.ElementAt(m).CurrencyId;
                        decimal Rate = 1;
                        if (currencyId != 0)
                        {
                            clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
                            if (currency != null)
                            {
                                Rate = currency.CurrencyRate;
                            }
                            else
                            {
                                Rate = 1;
                            }
                        }


                        totalPrice += (decimal)dgvTickets.Rows[m].Cells[2].Value * Rate;


                    }
                    decimal rate;
                    if (cmbCurrency.SelectedIndex != -1)
                    {
                        clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                        rate = currency.CurrencyRate;
                        lblNetPriceSymbol.Text = currency.CurrencySymbol;
                        lblTotalPiceSymbol.Text = currency.CurrencySymbol;
                    }
                    else
                    {
                        rate = 1;
                        lblNetPriceSymbol.Text = "L.L";
                        lblTotalPiceSymbol.Text = "L.L";
                    }


                    txtTotalPrice.Text = (totalPrice / rate).ToString(currencyFormat);
                    netPrice = totalPrice - totalPrice * (decimal.Parse(nmDiscount.Value.ToString()) * Convert.ToDecimal( 0.01));
                    txtnetPrice.Text = (netPrice / rate).ToString(currencyFormat);
                        dgvTickets.ClearSelection();
                        DataChanged = true;

                    }
                
            }
            else
            {
                allowRemove = false;
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
            btnRemove.Visible = false;
        }

        private void btnLRefresh_Click(object sender, EventArgs e)
        {
            if(cmbCustomerName.SelectedIndex!=-1)
            {
                CustomerticketList = invoiceConnection.CustomerTicketsArrayList(CustomerID);
               
                chLbCustomerTickets.ValueMember = "ticketId";
                chLbCustomerTickets.DataSource = CustomerticketList;
                chLbCustomerTickets.DisplayMember = "ToStR";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblInvoiceNo.Visible = true;
            cmbInvoceNo.Visible = true;
            InvoiceId = InvoiceSavedID;
            CustomerID = CustomerSavedID;
            btnCancel.Visible = false;
            iamAdding = false;
            ResetFields();
            DisplayData(CustomerID,InvoiceId);
            DataChanged = false;
            if (canAddOnly)
            {
                saveButton.Enabled = false;
                readOnlyEveryThing();
            }
        }

        private void TStripNew_Click(object sender, EventArgs e)
        {
            iamAdding = true;
            lblInvoiceNo.Visible = false;
            cmbInvoceNo.Visible = false;
            if (canAddOnly)
            {
                saveButton.Enabled = true;
                unReadOnlyEveryThing();
            }
            stsLblInfo.Text = "Add Invoice";
            SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged)
            {
                DialogResult myreply = MessageBox.Show("Do you want to save changes ?", "Save Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myreply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    simpleSound.Play();
                }
            }

            InvoiceSavedID = InvoiceId;
            CustomerSavedID = CustomerID;

            InvoiceId = 0;
            cmbCustomerName.SelectedIndex = -1;
            ResetFields();
            chLbCustomerTickets.DataSource = null;
            btnCancel.Visible = true;
            DataChanged = false;

        }

        private void TStripDelete_Click(object sender, EventArgs e)
        {
            if (this.InvoiceId != 0)
            {
                try
                {
                    
                    SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.notify);
                    DialogResult myreply = MessageBox.Show("Are you sure?", "Delete Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myreply == DialogResult.Yes)
                    {
                        bool myResult1 = invoiceConnection.DeleteInvoiceTickets(InvoiceId);
                        if (!myResult1)
                        {
                            return;
                        }
                        else
                        {

                            bool myResult2 = invoiceConnection.DeleteInVoice(InvoiceId);
                            if(myResult2)
                            {
                                ResetFields();
                                int index = CustomerInvoiceList.IndexOf(invoiceData);
                                CustomerInvoiceList = invoiceConnection.CustomerInvoicesArrayList(CustomerID);
                                index -= 1;
                                InvoiceId = 0;
                                if (index >= 0)
                                {
                                    InvoiceId = CustomerInvoiceList[index].InvoiceId;
                                }
                                else if(index==-1&& CustomerInvoiceList.Count>0)
                                {
                                    index = 0;
                                    InvoiceId = CustomerInvoiceList[index].InvoiceId;
                                }

                               DisplayData(CustomerID, InvoiceId);
                                DataChanged = false;
                                stsLblInfo.Text = "Successfully Deleted";
                                simpleSound.Play();
                            }
                           
                        }
                    }

                }
                catch (Exception ex)
                {
                    DataChanged = false;
                    stsLblInfo.Text = "Error: Deleting the Invoice";
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if ((cmbCustomerName.Items.Count > 0))
            {
                if (cmbCustomerName.SelectedIndex == 0 || cmbCustomerName.SelectedIndex == -1)
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.Items.Count - 1;
                }
                else
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {

            if ((cmbCustomerName.Items.Count > 0))
            {
                if (cmbCustomerName.SelectedIndex == cmbCustomerName.Items.Count - 1 || cmbCustomerName.SelectedIndex == -1)
                {
                    cmbCustomerName.SelectedIndex = 0;
                }
                else
                {
                    cmbCustomerName.SelectedIndex = cmbCustomerName.SelectedIndex + 1;
                }
            }
        }

        private void dgvTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvTickets.RowCount > 0)
            {
               
                if (e.RowIndex >= 0)
                {
           
                    allowRemove = true;
                    btnRemove.Visible = true;
                }
            }
        }

        private void frmTicketsInvoice_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cmbInvoceNo.SelectedIndex != -1)
            {
                InvoiceId = Convert.ToInt32(cmbInvoceNo.SelectedValue);
            }
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                }
            }
        }

      
        private void TStripPrint_Click(object sender, EventArgs e)
        {
          

            if (cmbInvoceNo.SelectedIndex!=-1&&CustomerInvoiceList.Count>0&&ticketList.Count>0)
            {
                
               
                if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                {
                    MessageBox.Show("Please enter Customer Name ");
                    return;
                }
                clsCurrency currency;
                if (cmbCurrency.SelectedIndex != -1)
                {
                    currency = (clsCurrency)cmbCurrency.SelectedItem;

                }
                else
                {
                    currency = new clsCurrency();
                    currency.CurrencyRate = 1;
                    currency.CurrencyId = 0;
                    currency.CurrencySymbol = "L.L";
                }
                if (DataChanged == true)
                {
                   
                    DialogResult myReply = MessageBox.Show("Do you want to save changes before printing ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (myReply == DialogResult.Yes)
                    {
                        SaveDataInfo();
                        cmbCurrency.SelectedValue = currency.CurrencyId;
                        
                    }
                }
                clsTicketInvoice invoice = (clsTicketInvoice)cmbInvoceNo.SelectedItem;
                invoice.CustomerName = txtCustomerName.Text;
               
               printticketInvoice frm = new printticketInvoice(ticketList,invoice,double.Parse(txtTotalPrice.Text),double.Parse(txtnetPrice.Text),currency);
                frm.Show();
            }
            else
            {
                SoundPlayer simpleSound = new SoundPlayer(Properties.Resources.ding);
                simpleSound.Play();
            }

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void nmDiscount_ValueChanged(object sender, EventArgs e)
        {
            if (totalPrice != 0)
            {
                netPrice = totalPrice - totalPrice * decimal.Parse(nmDiscount.Value.ToString()) * Convert.ToDecimal(0.01);
                if (cmbCurrency.SelectedIndex != -1)
                {
                    clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                    txtnetPrice.Text = (netPrice/currency.CurrencyRate).ToString(currencyFormat);
                }
                else
                {
                    txtnetPrice.Text = netPrice.ToString(currencyFormat);
                }
                
                DataChanged = true;
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void accessRight()
        {
            if (!right.Adding)
            {
                addButton.Enabled = false;
            }
            if (!right.Deleting)
            {
                deleteButton.Enabled = false;
            }
            if (!right.Printing)
            {
                printButton.Enabled = false;
            }
            if (!right.Updating)
            {
                saveButton.Enabled = false;
                readOnlyEveryThing();
            }
            if (!right.Updating && right.Adding)
            {
                canAddOnly = true;
            }
            if (!right.Adding && right.Updating)
            {
                canUpdateOnly = true;
            }
        }

        private void readOnlyEveryThing()
        {
            //cmbCurrency.Enabled = false;
            //cmbInvoceNo.Enabled = false;
            //cmbCustomerName.Enabled = false;
            nmDiscount.Enabled = false;
            //chLbCustomerTickets.Enabled = false;
            dtpInvoiceDate.Enabled = false;

            btnAdd.Enabled = false;
            btnRemove.Enabled = false;
            //btnLRefresh.Enabled = false;

            txtCustomerName.ReadOnly = true;


        }

        private void unReadOnlyEveryThing()
        {
            cmbCurrency.Enabled = true;
            cmbInvoceNo.Enabled = true;
            cmbCustomerName.Enabled = true;
            nmDiscount.Enabled = true;
            chLbCustomerTickets.Enabled = true;
            dtpInvoiceDate.Enabled = true;

            btnAdd.Enabled = true;
            btnRemove.Enabled = true;
            btnLRefresh.Enabled = true;

            txtCustomerName.ReadOnly = false;
        }

        private void chLbCustomerTickets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(chLbCustomerTickets.SelectedIndex!=-1)
            {
                btnAdd.Visible = true;
            }
        }

        private void frmTicketsInvoice_MouseClick(object sender, MouseEventArgs e)
        {
            lbResult.Visible = false;
        }
    }
}
