﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Forms
{
    public partial class frmUserAccess : Form
    {
        List<clsUser> UserList;
        int UserID = 0;
        bool DataChanged = false;
        bool skip ;
        List<clsScreen> screenList;
        clsAccessRights accessData;
        AccessRights accessConncetion;
        User userConnection;
        public frmUserAccess()
        {
            InitializeComponent();
           
        }

        private void frmUserAccess_Load(object sender, EventArgs e)
        {
            accessConncetion = new AccessRights();
            accessData = new clsAccessRights();
            userConnection = new User();
            UserList = userConnection.EmployeeUsersArrayList();
            if (UserList.Count > 0)
            {
                this.UserID = UserList[UserList.Count - 1].UserId;
            }
            skip = true;
            Display(0);
            screenList = new List<clsScreen>();
            screenList.Add(new clsScreen("   -----Screens-----   ", 0));
            screenList.Add(new clsScreen("Branches Info",1));
            screenList.Add(new clsScreen("Suppliers", 2));
            screenList.Add(new clsScreen("Customers", 3));
            screenList.Add(new clsScreen("Package Setup", 4));
            screenList.Add(new clsScreen("Tickets", 5));
            screenList.Add(new clsScreen("Countries ", 6));
            screenList.Add(new clsScreen("Cities", 7));
            screenList.Add(new clsScreen("Nationality", 8));
            screenList.Add(new clsScreen("Groups", 9));
            screenList.Add(new clsScreen("Airlines", 10));
            screenList.Add(new clsScreen("Bank", 11));
            screenList.Add(new clsScreen("Item", 12));
            screenList.Add(new clsScreen("Currency", 13));
            screenList.Add(new clsScreen("Add User", 14));
            screenList.Add(new clsScreen("Expenses Type ", 15));
            screenList.Add(new clsScreen("Expenses", 16));
            screenList.Add(new clsScreen("Supplier Opening Balance", 17));
            screenList.Add(new clsScreen("Customer Opening Balance", 18));
            screenList.Add(new clsScreen("Return Ticket To Supplier", 19));
            screenList.Add(new clsScreen("Return Item To Supplier", 20));
            screenList.Add(new clsScreen("Return Ticket From Customer", 21));
            screenList.Add(new clsScreen("Return Package From Customer", 22));
            screenList.Add(new clsScreen("Supplier Ticket Invoice", 23));
            screenList.Add(new clsScreen("Supplier Ticket Payments", 24));
            screenList.Add(new clsScreen("Supplier Item Invoice", 25));
            screenList.Add(new clsScreen("Supplier Item Payments", 26));
            screenList.Add(new clsScreen("Ticket Invoce", 27));
            screenList.Add(new clsScreen("Ticket Payment", 28));
            screenList.Add(new clsScreen("Package Invoice", 29));
            screenList.Add(new clsScreen("Package Payments", 30));
            screenList.Add(new clsScreen("   -----Reports-----   ", 0));
            screenList.Add(new clsScreen("Customer Statment Of Account", 31));
            screenList.Add(new clsScreen("Customer Ticket Sold", 32));
            screenList.Add(new clsScreen("Customer Packages Sold", 33));
            screenList.Add(new clsScreen("Customer Summary Of Payments", 34));
            screenList.Add(new clsScreen("Customer Check Status", 35));
            screenList.Add(new clsScreen("Customer Pending Payment", 36));
            screenList.Add(new clsScreen("Customer Returned Items", 37));
            screenList.Add(new clsScreen("Customer Payments", 38));
            screenList.Add(new clsScreen("Supplier Statment Of Account", 39));
            screenList.Add(new clsScreen("Supplier Ticket Issued", 40));
            screenList.Add(new clsScreen("Supplier Item Issued", 41));
            screenList.Add(new clsScreen("Supplier Payments", 42));
            screenList.Add(new clsScreen("Supplier Payments By Type", 43));
            screenList.Add(new clsScreen("Supplier Summary Of Payments", 44));
            screenList.Add(new clsScreen("Supplier Returned Items", 45));
            screenList.Add(new clsScreen("Sales By Type", 46));
            screenList.Add(new clsScreen("Summary Of Sales", 47));
            screenList.Add(new clsScreen("Number Of Packages Sold", 48));
            screenList.Add(new clsScreen("Daily Payments", 49));
            screenList.Add(new clsScreen("Expenses Reports", 50));
            screenList.Add(new clsScreen("Income Statment", 51));


            lbScreens.ValueMember = "ScreenId";
            lbScreens.DataSource = screenList;
            lbScreens.DisplayMember = "ScreenName";




        }

        public void Display(int uId)
        {
            if (UserList.Count > 0 && uId == 0)
            {
                cmbUser.ValueMember = "UserId";
                cmbUser.DataSource = UserList;
                cmbUser.DisplayMember = "Name";
                cmbUser.SelectedIndex = UserList.Count - 1;
                this.UserID = UserList[UserList.Count - 1].UserId;
            }
            else if (UserList.Count > 0 && uId != 0)
            {
                cmbUser.ValueMember = "UserId";
                cmbUser.DataSource = UserList;
                cmbUser.DisplayMember = "Name";
                cmbUser.SelectedValue = uId;
                this.UserID = uId;
            }
            else if (UserList.Count == 0)
            {
                ResetFeilds();
                this.UserID = 0;
            }
            
            DataChanged = false;
        }

        public void ResetFeilds()
        {
            cmbUser.SelectedIndex = -1;
        }

        private void cmbUser_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbUser.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {
                return;
            }
            clsUser[] result = (from i in UserList
                                    where i.Name.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {
                return; // return with listbox's Visible set to false if nothing found
            }
            else
            {
                lbResult.Items.Clear(); 
                lbResult.ValueMember = "UserId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "Name";
                lbResult.Visible = true; 
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                cmbUser.SelectedItem = lbResult.SelectedItem;
            }
            lbResult.Visible = false;
        }

        private void cmbUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbResult.Visible = false;
            if (cmbUser.SelectedIndex != -1 && UserList.Count > 0)
            {
                
                lbScreens.SelectedIndex = -1;
                skip = true;
            }
        }

        private void lbScreens_SelectedIndexChanged(object sender, EventArgs e)
        {
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();
                    
                }
            }


            chAccessRights.Items.Clear();
        ///    accessData = null;
           // accessData = new clsAccessRights();

            if (cmbUser.SelectedIndex!=-1&&lbScreens.SelectedIndex!=-1)
            {
               
                UserID = int.Parse(cmbUser.SelectedValue.ToString());
                int ScreenID = int.Parse(lbScreens.SelectedValue.ToString());
      
                if (ScreenID!=0)
                {

                    accessData = accessConncetion.getUserScreenRights(UserID, ScreenID);
                  
                    if (!accessConncetion.UserRightsExists(UserID, ScreenID))
                    {
                        accessData.UserId = UserID;
                        accessData.ScreenId = ScreenID;
                        accessData.CanAccess = false;
                        accessData.Adding = false;
                        accessData.Deleting = false;
                        accessData.Updating = false;
                        accessData.Printing = false;
                     
                    }
                    
                    if (ScreenID >= 1 && ScreenID <= 22)
                    {
                        chAccessRights.Items.Add("Allow Access");
                        chAccessRights.SetItemChecked(0, accessData.CanAccess);
                        chAccessRights.Items.Add("Allow Add");
                        chAccessRights.SetItemChecked(1,accessData.Adding);
                        chAccessRights.Items.Add("Allow Delete");
                        chAccessRights.SetItemChecked(2, accessData.Deleting);
                        chAccessRights.Items.Add("Allow Update");
                        chAccessRights.SetItemChecked(3, accessData.Updating);

                       

                    }
                    else if (ScreenID >= 23 && ScreenID <= 30)
                    {
                        chAccessRights.Items.Add("Allow Access");
                        chAccessRights.SetItemChecked(0, accessData.CanAccess);
                        chAccessRights.Items.Add("Allow Add");
                        chAccessRights.SetItemChecked(1, accessData.Adding);
                        chAccessRights.Items.Add("Allow Delete");
                        chAccessRights.SetItemChecked(2, accessData.Deleting);
                        chAccessRights.Items.Add("Allow Update");
                        chAccessRights.SetItemChecked(3, accessData.Updating);
                        chAccessRights.Items.Add("Allow Print");
                        chAccessRights.SetItemChecked(4, accessData.Printing);
                    }
                    else if (ScreenID > 30)
                    {
                        chAccessRights.Items.Add("Allow Access");
                        chAccessRights.SetItemChecked(0, accessData.CanAccess);

                    }
                    DataChanged = false;
                }
                
            }
        }

        private void chAccessRights_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            DataChanged = true;
        }

        private void TStripSave_Click(object sender, EventArgs e)
        {
            if(DataChanged)
            {
                
               SaveDataInfo();
              
            }
        }

        private void SaveDataInfo()
        {

            List<int> indexList = new List<int>();
            for (int i = 0; i < chAccessRights.Items.Count; i++)
            {
                if (chAccessRights.GetItemCheckState(i) == CheckState.Checked)
                {
                    indexList.Add(i);
                }

            }
            if (indexList.Contains(0))
            {
                accessData.CanAccess = true;
            }
            else
            {
                accessData.CanAccess = false;
            }
            if (indexList.Contains(1))
            {
                accessData.Adding = true;
            }
            else
            {
                accessData.Adding = false;
            }
            if (indexList.Contains(2))
            {
                accessData.Deleting = true;
            }
            else
            {
                accessData.Deleting = false;
            }
            if (indexList.Contains(3))
            {
                accessData.Updating = true;
            }
            else
            {
                accessData.Updating = false;
            }
            if (indexList.Contains(4))
            {
                accessData.Printing = true;
            }
            else
            {
                accessData.Printing = false;
            }

            try
            {
                if (accessConncetion.UserRightsExists(accessData.UserId, accessData.ScreenId))
                {
                    accessConncetion.UpdateUserAccessRights(accessData);

                }
                else
                {
                    accessConncetion.AddUserAccess(accessData);


                }
                SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
                notifySound.Play();
                stsLblInfo.Text = "Changed has been saved...";
                DataChanged = false;
            }
            catch(Exception ex)
            {
                SoundPlayer notifySound = new SoundPlayer(Properties.Resources.ding);
                notifySound.Play();
                MessageBox.Show(ex.Message.ToString());
            }

            
        }

        private void TStripBackword_Click(object sender, EventArgs e)
        {
            if (cmbUser.Items.Count > 0)
            {
                if (cmbUser.SelectedIndex == 0 || cmbUser.SelectedIndex == -1)
                {
                    int Users = cmbUser.Items.Count;
                    cmbUser.SelectedIndex = Users - 1;
                }
                else
                {
                    cmbUser.SelectedIndex = cmbUser.SelectedIndex - 1;
                }
            }
        }

        private void TStripForword_Click(object sender, EventArgs e)
        {
            if (cmbUser.Items.Count > 0)
            {
                if (cmbUser.SelectedIndex == cmbUser.Items.Count - 1 || cmbUser.SelectedIndex == -1)
                {

                    cmbUser.SelectedIndex = 0;
                }
                else
                {
                    cmbUser.SelectedIndex = cmbUser.SelectedIndex + 1;
                }
            }
        }

        private void frmUserAccess_FormClosing(object sender, FormClosingEventArgs e)
        {
            SoundPlayer notifySound = new SoundPlayer(Properties.Resources.notify);
            if (DataChanged == true)
            {
                DialogResult myReply = MessageBox.Show("Do you want to save changes ?", "Save changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (myReply == DialogResult.Yes)
                {
                    SaveDataInfo();
                    notifySound.Play();

                }
            }
        }
    }
}
