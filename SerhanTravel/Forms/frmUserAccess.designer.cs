﻿namespace SerhanTravel.Forms
{
    partial class frmUserAccess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUserAccess));
            this.lbResult = new System.Windows.Forms.ListBox();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chAccessRights = new System.Windows.Forms.CheckedListBox();
            this.stsInfo = new System.Windows.Forms.StatusStrip();
            this.stsLblInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbScreens = new System.Windows.Forms.ListBox();
            this.TStrip = new System.Windows.Forms.ToolStrip();
            this.TStripSave = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TStripBackword = new System.Windows.Forms.ToolStripButton();
            this.TStripForword = new System.Windows.Forms.ToolStripButton();
            this.stsInfo.SuspendLayout();
            this.TStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbResult
            // 
            this.lbResult.FormattingEnabled = true;
            this.lbResult.HorizontalScrollbar = true;
            this.lbResult.Location = new System.Drawing.Point(91, 81);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(194, 30);
            this.lbResult.TabIndex = 188;
            this.lbResult.Visible = false;
            this.lbResult.SelectedIndexChanged += new System.EventHandler(this.lbResult_SelectedIndexChanged);
            // 
            // cmbUser
            // 
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(91, 60);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(194, 21);
            this.cmbUser.TabIndex = 187;
            this.cmbUser.SelectedIndexChanged += new System.EventHandler(this.cmbUser_SelectedIndexChanged);
            this.cmbUser.TextChanged += new System.EventHandler(this.cmbUser_TextChanged);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = "lblSerNo";
            this.label1.AccessibleName = "lblSerNo";
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 186;
            this.label1.Text = "User Name";
            // 
            // chAccessRights
            // 
            this.chAccessRights.BackColor = System.Drawing.Color.Gray;
            this.chAccessRights.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chAccessRights.ForeColor = System.Drawing.Color.White;
            this.chAccessRights.FormattingEnabled = true;
            this.chAccessRights.Location = new System.Drawing.Point(349, 113);
            this.chAccessRights.Name = "chAccessRights";
            this.chAccessRights.Size = new System.Drawing.Size(302, 277);
            this.chAccessRights.TabIndex = 190;
            this.chAccessRights.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.chAccessRights_ItemCheck);
            // 
            // stsInfo
            // 
            this.stsInfo.AccessibleDescription = "stsInfo";
            this.stsInfo.AccessibleName = "stsInfo";
            this.stsInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsLblInfo});
            this.stsInfo.Location = new System.Drawing.Point(0, 427);
            this.stsInfo.Name = "stsInfo";
            this.stsInfo.Size = new System.Drawing.Size(668, 22);
            this.stsInfo.TabIndex = 231;
            this.stsInfo.Text = "StatusStrip1";
            // 
            // stsLblInfo
            // 
            this.stsLblInfo.AccessibleDescription = "stsLblInfo";
            this.stsLblInfo.AccessibleName = "stsLblInfo";
            this.stsLblInfo.ActiveLinkColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stsLblInfo.Name = "stsLblInfo";
            this.stsLblInfo.Size = new System.Drawing.Size(129, 17);
            this.stsLblInfo.Text = "User Access Rights Info";
            // 
            // lbScreens
            // 
            this.lbScreens.BackColor = System.Drawing.Color.Gray;
            this.lbScreens.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbScreens.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbScreens.ForeColor = System.Drawing.Color.White;
            this.lbScreens.FormattingEnabled = true;
            this.lbScreens.ItemHeight = 19;
            this.lbScreens.Location = new System.Drawing.Point(12, 98);
            this.lbScreens.Name = "lbScreens";
            this.lbScreens.Size = new System.Drawing.Size(331, 327);
            this.lbScreens.TabIndex = 232;
            this.lbScreens.SelectedIndexChanged += new System.EventHandler(this.lbScreens_SelectedIndexChanged);
            // 
            // TStrip
            // 
            this.TStrip.AccessibleDescription = "TStrip";
            this.TStrip.AccessibleName = "TStrip";
            this.TStrip.AutoSize = false;
            this.TStrip.BackColor = System.Drawing.Color.NavajoWhite;
            this.TStrip.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStripSave,
            this.ToolStripSeparator1,
            this.TStripBackword,
            this.TStripForword});
            this.TStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.TStrip.Location = new System.Drawing.Point(0, 0);
            this.TStrip.Name = "TStrip";
            this.TStrip.Size = new System.Drawing.Size(668, 58);
            this.TStrip.Stretch = true;
            this.TStrip.TabIndex = 233;
            // 
            // TStripSave
            // 
            this.TStripSave.AccessibleDescription = "TStripSave";
            this.TStripSave.AccessibleName = "TStripSave";
            this.TStripSave.AutoSize = false;
            this.TStripSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripSave.Image = ((System.Drawing.Image)(resources.GetObject("TStripSave.Image")));
            this.TStripSave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripSave.Name = "TStripSave";
            this.TStripSave.Size = new System.Drawing.Size(40, 38);
            this.TStripSave.ToolTipText = "Save Changes";
            this.TStripSave.Click += new System.EventHandler(this.TStripSave_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 58);
            // 
            // TStripBackword
            // 
            this.TStripBackword.AccessibleDescription = "TStripBackword";
            this.TStripBackword.AccessibleName = "TStripBackword";
            this.TStripBackword.AutoSize = false;
            this.TStripBackword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripBackword.Image = ((System.Drawing.Image)(resources.GetObject("TStripBackword.Image")));
            this.TStripBackword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripBackword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripBackword.Name = "TStripBackword";
            this.TStripBackword.Size = new System.Drawing.Size(40, 38);
            this.TStripBackword.ToolTipText = "Backword";
            this.TStripBackword.Click += new System.EventHandler(this.TStripBackword_Click);
            // 
            // TStripForword
            // 
            this.TStripForword.AccessibleDescription = "TStripForword";
            this.TStripForword.AccessibleName = "TStripForword";
            this.TStripForword.AutoSize = false;
            this.TStripForword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.TStripForword.Image = ((System.Drawing.Image)(resources.GetObject("TStripForword.Image")));
            this.TStripForword.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.TStripForword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.TStripForword.Name = "TStripForword";
            this.TStripForword.Size = new System.Drawing.Size(40, 38);
            this.TStripForword.Text = "ToolStripButton1";
            this.TStripForword.ToolTipText = "Forword";
            this.TStripForword.Click += new System.EventHandler(this.TStripForword_Click);
            // 
            // frmUserAccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.ClientSize = new System.Drawing.Size(668, 449);
            this.Controls.Add(this.lbResult);
            this.Controls.Add(this.TStrip);
            this.Controls.Add(this.lbScreens);
            this.Controls.Add(this.stsInfo);
            this.Controls.Add(this.chAccessRights);
            this.Controls.Add(this.cmbUser);
            this.Controls.Add(this.label1);
            this.Name = "frmUserAccess";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Access";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUserAccess_FormClosing);
            this.Load += new System.EventHandler(this.frmUserAccess_Load);
            this.stsInfo.ResumeLayout(false);
            this.stsInfo.PerformLayout();
            this.TStrip.ResumeLayout(false);
            this.TStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbResult;
        private System.Windows.Forms.ComboBox cmbUser;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chAccessRights;
        internal System.Windows.Forms.StatusStrip stsInfo;
        internal System.Windows.Forms.ToolStripStatusLabel stsLblInfo;
        private System.Windows.Forms.ListBox lbScreens;
        internal System.Windows.Forms.ToolStrip TStrip;
        internal System.Windows.Forms.ToolStripButton TStripSave;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripButton TStripBackword;
        internal System.Windows.Forms.ToolStripButton TStripForword;
    }
}