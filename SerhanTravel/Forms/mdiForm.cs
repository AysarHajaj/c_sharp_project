﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Connections;
using SerhanTravel.Forms;
using SerhanTravel.Objects;
using SerhanTravel.reports;

namespace SerhanTravel
{
    public partial class mdiForm : Form
    {   
        clsUser user;
        clsAccessRights right;
        AccessRights rightConnection = new AccessRights();
        List<clsAccessRights> rightsList;
       internal  mdiForm(clsUser user)
        {
            InitializeComponent();
            this.user = user;
        }
        internal mdiForm()
        {
            InitializeComponent();
           
        }

        private void mdiForm_Load_1(object sender, EventArgs e)
        {
            rightsList = new List<clsAccessRights>();
            rightsList.Insert(0, new clsAccessRights());
            
            if(user.RoleId!=1)
            {
                userAccessRightsToolStripMenuItem.Visible = false;
            }
           
            for(int i = 1; i <= 51; i++)
            {
                if(user.RoleId!=1)
                {
                    right = new clsAccessRights();
                    if (!rightConnection.UserRightsExists(user.UserId, i))
                    {
                        rightsList.Insert(i, new clsAccessRights());
                        disableButtons(i);
                    }
                    else
                    {
                        right = rightConnection.getUserScreenRights(user.UserId, i);
                        rightsList.Insert(i, right);
                        if (!right.CanAccess)
                        {
                            disableButtons(i);
                        }
                    }
                }
                else
                {
                    right = new clsAccessRights();
                    right.UserId = this.user.UserId;
                    right.CanAccess = true;
                    right.Adding = true;
                    right.Deleting = true;
                    right.Updating = true;
                    right.Printing = true;
                    rightsList.Insert(i, right);
                }
            }           
        }

        private void MSFileBranchesInfo_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCompany")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCompany frm = new frmCompany(rightsList[1]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void TSCustomer_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomer")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomer frm = new frmCustomer(rightsList[3]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void TSSupplier_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSuppliers")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSuppliers frm = new frmSuppliers(rightsList[2]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void TSTickets_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTickets")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTickets frm = new frmTickets(rightsList[5]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void CitiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCity")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCity city = new frmCity(rightsList[7]);
            city.MdiParent = this;
            city.Show();
        }

        private void CountriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCountry")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCountry country = new frmCountry(rightsList[6]);
            country.MdiParent = this;
            country.Show();
        }

        private void NatiionalityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmNationality")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmNationality Nationality = new frmNationality(rightsList[8]);
            Nationality.MdiParent = this;
            Nationality.Show();
        }

        private void airLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmAirLine")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmAirLine AirLine = new frmAirLine(rightsList[10]);
            AirLine.MdiParent = this;
            AirLine.Show();
        }

        private void bankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmBanks")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmBanks Bank = new frmBanks(rightsList[11]);
            Bank.MdiParent = this;
            Bank.Show();
        }

        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmItem")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmItem Item = new frmItem(rightsList[12]);
            Item.MdiParent = this;
            Item.Show();
        }

        private void currencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCurrency")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCurrency Currency = new frmCurrency(rightsList[13]);
            Currency.MdiParent = this;
            Currency.Show();
        }

        private void TSPackages_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPackage")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPackage Package = new frmPackage(rightsList[4]);
            Package.MdiParent = this;
            Package.Show();
        }

        private void TSTicketsInvoice_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTicketsInvoice")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTicketsInvoice frm = new frmTicketsInvoice(rightsList[27]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void mdiForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPwdChange")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPwdChange frm = new frmPwdChange(this.user);
            frm.MdiParent = this;
            frm.Show();
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmRegistration")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmRegistration frm = new frmRegistration(rightsList[14]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void GroupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmGroup")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmGroup frm = new frmGroup(rightsList[9]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void TSPackageSales_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTicketPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTicketPayments frm = new frmTicketPayments(rightsList[28]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPackageInvoice")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPackageInvoice frm = new frmPackageInvoice(rightsList[29]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPackagePayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPackagePayments frm = new frmPackagePayments(rightsList[30]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierTicketInvoice")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierTicketInvoice frm = new frmSupplierTicketInvoice(rightsList[23]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierTicketPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierTicketPayments frm = new frmSupplierTicketPayments(rightsList[24]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersSOA_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerStatmentOfAcount")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerStatmentOfAcount frm = new frmCustomerStatmentOfAcount();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersSOP_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "CustomerSummaryOfPayment")
                {
                    formOpened.Focus();
                    return;
                }
            }
            CustomerSummaryOfPayment frm = new CustomerSummaryOfPayment();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersCStatus_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerChequeStatus")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerChequeStatus frm = new frmCustomerChequeStatus();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersPending_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerPendingPayment")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerPendingPayment frm = new frmCustomerPendingPayment();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersTSold_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTicketSold")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTicketSold frm = new frmTicketSold();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsCustomersPSold_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPackageSold")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPackageSold frm = new frmPackageSold();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsSuppliersPIssued_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmItemsIssued")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmItemsIssued frm = new frmItemsIssued();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsSuppliersTIssued_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTicketIssued")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTicketIssued frm = new frmTicketIssued();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsDailySalesByType_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSalesByType")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSalesByType frm = new frmSalesByType();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsDailySalesSummary_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSummeryOfSales")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSummeryOfSales frm = new frmSummeryOfSales();
            frm.MdiParent = this;
            frm.Show();
        }

        private void numberOfPackageSoldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmNumberOfPackagesSold")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmNumberOfPackagesSold frm = new frmNumberOfPackagesSold();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSFileSuppliers_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSuppliers")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSuppliers frm = new frmSuppliers(rightsList[2]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSFileCustomer_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomer")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomer frm = new frmCustomer(rightsList[3]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSFilePackages_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPackage")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPackage frm = new frmPackage(rightsList[4]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSFileTickets_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmTickets")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmTickets frm = new frmTickets(rightsList[5]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void userAccessRightsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmUserAccess")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmUserAccess frm = new frmUserAccess();
            frm.MdiParent = this;
            frm.Show();
        }

     /*   private void toolStripSupplierItemInvoice_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierItemInvoice")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierItemInvoice frm = new frmSupplierItemInvoice(rightsList[25]);
            frm.MdiParent = this;
            frm.Show();
        }*/

        private void toolStripSupplierItemPayments_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierItemPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierItemPayments frm = new frmSupplierItemPayments(rightsList[26]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void disableButtons(int i)
        {
            if (i == 1)
            {
                MSFileBranchesInfo.Enabled = false;
            }
            if (i == 2)
            {
                MSFileSuppliers.Enabled = false;
                TSSupplier.Enabled = false;
            }
            if (i == 3)
            {
                MSFileCustomer.Enabled = false;
                TSCustomer.Enabled = false;
            }
            if (i == 4)
            {
                MSFilePackages.Enabled = false;
                TSPackages.Enabled = false;
            }
            if (i == 5)
            {
                MSFileTickets.Enabled = false;
                TSTickets.Enabled = false;
            }
            if (i == 6)
            {
                CountriesToolStripMenuItem.Enabled = false;
            }
            if (i == 7)
            {
                CitiesToolStripMenuItem.Enabled = false;
            }
            if (i == 8)
            {
                NatiionalityToolStripMenuItem.Enabled = false;
            }
            if (i == 9)
            {
                GroupsToolStripMenuItem.Enabled = false;
            }
            if (i == 10)
            {
                airLineToolStripMenuItem.Enabled = false;
            }
            if (i == 11)
            {
                bankToolStripMenuItem.Enabled = false;
            }
            if (i == 12)
            {
                itemToolStripMenuItem.Enabled = false;
            }
            if (i == 13)
            {
                currencyToolStripMenuItem.Enabled = false;
            }
            if (i == 14)
            {
                addUserToolStripMenuItem.Enabled = false;
            }
           
                 if (i == 15)
            {
                expensesTypeToolStripMenuItem.Enabled = false;
            }
            if (i == 16)
            {
                expensesToolStripMenuItem.Enabled = false;
            }
            if (i == 17)
            {
                supplierOpeningBalanceToolStripMenuItem.Enabled = false;
            }
            if (i == 18)
            {
                customerOpeningBalanceToolStripMenuItem.Enabled = false;
            }
            if (i == 19)
            {
                tsSupplierReturnTicket.Enabled = false;
            }
            if (i == 20)
            {
                tsSupplierReturnItem.Enabled = false;
            }
            if (i == 21)
            {
                tsCustomerReturnedTickets.Enabled = false;
            }
            if (i == 22)
            {
                tsCustomerReturnedPackages.Enabled = false;
            }
           
            if (i == 23)
            {
                toolStripSupplierTicketInvoice.Enabled = false;
            }
            if (i == 24)
            {
                toolStripSupplierTicketPayment.Enabled = false;
            }
           /* if (i == 25)
            {
                toolStripSupplierItemInvoice.Enabled = false;
            }*/
            if (i == 26)
            {
                toolStripSupplierItemPayments.Enabled = false;
            }
            if (i == 27)
            {
                TSTicketsInvoice.Enabled = false;
            }
            if (i == 28)
            {
                TSTicketPayment.Enabled = false;
            }
            if (i == 29)
            {
                toolStripPackageInvoice.Enabled = false;
            }
            if (i == 30)
            {
                toolStripPackagePayment.Enabled = false;
            }
            if (i == 31)
            {
                MSReportsCustomerStatementOfAccount.Enabled = false;
            }
            if (i == 32)
            {
                MSReportsCustomersTicketSold.Enabled = false;
            }
            if (i == 33)
            {
                MSReportsCustomersPSold.Enabled = false;
            }
            if (i == 34)
            {
                MSReportsCustomersSOP.Enabled = false;
            }
            if (i == 35)
            {
                MSReportsCustomersCStatus.Enabled = false;
            }
            if (i == 36)
            {
                MSReportsCustomersPending.Enabled = false;
            }
            if (i == 37)
            {
                returnedItemsToolStripMenuItem.Enabled = false;
            }
            if (i == 38)
            {
                customerPaymentsToolStripMenuItem.Enabled = false;
            }
            if (i == 39)
            {
                MSReportsSuppliersSOA.Enabled = false;
            }
            if (i == 40)
            {
                MSReportsSuppliersTIssued.Enabled = false;
            }
            if (i == 41)
            {
                MSReportsSuppliersPIssued.Enabled = false;
            }
            if (i == 42)
            {
                MSReportsSuppliersSPayments.Enabled = false;
            }
            if (i == 43)
            {
                MSReportsSuppliersPByType.Enabled = false;
            }
            if (i == 44)
            {
                MSReportsSuppliersSOPayments.Enabled = false;
            }
            if (i == 45)
            {
                returnedItemsToolStripMenuItem1.Enabled = false;
            }
            if (i == 46)
            {
                MSReportsDailySalesByType.Enabled = false;
            }
            if (i == 47)
            {
                MSReportsDailySalesSummary.Enabled = false;
            }
            if (i == 48)
            {
                numberOfPackageSoldToolStripMenuItem.Enabled = false;
            }
            if (i == 49)
            {
                DailypaymentsToolStripMenuItem.Enabled = false;
            }
            if (i == 50)
            {
                expensesReportsToolStripMenuItem.Enabled = false;
            }
            if (i == 51)
            {
                incomeStatmentToolStripMenuItem.Enabled = false;
            }
        }

        private void MSReportsSuppliersSOA_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierStatementOfAcount")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierStatementOfAcount frm = new frmSupplierStatementOfAcount();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsSuppliersSPayments_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierPayments frm = new frmSupplierPayments();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsSuppliersPByType_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmPaymentByType")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmPaymentByType frm = new frmPaymentByType();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MSReportsSuppliersSOPayments_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierSummeryOfPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierSummeryOfPayments frm = new frmSupplierSummeryOfPayments();
            frm.MdiParent = this;
            frm.Show();
        }

        private void signOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != "frmLogin"&& Application.OpenForms[i].Name != "mdiForm")
                     Application.OpenForms[i].Close();
                else if(Application.OpenForms[i].Name == "mdiForm")
                {
                    Application.OpenForms[i].Hide();
                }
            }
            frmLogin frm = new frmLogin();
            frm.Show();        
        }

        private void customerOpeningBalanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerBalanceInventory")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerBalanceInventory frm = new frmCustomerBalanceInventory(rightsList[18]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void supplierOpeningBalanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierBalanceInventory")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierBalanceInventory frm = new frmSupplierBalanceInventory(rightsList[17]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void expensesTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmExpensesType")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmExpensesType frm = new frmExpensesType(rightsList[15]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void expensesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmExpenses")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmExpenses frm = new frmExpenses(rightsList[16]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void expensesReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmExpensesReport")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmExpensesReport frm = new frmExpensesReport();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsCustomerReturnedTickets_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerReturnedTicket")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerReturnedTicket frm = new frmCustomerReturnedTicket(rightsList[21]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsCustomerReturnedPackages_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerReturnedPackage")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerReturnedPackage frm = new frmCustomerReturnedPackage(rightsList[22]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsSupplierReturnTicket_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierReturnedTicket")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierReturnedTicket  frm = new frmSupplierReturnedTicket(rightsList[19]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsSupplierReturnItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSupplierReturnedItem")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSupplierReturnedItem frm = new frmSupplierReturnedItem(rightsList[20]);
            frm.MdiParent = this;
            frm.Show();
        }

        private void returnedItemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustReturnedItems")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustReturnedItems frm = new frmCustReturnedItems();
            frm.MdiParent = this;
            frm.Show();
        }

        private void returnedItemsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmSuppReturnedItems")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmSuppReturnedItems frm = new frmSuppReturnedItems();
            frm.MdiParent = this;
            frm.Show();
        }

        private void customerPaymentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmCustomerPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmCustomerPayments frm = new frmCustomerPayments();
            frm.MdiParent = this;
            frm.Show();
        }

        private void paymentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmDailyPayments")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmDailyPayments frm = new frmDailyPayments();
            frm.MdiParent = this;
            frm.Show();
        }

        private void incomeStatmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form formOpened in this.MdiChildren)
            {
                if (formOpened.Name == "frmIncomeStatment")
                {
                    formOpened.Focus();
                    return;
                }
            }
            frmIncomeStatment frm = new frmIncomeStatment();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
