﻿namespace SerhanTravel
{
    partial class mdiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mdiForm));
            this.MSReportsCustomersPending = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersSOA = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersTIssued = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersPIssued = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersSPayments = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersPByType = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsSuppliersSOPayments = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsDailySales = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsDailySalesByType = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsDailySalesSummary = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfPackageSoldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DailypaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.TSSupplier = new System.Windows.Forms.ToolStripButton();
            this.toolStripSupplierTicketInvoice = new System.Windows.Forms.ToolStripButton();
            this.toolStripSupplierTicketPayment = new System.Windows.Forms.ToolStripButton();
            this.tsSupplierReturnTicket = new System.Windows.Forms.ToolStripButton();
            this.toolStripSupplierItemPayments = new System.Windows.Forms.ToolStripButton();
            this.tsSupplierReturnItem = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.TSCustomer = new System.Windows.Forms.ToolStripButton();
            this.TSTickets = new System.Windows.Forms.ToolStripButton();
            this.TSTicketsInvoice = new System.Windows.Forms.ToolStripButton();
            this.TSTicketPayment = new System.Windows.Forms.ToolStripButton();
            this.tsCustomerReturnedTickets = new System.Windows.Forms.ToolStripButton();
            this.TSPackages = new System.Windows.Forms.ToolStripButton();
            this.toolStripPackageInvoice = new System.Windows.Forms.ToolStripButton();
            this.toolStripPackagePayment = new System.Windows.Forms.ToolStripButton();
            this.tsCustomerReturnedPackages = new System.Windows.Forms.ToolStripButton();
            this.MSReportsSuppliers = new System.Windows.Forms.ToolStripMenuItem();
            this.returnedItemsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomersCStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.MSMain = new System.Windows.Forms.MenuStrip();
            this.MSFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MSFileBranchesInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.MSFileSuppliers = new System.Windows.Forms.ToolStripMenuItem();
            this.MSFileCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.MSFilePackages = new System.Windows.Forms.ToolStripMenuItem();
            this.MSFileTickets = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierOpeningBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerOpeningBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userAccessRightsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MSCodes = new System.Windows.Forms.ToolStripMenuItem();
            this.CountriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NatiionalityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.airLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expensesTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReports = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomers = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomerStatementOfAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomersTicketSold = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomersPSold = new System.Windows.Forms.ToolStripMenuItem();
            this.MSReportsCustomersSOP = new System.Windows.Forms.ToolStripMenuItem();
            this.returnedItemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expensesReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incomeStatmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1.SuspendLayout();
            this.MSMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // MSReportsCustomersPending
            // 
            resources.ApplyResources(this.MSReportsCustomersPending, "MSReportsCustomersPending");
            this.MSReportsCustomersPending.Name = "MSReportsCustomersPending";
            this.MSReportsCustomersPending.Click += new System.EventHandler(this.MSReportsCustomersPending_Click);
            // 
            // MSReportsSuppliersSOA
            // 
            resources.ApplyResources(this.MSReportsSuppliersSOA, "MSReportsSuppliersSOA");
            this.MSReportsSuppliersSOA.Name = "MSReportsSuppliersSOA";
            this.MSReportsSuppliersSOA.Click += new System.EventHandler(this.MSReportsSuppliersSOA_Click);
            // 
            // MSReportsSuppliersTIssued
            // 
            resources.ApplyResources(this.MSReportsSuppliersTIssued, "MSReportsSuppliersTIssued");
            this.MSReportsSuppliersTIssued.Name = "MSReportsSuppliersTIssued";
            this.MSReportsSuppliersTIssued.Click += new System.EventHandler(this.MSReportsSuppliersTIssued_Click);
            // 
            // MSReportsSuppliersPIssued
            // 
            resources.ApplyResources(this.MSReportsSuppliersPIssued, "MSReportsSuppliersPIssued");
            this.MSReportsSuppliersPIssued.Name = "MSReportsSuppliersPIssued";
            this.MSReportsSuppliersPIssued.Click += new System.EventHandler(this.MSReportsSuppliersPIssued_Click);
            // 
            // MSReportsSuppliersSPayments
            // 
            resources.ApplyResources(this.MSReportsSuppliersSPayments, "MSReportsSuppliersSPayments");
            this.MSReportsSuppliersSPayments.Name = "MSReportsSuppliersSPayments";
            this.MSReportsSuppliersSPayments.Click += new System.EventHandler(this.MSReportsSuppliersSPayments_Click);
            // 
            // MSReportsSuppliersPByType
            // 
            resources.ApplyResources(this.MSReportsSuppliersPByType, "MSReportsSuppliersPByType");
            this.MSReportsSuppliersPByType.Name = "MSReportsSuppliersPByType";
            this.MSReportsSuppliersPByType.Click += new System.EventHandler(this.MSReportsSuppliersPByType_Click);
            // 
            // MSReportsSuppliersSOPayments
            // 
            resources.ApplyResources(this.MSReportsSuppliersSOPayments, "MSReportsSuppliersSOPayments");
            this.MSReportsSuppliersSOPayments.Name = "MSReportsSuppliersSOPayments";
            this.MSReportsSuppliersSOPayments.Click += new System.EventHandler(this.MSReportsSuppliersSOPayments_Click);
            // 
            // MSReportsDailySales
            // 
            resources.ApplyResources(this.MSReportsDailySales, "MSReportsDailySales");
            this.MSReportsDailySales.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSReportsDailySalesByType,
            this.MSReportsDailySalesSummary,
            this.numberOfPackageSoldToolStripMenuItem,
            this.DailypaymentsToolStripMenuItem});
            this.MSReportsDailySales.Name = "MSReportsDailySales";
            // 
            // MSReportsDailySalesByType
            // 
            resources.ApplyResources(this.MSReportsDailySalesByType, "MSReportsDailySalesByType");
            this.MSReportsDailySalesByType.Name = "MSReportsDailySalesByType";
            this.MSReportsDailySalesByType.Click += new System.EventHandler(this.MSReportsDailySalesByType_Click);
            // 
            // MSReportsDailySalesSummary
            // 
            resources.ApplyResources(this.MSReportsDailySalesSummary, "MSReportsDailySalesSummary");
            this.MSReportsDailySalesSummary.Name = "MSReportsDailySalesSummary";
            this.MSReportsDailySalesSummary.Click += new System.EventHandler(this.MSReportsDailySalesSummary_Click);
            // 
            // numberOfPackageSoldToolStripMenuItem
            // 
            this.numberOfPackageSoldToolStripMenuItem.Name = "numberOfPackageSoldToolStripMenuItem";
            resources.ApplyResources(this.numberOfPackageSoldToolStripMenuItem, "numberOfPackageSoldToolStripMenuItem");
            this.numberOfPackageSoldToolStripMenuItem.Click += new System.EventHandler(this.numberOfPackageSoldToolStripMenuItem_Click);
            // 
            // DailypaymentsToolStripMenuItem
            // 
            this.DailypaymentsToolStripMenuItem.Name = "DailypaymentsToolStripMenuItem";
            resources.ApplyResources(this.DailypaymentsToolStripMenuItem, "DailypaymentsToolStripMenuItem");
            this.DailypaymentsToolStripMenuItem.Click += new System.EventHandler(this.paymentsToolStripMenuItem_Click);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.BackColor = System.Drawing.Color.LightSalmon;
            resources.ApplyResources(this.ToolStrip1, "ToolStrip1");
            this.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSupplier,
            this.toolStripSupplierTicketInvoice,
            this.toolStripSupplierTicketPayment,
            this.tsSupplierReturnTicket,
            this.toolStripSupplierItemPayments,
            this.tsSupplierReturnItem,
            this.toolStripSeparator1,
            this.TSCustomer,
            this.TSTickets,
            this.TSTicketsInvoice,
            this.TSTicketPayment,
            this.tsCustomerReturnedTickets,
            this.TSPackages,
            this.toolStripPackageInvoice,
            this.toolStripPackagePayment,
            this.tsCustomerReturnedPackages});
            this.ToolStrip1.Name = "ToolStrip1";
            // 
            // TSSupplier
            // 
            resources.ApplyResources(this.TSSupplier, "TSSupplier");
            this.TSSupplier.Image = global::SerhanTravel.Properties.Resources.Sponsor01;
            this.TSSupplier.Name = "TSSupplier";
            this.TSSupplier.Click += new System.EventHandler(this.TSSupplier_Click);
            // 
            // toolStripSupplierTicketInvoice
            // 
            resources.ApplyResources(this.toolStripSupplierTicketInvoice, "toolStripSupplierTicketInvoice");
            this.toolStripSupplierTicketInvoice.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.toolStripSupplierTicketInvoice.Image = global::SerhanTravel.Properties.Resources.if_invoice1;
            this.toolStripSupplierTicketInvoice.Margin = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierTicketInvoice.Name = "toolStripSupplierTicketInvoice";
            this.toolStripSupplierTicketInvoice.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierTicketInvoice.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSupplierTicketPayment
            // 
            resources.ApplyResources(this.toolStripSupplierTicketPayment, "toolStripSupplierTicketPayment");
            this.toolStripSupplierTicketPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.toolStripSupplierTicketPayment.Image = global::SerhanTravel.Properties.Resources.if_payment;
            this.toolStripSupplierTicketPayment.Margin = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierTicketPayment.Name = "toolStripSupplierTicketPayment";
            this.toolStripSupplierTicketPayment.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierTicketPayment.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // tsSupplierReturnTicket
            // 
            resources.ApplyResources(this.tsSupplierReturnTicket, "tsSupplierReturnTicket");
            this.tsSupplierReturnTicket.Name = "tsSupplierReturnTicket";
            this.tsSupplierReturnTicket.Click += new System.EventHandler(this.tsSupplierReturnTicket_Click);
            // 
            // toolStripSupplierItemPayments
            // 
            resources.ApplyResources(this.toolStripSupplierItemPayments, "toolStripSupplierItemPayments");
            this.toolStripSupplierItemPayments.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.toolStripSupplierItemPayments.Image = global::SerhanTravel.Properties.Resources.Itemspayment;
            this.toolStripSupplierItemPayments.Margin = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierItemPayments.Name = "toolStripSupplierItemPayments";
            this.toolStripSupplierItemPayments.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripSupplierItemPayments.Click += new System.EventHandler(this.toolStripSupplierItemPayments_Click);
            // 
            // tsSupplierReturnItem
            // 
            resources.ApplyResources(this.tsSupplierReturnItem, "tsSupplierReturnItem");
            this.tsSupplierReturnItem.Name = "tsSupplierReturnItem";
            this.tsSupplierReturnItem.Click += new System.EventHandler(this.tsSupplierReturnItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // TSCustomer
            // 
            resources.ApplyResources(this.TSCustomer, "TSCustomer");
            this.TSCustomer.Image = global::SerhanTravel.Properties.Resources.Globe02;
            this.TSCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.TSCustomer.Name = "TSCustomer";
            this.TSCustomer.Padding = new System.Windows.Forms.Padding(2);
            this.TSCustomer.Click += new System.EventHandler(this.TSCustomer_Click);
            // 
            // TSTickets
            // 
            resources.ApplyResources(this.TSTickets, "TSTickets");
            this.TSTickets.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.TSTickets.Image = global::SerhanTravel.Properties.Resources.Servant11;
            this.TSTickets.Margin = new System.Windows.Forms.Padding(2);
            this.TSTickets.Name = "TSTickets";
            this.TSTickets.Padding = new System.Windows.Forms.Padding(2);
            this.TSTickets.Click += new System.EventHandler(this.TSTickets_Click);
            // 
            // TSTicketsInvoice
            // 
            resources.ApplyResources(this.TSTicketsInvoice, "TSTicketsInvoice");
            this.TSTicketsInvoice.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.TSTicketsInvoice.Image = global::SerhanTravel.Properties.Resources.if_TicketInvoices;
            this.TSTicketsInvoice.Name = "TSTicketsInvoice";
            this.TSTicketsInvoice.Click += new System.EventHandler(this.TSTicketsInvoice_Click);
            // 
            // TSTicketPayment
            // 
            resources.ApplyResources(this.TSTicketPayment, "TSTicketPayment");
            this.TSTicketPayment.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.TSTicketPayment.Image = global::SerhanTravel.Properties.Resources.Itemspayment;
            this.TSTicketPayment.Margin = new System.Windows.Forms.Padding(2);
            this.TSTicketPayment.Name = "TSTicketPayment";
            this.TSTicketPayment.Padding = new System.Windows.Forms.Padding(2);
            this.TSTicketPayment.Click += new System.EventHandler(this.TSPackageSales_Click);
            // 
            // tsCustomerReturnedTickets
            // 
            resources.ApplyResources(this.tsCustomerReturnedTickets, "tsCustomerReturnedTickets");
            this.tsCustomerReturnedTickets.Name = "tsCustomerReturnedTickets";
            this.tsCustomerReturnedTickets.Click += new System.EventHandler(this.tsCustomerReturnedTickets_Click);
            // 
            // TSPackages
            // 
            resources.ApplyResources(this.TSPackages, "TSPackages");
            this.TSPackages.Image = global::SerhanTravel.Properties.Resources.Globe01;
            this.TSPackages.Margin = new System.Windows.Forms.Padding(2);
            this.TSPackages.Name = "TSPackages";
            this.TSPackages.Padding = new System.Windows.Forms.Padding(2);
            this.TSPackages.Click += new System.EventHandler(this.TSPackages_Click);
            // 
            // toolStripPackageInvoice
            // 
            resources.ApplyResources(this.toolStripPackageInvoice, "toolStripPackageInvoice");
            this.toolStripPackageInvoice.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.toolStripPackageInvoice.Image = global::SerhanTravel.Properties.Resources.summary01;
            this.toolStripPackageInvoice.Margin = new System.Windows.Forms.Padding(2);
            this.toolStripPackageInvoice.Name = "toolStripPackageInvoice";
            this.toolStripPackageInvoice.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripPackageInvoice.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripPackagePayment
            // 
            resources.ApplyResources(this.toolStripPackagePayment, "toolStripPackagePayment");
            this.toolStripPackagePayment.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.toolStripPackagePayment.Image = global::SerhanTravel.Properties.Resources.if_payment;
            this.toolStripPackagePayment.Margin = new System.Windows.Forms.Padding(2);
            this.toolStripPackagePayment.Name = "toolStripPackagePayment";
            this.toolStripPackagePayment.Padding = new System.Windows.Forms.Padding(2);
            this.toolStripPackagePayment.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // tsCustomerReturnedPackages
            // 
            resources.ApplyResources(this.tsCustomerReturnedPackages, "tsCustomerReturnedPackages");
            this.tsCustomerReturnedPackages.Name = "tsCustomerReturnedPackages";
            this.tsCustomerReturnedPackages.Click += new System.EventHandler(this.tsCustomerReturnedPackages_Click);
            // 
            // MSReportsSuppliers
            // 
            resources.ApplyResources(this.MSReportsSuppliers, "MSReportsSuppliers");
            this.MSReportsSuppliers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSReportsSuppliersSOA,
            this.MSReportsSuppliersTIssued,
            this.MSReportsSuppliersPIssued,
            this.MSReportsSuppliersSPayments,
            this.MSReportsSuppliersPByType,
            this.MSReportsSuppliersSOPayments,
            this.returnedItemsToolStripMenuItem1});
            this.MSReportsSuppliers.Name = "MSReportsSuppliers";
            // 
            // returnedItemsToolStripMenuItem1
            // 
            this.returnedItemsToolStripMenuItem1.Name = "returnedItemsToolStripMenuItem1";
            resources.ApplyResources(this.returnedItemsToolStripMenuItem1, "returnedItemsToolStripMenuItem1");
            this.returnedItemsToolStripMenuItem1.Click += new System.EventHandler(this.returnedItemsToolStripMenuItem1_Click);
            // 
            // MSReportsCustomersCStatus
            // 
            resources.ApplyResources(this.MSReportsCustomersCStatus, "MSReportsCustomersCStatus");
            this.MSReportsCustomersCStatus.Name = "MSReportsCustomersCStatus";
            this.MSReportsCustomersCStatus.Click += new System.EventHandler(this.MSReportsCustomersCStatus_Click);
            // 
            // MSMain
            // 
            resources.ApplyResources(this.MSMain, "MSMain");
            this.MSMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.MSMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSFile,
            this.MSCodes,
            this.MSReports});
            this.MSMain.Name = "MSMain";
            // 
            // MSFile
            // 
            resources.ApplyResources(this.MSFile, "MSFile");
            this.MSFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(129)))), ((int)(((byte)(0)))), ((int)(((byte)(127)))));
            this.MSFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSFileBranchesInfo,
            this.MSFileSuppliers,
            this.MSFileCustomer,
            this.MSFilePackages,
            this.MSFileTickets,
            this.changePasswordToolStripMenuItem,
            this.supplierOpeningBalanceToolStripMenuItem,
            this.customerOpeningBalanceToolStripMenuItem,
            this.userAccessRightsToolStripMenuItem,
            this.signOutToolStripMenuItem});
            this.MSFile.ForeColor = System.Drawing.Color.DarkGray;
            this.MSFile.Name = "MSFile";
            // 
            // MSFileBranchesInfo
            // 
            resources.ApplyResources(this.MSFileBranchesInfo, "MSFileBranchesInfo");
            this.MSFileBranchesInfo.Name = "MSFileBranchesInfo";
            this.MSFileBranchesInfo.Click += new System.EventHandler(this.MSFileBranchesInfo_Click);
            // 
            // MSFileSuppliers
            // 
            resources.ApplyResources(this.MSFileSuppliers, "MSFileSuppliers");
            this.MSFileSuppliers.Name = "MSFileSuppliers";
            this.MSFileSuppliers.Click += new System.EventHandler(this.MSFileSuppliers_Click);
            // 
            // MSFileCustomer
            // 
            resources.ApplyResources(this.MSFileCustomer, "MSFileCustomer");
            this.MSFileCustomer.Name = "MSFileCustomer";
            this.MSFileCustomer.Click += new System.EventHandler(this.MSFileCustomer_Click);
            // 
            // MSFilePackages
            // 
            resources.ApplyResources(this.MSFilePackages, "MSFilePackages");
            this.MSFilePackages.Name = "MSFilePackages";
            this.MSFilePackages.Click += new System.EventHandler(this.MSFilePackages_Click);
            // 
            // MSFileTickets
            // 
            resources.ApplyResources(this.MSFileTickets, "MSFileTickets");
            this.MSFileTickets.Name = "MSFileTickets";
            this.MSFileTickets.Click += new System.EventHandler(this.MSFileTickets_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            resources.ApplyResources(this.changePasswordToolStripMenuItem, "changePasswordToolStripMenuItem");
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // supplierOpeningBalanceToolStripMenuItem
            // 
            this.supplierOpeningBalanceToolStripMenuItem.Name = "supplierOpeningBalanceToolStripMenuItem";
            resources.ApplyResources(this.supplierOpeningBalanceToolStripMenuItem, "supplierOpeningBalanceToolStripMenuItem");
            this.supplierOpeningBalanceToolStripMenuItem.Click += new System.EventHandler(this.supplierOpeningBalanceToolStripMenuItem_Click);
            // 
            // customerOpeningBalanceToolStripMenuItem
            // 
            this.customerOpeningBalanceToolStripMenuItem.Name = "customerOpeningBalanceToolStripMenuItem";
            resources.ApplyResources(this.customerOpeningBalanceToolStripMenuItem, "customerOpeningBalanceToolStripMenuItem");
            this.customerOpeningBalanceToolStripMenuItem.Click += new System.EventHandler(this.customerOpeningBalanceToolStripMenuItem_Click);
            // 
            // userAccessRightsToolStripMenuItem
            // 
            this.userAccessRightsToolStripMenuItem.Name = "userAccessRightsToolStripMenuItem";
            resources.ApplyResources(this.userAccessRightsToolStripMenuItem, "userAccessRightsToolStripMenuItem");
            this.userAccessRightsToolStripMenuItem.Click += new System.EventHandler(this.userAccessRightsToolStripMenuItem_Click);
            // 
            // signOutToolStripMenuItem
            // 
            this.signOutToolStripMenuItem.Name = "signOutToolStripMenuItem";
            resources.ApplyResources(this.signOutToolStripMenuItem, "signOutToolStripMenuItem");
            this.signOutToolStripMenuItem.Click += new System.EventHandler(this.signOutToolStripMenuItem_Click);
            // 
            // MSCodes
            // 
            resources.ApplyResources(this.MSCodes, "MSCodes");
            this.MSCodes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CountriesToolStripMenuItem,
            this.CitiesToolStripMenuItem,
            this.NatiionalityToolStripMenuItem,
            this.GroupsToolStripMenuItem,
            this.airLineToolStripMenuItem,
            this.bankToolStripMenuItem,
            this.itemToolStripMenuItem,
            this.currencyToolStripMenuItem,
            this.addUserToolStripMenuItem,
            this.expensesTypeToolStripMenuItem,
            this.expensesToolStripMenuItem});
            this.MSCodes.ForeColor = System.Drawing.Color.DarkGray;
            this.MSCodes.Name = "MSCodes";
            // 
            // CountriesToolStripMenuItem
            // 
            this.CountriesToolStripMenuItem.Name = "CountriesToolStripMenuItem";
            resources.ApplyResources(this.CountriesToolStripMenuItem, "CountriesToolStripMenuItem");
            this.CountriesToolStripMenuItem.Click += new System.EventHandler(this.CountriesToolStripMenuItem_Click);
            // 
            // CitiesToolStripMenuItem
            // 
            this.CitiesToolStripMenuItem.Name = "CitiesToolStripMenuItem";
            resources.ApplyResources(this.CitiesToolStripMenuItem, "CitiesToolStripMenuItem");
            this.CitiesToolStripMenuItem.Click += new System.EventHandler(this.CitiesToolStripMenuItem_Click);
            // 
            // NatiionalityToolStripMenuItem
            // 
            this.NatiionalityToolStripMenuItem.Name = "NatiionalityToolStripMenuItem";
            resources.ApplyResources(this.NatiionalityToolStripMenuItem, "NatiionalityToolStripMenuItem");
            this.NatiionalityToolStripMenuItem.Click += new System.EventHandler(this.NatiionalityToolStripMenuItem_Click);
            // 
            // GroupsToolStripMenuItem
            // 
            this.GroupsToolStripMenuItem.Name = "GroupsToolStripMenuItem";
            resources.ApplyResources(this.GroupsToolStripMenuItem, "GroupsToolStripMenuItem");
            this.GroupsToolStripMenuItem.Click += new System.EventHandler(this.GroupsToolStripMenuItem_Click);
            // 
            // airLineToolStripMenuItem
            // 
            this.airLineToolStripMenuItem.Name = "airLineToolStripMenuItem";
            resources.ApplyResources(this.airLineToolStripMenuItem, "airLineToolStripMenuItem");
            this.airLineToolStripMenuItem.Click += new System.EventHandler(this.airLineToolStripMenuItem_Click);
            // 
            // bankToolStripMenuItem
            // 
            this.bankToolStripMenuItem.Name = "bankToolStripMenuItem";
            resources.ApplyResources(this.bankToolStripMenuItem, "bankToolStripMenuItem");
            this.bankToolStripMenuItem.Click += new System.EventHandler(this.bankToolStripMenuItem_Click);
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            resources.ApplyResources(this.itemToolStripMenuItem, "itemToolStripMenuItem");
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.itemToolStripMenuItem_Click);
            // 
            // currencyToolStripMenuItem
            // 
            this.currencyToolStripMenuItem.Name = "currencyToolStripMenuItem";
            resources.ApplyResources(this.currencyToolStripMenuItem, "currencyToolStripMenuItem");
            this.currencyToolStripMenuItem.Click += new System.EventHandler(this.currencyToolStripMenuItem_Click);
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            resources.ApplyResources(this.addUserToolStripMenuItem, "addUserToolStripMenuItem");
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // expensesTypeToolStripMenuItem
            // 
            this.expensesTypeToolStripMenuItem.Name = "expensesTypeToolStripMenuItem";
            resources.ApplyResources(this.expensesTypeToolStripMenuItem, "expensesTypeToolStripMenuItem");
            this.expensesTypeToolStripMenuItem.Click += new System.EventHandler(this.expensesTypeToolStripMenuItem_Click);
            // 
            // expensesToolStripMenuItem
            // 
            this.expensesToolStripMenuItem.Name = "expensesToolStripMenuItem";
            resources.ApplyResources(this.expensesToolStripMenuItem, "expensesToolStripMenuItem");
            this.expensesToolStripMenuItem.Click += new System.EventHandler(this.expensesToolStripMenuItem_Click);
            // 
            // MSReports
            // 
            resources.ApplyResources(this.MSReports, "MSReports");
            this.MSReports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSReportsCustomers,
            this.MSReportsSuppliers,
            this.MSReportsDailySales,
            this.expensesReportsToolStripMenuItem,
            this.incomeStatmentToolStripMenuItem});
            this.MSReports.ForeColor = System.Drawing.Color.DarkGray;
            this.MSReports.Name = "MSReports";
            // 
            // MSReportsCustomers
            // 
            resources.ApplyResources(this.MSReportsCustomers, "MSReportsCustomers");
            this.MSReportsCustomers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSReportsCustomerStatementOfAccount,
            this.MSReportsCustomersTicketSold,
            this.MSReportsCustomersPSold,
            this.MSReportsCustomersSOP,
            this.MSReportsCustomersCStatus,
            this.MSReportsCustomersPending,
            this.returnedItemsToolStripMenuItem,
            this.customerPaymentsToolStripMenuItem});
            this.MSReportsCustomers.Name = "MSReportsCustomers";
            // 
            // MSReportsCustomerStatementOfAccount
            // 
            resources.ApplyResources(this.MSReportsCustomerStatementOfAccount, "MSReportsCustomerStatementOfAccount");
            this.MSReportsCustomerStatementOfAccount.Name = "MSReportsCustomerStatementOfAccount";
            this.MSReportsCustomerStatementOfAccount.Click += new System.EventHandler(this.MSReportsCustomersSOA_Click);
            // 
            // MSReportsCustomersTicketSold
            // 
            resources.ApplyResources(this.MSReportsCustomersTicketSold, "MSReportsCustomersTicketSold");
            this.MSReportsCustomersTicketSold.Name = "MSReportsCustomersTicketSold";
            this.MSReportsCustomersTicketSold.Click += new System.EventHandler(this.MSReportsCustomersTSold_Click);
            // 
            // MSReportsCustomersPSold
            // 
            resources.ApplyResources(this.MSReportsCustomersPSold, "MSReportsCustomersPSold");
            this.MSReportsCustomersPSold.Name = "MSReportsCustomersPSold";
            this.MSReportsCustomersPSold.Click += new System.EventHandler(this.MSReportsCustomersPSold_Click);
            // 
            // MSReportsCustomersSOP
            // 
            resources.ApplyResources(this.MSReportsCustomersSOP, "MSReportsCustomersSOP");
            this.MSReportsCustomersSOP.Name = "MSReportsCustomersSOP";
            this.MSReportsCustomersSOP.Click += new System.EventHandler(this.MSReportsCustomersSOP_Click);
            // 
            // returnedItemsToolStripMenuItem
            // 
            this.returnedItemsToolStripMenuItem.Name = "returnedItemsToolStripMenuItem";
            resources.ApplyResources(this.returnedItemsToolStripMenuItem, "returnedItemsToolStripMenuItem");
            this.returnedItemsToolStripMenuItem.Click += new System.EventHandler(this.returnedItemsToolStripMenuItem_Click);
            // 
            // customerPaymentsToolStripMenuItem
            // 
            this.customerPaymentsToolStripMenuItem.Name = "customerPaymentsToolStripMenuItem";
            resources.ApplyResources(this.customerPaymentsToolStripMenuItem, "customerPaymentsToolStripMenuItem");
            this.customerPaymentsToolStripMenuItem.Click += new System.EventHandler(this.customerPaymentsToolStripMenuItem_Click);
            // 
            // expensesReportsToolStripMenuItem
            // 
            this.expensesReportsToolStripMenuItem.Name = "expensesReportsToolStripMenuItem";
            resources.ApplyResources(this.expensesReportsToolStripMenuItem, "expensesReportsToolStripMenuItem");
            this.expensesReportsToolStripMenuItem.Click += new System.EventHandler(this.expensesReportsToolStripMenuItem_Click);
            // 
            // incomeStatmentToolStripMenuItem
            // 
            this.incomeStatmentToolStripMenuItem.Name = "incomeStatmentToolStripMenuItem";
            resources.ApplyResources(this.incomeStatmentToolStripMenuItem, "incomeStatmentToolStripMenuItem");
            this.incomeStatmentToolStripMenuItem.Click += new System.EventHandler(this.incomeStatmentToolStripMenuItem_Click);
            // 
            // mdiForm
            // 
            resources.ApplyResources(this, "$this");
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::SerhanTravel.Properties.Resources.Serhan031;
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.MSMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IsMdiContainer = true;
            this.Name = "mdiForm";
            this.ShowIcon = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mdiForm_FormClosed);
            this.Load += new System.EventHandler(this.mdiForm_Load_1);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.MSMain.ResumeLayout(false);
            this.MSMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomersPending;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersSOA;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersTIssued;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersPIssued;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersSPayments;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersPByType;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliersSOPayments;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsDailySales;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsDailySalesByType;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsDailySalesSummary;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripButton TSSupplier;
        internal System.Windows.Forms.ToolStripButton TSCustomer;
        internal System.Windows.Forms.ToolStripButton TSTickets;
        internal System.Windows.Forms.ToolStripButton TSTicketsInvoice;
        internal System.Windows.Forms.ToolStripButton TSTicketPayment;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsSuppliers;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomersCStatus;
        internal System.Windows.Forms.MenuStrip MSMain;
        internal System.Windows.Forms.ToolStripMenuItem MSFile;
        internal System.Windows.Forms.ToolStripMenuItem MSFileBranchesInfo;
        internal System.Windows.Forms.ToolStripMenuItem MSFileSuppliers;
        internal System.Windows.Forms.ToolStripMenuItem MSFileCustomer;
        internal System.Windows.Forms.ToolStripMenuItem MSFilePackages;
        internal System.Windows.Forms.ToolStripMenuItem MSFileTickets;
        internal System.Windows.Forms.ToolStripMenuItem MSCodes;
        internal System.Windows.Forms.ToolStripMenuItem CountriesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem CitiesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem NatiionalityToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GroupsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem MSReports;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomers;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomerStatementOfAccount;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomersTicketSold;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomersPSold;
        internal System.Windows.Forms.ToolStripMenuItem MSReportsCustomersSOP;
        internal System.Windows.Forms.ToolStripButton TSPackages;
        internal System.Windows.Forms.ToolStripButton toolStripPackageInvoice;
        internal System.Windows.Forms.ToolStripButton toolStripPackagePayment;
        private System.Windows.Forms.ToolStripMenuItem airLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currencyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;
        internal System.Windows.Forms.ToolStripButton toolStripSupplierTicketInvoice;
        internal System.Windows.Forms.ToolStripButton toolStripSupplierTicketPayment;
        private System.Windows.Forms.ToolStripMenuItem numberOfPackageSoldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userAccessRightsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripButton toolStripSupplierItemPayments;
        private System.Windows.Forms.ToolStripMenuItem signOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem customerOpeningBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierOpeningBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expensesTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expensesReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsCustomerReturnedTickets;
        private System.Windows.Forms.ToolStripButton tsCustomerReturnedPackages;
        private System.Windows.Forms.ToolStripButton tsSupplierReturnTicket;
        private System.Windows.Forms.ToolStripButton tsSupplierReturnItem;
        private System.Windows.Forms.ToolStripMenuItem returnedItemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returnedItemsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DailypaymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incomeStatmentToolStripMenuItem;
    }
}

