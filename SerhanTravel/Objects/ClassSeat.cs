﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class ClassSeat
    {
        private int classId;
        private string className;


        public ClassSeat(int classId,string className)
        {
            this.classId = classId;
            this.className = className;
        }
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        public int ClassId
        {
            get { return classId; }
            set { classId = value; }
        }

    }
}
