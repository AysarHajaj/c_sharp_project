﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsAccessRights
    {
        private int userId;
        private int screenId;
        private bool canAccess;
        private bool adding;
        private bool deleting;
        private bool updating;
        private bool printing;

        public bool CanAccess
        {
            get { return canAccess; }
            set { canAccess = value; }
        }

        public bool Printing
        {
            get { return printing; }
            set { printing = value; }
        }


        public bool Updating
        {
            get { return updating; }
            set { updating = value; }
        }


        public bool Deleting
        {
            get { return deleting; }
            set { deleting = value; }
        }


        public bool Adding
        {
            get { return adding; }
            set { adding = value; }
        }


        public int ScreenId
        {
            get { return screenId; }
            set { screenId = value; }
        }


        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

    }
}
