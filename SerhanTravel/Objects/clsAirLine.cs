﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsAirLine
    {
        private int airlineId;
        private string airLineName;

        public string AirLineName
        {
            get { return airLineName; }
            set { airLineName = value; }
        }

        public int AirLineId
        {
            get { return airlineId; }
            set { airlineId = value; }
        }

    }
}
