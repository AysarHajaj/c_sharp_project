﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsBank
    {
        private int bankId;
        private string bankName;

        public string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }


        public int BankId
        {
            get { return bankId; }
            set { bankId = value; }
        }

        
    }
}
