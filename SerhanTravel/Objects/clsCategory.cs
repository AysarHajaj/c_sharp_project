﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsCategory
    {
        private int categoryId;
        private string categoryName;


        public clsCategory(int categoryId,string categoryName)
        {
            this.categoryId = categoryId;
            this.categoryName = categoryName;
        }

        public string CategoryName
        {
            get { return categoryName; }
            set { categoryName = value; }
        }


        public int CategoryId
        {
            get { return categoryId; }
            set { categoryId = value; }
        }

    }
}
