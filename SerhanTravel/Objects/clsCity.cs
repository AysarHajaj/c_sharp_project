﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsCity
    {
        private int cityId;
        private string arabicName;
        private string englishName;
        private int countryId;

        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }

        public string EnglishName
        {
            get { return englishName; }
            set { englishName = value; }
        }

        public string ArabicName
        {
            get { return arabicName; }
            set { arabicName = value; }
        }

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

    }
}
