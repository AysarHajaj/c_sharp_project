﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsCompany
    {
        private int branchId;
        private string companyName;
        private string companyActivity;
        private string crNumber;
        private string address;
        private string phoneNumber;
        private string faxNumber;
        private string emailAddress;
        private string webSite;

        public string WebSite
        {
            get { return webSite; }
            set { webSite = value; }
        }

        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }

        public string FaxNumber
        {
            get { return faxNumber; }
            set { faxNumber = value; }
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string CRNumber
        {
            get { return crNumber; }
            set { crNumber = value; }
        }

        public string CompanyActivity
        {
            get { return companyActivity; }
            set { companyActivity = value; }
        }


        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public int BranchID
        {
            get { return branchId; }
            set { branchId = value; }
        }

    }
}
