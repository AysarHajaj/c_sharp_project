﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsCurrency
    {
        private int currencyId;
        private decimal currencyRate;
        private string currencySymbol;
        private string currencyName;

        public string CurrencyName
        {
            get { return currencyName; }
            set { currencyName = value; }
        }

        public string CurrencySymbol
        {
            get { return currencySymbol; }
            set { currencySymbol = value; }
        }


        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

    }
}
