﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsCustomer
    {
        private int customerId;
        private string customerName;
        private int nationalityId;
        private int customerType;
        private int countryId;
        private int cityId;
        private string address;
        private DateTime birthDate;
        private string birthPlace;
        private string mobilePhone;
        private string landPhone;
        private string faxNo;
        private string email;
        private string webSite;
        private int status;



        public int NationalityId
        {
            get { return nationalityId; }
            set { nationalityId = value; }
        }

        

        public int Status
        {
            get { return status; }
            set { status = value; }
        }


        public string WebSite
        {
            get { return webSite; }
            set { webSite = value; }
        }


        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string FaxNo
        {
            get { return faxNo; }
            set { faxNo = value; }
        }

        public string LandPhone
        {
            get { return landPhone; }
            set { landPhone = value; }
        }

        public string MobilePhone
        {
            get { return mobilePhone; }
            set { mobilePhone = value; }
        }

        public string BirthPlace
        {
            get { return birthPlace; }
            set { birthPlace = value; }
        }

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }

        public int CustomerType
        {
            get { return customerType; }
            set { customerType = value; }
        }


        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

               
    }
}
