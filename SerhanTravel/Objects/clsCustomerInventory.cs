﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
  public  class clsCustomerInventory
    {
        private int inventoryId;
        private int customerId;
        private decimal balance;
        private DateTime  date;
        private int currenncyId;
        private decimal currencyRate;

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int CurrencyId
        {
            get { return currenncyId; }
            set { currenncyId = value; }
        }

        public  DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public decimal Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public int InventoryId
        {
            get { return inventoryId; }
            set { inventoryId = value; }
        }

        public string DateString
        {
            get
            {
                return "("+inventoryId+")-"+this.Date.ToShortDateString();


            }
         
        }
    }
}
