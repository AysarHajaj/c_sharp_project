﻿using SerhanTravel.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsExpenses
    {
        private int expensesId;
        private DateTime expensesDate;
        private int expensesType;
        private int expensesInvoiceId;
        private decimal expensesAmount;
        private int expensesCurrencyId;
        private string expensesRemarks;
        private decimal currencyRate;
        Currency currencyConnection = new Currency();
        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public string ExpensesRemarks
        {
            get { return expensesRemarks; }
            set { expensesRemarks = value; }
        }



        public int ExpensesCurrencyId
        {
            get { return expensesCurrencyId; }
            set { expensesCurrencyId = value; }
        }


        public decimal ExpensesAmount
        {
            get { return expensesAmount; }
            set { expensesAmount = value; }
        }


        public int ExpensesInvoiceId
        {
            get { return expensesInvoiceId; }
            set { expensesInvoiceId = value; }
        }

        public int ExpensesType
        {
            get { return expensesType; }
            set { expensesType = value; }
        }

        public DateTime ExpensesDate
        {
            get { return expensesDate; }
            set { expensesDate = value; }
        }


        public int ExpensesId
        {
            get { return expensesId; }
            set { expensesId = value; }
        }

        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(expensesCurrencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }

        public string ExpensesTypeString
        {
            get
            {
                ExpensesType typeConnection = new ExpensesType();
                clsExpensesType type = typeConnection.getExpensesTypeById(ExpensesType);
                return type.ExpensesTypeEngName;
            }
        }

      
    }
}
