﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsExpensesType
    {
        private int expensesTypeId;
        private string expensesTypeEngName;
        private string expensesTypeArabName;
        private decimal amount;

        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public string ExpensesTypeArabName
        {
            get { return expensesTypeArabName; }
            set { expensesTypeArabName = value; }
        }


        public string ExpensesTypeEngName
        {
            get { return expensesTypeEngName; }
            set { expensesTypeEngName = value; }
        }


        public int ExpensesTypeId
        {
            get { return expensesTypeId; }
            set { expensesTypeId = value; }
        }

    }
}
