﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsGroup
    {
        private int groupId;
        private string groupName;

        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }

        public int GroupId
        {
            get { return groupId; }
            set { groupId = value; }
        }

    }
}
