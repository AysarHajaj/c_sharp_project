﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsInvoicePackagesDetails
    {
        private int invoicePackagesId;
        private int invoiceId;
        private clsPackage package;

        public clsPackage Package
        {
            get { return package; }
            set { package = value; }
        }



        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }


        public int InvoicePackagesId
        {
            get { return invoicePackagesId; }
            set { invoicePackagesId = value; }
        }

    }
}
