﻿using SerhanTravel.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsItem
    {
        private int ItemID;
        private string ItemName;
        private string ItemDescription;
        private int supplierId;
        private decimal cost;
        private int currencyId;
       string currencyFormat = "#,##0;-#,##0;0";
        private int quantity;
        private int categoryId;
        private decimal currencyRate;
        private string customerName;
        private string status;
        Currency currencyConnection = new Currency();
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public decimal Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public int CategoryId
        {
            get { return categoryId; }
            set { categoryId = value; }
        }

        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        

        public clsItem()
        {

        }

       

        public string itemDescription
        {
            get { return ItemDescription; }
            set { ItemDescription = value; }
        }


        public string itemName
        {
            get { return ItemName; }
            set { ItemName = value; }
        }


        public int itemID
        {
            get { return ItemID; }
            set { ItemID = value; }
        }
        
        public string ToStrings
        {
            get
            {
                Supplier connection = new Supplier();

                return ItemName + " from " + connection.getSupplier(supplierId) ;
            }
        }

        public string ToToString
        {
            get
            {
                Supplier connection = new Supplier();

                return ItemName + " from " + connection.getSupplier(supplierId) + " Cost  " + cost.ToString(currencyFormat)+ " " + CurrecnySymbol + "  " + ItemDescription;
            }
        }

        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }
        public decimal totalCost
        {
            get
            {
                return (decimal)quantity * cost;
            }
        }

        public string SupplierReturn
        {

            get
            {
                return ItemName +"("+CustomerName+")" +"Cost " + Cost + " " + CurrecnySymbol;
            }
        }
        public string CostString
        {
            get
            {
                return cost + " " + CurrecnySymbol;
            }
        }
    }
}
