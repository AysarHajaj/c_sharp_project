﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsNationality
    {
        private int nationalityId;
        private string arabicName;
        private string englishName;

        public string EnglishName
        {
            get { return englishName; }
            set { englishName = value; }
        }

        public string ArabicName
        {
            get { return arabicName; }
            set { arabicName = value; }
        }

        public int NationalityId
        {
            get { return nationalityId; }
            set { nationalityId = value; }
        }

    }
}
