﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsNumberOfPackageSold
    {
        private int packageId;
        private string packageName;
        private int number;
        private double percentage;

        public double Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }

        public int Number
        {
            get { return number; }
            set { number = value; }
        }



        public string PackageName
        {
            get { return packageName; }
            set { packageName = value; }
        }


        public int PackageId
        {
            get { return packageId; }
            set { packageId = value; }
        }

    }
}
