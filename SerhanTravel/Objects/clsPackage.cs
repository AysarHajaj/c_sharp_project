﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using SerhanTravel.Connections;
using System.Windows.Forms;

namespace SerhanTravel.Objects
{
    class clsPackage
    {
        private int PackageID;
        private string PackageName;
        private int CountryID;
        private int PackageDays;
        private List<clsItem> PackageItems;
        string currencyFormat = "#,##0;-#,##0;Zero";
        private int currencyId;
        private decimal currencyRate;
        private decimal price;
        private decimal profit;
        private string  customerName;
        Currency currencyConnection = new Currency();
        Country countryConnnection = new Country();
        public string  CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public decimal Profit
        {
            get { return profit; }
            set { profit = value; }
        }

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }





        public clsPackage()
        {
            PackageItems = new List<clsItem>();
        }
        public List<clsItem> packageItems
        {
            get { return PackageItems; }
            set { PackageItems = value;

               
            }
        }



        public int packageDays
        {
            get { return PackageDays; }
            set { PackageDays = value; }
        }


        public int countryID
        {
            get { return CountryID; }
            set { CountryID = value; }
        }


        public string packageName
        {
            get { return PackageName; }
            set { PackageName = value; }
        }


        public int packageID
        {
            get { return PackageID; }
            set { PackageID = value; }
        }

        public string CountryName
        {
            get
            {
                return countryConnnection.getCountryById(countryID).EnglishName.ToString();
            }
        }

        public int getCount()
        {
            return packageItems.Count;
        }

        public decimal Cost
        {
            get
            {
                decimal cost = 0;
                int count = getCount();
                for (int i = 0; i < count; i++)
                {
                    clsItem item = this.packageItems[i];
                    cost += item.Cost* item.CurrencyRate;
                }
                
                cost /= this.CurrencyRate;
               
                return cost;
            }
        }

        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }

        public string PriceString
        {
            get
            {

                return Price.ToString(currencyFormat) + " " + CurrecnySymbol;
            }
        }
        public string ToStrings
        {
            get
            {
                
                return packageName + " to " + CountryName+ " for " + packageDays + " days for " + Price.ToString(currencyFormat) + " "+ CurrecnySymbol;
            }
        }
        public string itemsStr
        {
            get
            {
                string description = "";

                for(int i=0;i<packageItems.Count;i++)
                {
                    description+=(PackageItems[i].itemName+"\n");
                }
                return description;
                
            }
        }
        public string PackageDescription
        {
            get
            {

                return "("+ CustomerName + ")*"+PackageName+"*\t " +Price.ToString(currencyFormat)+" " + CurrecnySymbol+" .";
            }
        }
        public string ProfitString
        {
            get
            {

                return Profit.ToString(currencyFormat) + " " + CurrecnySymbol;
            }
        }
    }
}
