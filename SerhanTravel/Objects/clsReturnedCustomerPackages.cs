﻿using SerhanTravel.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsReturnedCustomerPackages
    {
        private int id;
        private int invoiceId;
        private DateTime date;
        private decimal returnedAmount;
        private clsPackage package;
        private int currencyId;
        private int itemId;
        private object item;
        private int packageId;
        private decimal currencyRate;
        Currency currencyConnection = new Currency();
        private string customer;

        public string Customer
        {
            get { return customer; }
            set { customer = value; }
        }

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }


        public int PackageId
        {
            get { return packageId; }
            set { packageId = value; }
        }


        public object Item
        {
            get { return item; }
            set { item = value; }
        }

        public int ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public clsPackage Package
        {
            get { return package; }
            set { package = value; }
        }

        public decimal ReturnAmount
        {
            get { return returnedAmount; }
            set { returnedAmount = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string ReturnedItem
        {
            get
            {
                if(ItemId==0)
                {
                    return "(Package)";
                }
                else
                {
                    return "(Item)";
                }
            }
        }
        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }
        /*   public decimal Remained
           {
               get
               {
                   return Convert.ToDecimal(this.package.Price) - this.returnedAmount;
               }
           }*/
    }
}
