﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsReturnedSupplierItem
    {
        private int id;
        private int invoiceId;
        private DateTime date;
        private decimal returnedAmount;
        private clsItem items;
        private int currencyId;
        private decimal currencyRate;
        string currencyFormat = "#,##0;-#,##0;Zero";
        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public clsItem Item
        {
            get { return items; }
            set { items = value; }
        }

        public decimal ReturnAmount
        {
            get { return returnedAmount; }
            set { returnedAmount = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Remained
        {
           get
            {
                return ((Item.Cost) - ReturnAmount).ToString(currencyFormat)+" "+Item.CurrecnySymbol;
            }
        }
  
        public string ReturnedAmountString
        {
            get
            {
                return ReturnAmount.ToString(currencyFormat) + " " + Item.CurrecnySymbol;
            }
        }
    }
}
