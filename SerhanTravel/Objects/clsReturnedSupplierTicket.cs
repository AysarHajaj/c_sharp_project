﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsReturnedSupplierTicket
    {
        private int id;
        private DateTime date;
        private decimal returnedAmount;
        private clsTickets tickets;
        private int currencyId;
        private decimal currencyRate;

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }
        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public clsTickets Ticket
        {
            get { return tickets; }
            set { tickets = value; }
        }

        public decimal ReturnAmount
        {
            get { return returnedAmount; }
            set { returnedAmount = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

 

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public decimal Remained
        {
            get
            {
                return Convert.ToDecimal(this.tickets.Cost) - this.returnedAmount;
            }
        }
    }
}
