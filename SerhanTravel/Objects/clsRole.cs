﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsRole
    {
        private int roleId;
        private string roleType;

        public string RoleType
        {
            get { return roleType; }
            set { roleType = value; }
        }


        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }

    }
}
