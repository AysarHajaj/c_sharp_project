﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsSalesByType
    {
        private int invoiceId;
        private string description;
        private string paidAmount;
        private string  cost;
        private string profit;

        public string Profit
        {
            get { return profit; }
            set { profit = value; }
        }

        public string  CostPrice
        {
            get { return cost; }
            set { cost = value; }
        }


        public string PaidAmount 
        {
            get { return paidAmount; }
            set { paidAmount = value; }
        }


        public string Description
        {
            get { return description; }
            set { description = value; }
        }


        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }


    }
}
