﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsScreen
    {
        private int screenId;
        private string screenName;
        private clsAccessRights screenAccessRights;

        public clsScreen(string Name,int ScreenId)
        {
            this.ScreenId = ScreenId;
            this.ScreenName = Name;
        }
        public clsAccessRights ScreenAccessRights
        {
            get { return screenAccessRights; }
            set { screenAccessRights = value; }
        }


        public string ScreenName
        {
            get { return screenName; }
            set { screenName = value; }
        }


        public int ScreenId
        {
            get { return screenId; }
            set { screenId = value; }
        }

    }
}
