﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsStatus
    {
        private int statusId;
        private string statusName;

        public clsStatus(int statusId,string statusName)
        {
            this.statusId = statusId;
            this.statusName = statusName;
        }


        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

    }
}
