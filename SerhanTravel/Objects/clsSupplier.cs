﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsSupplier
    {
        private int supplierId;
        private string supplierName;
        private string contactPerson;
        private int groupId;
        private int countryId;
        private int cityId;
        private string address;
        private string mobilePhone;
        private string landPhone;
        private string faxNo;
        private string email;
        private string webSite;
        private int status;
        public int GroupId
        {
            get { return groupId; }
            set { groupId = value; }
        }

        public string ContactPerson
        {
            get { return contactPerson; }
            set { contactPerson = value; }
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }


        public string WebSite
        {
            get { return webSite; }
            set { webSite = value; }
        }


        public string Email
        {
            get { return email; }
            set { email = value; }
        }


        public string FaxNo
        {
            get { return faxNo; }
            set { faxNo = value; }
        }

        public string LandPhone
        {
            get { return landPhone; }
            set { landPhone = value; }
        }

        public string MobilePhone
        {
            get { return mobilePhone; }
            set { mobilePhone = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public int CityId
        {
            get { return cityId; }
            set { cityId = value; }
        }

        public int CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }



        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }

        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }


    }
}


