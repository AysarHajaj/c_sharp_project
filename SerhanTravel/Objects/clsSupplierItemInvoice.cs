﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsSupplierItemInvoice
    {
        private int invoiceId;
        private int supplierId;
        private DateTime date;

        public clsSupplierItemInvoice()
        {
            Items = new List<clsItem>();
        }
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        private List<clsItem> items;

        public List<clsItem> Items
        {
            get { return items; }
            set { items = value; }
        }

        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }


        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }

    }
}
