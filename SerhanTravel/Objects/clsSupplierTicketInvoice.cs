﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsSupplierTicketInvoice
    {
        private int invoiceId;
        private int supplierId;
        private string supplierName;
        private DateTime date;
        private decimal totalPrice;
        private List<clsTickets> tickets;

        public List<clsTickets> Tickets
        {
            get { return tickets; }
            set { tickets = value; }
        }


        public decimal TotalPrice
        {
            get { return totalPrice; }
            set { totalPrice = value; }
        }


        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }


        public string SupplierName
        {
            get { return supplierName; }
            set { supplierName = value; }
        }


        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }


        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }

    }
}
