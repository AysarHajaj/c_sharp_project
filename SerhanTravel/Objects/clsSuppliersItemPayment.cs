﻿using SerhanTravel.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsSuppliersItemPayment
    {
        private int supplierInvoiceId;
        private DateTime date;
        private decimal payment;
        private string chequeNo;
        private int bankId;
        private string chequeDate;
        private int paymentType;
        private string collectedDate;
        private int paymentId;
        private int currencyId;
        private decimal currencyRate;
        private int supplierId;
        Bank bankConnection = new Bank();
        Currency currencyConnection = new Currency();
        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }

        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public clsSuppliersItemPayment()
        {

        }


        public int PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }



        public int PaymentId
        {
            get { return paymentId; }
            set { paymentId = value; }
        }

        public string CollectedDate
        {
            get { return collectedDate; }
            set { collectedDate = value; }
        }

        public string ChequeDate
        {
            get { return chequeDate; }
            set { chequeDate = value; }
        }

        public int BankId
        {
            get { return bankId; }
            set { bankId = value; }
        }

        public string ChequeNo
        {
            get { return chequeNo; }
            set { chequeNo = value; }
        }

        public decimal Payment
        {
            get { return payment; }
            set { payment = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }


        public int SupplierInvoiceId
        {
            get { return supplierInvoiceId; }
            set { supplierInvoiceId = value; }
        }
        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }
        public string BankName
        {
            get
            {
                return bankConnection.getBankById(BankId).BankName;
            }
        }
    }
}
