﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsTicketInvoice
    {
        private int invoiceId;
        private int customerId;
        private DateTime invoiceDate;
        private string customerName;
        private decimal discount;
        private decimal netPrice;


   


        public decimal NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }

        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public DateTime InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }

        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }

    }
}
