﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel
{
    class clsTickets
    {
        private int ticketId;
        private DateTime ticketDate;
        private int airLineId;
        private int supplierId;
        private string remarks;
        private int ticketClassId;
        private string flightNo;
        private string destination;
        private string ticketNo;
        private string flightSchedual;
        private string travellerName;
        private decimal price;
        private int customerId;
        Customer customerConnection = new Customer();
        private decimal cost;
        private int currencyId;
        private string  status;
        private string referenceNo;
        private decimal profit;
        private decimal currencyRate;
        string currencyFormat = "#,##0.00;-#,##0.00;Zero";
        Currency currencyConnection = new Currency();
        AirLines airLinConnection = new AirLines();
        public decimal CurrencyRate
        {
            get { return currencyRate; }
            set { currencyRate = value; }
        }

        public decimal Profit
        {
            get { return profit; }
            set { profit = value; }
        }


        public string ReferenceNo
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        public string  Status
        {
            get { return status; }
            set { status = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }

        public decimal Cost
        {
            get { return cost; }
            set { cost = value; }
        }


        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }


   

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }


        public string TravellerName
        {
            get { return travellerName; }
            set { travellerName = value; }
        }

        public string FlightSchedual
        {
            get { return flightSchedual; }
            set { flightSchedual = value; }
        }
        AirLines airlineConnection = new AirLines();
        public string AirlineName
        {
            get
            {
                return airLinConnection.getAirLineById(AirLineId).AirLineName;
            }
        }

        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }

        public string Destination
        {
            get { return destination; }
            set { destination = value; }
        }


        public string FlightNo
        {
            get { return flightNo; }
            set { flightNo = value; }
        }

        public int TicketClassId
        {
            get { return ticketClassId; }
            set { ticketClassId = value; }
        }


        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }


        public int SupplierId
        {
            get { return supplierId; }
            set { supplierId = value; }
        }


        public int AirLineId
        {
            get { return airLineId; }
            set { airLineId = value; }
        }

        public DateTime TicketDate
        {
            get { return ticketDate; }
            set { ticketDate = value; }
        }

        public int TicketId
        {
            get { return ticketId; }
            set { ticketId = value; }
        }

        public string CurrecnySymbol
        {
            get
            {
               
                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);
             
                if (currency == null)
                    return string.Empty;
                else
                 return currency.CurrencySymbol;
            }
        }
        public string ReturnString
        {

            get
            {

                return "Ticket No: " + ticketNo + " for " + travellerName + " Price " + Price.ToString(currencyFormat) + " " + CurrecnySymbol;
            }
        }
        public string SupplierReturn
        {

            get
            {

                return "Ticket No: " + ticketNo + " for " + travellerName + " Cost " + Cost.ToString(currencyFormat) + " " + CurrecnySymbol;
            }
        }
        public string ToStR
        {
         
            get
            {
                
                return "Ticket No: " + ticketNo + " for " + travellerName + " Reserved at " +ticketDate.ToShortDateString();
            }
        }

        public string Description
        {

            get
            {
                //return "This ticket reserved by " + customerConnection.getCustomerById(this.customerId).CustomerName + "\nat " + this.ticketDate.ToShortDateString() + "\nAirLine : " + Module.getAirLineById(this.airLineId).AirLineName;
                 if(CustomerId!=0)
                     return "This Ticket Reserved by " + customerConnection.getCustomerById(this.customerId).CustomerName +"\nTraveler Name: "+travellerName+ "\nat " + this.ticketDate.ToShortDateString() + "\nAirLine : " + airLinConnection.getAirLineById(this.airLineId).AirLineName;
                 else
                     return "This Ticket Reserved by a public Customer\nTraveler Name: "+travellerName+"\nat " + this.ticketDate.ToShortDateString() + "\nAirLine : " + airLinConnection.getAirLineById(this.airLineId).AirLineName;
            }
        }

        public string SupplierToString
        {

            get
            {
                return "Ticket No: "+ticketNo + " for " + travellerName + " Reserved at " + ticketDate.ToShortDateString()+" Cost : " +cost+" "+CurrecnySymbol;
            }
        }

        public string TicketString
        {
            get
            {
                return TicketNo +" - "+ TravellerName;
            }
        }
    }
}
