﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsType
    {
        private int typeId;
        private string typeName;

      public  clsType(int typeId, string typeName)
        {
            this.typeId = typeId;
            this.typeName = typeName;
        }

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        public int TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }

    }
}
