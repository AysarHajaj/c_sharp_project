﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.Objects
{
    class clsUser
    {
        private int userId;
        private string userName;
        private string password;
        private int roleId;
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }


        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

    }
}
