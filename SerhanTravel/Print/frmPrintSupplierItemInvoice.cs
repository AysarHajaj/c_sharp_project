﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class frmPrintSupplierItemInvoice : Form
    {

        List<clsItem> itemlist;
        clsSupplierItemInvoice invoice;
        double totalPrice;
        List<clsCompany> company;
        Company CompanyInfo;
        clsCurrency currency;


        internal frmPrintSupplierItemInvoice(List<clsItem> itemList, clsSupplierItemInvoice invoice, double totalPrice, clsCurrency currency)
        {
            InitializeComponent();
            this.itemlist = itemList;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            this.invoice = invoice;
            this.totalPrice = totalPrice;
            this.currency = currency;
        }
        static DataTable ConvertToDatatable(List<clsItem> itemList)
        {
            DataTable dt = new DataTable();

            /*
            dt.Columns.Add("itemName");
            dt.Columns.Add("itemDescription");
            dt.Columns.Add("Cost");
            dt.Columns.Add("Quantity");
            dt.Columns.Add("totalCost");
            dt.Columns.Add("CurrecnySymbol");

            foreach (var item in itemList)
            {
                var row = dt.NewRow();


                row["itemName"] = item.itemName;
                row["itemDescription"] = item.itemDescription;
                row["Cost"] = Convert.ToString(item.Cost);
                row["CurrecnySymbol"] = Convert.ToString(item.CurrecnySymbol);
                row["Quantity"] = item.Quantity;
                row["totalCost"] = item.totalCost;
                


                dt.Rows.Add(row);
            }*/

            return dt;
        }
        private void frmPrintSupplierItemInvoice_Load(object sender, EventArgs e)
        {
            /*
            DataTable dtt = ConvertToDatatable(itemlist);


            this.reportViewer1.LocalReport.DataSources.Clear();

            ReportDataSource reportsource = new ReportDataSource("dsSupplierItemsInvoice", dtt);
            this.reportViewer1.LocalReport.DataSources.Add(reportsource);

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                     new Microsoft.Reporting.WinForms.ReportParameter("invoiceDate",invoice.Date.ToShortDateString()),
                      new Microsoft.Reporting.WinForms.ReportParameter("invoiceId",invoice.InvoiceId.ToString()),
                       new Microsoft.Reporting.WinForms.ReportParameter("supplierName",invoice.SupplierName),
                        new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                           new Microsoft.Reporting.WinForms.ReportParameter("totalPrice",totalPrice.ToString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("supplierName", invoice.SupplierName.ToString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("currency",currency.CurrencySymbol.ToString())

                  };

            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();*/
        }
    }
}
