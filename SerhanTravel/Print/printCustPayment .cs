﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class printCustTicketPayment : Form
    {

        clsTicketsPayment payment;
        List<clsCompany> company;
        Company CompanyInfo;
        int paymetNo;
        string customerName;
        string paymentFor;
        Currency currencyConnection;
        internal printCustTicketPayment( clsTicketsPayment payment, int paymetNo,string customerName,string paymentFor)
        {
            InitializeComponent();
            this.customerName = customerName;
            this.payment = payment;
            this.paymetNo = paymetNo;
            this.paymentFor = paymentFor;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
        
        }

        private void printTicketPayment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            string paymentType = "";
            if (payment.PaymentType == 1)
            {
                paymentType = "Cheque , Cheque No : " + payment.ChequeNo;
            }
            else
            {
                paymentType = "Cash";
            }
            string tafqeet =TafqeetUSD.NumWordsWrapper((double)payment.Payment);
            clsCurrency currency=new clsCurrency();
            if (payment.CurrencyId!=0)
            {
               currency = currencyConnection.GetCurrencyById(payment.CurrencyId);
                tafqeet += " " + currency.CurrencyName + " Only.";
            }
            else
            {
                currency.CurrencySymbol = "L.L";
                tafqeet += " L.L Only.";
            }
            string format = "#,##0.00;-#,##0.00;Zero";
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                         new Microsoft.Reporting.WinForms.ReportParameter("customerName",this.customerName),
                          new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                          new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("paymentDate",payment.Date.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("paymentNo",paymetNo.ToString()),
                                   new Microsoft.Reporting.WinForms.ReportParameter("payment",payment.Payment.ToString(format)),
                                                new Microsoft.Reporting.WinForms.ReportParameter("paymentFor",paymentFor ),
                                                    new Microsoft.Reporting.WinForms.ReportParameter("paymentType",paymentType .ToString()),
                                                        new Microsoft.Reporting.WinForms.ReportParameter("tafqeet",tafqeet),
                                                         new Microsoft.Reporting.WinForms.ReportParameter("currecnySymbol",currency.CurrencySymbol)
                                                       
                  };

            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
        }
    }
}
