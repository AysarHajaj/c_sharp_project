﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class printPackageInvoice : Form
    {
        List<clsPackage> packageList;
        clsPackageInvoice invoice;
        clsCurrency currency;
        List<clsCompany> company;
        Company CompanyInfo;
        double totalprice;
        double netPrice;
        internal printPackageInvoice(List<clsPackage>packageList,clsPackageInvoice invoice,clsCurrency currency,double totalprice,double netPrice)
        {
            InitializeComponent();
            this.packageList = packageList;
            this.invoice = invoice;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            this.currency = currency;
            this.totalprice = totalprice;
            this.netPrice = netPrice;

        }
        static DataTable ConvertToDatatable(List<clsPackage> packageList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("PackageName");
            dt.Columns.Add("CountryName");
            dt.Columns.Add("packageDays");
            dt.Columns.Add("Price");
            dt.Columns.Add("itemsStr");
            dt.Columns.Add("CurrecnySymbol");

            foreach (var item in packageList)
            {
                var row = dt.NewRow();


                row["PackageName"] = item.packageName;
                row["CountryName"] = item.CountryName;
                row["packageDays"] = Convert.ToString(item.packageDays);
                row["Price"] = Convert.ToString(item.Price);
                row["itemsStr"] = item.itemsStr;
                row["CurrecnySymbol"] = item.CurrecnySymbol;

                dt.Rows.Add(row);
            }

            return dt;
        }
        private void printPackageInvoice_Load(object sender, EventArgs e)
        {
         DataTable dtt = ConvertToDatatable(packageList);

            string format = "#,##0.00;-$#,##0.00;Zero";

            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsPackageInvoice", dtt);
       
             this.reportViewer1.LocalReport.DataSources.Add(reportsource);


              Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                    {
                       new Microsoft.Reporting.WinForms.ReportParameter("invoiceDate",invoice.InvoiceDate.ToShortDateString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("invoiceId",invoice.InvoiceId.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("customerName",invoice.CustomerName),
                          new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                          new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("netPrice",netPrice.ToString(format)),
                             new Microsoft.Reporting.WinForms.ReportParameter("totalPrice",totalprice.ToString(format)),
                              new Microsoft.Reporting.WinForms.ReportParameter("discount",invoice.Discount.ToString()),
                          new Microsoft.Reporting.WinForms.ReportParameter("currency",currency.CurrencySymbol)
                    };

              this.reportViewer1.LocalReport.SetParameters(p);

            this.reportViewer1.RefreshReport();
          
        }
    }
}
