﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class printPackagePayment : Form
    {
        clsPackagesPayment payment;
        clsPackageInvoice invoice;
   
        List<clsCompany> company;
        Company CompanyInfo;
        int paymetNo;

        Currency currencyConnection;

        internal printPackagePayment(clsPackageInvoice invoice,clsPackagesPayment payment,int paymetNo)
        {
            InitializeComponent();
            this.invoice = invoice;
            this.payment = payment;
            this.paymetNo = paymetNo;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
      
        }

    
        private void printPackagePayment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
          string  currencyFormat = "#,##0.00;-#,##0.00;Zero";
            string paymentType = "";
            if (payment.PaymentType == 1)
            {
                paymentType = "Cheque , Cheque No : " + payment.ChequeNo;
            }
            else
            {
                paymentType = "Cash";
            }

            string paymentFor = "Package(s) Reservation ";
            clsCurrency currency = new clsCurrency();
            string tafqeet = TafqeetUSD.NumWordsWrapper((double)payment.Payment);
            if (payment.CurrencyId != 0)
            {
                currency = currencyConnection.GetCurrencyById(payment.CurrencyId);
                tafqeet += " " + currency.CurrencyName + " Only.";
            }
            else
            {
                currency.CurrencySymbol = "L.L";
                tafqeet += " L.L Only.";
            }

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                       new Microsoft.Reporting.WinForms.ReportParameter("invoiceDate",invoice.InvoiceDate.ToShortDateString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("invoiceId",invoice.InvoiceId.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("customerName",invoice.CustomerName),
                          new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                          new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("paymentDate",payment.Date.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("paymentNo",paymetNo.ToString()),
                                   new Microsoft.Reporting.WinForms.ReportParameter("payment",payment.Payment.ToString(currencyFormat)),
                                                new Microsoft.Reporting.WinForms.ReportParameter("paymentFor",paymentFor ),
                                                          new Microsoft.Reporting.WinForms.ReportParameter("tafqeet",tafqeet),
                                                         new Microsoft.Reporting.WinForms.ReportParameter("currecnySymbol",currency.CurrencySymbol),
                                                    new Microsoft.Reporting.WinForms.ReportParameter("paymentType",paymentType .ToString())
                  };

            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
        }
    }
}
