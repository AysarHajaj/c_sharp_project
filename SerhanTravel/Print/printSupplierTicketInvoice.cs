﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class printSupplierTicketInvoice : Form
    {
        
            List<clsTickets> itemlist;
        clsSupplierTicketInvoice invoice;
        double totalPrice;
        List<clsCompany> company;
        Company CompanyInfo;
        clsCurrency currency;

        internal printSupplierTicketInvoice(List<clsTickets> itemList,clsSupplierTicketInvoice invoice,double totalPrice,clsCurrency currency)
        {
            InitializeComponent();
            this.itemlist = itemList;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            this.invoice = invoice;
            this.totalPrice = totalPrice;
            this.currency = currency;
            
        }
        
        static DataTable ConvertToDatatable(List<clsTickets> itemList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("TicketNo");
            dt.Columns.Add("FlightNo");
            dt.Columns.Add("Cost");
            dt.Columns.Add("CurrecnySymbol");

            foreach (var item in itemList)
            {
                var row = dt.NewRow();


                row["TicketNo"] = item.TicketNo;
                row["FlightNo"] = item.FlightNo;
                row["Cost"] =Convert.ToString(item.Cost);
                row["CurrecnySymbol"] = Convert.ToString(item.CurrecnySymbol);


                dt.Rows.Add(row);
            }

            return dt;
        }
        private void printSupplierInvoice_Load(object sender, EventArgs e)
        {

           DataTable dtt = ConvertToDatatable(itemlist);

           
            this.reportViewer1.LocalReport.DataSources.Clear();

            ReportDataSource reportsource = new ReportDataSource("dsSupplierTicketInvoice", dtt);
            this.reportViewer1.LocalReport.DataSources.Add(reportsource);
           
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                     new Microsoft.Reporting.WinForms.ReportParameter("invoiceDate",invoice.Date.ToShortDateString()),
                      new Microsoft.Reporting.WinForms.ReportParameter("invoiceId",invoice.InvoiceId.ToString()),
                       new Microsoft.Reporting.WinForms.ReportParameter("supplierName",invoice.SupplierName),
                        new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                           new Microsoft.Reporting.WinForms.ReportParameter("totalPrice",totalPrice.ToString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("supplierName", invoice.SupplierName.ToString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("currency",currency.CurrencySymbol.ToString())

                  };
                 
      this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }
    }
}
