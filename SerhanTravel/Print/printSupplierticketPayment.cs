﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.Print
{
    public partial class printSupplierticketPayment : Form
    {

        clsSuppliersTickertPayment payment;
        List<clsCompany> company;
        Company CompanyInfo;
        int paymetNo;
        clsSupplier supplier;
        Currency currencyConnection;
        internal printSupplierticketPayment( clsSuppliersTickertPayment payment, int paymetNo,clsSupplier supplier)
        {
            InitializeComponent();
            this.payment = payment;
            this.paymetNo = paymetNo;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            this.supplier = supplier;
        }
        
        private void printSupplierPayment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
         string paymentType = "";
            if (payment.PaymentType == 1)
            {
                paymentType = "Cheque , Cheque No : " + payment.ChequeNo;
            }
            else
            {
                paymentType = "Cash ";
            }
            string tafqeet = TafqeetUSD.NumWordsWrapper((double)payment.Payment)+" " ;
            clsCurrency currency= currencyConnection.GetCurrencyById(payment.CurrencyId);
            if(currency!=null)
            {
                tafqeet += currency.CurrencyName;
            }
            tafqeet += " only.";


           // supplier.MobilePhone = 135;
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                      
                          new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                          new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("paymentDate",payment.Date.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("paymentNo",paymetNo.ToString()),
                                   new Microsoft.Reporting.WinForms.ReportParameter("payment",payment.Payment.ToString()),
                                     new Microsoft.Reporting.WinForms.ReportParameter("supplierPhone",supplier.MobilePhone.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("supplierAddress",supplier.Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("contactPerson",supplier.ContactPerson.ToString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("supplierName",supplier.SupplierName.ToString()),
                                   new Microsoft.Reporting.WinForms.ReportParameter("tafqeet",tafqeet.ToString()),
                                   new Microsoft.Reporting.WinForms.ReportParameter("currecnySymbol",payment.CurrecnySymbol.ToString()),
                                                    new Microsoft.Reporting.WinForms.ReportParameter("paymentType",paymentType .ToString()),
                                                    new Microsoft.Reporting.WinForms.ReportParameter("paymentFor","Ticket(s) Reservation.")
                  };

            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
        }
    }
}
