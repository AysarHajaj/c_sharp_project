﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using SerhanTravel.Objects;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;

namespace SerhanTravel.Print
{
    public partial class printticketInvoice : Form
    {
        List<clsTickets> ticketList;
        List <clsCompany> company;
        Company CompanyInfo;
        clsTicketInvoice invoice;
        private double totalPrice;
        private double netPrice;
        clsCurrency currency;

        internal printticketInvoice(List<clsTickets> ticketList, clsTicketInvoice invoice,double totalPrice,double  netPrice,clsCurrency currency)
        {
            InitializeComponent();
            this.ticketList = ticketList;
            this.invoice = invoice;
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            this.netPrice = netPrice;
            this.totalPrice = totalPrice;
            this.currency = currency;

        }
  
        static DataTable ConvertToDatatable(List<clsTickets> ticketlist)
        {
            DataTable dt = new DataTable();


             dt.Columns.Add("ticketNo");
            dt.Columns.Add("destination");
              dt.Columns.Add("flightNo");
            dt.Columns.Add("travellerName");
            dt.Columns.Add("price");
            dt.Columns.Add("CurrecnySymbol");
            foreach (var item in ticketlist)
            {
                var row = dt.NewRow();

         
                row["ticketNo"] =item.TicketNo;
                row["destination"] = item.Destination;
                row["travellerName"] = item.TravellerName;
                row["flightNo"] = Convert.ToString(item.FlightNo);
                row["price"] = Convert.ToString(item.Price);
                row["CurrecnySymbol"] = item.CurrecnySymbol;



                dt.Rows.Add(row);
            }

            return dt;
        }
        private void printticketInvoice_Load(object sender, EventArgs e)
        {
           
         DataTable dtt = ConvertToDatatable(ticketList);
            string format = "#,##0.00;-#,##0.00;Zero";

            this.reportViewer1.LocalReport.DataSources.Clear();
    
         ReportDataSource reportsource = new ReportDataSource("dsticketinvoice", dtt);
             this.reportViewer1.LocalReport.DataSources.Add(reportsource);

          Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                {
                     new Microsoft.Reporting.WinForms.ReportParameter("invoiceDate",invoice.InvoiceDate.ToShortDateString()),
                      new Microsoft.Reporting.WinForms.ReportParameter("invoiceId",invoice.InvoiceId.ToString()),
                       new Microsoft.Reporting.WinForms.ReportParameter("customerName",invoice.CustomerName),
                        new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                           new Microsoft.Reporting.WinForms.ReportParameter("totalPrice",totalPrice.ToString(format)),
                           new Microsoft.Reporting.WinForms.ReportParameter("netPrice",netPrice.ToString(format)),
                            new Microsoft.Reporting.WinForms.ReportParameter("discount",invoice.Discount.ToString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("currency",currency.CurrencySymbol)

                };

            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
       
        }


    }
}

