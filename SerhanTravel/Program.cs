﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerhanTravel.Forms;

namespace SerhanTravel
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            frmLogin login = new frmLogin();
            
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
          
            Application.Run(login);
        }
    }
}
