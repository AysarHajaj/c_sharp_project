﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class CustomerSummaryOfPayment : Form
    {
        private List<clsCurrency> currencies;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList;
        Customer customerConnection;
        clsCustomer customerData;
        Ticket ticketConnection;
        private List<clsTickets> ticketList;
        TicketsPayment paymentConnection;
        List<clsTicketsPayment> invoicepaymentList;
        PackageInvoice packageInvoiceConnection;
        List<clsPackageInvoice> packageInvoiceList;
        PackagesPayment packagePaymentConnection;
        List<clsPackage> packageList;
        List<clsPackagesPayment> packageInvoicepaymentList;
        bool skip = false;
        int ticketsSold;
        int packagesSold;
        decimal ticketsPrice;
        decimal packagesPrice;
        decimal packagesPaid;
        decimal ticketsPaid;
        ReturnedCustomerPackage returnPackage;
        ReturnedCustomerTicket returnTicket;
        clsCustomerInventory inventoryData;
        CustomerBalanceInventory inventoryConnection;
        List<clsCompany> company;
        Company CompanyInfo;
        Currency currencyConnection;
        public CustomerSummaryOfPayment()
        {
            InitializeComponent();
        }
     
        private void CustomerSummaryOfPayment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            ticketConnection = new Ticket();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            returnTicket = new ReturnedCustomerTicket();
            returnPackage = new ReturnedCustomerPackage();
            lbResult.Visible = false;
            customerConnection = new Customer();
            inventoryConnection = new CustomerBalanceInventory();
            paymentConnection = new TicketsPayment();
            invoicepaymentList = new List<clsTicketsPayment>();
            packageInvoiceConnection = new PackageInvoice();
            packagePaymentConnection = new PackagesPayment();
            customerList = customerConnection.CustomersArrayList();
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
        
            reportviewer1.Visible = false;
            this.reportviewer1.RefreshReport();

       
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbCustomerName.SelectedIndex != -1)
            {
                skip = true;
            }
        }

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.reportviewer1.LocalReport.DataSources.Clear();
            decimal OpenningBalance = 0;
            this.reportviewer1.RefreshReport();
            string returnedTickets, returnedPackages;
            int nmOfReturnedTickets, nmOfReturnedItem, nmofReturnedPackages;
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                nmOfReturnedTickets = 0;
                nmOfReturnedItem = 0;
                nmofReturnedPackages = 0;
                returnedTickets = " ";
                returnedPackages = " ";
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                customerData = (clsCustomer)cmbCustomerName.SelectedItem;
                ticketList = ticketConnection.CustomerTickets(fromDate, toDate, customerData.CustomerId);
                inventoryData = inventoryConnection.CustomerLastInventory(customerData.CustomerId);
                if (inventoryData == null)
                {
                    inventoryData = new clsCustomerInventory();
                    inventoryData.Date = DefaultDate;
                    inventoryData.Balance = 0;
                    inventoryData.CustomerId = customerData.CustomerId;
                    inventoryData.InventoryId = 0;
                    inventoryData.CurrencyId = 0;
                }
                OpenningBalance += inventoryData.Balance;
                OpenningBalance += customerConnection.getCustomerBalance(customerData.CustomerId, inventoryData.Date, fromDate);
                OpenningBalance /= currency.CurrencyRate;
                decimal Paid = paymentConnection.TotalPaid(customerData.CustomerId, inventoryData.Date, fromDate);
                Paid += packagePaymentConnection.TotalPaid(customerData.CustomerId, inventoryData.Date, fromDate);
                Paid /= currency.CurrencyRate;
                OpenningBalance -= Paid;
                ticketsSold = 0;
                packagesSold = 0;
                ticketsPrice = 0;
                packagesPrice = 0;
                packagesPaid = 0;
                ticketsPaid = 0;
                ticketsSold = ticketList.Count;
                for (int i = 0; i < ticketList.Count; i++)
                {
                    clsTickets tickets = ticketList[i];

                    if (returnTicket.TicektIDExist(tickets.TicketId))
                    {
                        ticketsPrice += tickets.Price * tickets.CurrencyRate - returnTicket.getReturnAmount(tickets.TicketId);
                        nmOfReturnedTickets++;
                    }
                    else
                    {
                        ticketsPrice += tickets.Price * tickets.CurrencyRate;
                    }
                }
                invoicepaymentList = paymentConnection.CustomerPaymment(customerData.CustomerId,fromDate,toDate);

                for (int i2 = 0; i2 < invoicepaymentList.Count; i2++)
                {

                    ticketsPaid += invoicepaymentList[i2].Payment * invoicepaymentList[i2].CurrencyRate;
                }


                packageInvoiceList = packageInvoiceConnection.CustomerInvoicesAfterInventory(fromDate, toDate, customerData.CustomerId);

                for (int i = 0; i < packageInvoiceList.Count; i++)
                {
                    clsPackageInvoice invoice = packageInvoiceList[i];
                    packageList = packageInvoiceConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                    packagesSold += packageList.Count;
                    if (returnPackage.InvoiceExist(invoice.InvoiceId))
                    {
                        packagesPrice += invoice.NetPrice - returnPackage.getReturnAmount(invoice.InvoiceId);
                        List<clsReturnedCustomerPackages> returnlist = returnPackage.getReturnedPackage(invoice.InvoiceId);
                        foreach (clsReturnedCustomerPackages item in returnlist)
                        {
                            if (item.ItemId == 0)
                            {
                                nmofReturnedPackages++;
                            }
                            else
                            {
                                nmOfReturnedItem++;
                            }
                        }
                    }
                    else
                    {
                        packagesPrice += invoice.NetPrice;
                    }


                  
                }
                packageInvoicepaymentList = packagePaymentConnection.CustomerPayments(customerData.CustomerId, fromDate, toDate);

                for (int i2 = 0; i2 < packageInvoicepaymentList.Count; i2++)
                {
                    packagesPaid += packageInvoicepaymentList[i2].Payment * packageInvoicepaymentList[i2].CurrencyRate;
                }
                if (nmOfReturnedTickets>0)
                {
                    returnedTickets = nmOfReturnedTickets + " Ticket(s) Returned.";
                }
                if(nmofReturnedPackages>0)
                {
                    returnedPackages = nmofReturnedPackages + " Package(s) Returned";
                 
                }
                if(nmOfReturnedItem>0)
                {
                    if(nmofReturnedPackages > 0)
                    {
                        returnedPackages += "\n\n";
                    }
                    returnedPackages += nmOfReturnedItem + " Item(s) Returned.";
                }
                packagesPrice /= currency.CurrencyRate;
                ticketsPrice/= currency.CurrencyRate;
                packagesPaid/= currency.CurrencyRate;
                ticketsPaid/= currency.CurrencyRate;

                DataTable dtt = new DataTable();
                this.reportviewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsSummaryofAccount", dtt);
                this.reportviewer1.LocalReport.DataSources.Add(reportsource);
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {
                        new Microsoft.Reporting.WinForms.ReportParameter("customerName",customerData.CustomerName),
                        new Microsoft.Reporting.WinForms.ReportParameter("ticketsSold",ticketsSold.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("packagesSold",packagesSold.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("ticketsPrice",ticketsPrice.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("packagesPrice",packagesPrice.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("ticketsPaid",ticketsPaid.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("packagesPaid",packagesPaid.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("openingBalance",OpenningBalance.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("fromDate", fromDate.ToShortDateString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("toDate", toDate.ToShortDateString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("returnedTickets",returnedTickets),
                        new Microsoft.Reporting.WinForms.ReportParameter("returnedPackages",returnedPackages),
                        new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                        new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                        new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())
                      };

                reportviewer1.Visible = true;
                this.reportviewer1.LocalReport.SetParameters(p);
                this.reportviewer1.RefreshReport();
                skip = true;

            }



        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
