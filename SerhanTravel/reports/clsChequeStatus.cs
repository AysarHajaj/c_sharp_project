﻿using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.reports
{
    class clsChequeStatus
    {
        private string  name;
        private string mobilePhone;
        private double payment;
        private int  invoiceId;
        private string paymentDate;
        private string chequeNo;
        private string bankName;
        private string chequeDate;
        private string collectedDate;
        private string  type;
        private int currencyId;
        private string ticketNo;
        Currency currencyConnection = new Currency();
        public string TicketNo
        {
            get { return ticketNo; }
            set { ticketNo = value; }
        }

        public int CurrencyId
        {
            get { return currencyId; }
            set { currencyId = value; }
        }
        public string CurrecnySymbol
        {
            get
            {

                clsCurrency currency = currencyConnection.GetCurrencyById(currencyId);

                if (currency == null)
                    return string.Empty;
                else
                    return currency.CurrencySymbol;
            }
        }

        public string  Type
        {
            get { return type; }
            set { type = value; }
        }



     public string Description
        {
           get
            {
                    return "From " + name + "\n" + type + " Reservation.\n" + "Phone :" + mobilePhone;
            }
        }
        public string CollectedDate
        {
            get { return collectedDate; }
            set { collectedDate = value; }
        }

        public string ChequeDate
        {
            get { return chequeDate; }
            set { chequeDate = value; }
        }

        public string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }


        public string ChequeNo
        {
            get { return chequeNo; }
            set { chequeNo = value; }
        }


        public string PaymentDate
        {
            get { return paymentDate; }
            set { paymentDate = value; }
        }


        public int  InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }


        public double Payment
        {
            get { return payment; }
            set { payment = value; }
        }


        public string MobilePhone
        {
            get { return mobilePhone; }
            set { mobilePhone = value; }
        }


        public string  Name
        {
            get { return name; }
            set { name = value; }
        }


    }
}
