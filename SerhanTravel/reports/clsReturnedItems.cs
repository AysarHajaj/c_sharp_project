﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.reports
{
    class clsReturnedItems
    {
       

        private int invoiceId;
        private string name;
        private decimal netPrice;
        private DateTime returnedDate;
        private DateTime invoiceDate;
        private decimal returnAmount;
        private string returnType;
        private int quantityReturned;
        private string description;
        private decimal remainedAmount;
        private string ticketno;

        public string TicketNo
        {
            get { return ticketno; }
            set { ticketno = value; }
        }

        public decimal RemainedAmount
        {
            get { return remainedAmount; }
            set { remainedAmount = value; }
        }


        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int QuantityReturned
        {
            get { return quantityReturned; }
            set { quantityReturned = value; }
        }


        public string ReturnType
        {
            get { return returnType; }
            set { returnType = value; }
        }


        public decimal ReturnAmount
        {
            get { return returnAmount; }
            set { returnAmount = value; }
        }

        public DateTime InvoiceDate
        {
            get { return invoiceDate; }
            set { invoiceDate = value; }
        }


        public DateTime ReturnedDate
        {
            get { return returnedDate; }
            set { returnedDate = value; }
        }


        public decimal NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }


        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public int InvoiceId
        {
            get { return invoiceId; }
            set { invoiceId = value; }
        }


    }
}
