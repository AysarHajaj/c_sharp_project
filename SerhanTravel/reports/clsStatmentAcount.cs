﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.reports
{
    class clsStatmentAcount
    {
        private DateTime date;
        private string description;
        private string amountDue;
        private string paidAmount;
        private string balance;
        private string customerName;
        private bool isReturned;
        private decimal netPrice;
        private int customerId;

        public int CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public decimal NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }

        public bool IsReturned
        {
            get { return isReturned; }
            set { isReturned = value; }
        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }


        public string Balance
        {
            get { return balance; }
            set { balance = value; }
        }


        public string PaidAmount
        {
            get { return paidAmount; }
            set { paidAmount = value; }
        }


        public string AmountDue
        {
            get { return amountDue; }
            set { amountDue = value; }
        }


        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

    }
}
