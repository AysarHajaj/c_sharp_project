﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhanTravel.reports
{
    class clsSupplierPayments
    {
        private string description;
        private string payment;
        private string date;
        private string paymentType;
        private string currency;
        private decimal netPrice;

        public decimal NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }

        public string Currency
        {
            get { return currency; }
            set { currency = value; }
        }


        public string PaymentType
        {
            get { return paymentType; }
            set { paymentType = value; }
        }


        public string Date
        {
            get { return date; }
            set { date = value; }
        }


        public string Payment
        {
            get { return payment; }
            set { payment = value; }
        }


        public string Description
        {
            get { return description; }
            set { description = value; }
        }

    }
}
