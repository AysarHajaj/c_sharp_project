﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace SerhanTravel.reports
{
    public partial class frmCustReturnedItems : Form
    {
        ReturnedCustomerTicket returnCustomerTicket;
        private string currencyFormat;
        ReturnedCustomerPackage returnedCustomerPackage;
        List<clsReturnedItems> ticketList;
        List<clsReturnedItems> packageList;
        DateTime fromDate, toDate;
        private List<clsCurrency> currencies;
        clsCurrency currency;
        Currency currencyConnection;
        public frmCustReturnedItems()
        {
            InitializeComponent();
        }

        private void frmCustReturnedItems_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            returnedCustomerPackage = new ReturnedCustomerPackage();
            returnCustomerTicket = new ReturnedCustomerTicket();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            currency = (clsCurrency)cmbCurrency.SelectedItem;
            fromDate = dtpFrom.Value;
            toDate = dtpTo.Value;
            packageList = returnedCustomerPackage.getReturnedPackagesList(fromDate, toDate);
            ticketList = returnCustomerTicket.getReturnedTicketsArrayList(fromDate, toDate);

            DataTable dt = ConvertToDatatable(ticketList, packageList);

            this.reportviewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsReturnedItems", dt);

            this.reportviewer1.LocalReport.DataSources.Add(reportsource);


            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {

                        
                             new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("toDate", toDate.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("selectedRate",currency.CurrencyRate.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedSymbol",currency.CurrencySymbol),
                             new Microsoft.Reporting.WinForms.ReportParameter("returnedFrom","Customer Returned Items")

                  };
            reportviewer1.Visible = true;
            this.reportviewer1.LocalReport.SetParameters(p);

            this.reportviewer1.RefreshReport();
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        DataTable ConvertToDatatable(List<clsReturnedItems> ticketlist, List<clsReturnedItems> packageList)
        {
            DataTable dt = new DataTable();

           
            dt.Columns.Add("Description");
            dt.Columns.Add("RemainedAmount");
            dt.Columns.Add("Name");
            dt.Columns.Add("ReturnedDate");
            dt.Columns.Add("ReturnAmount");
            dt.Columns.Add("ReturnType");
            foreach (clsReturnedItems ticket in ticketlist)
            {
                ticket.Description = "Ticket No :" +ticket.TicketNo + "\nPrice : " + (ticket.NetPrice/currency.CurrencyRate).ToString(currencyFormat)+" "+currency.CurrencySymbol+"\nDate : "+ticket.InvoiceDate.ToShortDateString();
                var row = dt.NewRow();
                row["ReturnType"] = ticket.ReturnType;
                row["ReturnAmount"] = ticket.ReturnAmount;
                row["ReturnedDate"] = ticket.ReturnedDate.ToShortDateString();
                row["Name"] = ticket.Name;
                row["RemainedAmount"] = ticket.RemainedAmount;
                row["Description"] = ticket.Description;
                dt.Rows.Add(row);
            }
            foreach (clsReturnedItems ticket in packageList)
            {
                ticket.Description = "Package Invoice\nInvoice ID:" + ticket.InvoiceId + "\nPrice : " + (ticket.NetPrice/currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol + "\nDate : " + ticket.InvoiceDate.ToShortDateString();
                var row = dt.NewRow();
                row["ReturnType"] = ticket.ReturnType;
                row["ReturnAmount"] = ticket.ReturnAmount;
                row["ReturnedDate"] = ticket.ReturnedDate.ToShortDateString();
                row["Name"] = ticket.Name;
                row["RemainedAmount"] = ticket.RemainedAmount;
                row["Description"] = ticket.Description;
                dt.Rows.Add(row);
            }
            return dt;
        }

    }
}
