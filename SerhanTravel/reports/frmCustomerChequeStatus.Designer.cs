﻿namespace SerhanTravel.reports
{
    partial class frmCustomerChequeStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportviewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportviewer1
            // 
            this.reportviewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "dsCustomerChequeStatus";
            this.reportviewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportviewer1.LocalReport.ReportEmbeddedResource = "SerhanTravel.reports.rtpCustomerChequeStatus.rdlc";
            this.reportviewer1.Location = new System.Drawing.Point(0, 0);
            this.reportviewer1.Name = "reportviewer1";
            this.reportviewer1.ServerReport.BearerToken = null;
            this.reportviewer1.Size = new System.Drawing.Size(681, 340);
            this.reportviewer1.TabIndex = 0;
            // 
            // frmCustomerChequeStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 340);
            this.Controls.Add(this.reportviewer1);
            this.Name = "frmCustomerChequeStatus";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cheque Status";
            this.Load += new System.EventHandler(this.frmCustomerChequeStatus_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportviewer1;
    }
}