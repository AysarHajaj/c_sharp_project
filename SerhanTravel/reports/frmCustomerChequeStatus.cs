﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmCustomerChequeStatus : Form
    {
        public frmCustomerChequeStatus()
        {
            InitializeComponent();
        }

        private void frmCustomerChequeStatus_Load(object sender, EventArgs e)
        {

            DataTable ticketchequeDataTable = new DataTable();
            DataTable packagechequeDataTable = new DataTable();
        

            string packagequery= "Select Cu.Name,Cu.MobilePhone,PP.Payment,PP.Date,PP.ChequeNo,BN.BankName,PP.ChequeDate,PP.CollectedDate,PP.CurrencyId  " +
                               " from PackagePayments As PP " +
                                " Inner Join Customer As Cu on Cu.CustomerId = PP.CustomerId  " +
                                "Inner Join Bank as BN on BN.BankId = PP.BankId " +
                               " where(PP.PaymentType = 1 ) ";

            string ticketquery = " Select Cu.Name,Cu.MobilePhone,TP.Payment,TP.Date,TP.ChequeNo,BN.BankName,TP.ChequeDate,TP.CollectedDate,TP.CurrencyId " +
                             " from TicketsPayment As TP " +
                             " Inner Join Customer As Cu on Cu.CustomerId = TP.CustomerId  " +
                             " Inner Join Bank as BN on BN.BankId=TP.BankId " +
                             " where(TP.PaymentType = 1 );";


            ticketchequeDataTable = ChequeList(ticketquery,"Ticket");
            packagechequeDataTable = ChequeList(packagequery, "Package");
            ticketchequeDataTable.Merge(packagechequeDataTable);


            this.reportviewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsCustomerChequeStatus", ticketchequeDataTable);

            this.reportviewer1.LocalReport.DataSources.Add(reportsource);

            

          
  


            this.reportviewer1.RefreshReport();
        }

        


        internal DataTable ChequeList( string query,string type)
        {
            List<clsTicketsPayment> DisplayArray = new List<clsTicketsPayment>();

            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                List<clsChequeStatus> chequeList = new List<clsChequeStatus>();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open(); 
                }
                string sql = query;

                SqlCommand command = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = command.ExecuteReader();
                clsChequeStatus cheque;

                while (dr.Read())
                {
                    cheque = new clsChequeStatus();
                 
                    if (dr["Name"] == DBNull.Value)
                    {
                        cheque.Name = string.Empty;
                    }
                    else
                    {
                        cheque.Name = dr["Name"].ToString();
                    }
                    if (dr["MobilePhone"] == DBNull.Value)
                    {
                        cheque.MobilePhone = string.Empty;
                    }
                    else
                    {
                        cheque.MobilePhone = dr["MobilePhone"].ToString();
                    }
                    if (dr["Payment"] == DBNull.Value)
                    {
                        cheque.Payment = 0;
                    }
                    else
                    {
                        cheque.Payment = double.Parse(dr["Payment"].ToString());
                    }

                    if (dr["CurrencyId"] == DBNull.Value)
                    {
                        cheque.CurrencyId = 0;
                    }
                    else
                    {
                        cheque.CurrencyId = int.Parse(dr["CurrencyId"].ToString());
                    }
                    cheque.PaymentDate = DateTime.Parse(dr["Date"].ToString()).ToShortDateString();
                    if (dr["ChequeNo"] == DBNull.Value)
                    {
                        cheque.ChequeNo = string.Empty;
                    }
                    else
                    {
                        cheque.ChequeNo = dr["ChequeNo"].ToString();
                    }
                    if (dr["BankName"] == DBNull.Value)
                    {
                        cheque.BankName = string.Empty;
                    }
                    else
                    {
                        cheque.BankName = dr["BankName"].ToString();
                    }
                    if (dr["ChequeDate"] == DBNull.Value)
                    {
                        cheque.ChequeDate = string.Empty;
                    }
                    else
                    {
                            cheque.ChequeDate =DateTime.Parse(dr["ChequeDate"].ToString()).ToShortDateString();
                    }
                    if (dr["CollectedDate"] == DBNull.Value)
                    {
                        cheque.CollectedDate = "_";
                      
                    }
                    else
                    {
                        cheque.CollectedDate = DateTime.Parse(dr["CollectedDate"].ToString()).ToShortDateString();
                    }
                    cheque.Type = type;
                    chequeList.Add(cheque);

                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

                dt.Columns.Add("Payment");
                dt.Columns.Add("Description");
                dt.Columns.Add("ChequeNo");
                dt.Columns.Add("BankName");
                dt.Columns.Add("ChequeDate"); 
                dt.Columns.Add("CollectedDate");
                dt.Columns.Add("CurrecnySymbol");
                foreach (var item in chequeList)
                {
                    var row = dt.NewRow();


                    row["Payment"] = item.Payment;
                    row["Description"] = item.Description;
                    row["ChequeNo"] = item.ChequeNo;
                    row["BankName"] = item.BankName;
                    row["ChequeDate"] = item.ChequeDate;
                    row["CollectedDate"] = item.CollectedDate;
                    row["CurrecnySymbol"] = item.CurrecnySymbol;

                    dt.Rows.Add(row);
                }

                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }


    }
}
