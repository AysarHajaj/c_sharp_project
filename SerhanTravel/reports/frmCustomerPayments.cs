﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmCustomerPayments : Form
    {

        List<clsCustomer> customerList;
        List<clsTicketsPayment> ticketPayments;
        List<clsPackagesPayment> packagePayments;
        Customer customerConnection;
        TicketsPayment ticketPayment;
        PackagesPayment packagePayment;

        clsCustomer customerData;
        bool skip = false;
        private List<clsCurrency> currencies;
        string currencyFormat;

        Currency currencyConnection;
        public frmCustomerPayments()
        {
            InitializeComponent();
        }

        private void frmCustomerPayments_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            lbResult.Visible = false;
            ticketPayments = new List<clsTicketsPayment>();
            packagePayments = new List<clsPackagesPayment>();
            packagePayment = new PackagesPayment();
            ticketPayment = new TicketsPayment();
            customerData = new clsCustomer();
            customerConnection = new Customer();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            customerList = customerConnection.CustomersArrayList();
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            this.reportViewer1.RefreshReport();
        }



        static DataTable ConvertToDatatable(List<clsSupplierPayments> List)
        {
            DataTable dt = new DataTable();
            
            dt.Columns.Add("Description");
            dt.Columns.Add("Payment");
            dt.Columns.Add("Currency");
            dt.Columns.Add("Date");
            dt.Columns.Add("PaymentType");


            foreach (var item in List)
            {
                var row = dt.NewRow();

                row["Description"] = item.Description;
                row["Payment"] = item.Payment;
                row["Currency"] = item.Currency;
                row["Date"] = item.Date;
                row["PaymentType"] = item.PaymentType;

                dt.Rows.Add(row);
            }

            return dt;
        }


        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;
        }
        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {
                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;
            }


            lbResult.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            this.reportViewer1.LocalReport.DataSources.Clear();
            ticketPayments.Clear();
            packagePayments.Clear();
            this.reportViewer1.RefreshReport();
            if(cmbCurrency.SelectedIndex==-1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                customerData = (clsCustomer)cmbCustomerName.SelectedItem;
                ticketPayments = ticketPayment.CustomerPaymment(customerData.CustomerId,fromDate,toDate);
                List<clsSupplierPayments> PaymentList = new List<clsSupplierPayments>();
                foreach(clsTicketsPayment payments in ticketPayments)
                {
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment* payments.CurrencyRate) / currency.CurrencyRate).ToString();
                    payment.Description ="Ticket Payment\nPayment ID : "+payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t"+payments.ChequeNo;
                    }
                    PaymentList.Add(payment);
                }
                packagePayments = packagePayment.CustomerPayments(customerData.CustomerId,fromDate,toDate);
                foreach (clsPackagesPayment payments in packagePayments)
                {
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment * payments.CurrencyRate) / currency.CurrencyRate).ToString();
                    payment.Description = "Package Payment\nPayment ID : " + payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t" + payments.ChequeNo;
                    }
                    PaymentList.Add(payment);
                }
           

                DataTable paymentTable = ConvertToDatatable(PaymentList);
  
                DataView dv = paymentTable.DefaultView;
                dv.Sort = "Date desc";
                paymentTable = dv.ToTable();
                

                this.reportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsSupplierPayments", paymentTable);
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                    {

                         new Microsoft.Reporting.WinForms.ReportParameter("supplierName",customerData.CustomerName),
                          new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())
                    };
                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);
                this.reportViewer1.RefreshReport();
                this.reportViewer1.Visible = true;
                skip = true;



            }
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
