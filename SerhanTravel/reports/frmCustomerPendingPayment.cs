﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmCustomerPendingPayment : Form
    {
        private List<clsCurrency> currencies;
        Currency currencyConnection;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public frmCustomerPendingPayment()
        {
            InitializeComponent();
        }

        private void frmCustomerPendingPayment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            
        }
        static DataTable ConvertToDatatable(List<clsStatmentAcount> accountList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("Date");
            dt.Columns.Add("AmountDue");
            dt.Columns.Add("PaidAmount");
            dt.Columns.Add("Balance");
            dt.Columns.Add("CustomerName");

            foreach (clsStatmentAcount item in accountList)
            {
                var row = dt.NewRow();

                decimal Balance= (decimal.Parse(item.AmountDue) - decimal.Parse(item.PaidAmount));
                if (Balance <= 0)
                    continue;

                row["Date"] = item.Date;
                row["AmountDue"] = item.AmountDue;
                row["PaidAmount"] = item.PaidAmount;
                row["Balance"] = Balance;
                row["CustomerName"] = item.CustomerName;
                

                dt.Rows.Add(row);
            }

            return dt;
        }
        private void Display()
        {
            List<clsStatmentAcount> accountList = new List<clsStatmentAcount>();
            accountList.AddRange(TicketsInvoices(1));
            accountList.AddRange(TicketsInvoices(2));
            accountList.AddRange(PackagesInvoices(1));
            accountList.AddRange(PackagesInvoices(2));
            accountList.AddRange(CustomerBalances());
            List<clsStatmentAcount> finalList = new List<clsStatmentAcount>();
            TicketsPayment ticketsPayment = new TicketsPayment();
            PackagesPayment packagesPayment = new PackagesPayment();
            for(int i=0;i<accountList.Count;i++)
            {
                for(int j =i+1;j<accountList.Count;j++)
                {
                    if(accountList[i].CustomerId==accountList[j].CustomerId)
                    {
                        accountList[i].NetPrice += accountList[j].NetPrice;
                        accountList[i].AmountDue = accountList[i].NetPrice.ToString();
                        accountList[i].Balance = accountList[i].NetPrice.ToString();
                        accountList.RemoveAt(j);
                        j--;
                    }
                   
                }
                accountList[i].PaidAmount = (ticketsPayment.TotalPaid(accountList[i].CustomerId)+packagesPayment.TotalPaid(accountList[i].CustomerId)).ToString();
                decimal paidAmount = decimal.Parse(accountList[i].PaidAmount);
                decimal amountDue = decimal.Parse(accountList[i].AmountDue);
                 if(paidAmount > amountDue)
                {
                    accountList.RemoveAt(i);
                }
            }
            accountList.Sort((x, y) => x.CustomerName.CompareTo(y.CustomerName));
            DataTable data =ConvertToDatatable(accountList);

            clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsCustomerPendingPayments", data);

            this.reportViewer1.LocalReport.DataSources.Add(reportsource);
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                     {
                     
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedRate",currency.CurrencyRate.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedSymbol",currency.CurrencySymbol)

                     };


            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        internal List<clsStatmentAcount> TicketsInvoices(int num)
        {
            List<clsStatmentAcount> DisplayArray = new List<clsStatmentAcount>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
          
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql;
                if(num==1)
                {
                    sql = "  Select Sum(TI.Price*TI.CurrencyRate) as NetPrice,CU.MobilePhone,CU.Name,CU.CustomerId " +
                    "from Ticket as TI   Inner Join Customer AS Cu on TI.CustomerId = CU.CustomerId   " +
                    " where    TI.TicketId NOt In (Select RT.TicketId from ReturnedCustomerTicket as RT ) " +
                    " Group By CU.MobilePhone,CU.Name,CU.CustomerId " +
                    "order by CU.Name ;";
                }
                else
                {
                    sql= "	Select (TI.Price*TI.CurrencyRate - Sum(RT.ReturnAmount * RT.CurrencyRate)) as NetPrice,CS.MobilePhone,CS.Name,CS.CustomerId  " +
                    " from Ticket as TI Inner Join ReturnedCustomerTicket as RT on RT.TicketId = TI.TicketId " +
                    "Inner Join Customer as CS on CS.CustomerId = TI.CustomerId   " +
                    " Group BY TI.Price,TI.CurrencyRate,TI.TicketNo,TI.TicketDate,CS.MobilePhone,CS.Name,CS.CustomerId   " +
                    "order by CS.Name ;";
                }

                SqlCommand command = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = command.ExecuteReader();
                clsStatmentAcount account;
                while (dr.Read())
                {
                    account = new clsStatmentAcount();

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        account.CustomerId = 0;
                    }
                    else
                    {
                        account.CustomerId =int.Parse(dr["CustomerId"].ToString());
                    }
                    if (dr["Name"] == DBNull.Value)
                    {
                        account.CustomerName =string.Empty;
                    }
                    else
                    {
                        account.CustomerName = dr["Name"].ToString();
                    }
                    if (dr["MobilePhone"] != DBNull.Value)
                    {
                        account.CustomerName += "\nCustomer Phone : " + dr["MobilePhone"].ToString();
                    }
                   
                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        account.NetPrice= 0;
                        account.AmountDue = "0";
                        account.Balance = "0";
                    }
                    else
                    {
                        account.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                        account.AmountDue = dr["NetPrice"].ToString();
                        account.Balance = dr["NetPrice"].ToString();
                    }

                    DisplayArray.Add(account);


                }                                                                         


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();

              

                return DisplayArray; 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }




        internal List<clsStatmentAcount> CustomerBalances()
        {
            List<clsStatmentAcount> DisplayArray = new List<clsStatmentAcount>();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql;

                sql = "  Select (CB.Balance*CB.CurrencyRate) as NetPrice,CU.CustomerId,CU.MobilePhone,CU.Name " +
                "from CustomerBalanceInventory as CB Inner Join Customer as CU on CU.CustomerId=CB.CustomerId  " +
                " Order by CU.Name; ";
                SqlCommand command = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = command.ExecuteReader();
                clsStatmentAcount account;
                while (dr.Read())
                {
                    account = new clsStatmentAcount();

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        account.CustomerId = 0;
                    }
                    else
                    {
                        account.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    if (dr["Name"] == DBNull.Value)
                    {
                        account.CustomerName = string.Empty;
                    }
                    else
                    {
                        account.CustomerName = dr["Name"].ToString();
                    }
                    if (dr["MobilePhone"] != DBNull.Value)
                    {
                        account.CustomerName += "\nCustomer Phone : " + dr["MobilePhone"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        account.NetPrice = 0;
                        account.AmountDue = "0";
                        account.Balance = "0";
                    }
                    else
                    {
                        account.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                        account.AmountDue = dr["NetPrice"].ToString();
                        account.Balance = dr["NetPrice"].ToString();
                    }

                    DisplayArray.Add(account);


                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return DisplayArray;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }


        /* internal DataTable unCompletePaidInvoiceTicketList(int num)
               {
                   List<clsTicketInvoice> DisplayArray = new List<clsTicketInvoice>();

                   DataTable dt;
                   dt = new DataTable();
                   try
                   {
                       SqlConnection sqlCon;
                       BuildConnection aBuildConnection = new BuildConnection();

                       sqlCon = aBuildConnection.AddSqlConnection();

                       if (sqlCon.State != ConnectionState.Open)
                       {
                           sqlCon.Open();
                       }
                       string sql="";
                       if(num==1)
                       {
                           sql = "Select SUM(Tp.Payment*Tp.CurrencyRate) As totalPaids, TI.TicketNo as InvoiceId,TI.TicketDate as Date,(TI.Price*TI.CurrencyRate) as NetPrice,CU.MobilePhone,CU.Name  " +
                                 "   from Ticket as TI Inner Join TicketsPayment as Tp on Tp.TicketId = TI.TicketId " +
                                   " Inner Join Customer AS Cu on TI.CustomerId = CU.CustomerId  " +
                                   "  and TI.TicketId NOt In (Select RT.TicketId from ReturnedCustomerTicket as RT ) " +
                                    "    Group By TI.Price,TI.CurrencyRate ,TI.TicketNo,TI.TicketDate,CU.MobilePhone,CU.Name " +
                                    "  having TI.Price*TI.CurrencyRate > SUM(Tp.Payment * Tp.CurrencyRate);";

                       }
                       else
                       {
                           sql = "Select (Sum(Tp.Payment * Tp.CurrencyRate)) as totalPaids ,TI.TicketNo as InvoiceId,(TI.Price*TI.CurrencyRate - IsNull(amount.ReturnedAmount, 0)) as NetPrice,TI.TicketDate as Date,CUS.MobilePhone,CUS.Name " +
                        "from Ticket as TI Inner Join TicketsPayment as Tp on Tp.TicketId = TI.TicketId  " +
                        "Right Join (Select sum(RT.ReturnAmount*RT.CurrencyRate ) as ReturnedAmount,RT.TicketId from ReturnedCustomerTicket  RT  " +
                        "Group by RT.TicketId) as amount on TI.TicketId= amount.TicketId   " +
                        "Inner Join Customer as CUS on CUS.CustomerId = TI.CustomerId  " +
                        " Group By TI.Price ,TI.TicketId,amount.ReturnedAmount,TI.CurrencyRate,TI.TicketNo,TI.TicketDate,CUS.MobilePhone,CUS.Name   " +
                       " having(TI.Price*TI.CurrencyRate - IsNull(amount.ReturnedAmount,0)) > SUM(Tp.Payment * Tp.CurrencyRate);";
                       }

                       SqlCommand command = new SqlCommand(sql, sqlCon);
                       SqlDataReader dr = command.ExecuteReader();
                       clsTicketInvoice invoice;
                       clsCustomer customer;
                       dt.Columns.Add("customerName");
                       dt.Columns.Add("Description");
                       dt.Columns.Add("AmountDue");
                       dt.Columns.Add("PaidAmount");
                       dt.Columns.Add("Balance");



                       TicketInvoice ticketConnection = new TicketInvoice();




                       while (dr.Read())
                       {
                           invoice = new clsTicketInvoice();
                           customer = new clsCustomer();


                           decimal totalPaids;
                           if (dr["NetPrice"] == DBNull.Value)
                           {
                               invoice.NetPrice = 0;
                           }
                           else
                           {
                               invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                           }

                           if (dr["totalPaids"] == DBNull.Value)
                           {
                               totalPaids = 0;
                           }
                           else
                           {
                               totalPaids = decimal.Parse(dr["totalPaids"].ToString());
                           }
                           if(totalPaids>=invoice.NetPrice)
                           {
                               continue;
                           }


                           if (dr["Name"] == DBNull.Value)
                           {
                               customer.CustomerName = string.Empty;
                           }
                           else
                           {
                               customer.CustomerName = dr["Name"].ToString();
                           }
                           if (dr["MobilePhone"] == DBNull.Value)
                           {
                               customer.MobilePhone = string.Empty;
                           }
                           else
                           {
                               customer.MobilePhone = dr["MobilePhone"].ToString();
                           }



                           string ticketNo;
                           if (dr["InvoiceId"] == DBNull.Value)
                           {
                               ticketNo = " ";
                           }
                           else
                           {
                               ticketNo = (dr["InvoiceId"].ToString());
                           }


                           if (dr["Date"] == DBNull.Value)
                           {
                               invoice.InvoiceDate = DefaultDate;
                           }
                           else
                           {
                               invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                           }


                           var row = dt.NewRow();


                           row["customerName"] = customer.CustomerName;
                           row["Description"] = "Ticket No: " + ticketNo + "\nReserve Date : " + invoice.InvoiceDate.ToShortDateString()  +"\nCustomer Phone : " + customer.MobilePhone;
                           row["AmountDue"] = invoice.NetPrice;
                           row["PaidAmount"] = totalPaids;
                           row["Balance"] = invoice.NetPrice-totalPaids;
                           dt.Rows.Add(row);
                       }


                       dr.Close();
                       command.Dispose();
                       sqlCon.Close();
                       sqlCon.Dispose();



                       return dt; ;
                   }
                   catch (Exception ex)
                   {
                       MessageBox.Show(ex.Message);
                       return dt;
                   }
               }
               */



        internal List<clsStatmentAcount> PackagesInvoices(int num)
        {
            List<clsStatmentAcount> DisplayArray = new List<clsStatmentAcount>();

            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql="";
                if(num==1)
                {
                    sql= " Select TIN.InvoiceId,TIN.Date,(TIN.NetPrice-(TIN.Discount*TIN.NetPrice*0.01)) as NetPrice,CU.MobilePhone,CU.Name,CU.CustomerId " +
                               " from PackageInvoice As TIN Inner Join Customer AS Cu on TIN.CustomerId = CU.CustomerId " +
                                "where TIN.InvoiceId Not in(Select RP.InvoiceId from ReturnedCustomerPackage as RP )" +
                                "order by CU.Name ;";
                }
                else
                {
                    sql =
                        "Select(((TI.NetPrice-(TI.Discount*TI.NetPrice*0.01)) - sum(RP.ReturnAmount * RP.CurrencyRate))) as NetPrice ,TI.InvoiceId,TI.Date,CUS.MobilePhone,CUS.Name,CUS.CustomerId " +
                       " from PackageInvoice as TI "+
                        "Inner Join ReturnedCustomerPackage As RP on RP.InvoiceId = TI.InvoiceId "+
                        "Inner join Customer as CUS on CUS.CustomerId = TI.CustomerId "+
                       " Group by TI.NetPrice,TI.InvoiceId,TI.Date,CUS.MobilePhone,CUS.Name,CUS.CustomerId,TI.Discount " +
                       " order by CUS.Name ";
                }

                SqlCommand command = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = command.ExecuteReader();
                clsStatmentAcount account;
                while (dr.Read())
                {
                    account = new clsStatmentAcount();

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        account.CustomerId = 0;
                    }
                    else
                    {
                        account.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }
                    if (dr["Name"] == DBNull.Value)
                    {
                        account.CustomerName = string.Empty;
                    }
                    else
                    {
                        account.CustomerName = dr["Name"].ToString();
                    }
                    if (dr["MobilePhone"] != DBNull.Value)
                    {
                        account.CustomerName += "\nCustomer Phone : " + dr["MobilePhone"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        account.NetPrice = 0;
                        account.AmountDue = "0";
                        account.Balance = "0";
                    
                    }
                    else
                    {
                        account.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                        account.AmountDue = dr["NetPrice"].ToString();
                        account.Balance = dr["NetPrice"].ToString();
                       
                    }

                    DisplayArray.Add(account);
                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return DisplayArray; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return DisplayArray;
            }
        }






       /* internal DataTable unCompletePaidInvoicePackageList(int num)
        {
            List<clsTicketInvoice> DisplayArray = new List<clsTicketInvoice>();

            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql="";
                if(num==1)
                {
                    sql = "Select SUM(TPs.Payment*TPs.CurrencyRate) As totalPaids, TIN.InvoiceId,TIN.Date,TIN.NetPrice,CU.MobilePhone,CU.Name  " +
                          " from PackagePayments As TPs Inner Join PackageInvoice as TIN on TIN.InvoiceId = TPs.PackageInvoiceId  " +
                            " Inner Join Customer AS Cu on TIN.CustomerId = CU.CustomerId  " +
                            "where TIN.InvoiceId not In (Select RT.InvoiceId from ReturnedCustomerPackage as RT)  " +
                             "   Group By TIN.InvoiceId, TIN.Date, TIN.NetPrice, CU.MobilePhone, CU.Name " +
                             "having TIN.NetPrice>SUM(TPs.Payment*TPs.CurrencyRate);";
                }
                else
                {
                    sql =
                      " Select(Sum(Tp.Payment * Tp.CurrencyRate)) as totalPaids, TI.InvoiceId,TI.Date,(TI.NetPrice - IsNull(amount.ReturnedAmount, 0)) as NetPrice,CUS.MobilePhone,CUS.Name   " +
                  " from PackageInvoice as TI Inner Join PackagePayments as Tp on Tp.PackageInvoiceId = TI.InvoiceId  " +
                  " Right Join (Select sum(RP.ReturnAmount*Cs.CurrencyRate ) as ReturnedAmount,RP.InvoiceId from ReturnedCustomerPackage  RP  " +
                  " Inner Join Currency as Cs on Cs.CurrencyId=RP.CurrencyId  Group by RP.InvoiceId) as amount on TI.InvoiceId= amount.InvoiceId  " +
                  "  Inner Join Customer as CUS on CUS.CustomerId = TI.CustomerId " +
                  "  Group By TI.NetPrice ,TI.InvoiceId,amount.ReturnedAmount ,TI.Date,CUS.MobilePhone,CUS.Name " +
                  "  having (TI.NetPrice - IsNull(amount.ReturnedAmount,0)) > SUM(Tp.Payment * Tp.CurrencyRate)   ; ";
                }
                
                SqlCommand command = new SqlCommand(sql, sqlCon);
                SqlDataReader dr = command.ExecuteReader();
                clsTicketInvoice invoice;
                clsCustomer customer;
                dt.Columns.Add("customerName");
                dt.Columns.Add("Description");
                dt.Columns.Add("AmountDue");
                dt.Columns.Add("PaidAmount");
                dt.Columns.Add("Balance");



                PackageInvoice packageConnection = new PackageInvoice();




                while (dr.Read())
                {
                    invoice = new clsTicketInvoice();
                    customer = new clsCustomer();


                    decimal totalPaids;
                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["totalPaids"] == DBNull.Value)
                    {
                        totalPaids = 0;
                    }
                    else
                    {
                        totalPaids = decimal.Parse(dr["totalPaids"].ToString());
                    }
                    if (totalPaids >= invoice.NetPrice)
                    {
                        continue;
                    }


                    if (dr["Name"] == DBNull.Value)
                    {
                        customer.CustomerName = string.Empty;
                    }
                    else
                    {
                        customer.CustomerName = dr["Name"].ToString();
                    }
                    if (dr["MobilePhone"] == DBNull.Value)
                    {
                        customer.MobilePhone = string.Empty;
                    }
                    else
                    {
                        customer.MobilePhone = dr["MobilePhone"].ToString();
                    }


                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        invoice.InvoiceId = 0;
                    }
                    else
                    {
                        invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString());
                    }


                    if (dr["Date"] == DBNull.Value)
                    {
                        invoice.InvoiceDate = DefaultDate;
                    }
                    else
                    {
                        invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                    }


                    var row = dt.NewRow();


                    row["customerName"] = customer.CustomerName;
                    row["Description"] = "Invoice ID: " + invoice.InvoiceId + "\nInvoice Date : " + invoice.InvoiceDate.ToShortDateString() + "\nReserves " + packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId).Count + " Package\nCustomer Phone : " + customer.MobilePhone;
                    row["AmountDue"] = invoice.NetPrice;
                    row["PaidAmount"] = totalPaids;
                    row["Balance"] = invoice.NetPrice - totalPaids;
                    dt.Rows.Add(row);
                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }
        */
        private void reportViewer1_ReportRefresh(object sender, CancelEventArgs e)
        {
           // Display();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            Display();
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
