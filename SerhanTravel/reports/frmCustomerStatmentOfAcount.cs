﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmCustomerStatmentOfAcount : Form
    {
        private List<clsCurrency> currencies;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsCustomer> customerList;
        Customer customerConnection;
        clsCustomer customerData;
        Ticket ticketConnection;
        List<clsStatmentAcount> statmentAcount;
        private List<clsTickets> ticketList;
        TicketsPayment paymentConnection;
        List<clsTicketsPayment> ticketPaymentList;
        ReturnedCustomerPackage returnedCustomerPackage;
        ReturnedCustomerTicket returnedCustomerTicket;
        PackageInvoice packageInvoiceConnection;
        List<clsPackageInvoice> packageInvoiceList;
        PackagesPayment packagePaymentConnection;
        List<clsPackage> packageList;
        List<clsPackagesPayment> packageInvoicepaymentList;
        bool skip = false;
        clsCustomerInventory inventoryData;
        CustomerBalanceInventory inventoryConnection;
        List<clsCompany> company;
        Company CompanyInfo;
        Currency currencyConnection;
        public frmCustomerStatmentOfAcount()
        {
            InitializeComponent();
        }

       

     

        private void frmCustomerStatmentOfAcount_Load_1(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            inventoryConnection = new CustomerBalanceInventory();
            returnedCustomerPackage=new ReturnedCustomerPackage();
            returnedCustomerTicket=new ReturnedCustomerTicket();
            lbResult.Visible = false;
            customerConnection = new Customer();
            statmentAcount = new List<clsStatmentAcount>();
            paymentConnection = new TicketsPayment();
            ticketPaymentList = new List<clsTicketsPayment>();
            packageInvoiceConnection = new PackageInvoice();
            packagePaymentConnection = new PackagesPayment();
            ticketConnection = new Ticket();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            customerList = customerConnection.CustomersArrayList();
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            reportviewer1.Visible = false;

            this.reportviewer1.RefreshReport();

        }

    
        static DataTable ConvertToDatatable(List<clsStatmentAcount> accountList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("Date");
            dt.Columns.Add("Description");
            dt.Columns.Add("AmountDue");
            dt.Columns.Add("PaidAmount");
            dt.Columns.Add("Balance");

            foreach (var item in accountList)
            {
                var row = dt.NewRow();


                row["Date"] = item.Date;
                row["Description"] = item.Description;
                row["AmountDue"] = item.AmountDue;
                row["PaidAmount"] = item.PaidAmount;
                row["Balance"] = item.Balance;


                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCustomerName.SelectedIndex!=-1)
            {
                skip = true;
            }

        }

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if(skip)
            {
                skip = false;
                return;
            }
             lbResult.Visible = false;
             string textToSearch = cmbCustomerName.Text.ToLower();
             if (string.IsNullOrEmpty(textToSearch))
             {

                 return;
             }


             clsCustomer[] result = (from i in customerList
                                     where i.CustomerName.ToLower().Contains(textToSearch)
                                     select i).ToArray();
             if (result.Length == 0)
             {

                 return; // return with listbox's Visible set to false if nothing found

             }
             else
             {




                 lbResult.Items.Clear(); // remember to Clear before Add
                 lbResult.ValueMember = "CustomerId";
                 lbResult.Items.AddRange(result);
                 lbResult.DisplayMember = "CustomerName";
                 lbResult.Visible = true; // show the listbox again
                 lbResult.Height = 100;
             }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbCustomerName.SelectedItem = lbResult.SelectedItem;
                
            }


            lbResult.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.reportviewer1.LocalReport.DataSources.Clear();
            statmentAcount.Clear();
            this.reportviewer1.RefreshReport();
            decimal OpenningBalance = 0;
            decimal totalPaid = 0;
            decimal totalAmountDue = 0;
            if(cmbCurrency.SelectedIndex==-1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;

                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                customerData = (clsCustomer)cmbCustomerName.SelectedItem;
                ticketList = ticketConnection.CustomerTickets(fromDate,toDate,customerData.CustomerId);
                inventoryData = inventoryConnection.CustomerLastInventory(customerData.CustomerId);
                if(inventoryData==null)
                {
                    inventoryData = new clsCustomerInventory();
                    inventoryData.Date = DefaultDate;
                    inventoryData.Balance = 0;
                    inventoryData.CustomerId = customerData.CustomerId;
                    inventoryData.InventoryId = 0;
                    inventoryData.CurrencyId = 0;
                }

                OpenningBalance += inventoryData.Balance;
                OpenningBalance += customerConnection.getCustomerBalance(customerData.CustomerId, inventoryData.Date, fromDate);
                OpenningBalance /= currency.CurrencyRate;
                decimal Paid = paymentConnection.TotalPaid(customerData.CustomerId, inventoryData.Date, fromDate);
                Paid += packagePaymentConnection.TotalPaid(customerData.CustomerId, inventoryData.Date, fromDate);
                Paid /= currency.CurrencyRate;
                totalAmountDue += OpenningBalance;
                totalPaid += Paid;
                clsStatmentAcount acount = new clsStatmentAcount();
                acount.AmountDue = OpenningBalance.ToString();
                acount.Description = "*OPENING ACCOUNT BALANCE*\n*TOTALS BEFORE REQUESTED PERIOD*";
                acount.PaidAmount = Paid.ToString();
                acount.Balance = (OpenningBalance-Paid).ToString();
                acount.Date = (fromDate.AddDays(-1));
             


                for (int i = 0; i < ticketList.Count; i++)
                {

                    clsStatmentAcount account = new clsStatmentAcount();
                    clsTickets tickets = ticketList[i];
                    account.Date = tickets.TicketDate;
                    account.Description =tickets.AirlineName+"  "+tickets.TicketNo + ":" + tickets.TravellerName;
                    account.AmountDue = ((tickets.Price * tickets.CurrencyRate) / currency.CurrencyRate).ToString();
                    account.PaidAmount = "0";
                    account.Balance = "0";
                    statmentAcount.Add(account);
                    if (returnedCustomerTicket.TicektIDExist(tickets.TicketId))
                    {
                        decimal tPrice = (tickets.Price * tickets.CurrencyRate) - returnedCustomerTicket.getReturnAmount(tickets.TicketId);
                        totalAmountDue += tPrice / currency.CurrencyRate;
                        clsReturnedCustomerTicket returnedTicket = returnedCustomerTicket.getReturnedTicket(tickets.TicketId);
                        clsStatmentAcount returnAcount = new clsStatmentAcount();
                        returnAcount.Date = returnedTicket.Date;
                        returnAcount.AmountDue = "0";
                        returnAcount.Description = "Return " + tickets.AirlineName + "  " + returnedTicket.Ticket.TicketNo+" : "+returnedTicket.Ticket.TravellerName;
                        returnAcount.PaidAmount = (returnedCustomerTicket.getReturnAmount(tickets.TicketId) / currency.CurrencyRate).ToString();
                        returnAcount.Balance = "0";
                        returnAcount.IsReturned = true;
                        returnAcount.NetPrice = ((tickets.Price * tickets.CurrencyRate) / currency.CurrencyRate);
                        statmentAcount.Add(returnAcount);
                    }
                    else
                    {
                        totalAmountDue += (tickets.Price * tickets.CurrencyRate) / currency.CurrencyRate;
                    }
                }

                    ticketPaymentList = paymentConnection.CustomerPaymment(customerData.CustomerId,fromDate,toDate);
                    for (int i2 = 0; i2 < ticketPaymentList.Count; i2++)
                    {
                        clsStatmentAcount account2 = new clsStatmentAcount();
                        clsTicketsPayment payment = ticketPaymentList[i2];
                        account2.Date = payment.Date;
                        account2.AmountDue = "0";
                        account2.Balance = "0";
                        account2.PaidAmount = ((payment.Payment * payment.CurrencyRate)/currency.CurrencyRate).ToString();
                        totalPaid += ((payment.Payment * payment.CurrencyRate) / currency.CurrencyRate);
                        if (payment.PaymentType==1)
                        {
                            account2.Description = "CHECK#"+payment.ChequeNo;
                        }
                        else
                        {
                            account2.Description = "PAYMENT ON ACCOUNT CASH";
                        }
                        
                        statmentAcount.Add(account2);
                    }

               packageInvoiceList = packageInvoiceConnection.CustomerInvoicesAfterInventory(fromDate,toDate,customerData.CustomerId);

                for (int i = 0; i < packageInvoiceList.Count; i++)
                {
                    clsStatmentAcount account = new clsStatmentAcount();
                    clsPackageInvoice invoice = packageInvoiceList[i];
                    packageList = packageInvoiceConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                    account.PaidAmount = "0";
                    account.Balance = "0";
                    account.Date = invoice.InvoiceDate;
                    string text = "";
                    if (packageList.Count > 0)
                    {
                       text += "Invoice ID : " + invoice.InvoiceId + ".\n ";
                       text += "Reserve " + packageList.Count + " Package(s) :\n";
                        int index = 0;
                       foreach (clsPackage package in packageList)
                        {
                            index++;
                            text +=index+ ". " + package.packageName + " : " + package.CustomerName+"\n";
                        }
                    }
                    else
                    {
                        text = "(" + invoice.InvoiceId + ")-Empty invoice\n(Package Invoice)";
                    }

                    account.Description = text;
                    account.AmountDue = (invoice.NetPrice / currency.CurrencyRate).ToString();
                    statmentAcount.Add(account);
                    if (returnedCustomerPackage.InvoiceExist(invoice.InvoiceId))
                    {
                        decimal tPrice = invoice.NetPrice - returnedCustomerPackage.getReturnAmount(invoice.InvoiceId);
                        totalAmountDue += tPrice / currency.CurrencyRate;
                        List<clsReturnedCustomerPackages> returnList = new List<clsReturnedCustomerPackages>();
                        returnList.Clear();
                        returnList = returnedCustomerPackage.getReturnedPackage(invoice.InvoiceId);
                        for (int index = 0; index < returnList.Count; index++)
                        {
                            clsReturnedCustomerPackages item = returnList[index];
                            clsStatmentAcount returnAcount = new clsStatmentAcount();
                            returnAcount.Date = item.Date;
                            decimal amountdue = tPrice / currency.CurrencyRate;
                            returnAcount.AmountDue = "0";
                            returnAcount.Balance = "0";
                            returnAcount.IsReturned = true;
                            returnAcount.PaidAmount = ((item.ReturnAmount*item.CurrencyRate)/currency.CurrencyRate).ToString();
                            if (index > 0)
                            {
                                returnAcount.NetPrice = statmentAcount[statmentAcount.Count - 1].NetPrice - ((item.ReturnAmount * item.CurrencyRate) / currency.CurrencyRate);
                            }
                            else
                            {
                                returnAcount.NetPrice = (invoice.NetPrice / currency.CurrencyRate);
                            }
                            string description;
                            if (item.ItemId == 0)
                            {
                                clsPackage package = (clsPackage)item.Item;
                                description = "(" + invoice.InvoiceId + ")-Return " + package.packageName+" : "+item.Customer;
                            }
                            else
                            {
                                clsItem myItem = (clsItem)item.Item;
                                description = "(" + invoice.InvoiceId + ")-Return " + myItem.itemName + " : " + item.Customer;
                            }
                            returnAcount.Description = description;
                            
                            statmentAcount.Add(returnAcount);
                        }
                        invoice.NetPrice = tPrice;

                    }
                    else
                    {
                        totalAmountDue += invoice.NetPrice / currency.CurrencyRate;
                    }
                }
                    packageInvoicepaymentList = packagePaymentConnection.CustomerPayments(customerData.CustomerId,fromDate,toDate);
                    
                    for (int i2 = 0; i2 < packageInvoicepaymentList.Count; i2++)
                    {
                        clsStatmentAcount account2 = new clsStatmentAcount();
                        clsPackagesPayment payment = packageInvoicepaymentList[i2];
                        account2.Date = payment.Date;
                        account2.AmountDue = "0";
                        account2.Balance = "0";
                        account2.PaidAmount = (payment.Payment * payment.CurrencyRate / currency.CurrencyRate).ToString();
                        totalPaid += ((payment.Payment * payment.CurrencyRate / currency.CurrencyRate));
                      
                        if (payment.PaymentType == 1)
                        {
                            account2.Description = "CHECK#" + payment.ChequeNo;
                        }
                        else
                        {
                            account2.Description = "PAYMENT ON ACCOUNT CASH";
                        }
                        statmentAcount.Add(account2);

                    }

                statmentAcount.Sort((x, y) => x.Date.CompareTo(y.Date));
                statmentAcount.Insert(0, acount);
                for (int index=1;index<=statmentAcount.Count-1;index++)
                {
                    clsStatmentAcount accout = statmentAcount[index];
                    if(accout.IsReturned)
                    {
                        accout.Balance = (decimal.Parse(statmentAcount[index - 1].Balance) - 
                            decimal.Parse(accout.PaidAmount.ToString())).ToString();
                    }
                    else
                    {
                        accout.Balance = (decimal.Parse(statmentAcount[index - 1].Balance) + decimal.Parse(accout.AmountDue.ToString())
                            - decimal.Parse(accout.PaidAmount.ToString())).ToString();
                    }
                }
                DataTable dtt = ConvertToDatatable(statmentAcount);
                this.reportviewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsCustomerStatmentOfAcount", dtt);

                this.reportviewer1.LocalReport.DataSources.Add(reportsource);


                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {

                         new Microsoft.Reporting.WinForms.ReportParameter("customerName",customerData.CustomerName),
                          new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                          new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("totalPaid",totalPaid.ToString()),
                               new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                         new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                             new Microsoft.Reporting.WinForms.ReportParameter("totalAmountDue",totalAmountDue.ToString())

                      };
                reportviewer1.Visible = true;
                this.reportviewer1.LocalReport.SetParameters(p);

                this.reportviewer1.RefreshReport();
                skip = true;

            }



        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();
           
            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
