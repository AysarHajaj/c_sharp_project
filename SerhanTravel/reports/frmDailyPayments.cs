﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmDailyPayments : Form
    {

        List<clsTicketsPayment> ticketPayments;
        List<clsPackagesPayment> packagePayments;
        TicketsPayment ticketPayment;
        PackagesPayment packagePayment;
        Customer customerConnection;
        private List<clsCurrency> currencies;
        List<clsCustomer> customerList;
        Ticket ticketConnection;
        PackageInvoice invoiceConnection;
        Currency currencyConnection;
        public frmDailyPayments()
        {
            InitializeComponent();
        }

        private void frmDailyPayments_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
             ticketPayments = new List<clsTicketsPayment>();
            packagePayments = new List<clsPackagesPayment>();
            packagePayment = new PackagesPayment();
            ticketPayment = new TicketsPayment();
            ticketConnection = new Ticket();
            invoiceConnection = new PackageInvoice();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            customerConnection = new Customer();
            customerList = customerConnection.CustomersArrayList();
            reportViewer1.Visible = false;
            this.reportViewer1.RefreshReport();
        }



        static DataTable ConvertToDatatable(List<clsSupplierPayments> List)
        {
            DataTable dt = new DataTable();
            
            dt.Columns.Add("Description");
            dt.Columns.Add("Payment");
            dt.Columns.Add("Currency");
            dt.Columns.Add("Date");
            dt.Columns.Add("PaymentType");


            foreach (var item in List)
            {
                var row = dt.NewRow();

                row["Description"] = item.Description;
                row["Payment"] = item.Payment;
                row["Currency"] = item.Currency;
                row["Date"] = item.Date;
                row["PaymentType"] = item.PaymentType;

                dt.Rows.Add(row);
            }

            return dt;
        }

       
        private void btnSearch_Click(object sender, EventArgs e)
        {

            this.reportViewer1.LocalReport.DataSources.Clear();
            ticketPayments.Clear();
            packagePayments.Clear();
            this.reportViewer1.RefreshReport();
            if(cmbCurrency.SelectedIndex==-1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
          
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                ticketPayments = ticketPayment.CustomerPaymment(fromDate,toDate);
                List<clsSupplierPayments> PaymentList = new List<clsSupplierPayments>();
                foreach(clsTicketsPayment payments in ticketPayments)
                {
                    clsCustomer customer = new clsCustomer();
                    var result = from x in customerList where (x.CustomerId == payments.CustomerId) select x.CustomerName;
                    foreach(string name in result)
                    {
                        customer.CustomerName = name;
                    }
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment* payments.CurrencyRate) / currency.CurrencyRate).ToString();
                    payment.Description ="Ticket Payment\nPayment ID : "+payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t"+payments.ChequeNo;
                    }
                    if (payments.CustomerId == 0)
                    {
                        clsTickets ticket = ticketConnection.getTicketById(payments.TicketId);
                        payment.Description += "\nMade by Public Customer\nReserved To :" + ticket.TravellerName + "\nTicket NO:" + ticket.TicketNo;
                    }
                    else
                    {
                    payment.Description += "\nFrom "+customer.CustomerName;
                    }
                    PaymentList.Add(payment);
                  
                }
                packagePayments = packagePayment.CustomerPayments(fromDate,toDate);
                foreach (clsPackagesPayment payments in packagePayments)
                {
                    clsCustomer customer = new clsCustomer();
                    var result = from x in customerList where (x.CustomerId == payments.CustomerId) select x.CustomerName;
                    foreach (string name in result)
                    {
                        customer.CustomerName = name;
                    }
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment * payments.CurrencyRate) / currency.CurrencyRate).ToString();
                    payment.Description = "Package Payment\nPayment ID : " + payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t" + payments.ChequeNo;
                    }
                    if (payments.CustomerId == 0)
                    {
                        payment.Description += "\nMade by Public Customer\nInvoice ID :"+payments.PackageInvoiceId ;
                    }
                    else
                    {
                        payment.Description += "\nFrom " + customer.CustomerName;
                    }
                    PaymentList.Add(payment);
                }
           

                DataTable paymentTable = ConvertToDatatable(PaymentList);
  
                DataView dv = paymentTable.DefaultView;
                dv.Sort = "Date desc";
                paymentTable = dv.ToTable();
                

                this.reportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsSupplierPayments", paymentTable);
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                    {

                          new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())
                    };
                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);
                this.reportViewer1.RefreshReport();
                this.reportViewer1.Visible = true;
            
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

       
    }
}
