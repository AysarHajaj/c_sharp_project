﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmExpensesReport : Form
    {
        List<clsExpenses> expensesList;
        Expenses expensesConnection;
        List<clsExpensesType> typeList;
        clsExpensesType typeData;
        ExpensesType typeConnection;
        public frmExpensesReport()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            this.reportviewer1.LocalReport.DataSources.Clear();
            expensesList.Clear();
            this.reportviewer1.RefreshReport();
            if(cmbExpenses.SelectedIndex!=-1&& typeList.Count>0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                expensesList = expensesConnection.ExpensesListByTypeId(fromDate,toDate,typeData.ExpensesTypeId);
                

                DataTable dtt = ConvertToDatatable(expensesList);



                this.reportviewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsExpenses", dtt);

                this.reportviewer1.LocalReport.DataSources.Add(reportsource);
                

                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {

                         new Microsoft.Reporting.WinForms.ReportParameter("expensesType",typeData.ExpensesTypeEngName),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString())

                      };
                reportviewer1.Visible = true;
                this.reportviewer1.LocalReport.SetParameters(p);

                this.reportviewer1.RefreshReport();
            }
        }
  
        static DataTable ConvertToDatatable(List<clsExpenses> expenses)
        {
  
            DataTable dt = new DataTable();
            dt.Columns.Add("ExpensesAmount");
            dt.Columns.Add("ExpensesCurrencyId");
            dt.Columns.Add("ExpensesDate");
            dt.Columns.Add("ExpensesId");
            dt.Columns.Add("ExpensesInvoiceId");
            dt.Columns.Add("ExpensesRemarks");
            dt.Columns.Add("ExpensesType");
            dt.Columns.Add("CurrecnySymbol");
            dt.Columns.Add("ExpensesTypeString");
            dt.Columns.Add("CurrencyRate");
            foreach (var item in expenses)
            {
                var row = dt.NewRow();
                row["ExpensesAmount"] = item.ExpensesAmount;
                row["ExpensesCurrencyId"] = item.ExpensesCurrencyId;
                row["ExpensesDate"] = item.ExpensesDate.ToShortDateString();
                row["ExpensesId"] = item.ExpensesId;
                row["ExpensesInvoiceId"] = item.ExpensesInvoiceId;
                row["ExpensesRemarks"] = item.ExpensesRemarks;
                row["ExpensesType"] = item.ExpensesType;
                row["CurrecnySymbol"] = item.CurrecnySymbol;
                row["ExpensesTypeString"] = item.ExpensesTypeString;
                row["CurrencyRate"] = item.CurrencyRate; ;
                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbExpenses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbExpenses.SelectedIndex!=-1&& typeList.Count>0)
            {
                typeData = (clsExpensesType)cmbExpenses.SelectedItem;

            }
        }

        private void frmExpensesReport_Load(object sender, EventArgs e)
        {
            typeConnection = new ExpensesType();
            expensesList = new List<clsExpenses>();
            typeList = typeConnection.ExpensesTypeArrayList();
            expensesConnection = new Expenses();
            clsExpensesType expenses = new clsExpensesType();
            expenses.ExpensesTypeEngName = "All Types";
            expenses.ExpensesTypeId = 0;
            typeList.Insert(0, expenses);
            cmbExpenses.ValueMember = "ExpensesTypeId";
            cmbExpenses.DataSource = typeList;
            cmbExpenses.DisplayMember = "ExpensesTypeEngName";
        
            reportviewer1.Visible = false;
            this.reportviewer1.RefreshReport();
        }
    }
}
