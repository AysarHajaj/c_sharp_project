﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmIncomeStatment : Form
    {
        clsCurrency currency;
        private List<clsCurrency> currencies;
        DateTime fromDate;
        DateTime toDate;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public string currencyFormat = "#,##0;-#,##0;0";
        PackageInvoice invoiceConnection;
        Ticket ticketConnection;
        Expenses expensesConnection;
        List<clsCompany> company;
        Company CompanyInfo;
        ReturnedCustomerPackage returnPackageConnection;
        ReturnedCustomerTicket returnTicketConnection;
        Currency currencyConnection;
        public frmIncomeStatment()
        {
            InitializeComponent();
        }

     

        private void frmIncomeStatment_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            returnTicketConnection = new ReturnedCustomerTicket();
            returnPackageConnection = new ReturnedCustomerPackage();
            expensesConnection = new Expenses();
            ticketConnection = new Ticket();
            invoiceConnection = new PackageInvoice();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            dtpFrom.Value = DateTime.Today;
            dtpTo.Value = DateTime.Today;
            reportviewer1.Visible = false;
            this.reportviewer1.LocalReport.DataSources.Clear();
            this.reportviewer1.RefreshReport();
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            currency = (clsCurrency)cmbCurrency.SelectedItem;
            fromDate = dtpFrom.Value;
            toDate = dtpTo.Value;
            decimal ticketsSales=0, PackageSales=0, TotalSales=0;
            List<clsPackageInvoice> packageInvoiceList = invoiceConnection.GetInvoicesList(fromDate, toDate);
            foreach(clsPackageInvoice invoice in packageInvoiceList)
            {
                PackageSales += invoice.NetPrice;
                List <clsPackage> packagelist = invoiceConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                foreach(clsPackage package in packagelist)
                {
                    PackageSales -= (package.Cost * package.CurrencyRate);
                }
                if(returnPackageConnection.InvoiceExist(invoice.InvoiceId))
                {
                    PackageSales -= returnPackageConnection.getReturnAmount(invoice.InvoiceId);
                }
            }
            List<clsTickets> ticketList = ticketConnection.TicketsArrayList(fromDate, toDate);
            foreach(clsTickets ticket in ticketList)
            {
                ticketsSales += (ticket.Price * ticket.CurrencyRate);
                ticketsSales -= (ticket.Cost * ticket.CurrencyRate);
                if(returnTicketConnection.TicektIDExist(ticket.TicketId))
                {
                    ticketsSales -= returnTicketConnection.getReturnAmount(ticket.TicketId);
                }
            }
            TotalSales = ticketsSales + PackageSales;
            List<clsExpenses> expensesList = expensesConnection.ExpensesListByTypeId(fromDate, toDate, 0);
            ExpensesType typeConnection = new ExpensesType();
            List<clsExpensesType> expensesTypes = typeConnection.ExpensesTypeArrayList();
            for (int i = 0; i < expensesList.Count; i++)
            {
                expensesList[i].ExpensesAmount = (expensesList[i].ExpensesAmount * expensesList[i].CurrencyRate);
                for (int j = i + 1; j < expensesList.Count; j++)
                {
                    if (expensesList[i].ExpensesType == expensesList[j].ExpensesType)
                    {
                        expensesList[i].ExpensesAmount += (expensesList[j].ExpensesAmount * expensesList[j].CurrencyRate);
                        expensesList.RemoveAt(j);
                        j--;
                    }

                }
            }
            decimal TotalExpenses = 0;
            foreach(clsExpensesType type in expensesTypes)
            {
                var result = from expenses in expensesList where type.ExpensesTypeId == expenses.ExpensesType select expenses.ExpensesAmount;
                type.Amount = 0;
                foreach(decimal amount in result)
                {
                    type.Amount = amount/currency.CurrencyRate;
                    TotalExpenses += type.Amount;
                }
            }
            ticketsSales /= currency.CurrencyRate;
            PackageSales /= currency.CurrencyRate;
            TotalSales /= currency.CurrencyRate;
            decimal netIncome = TotalSales - TotalExpenses;
            DataTable dtt = ConvertToDatatable(expensesTypes);
            this.reportviewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsExpenses", dtt);

            this.reportviewer1.LocalReport.DataSources.Add(reportsource);


            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                          new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                          new Microsoft.Reporting.WinForms.ReportParameter("ticektsSales",ticketsSales.ToString()),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("netIncome",netIncome.ToString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("packagesSales",PackageSales.ToString()),
                               new Microsoft.Reporting.WinForms.ReportParameter("companyName",company[0].CompanyName),
                        new Microsoft.Reporting.WinForms.ReportParameter("totalSales",TotalSales.ToString())
                  };
            reportviewer1.Visible = true;
            this.reportviewer1.LocalReport.SetParameters(p);

            this.reportviewer1.RefreshReport();
        }

        static DataTable ConvertToDatatable(List<clsExpensesType> types)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("Amount");
            dt.Columns.Add("ExpensesTypeEngName");

            foreach (var item in types)
            {
                var row = dt.NewRow();


                row["Amount"] = item.Amount;
                row["ExpensesTypeEngName"] = item.ExpensesTypeEngName;

                dt.Rows.Add(row);
            }

            return dt;
        }

    }
}
