﻿
using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmItemsIssued : Form
    {
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        SupplierItemInvoice invoiceConnection;
        List<clsSupplierItemInvoice> invocieList;
        bool skip = false;
        int SupplierID = 0;
        List<clsItem> SupplieritemsList;
        Item itemConnection;
        ReturnedSupplierItem retunedItems;
        public frmItemsIssued()
        {
            InitializeComponent();
        }

        private void frmItemsIssued_Load(object sender, EventArgs e)
        {
            retunedItems = new ReturnedSupplierItem();
            supplierConnection = new Supplier();
            invoiceConnection = new SupplierItemInvoice();
            itemConnection = new Item();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            lbResult.Visible = false;
            this.reportViewer1.RefreshReport();
        }

        private void cmbSupplier_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;

                cmbSupplier.SelectedItem = lbResult.SelectedItem;

            }
            lbResult.Visible = false;
        }

        internal DataTable ConvertToDatatable(List<clsItem> itemlist)
        {
            DataTable dt = new DataTable();

            
            dt.Columns.Add("itemName");
            dt.Columns.Add("itemDescription");
            dt.Columns.Add("Cost");
            dt.Columns.Add("CurrecnySymbol"); 
            dt.Columns.Add("Status");
            dt.Columns.Add("CustomerName");

            foreach (var item in itemlist)
            {
                var row = dt.NewRow();


                row["itemName"] = item.itemName;
                row["itemDescription"] = item.itemDescription;
                row["CurrecnySymbol"] = Convert.ToString(item.CurrecnySymbol);
                row["Cost"] = Convert.ToString(item.Cost);
                row["CustomerName"] = item.CustomerName;
                row["Status"] = item.Status;


                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;

        }

      /*  internal int GetItemQuantity(int ItemID)
        {
            int quantity = 0;
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();
                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "Select count(IPD.ItemId) as quantity  " +
                                "from PackageInvoice as PIN Inner Join InvoicePackages as INP on INP.InvoiceId=PIN.InvoiceId " +
                                " Inner Join InvoicePackageDetail as IPD on IPD.InvPackDetails=INP.InvoicePackagesId " +
                                "where PIN.Date between  @fromDate and @toDate and IPD.ItemId=@ItemId " +
                                "Group by IPD.ItemId;";

                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value=dtpFrom.Value;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value=dtpTo.Value ;
                cmd.Parameters.Add("@ItemId", SqlDbType.Int).Value= ItemID;

                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    if(dr["quantity"]==DBNull.Value)
                    {
                        quantity = 0;
                    }
                    else
                    {
                        quantity = int.Parse(dr["quantity"].ToString());
                    }
                  
                }


                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return quantity;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return quantity;
            }


        }*/

        private void button1_Click(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.DataSources.Clear();
            lbResult.Visible = false;

            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                SupplieritemsList = new List<clsItem>();
                reportViewer1.Visible = true;
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                SupplierID = int.Parse(cmbSupplier.SelectedValue.ToString());
                invocieList = invoiceConnection.SupplierInvoices(fromDate, toDate, SupplierID);
                foreach(clsSupplierItemInvoice invoice in invocieList)
                {
                    foreach (clsItem item in invoice.Items)
                    {
                        if (retunedItems.IsItemReturned(invoice.InvoiceId,item.itemID,item.CustomerName))
                        {
                            item.Cost = ((item.Cost * item.CurrencyRate) - retunedItems.getReturnAmount(invoice.InvoiceId, item.itemID, item.CustomerName))/item.CurrencyRate;
                            item.Status = "Returned.";
                        }
                        else
                        {
                            item.Status = "Not Returned.";
                        }
                    }

                    SupplieritemsList.AddRange(invoice.Items);
                }
               
                DataTable dtt = ConvertToDatatable(SupplieritemsList);
                ReportDataSource reportsource = new ReportDataSource("dsItemsIssued", dtt);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);

                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {
                                    new Microsoft.Reporting.WinForms.ReportParameter("fromDate",dtpFrom.Value.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("toDate",dtpFrom.Value.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())

                      };


                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.Refresh();
                this.reportViewer1.RefreshReport();
                skip = true;
            }
        }
    }
}
