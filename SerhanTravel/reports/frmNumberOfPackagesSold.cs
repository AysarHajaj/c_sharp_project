﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmNumberOfPackagesSold : Form
    {
        Package packageConnection = new Package();
        List<clsPackage> packageList=new List<clsPackage>();
        List<clsNumberOfPackageSold> packagesSold = new List<clsNumberOfPackageSold>();
        int allPackagesSold;
        public frmNumberOfPackagesSold()
        {
            InitializeComponent();
        }

        private void frmNumberOfPackagesSold_Load(object sender, EventArgs e)
        {
            packageList = packageConnection.PackageArrayList();
            
        }

        static DataTable ConvertToDatatable(List<clsNumberOfPackageSold> numbers)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("PackageId");
            dt.Columns.Add("PackageName");
           
            dt.Columns.Add("Number");
            dt.Columns.Add("Percentage");

            foreach (var item in numbers)
            {
                var row = dt.NewRow();


                row["PackageId"] = Convert.ToString(item.PackageId);
                row["PackageName"] = item.PackageName;
                row["Number"] = Convert.ToString(item.Number);
                row["Percentage"] = Convert.ToString(item.Percentage);
                


                dt.Rows.Add(row);
            }

            return dt;
        }

        internal clsNumberOfPackageSold getNumberOfPackagesSold(clsPackage package)
        {
            int count = 0;
            clsNumberOfPackageSold number = new clsNumberOfPackageSold();

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = " select COUNT(IP.PackageId) as number " +
                    " from InvoicePackages as IP Inner Join PackageInvoice as PIN on PIN.InvoiceId=IP.InvoiceId " +
                    " Where PIN.Date between @fromDate and @toDate  " +
                    " and IP.PackageId=@PackageId ";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = dtpFrom.Value;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = dtpTo.Value;
                cmd.Parameters.Add("@PackageId", SqlDbType.Int).Value = package.packageID;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["number"] == DBNull.Value)
                    { count = 0; }
                    else
                    { count = int.Parse(dr["number"].ToString()); }
                }
                number.PackageId = package.packageID;
                number.PackageName = package.packageName;
                number.Number = count;
                double n= (double)(count * 100) / allPackagesSold;
                number.Percentage = Math.Round(n, 2, MidpointRounding.AwayFromZero);



                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return number;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return number;
            }
        }


        internal int getAllSoldPackages()
        {
            int count = 0;
            

            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();
                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = "select COUNT(IP.PackageId) as number " +
                    " from InvoicePackages as IP Inner Join PackageInvoice as PIN on PIN.InvoiceId=IP.InvoiceId " +
                    " Where PIN.Date between @fromDate and @toDate ;";
                SqlCommand cmd = new SqlCommand(sql, sqlCon);
                cmd.Parameters.Add("@fromDate", SqlDbType.Date).Value = dtpFrom.Value;
                cmd.Parameters.Add("@toDate", SqlDbType.Date).Value = dtpTo.Value;
                SqlDataReader dr = cmd.ExecuteReader();



                if (dr.HasRows)
                {
                    dr.Read();

                    if (dr["number"] == DBNull.Value)
                    { count = 0; }
                    else
                    { count = int.Parse(dr["number"].ToString()); }
                }
                



                sqlCon.Close();
                sqlCon.Dispose();
                cmd.Dispose();
                dr.Close();
                return count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return count;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            allPackagesSold = 0;
            packagesSold.Clear();
            allPackagesSold = getAllSoldPackages();
            for (int i = 0; i < packageList.Count; i++)
            {
                packagesSold.Add(getNumberOfPackagesSold(packageList[i]));
            }
            DataTable dtt = new DataTable();
            dtt = ConvertToDatatable(packagesSold);
            DataView dv = dtt.DefaultView;
            dv.Sort = "Number desc";
            dtt = dv.ToTable();
            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsNumberOfPackagesSold", dtt);
            this.reportViewer1.LocalReport.DataSources.Add(reportsource);

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                                  new Microsoft.Reporting.WinForms.ReportParameter("fromDate",dtpFrom.Value.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("toDate",dtpTo.Value.ToShortDateString())

                  };


            this.reportViewer1.LocalReport.SetParameters(p);
            this.reportViewer1.RefreshReport();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
