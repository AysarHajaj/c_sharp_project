﻿
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;

namespace SerhanTravel.reports
{
    public partial class frmPackageSold : Form
    {
        List<clsCustomer> customerList;
        Customer customerConnection;
        private bool skip;
        PackagesSold PackageSoldConnection;
        List<clsPackage> CustomerpackagesList;
        private List<clsCurrency> currencies;
        Currency currencyConnection;
        public int CustomerID { get; private set; }

        public frmPackageSold()
        {
            InitializeComponent();
        }

        private void frmPackageSold_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            customerConnection = new Customer();
            PackageSoldConnection = new PackagesSold();
            customerList = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);

            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            reportViewer1.Visible = false;
            lbResult.Visible = false;
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;

                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        static DataTable ConvertToDatatable(List<clsPackage> packageList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("PackageName");
            dt.Columns.Add("CountryName");
            dt.Columns.Add("packageDays");
            dt.Columns.Add("Price");
            dt.Columns.Add("itemsStr");
            dt.Columns.Add("CurrecnySymbol");
            dt.Columns.Add("CurrencyRate");

            foreach (var item in packageList)
            {
                var row = dt.NewRow();


                row["PackageName"] = item.packageName;
                row["CountryName"] = item.CountryName;
                row["packageDays"] = Convert.ToString(item.packageDays);
                row["Price"] = Convert.ToString(item.Price);
                row["itemsStr"] = item.itemsStr;
                row["CurrecnySymbol"] = item.CurrecnySymbol;
                row["CurrencyRate"] = item.CurrencyRate;


                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            reportViewer1.Visible = true;
            reportViewer1.LocalReport.DataSources.Clear();
            lbResult.Visible = false;
            if(cmbCurrency.SelectedIndex==-1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                DateTime fromDate, toDate;
                fromDate = dtpFrom.Value;
                toDate = dtpTo.Value;

                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                string customerName;
                clsCustomer customerData = (clsCustomer)cmbCustomerName.SelectedItem;
                customerName = customerData.CustomerName;
                if (customerData.CustomerId == 0)
                {
                    customerName = " ";
                }
                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                CustomerpackagesList = PackageSoldConnection.getPackagesSoldList(fromDate,toDate,CustomerID);
                for (int i = 0; i < CustomerpackagesList.Count; i++)
                {
                    CustomerpackagesList[i].packageName = (i + 1) + ". " + CustomerpackagesList[i].packageName;
                }

                DataTable dtt = ConvertToDatatable(CustomerpackagesList);

                ReportDataSource reportsource = new ReportDataSource("dsPackageSold", dtt);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);

                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {
                        new Microsoft.Reporting.WinForms.ReportParameter("customerName",customerName),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedRate",currency.CurrencyRate.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedSymbol",currency.CurrencySymbol),
                             new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())

                      };


                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.Refresh();
                this.reportViewer1.RefreshReport();
                skip = true;
            }
        }
    }
}
