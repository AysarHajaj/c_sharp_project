﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmPaymentByType : Form
    {
        List<clsSupplier> supplierList;
        List<clsSuppliersTickertPayment> ticketPayments;
        List<clsSuppliersItemPayment> itemPayments;
        Supplier supplierConnection;
        SuppliersTicketPayment ticketPayment;
        SuppliersItemPayment itemPayment;
        clsSupplier supplierData;
        bool skip = false;
        private List<clsCurrency> currencies;
        string currencyFormat;
        int type = -1;
        Currency currencyConnection;
        public frmPaymentByType()
        {
            InitializeComponent();
        }

        private void frmPaymentByType_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            currencyFormat = "#,##0.00;-#,##0.00;0";
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            lbResult.Visible = false;
            rbCash.Checked = false;
            rbCheque.Checked = false;
            ticketPayments = new List<clsSuppliersTickertPayment>();
            itemPayments = new List<clsSuppliersItemPayment>();
            itemPayment = new SuppliersItemPayment();
            ticketPayment = new SuppliersTicketPayment();
            supplierData = new clsSupplier();
            supplierConnection = new Supplier();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            this.reportViewer1.RefreshReport();


        }


        static DataTable ConvertToDatatable(List<clsSupplierPayments> List)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Description");
            dt.Columns.Add("Payment");
            dt.Columns.Add("Currency");
            dt.Columns.Add("Date");
            dt.Columns.Add("PaymentType");


            foreach (var item in List)
            {
                var row = dt.NewRow();

                row["Description"] = item.Description;
                row["Payment"] = item.Payment;
                row["Currency"] = item.Currency;
                row["Date"] = item.Date;
                row["PaymentType"] = item.PaymentType;

                dt.Rows.Add(row);
            }

            return dt;
        }


        private void cmbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip=true;
        }
        private void cmbSupplierName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbSupplierName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbSupplierName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void rbCash_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCash.Checked)
            {
                type = 0;

            }
        }

        private void rbCheque_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCheque.Checked)
            {
                type = 1;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (type == -1)
            {
                MessageBox.Show("Please choose a type");
                return;
            
            }
            if (cmbSupplierName.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a supplier");
                return;
            }

            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.RefreshReport();
            if (cmbSupplierName.SelectedIndex != -1 && supplierList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                supplierData = (clsSupplier)cmbSupplierName.SelectedItem;
                ticketPayments = ticketPayment.SuppliersTicketsPayments(supplierData.SupplierId, fromDate, toDate);
                List<clsSupplierPayments> PaymentList = new List<clsSupplierPayments>();
                foreach (clsSuppliersTickertPayment payments in ticketPayments)
                {
                    if(payments.PaymentType!=type)
                    {
                        continue;
                    }
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment * payments.CurrencyRate) / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol;
                    payment.Description = "Ticket Payment\nPayment ID : " + payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t" + payments.ChequeNo;
                    }
                    PaymentList.Add(payment);
                }
                itemPayments = itemPayment.SupplierPayment(supplierData.SupplierId, fromDate, toDate);
                foreach (clsSuppliersItemPayment payments in itemPayments)
                {
                    if (payments.PaymentType != type)
                    {
                        continue;
                    }
                    clsSupplierPayments payment = new clsSupplierPayments();
                    payment.Payment = ((payments.Payment * payments.CurrencyRate) / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol;
                    payment.Description = "Item Payment\nPayment ID : " + payments.PaymentId;
                    payment.Currency = currency.CurrencyName;
                    payment.Date = payments.Date.ToShortDateString();
                    if (payments.PaymentType == 0)
                    {
                        payment.PaymentType = "Cash";
                    }
                    else
                    {
                        payment.PaymentType = "Cheque of number:\t" + payments.ChequeNo;
                    }
                    PaymentList.Add(payment);
                }


                DataTable paymentTable = ConvertToDatatable(PaymentList);

                DataView dv = paymentTable.DefaultView;
                dv.Sort = "Date desc";
                paymentTable = dv.ToTable();


                this.reportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsSupplierPayments", paymentTable);
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                    {

                         new Microsoft.Reporting.WinForms.ReportParameter("supplierName",supplierData.SupplierName),
                          new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                           new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())
                    };
                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);
                this.reportViewer1.RefreshReport();
                this.reportViewer1.Visible = true;
                skip = true;

            }

        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
