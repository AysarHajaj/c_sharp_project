﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmSalesByType : Form
    {
        clsCurrency currency;
        private List<clsCurrency> currencies;
        DateTime fromDate ;
        DateTime toDate;
        ReturnedCustomerPackage returnedPackage;
        ReturnedCustomerTicket returnedTicekt;
        int type = -1;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        public string currencyFormat = "#,##0;-#,##0;0";
        Currency currencyConnection;
        public frmSalesByType()
        {
            InitializeComponent();
        }

        private void frmSalesByType_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            returnedPackage = new ReturnedCustomerPackage();
            returnedTicekt = new ReturnedCustomerTicket();
            dtpFrom.Value = DateTime.Today;
            dtpTo.Value = DateTime.Today;
            rbPackages.Checked = false;
            rbTickets.Checked = false;
            reportViewer1.Visible = false;
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.RefreshReport();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (type == -1)
            {
                MessageBox.Show("Please choose a type");
                return;
            }
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            string query1 = "";
            string query2 = "";
           fromDate = dtpFrom.Value;
           toDate = dtpTo.Value;
            DataTable unPaidTable;
            DataTable PaidTable;
            string typeName = "";
            currency = (clsCurrency)cmbCurrency.SelectedItem;
          
            if (type == 0)
            {
                query1 = "Select TIN.TicketId,TIN.TicketNo as InvoiceId,TIN.TicketDate as Date,TIN.Name,TIN.CustomerId,(TIN.Price*TIN.CurrencyRate) as NetPrice " +
                                "from Ticket As TIN  " +
                                "where TIN.TicketDate between @fromdate and @toDate  " +
                                "Group By  TIN.TicketId,TIN.TicketDate,TIN.Price,TIN.Name,TIN.CustomerId,TIN.TicketNo,TIN.CurrencyRate " +
                                "order by TIN.CustomerId; ";

                PaidTable = TicketInvoices(query1);
               /* query2 = "Select TIN.TicketId,TIN.TicketNo as InvoiceId,TIN.TicketDate as Date,TIN.Name,TIN.CustomerId,(TIN.Price*TIN.CurrencyRate) as NetPrice " +
                                "from Ticket As TIN where TIN.TicketDate between @fromdate and @toDate and TIN.TicketId not in(select TPS.TicketId  " +
                                "from TicketsPayment as TPS)  " +
                                "Group By  TIN.TicketNo,TIN.TicketDate,TIN.Price,TIN.Name,TIN.CustomerId,TIN.CurrencyRate,TIN.TicketId " +
                                "order by TIN.CustomerId;  ";
                unPaidTable = unPaidInvoiceTicketList(query2);*/
                typeName = "Ticket Sales";

            }

            else 
            {
                query1 = "Select TIN.InvoiceId,TIN.Name,TIN.Date,TIN.CustomerId,(TIN.NetPrice-(TIN.NetPrice*TIN.Discount*0.01)) as NetPrice " +
                                "from PackageInvoice As TIN " +
                                "where TIN.Date between @fromdate and @toDate " +
                                "Group By  TIN.InvoiceId,TIN.Date,TIN.NetPrice,TIN.Name,TIN.CustomerId,TIN.Discount " +
                                "order by TIN.Name; ";
                PaidTable = PackageInvoices(query1);

               /* query2 = "Select TIN.InvoiceId,TIN.Name,TIN.Date,TIN.NetPrice,TIN.Name,TIN.CustomerId " +
                                "from PackageInvoice As TIN " +
                                "where TIN.Date between @fromdate and @toDate and TIN.InvoiceId not in(select TPS.PackageInvoiceId " +
                                "from PackagePayments as TPS) " +
                                "Group By  TIN.InvoiceId,TIN.Date,TIN.NetPrice,TIN.Name,TIN.CustomerId " +
                                "order by TIN.Name; ";
                unPaidTable =  unPaidInvoicePackageList(query2);*/

                typeName = "Package Sales";
            }


            DataTable dtt = new DataTable();
           // dtt.Merge(unPaidTable);
            dtt.Merge(PaidTable);
            DataView dv = dtt.DefaultView;
            dv.Sort = "Date desc";
            dtt = dv.ToTable();

            this.reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsSalesByType", dtt);

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {

                              new Microsoft.Reporting.WinForms.ReportParameter("Type",typeName.ToString()),
                               new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                                  new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                               new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString())
                  };
            reportViewer1.Visible = true;

            this.reportViewer1.LocalReport.SetParameters(p);

            this.reportViewer1.LocalReport.DataSources.Add(reportsource);
            this.reportViewer1.RefreshReport();





        }

        private void rbPackages_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPackages.Checked)
            {
                type = 1;

            }
        }

        private void rbTickets_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTickets.Checked)
            {
                type = 0;
            }
        }
        internal DataTable TicketInvoices(string query)
        {

            Ticket ticket = new Ticket();
            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = query;

                SqlCommand command = new SqlCommand(sql, sqlCon);
                command.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                command.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = command.ExecuteReader();
                clsTicketInvoice invoice;
                clsCustomer customer;
                //dt.Columns.Add("customerName");
                dt.Columns.Add("InvoiceId");
                dt.Columns.Add("Description");
                dt.Columns.Add("Date");
                dt.Columns.Add("CostPrice");
                dt.Columns.Add("Profit");



                TicketInvoice ticketConnection = new TicketInvoice();




                while (dr.Read())
                {
                    invoice = new clsTicketInvoice();
                    customer = new clsCustomer();

                    if (dr["Name"] == DBNull.Value)
                    {
                        customer.CustomerName = string.Empty;
                    }
                    else
                    {
                        customer.CustomerName = dr["Name"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }
                    string TicketNo;
                    int ticketId;
                    if (dr["TicketId"] == DBNull.Value)
                    {
                        ticketId = 0;
                    }
                    else
                    {
                        ticketId = int.Parse(dr["TicketId"].ToString());
                    }
                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        TicketNo = string.Empty;
                    }
                    else
                    {
                        TicketNo = dr["InvoiceId"].ToString();
                    }
                    if (ticketId != 0)
                    {
                        if (returnedTicekt.TicektIDExist(ticketId))
                        {
                            invoice.NetPrice = invoice.NetPrice - (returnedTicekt.getReturnAmount(ticketId));
                            TicketNo += "\n(Returned).";
                        }
                    }

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        customer.CustomerId = 0;
                    }
                    else
                    {
                        customer.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }

                    if (dr["Date"] == DBNull.Value)
                    {
                        invoice.InvoiceDate = DefaultDate;
                    }
                    else
                    {
                        invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                    }


                    var row = dt.NewRow();


                    string description;

                    description = "Reserved To : " + customer.CustomerName + "\nat " + invoice.InvoiceDate.ToShortDateString() + "\nPrice : "
                        + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol;
                    clsTickets tickets = ticket.getTicketById(ticketId);
                    row["Description"] = description;
                    row["InvoiceId"] = TicketNo;
                    row["Date"] = invoice.InvoiceDate.ToShortDateString();
                    row["CostPrice"] = (tickets.Cost * tickets.CurrencyRate) / currency.CurrencyRate;
                    row["Profit"] = ((invoice.NetPrice - (tickets.Cost * tickets.CurrencyRate))) / currency.CurrencyRate;
                    dt.Rows.Add(row);
                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }



       /* internal DataTable unPaidInvoiceTicketList(string query)
        {

            Ticket ticket = new Ticket();
            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = query;

                SqlCommand command = new SqlCommand(sql, sqlCon);
                command.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                command.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = command.ExecuteReader();
                clsTicketInvoice invoice;
                clsCustomer customer;
                //dt.Columns.Add("customerName");
                dt.Columns.Add("Description");
                dt.Columns.Add("PaidAmount");
                dt.Columns.Add("InvoiceId");
                dt.Columns.Add("Date");
                dt.Columns.Add("CostPrice");
                dt.Columns.Add("Profit");


                TicketInvoice ticketConnection = new TicketInvoice();




                while (dr.Read())
                {
                    invoice = new clsTicketInvoice();
                    customer = new clsCustomer();


                    if (dr["Name"] == DBNull.Value)
                    {
                        customer.CustomerName = string.Empty;
                    }
                    else
                    {
                        customer.CustomerName = dr["Name"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }
                    string TicketNo;
                    int ticketId;
                    if (dr["TicketId"] == DBNull.Value)
                    {
                        ticketId = 0;
                    }
                    else
                    {
                        ticketId = int.Parse(dr["TicketId"].ToString());
                    }
                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        TicketNo = string.Empty;
                    }
                    else
                    {
                        TicketNo = dr["InvoiceId"].ToString();
                    }
                    if (ticketId != 0)
                    {
                        if (returnedTicekt.TicektIDExist(ticketId))
                        {
                            invoice.NetPrice = invoice.NetPrice - (returnedTicekt.getReturnAmount(ticketId));
                        }
                    }

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        customer.CustomerId = 0;
                    }
                    else
                    {
                        customer.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }

                    if (dr["Date"] == DBNull.Value)
                    {
                        invoice.InvoiceDate = DefaultDate;
                    }
                    else
                    {
                        invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                    }


                    var row = dt.NewRow();


                    string description;

                    description = "Reserved To : " + customer.CustomerName + "\nat " + invoice.InvoiceDate.ToShortDateString() + "\nPrice : "
                        + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol;
                    clsTickets tickets = ticket.getTicketById(ticketId);
                    row["Description"] = description;
                    row["PaidAmount"] = 0;
                    row["InvoiceId"] = TicketNo;
                    row["Date"] = invoice.InvoiceDate.ToShortDateString();
                    row["CostPrice"] = (tickets.Cost * tickets.CurrencyRate) / currency.CurrencyRate;
                    row["Profit"] = ((invoice.NetPrice - (tickets.Cost * tickets.CurrencyRate))) / currency.CurrencyRate;
                    dt.Rows.Add(row);


                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }
        */

        internal DataTable PackageInvoices(string query)
        {


            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = query;

                SqlCommand command = new SqlCommand(sql, sqlCon);
                command.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                command.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = command.ExecuteReader();
                clsPackageInvoice invoice;
                clsCustomer customer;
                // dt.Columns.Add("customerName");
                dt.Columns.Add("InvoiceId");
                dt.Columns.Add("Description");
                dt.Columns.Add("Date");
                dt.Columns.Add("CostPrice");
                dt.Columns.Add("Profit");


                PackageInvoice packageConnection = new PackageInvoice();




                while (dr.Read())
                {
                    invoice = new clsPackageInvoice();
                    customer = new clsCustomer();

                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        customer.CustomerId = 0;
                    }
                    else
                    {
                        customer.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }

                    if (dr["Name"] == DBNull.Value)
                    {
                        customer.CustomerName = string.Empty;
                    }
                    else
                    {
                        customer.CustomerName = dr["Name"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }
                    bool isReturned = false;
                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        invoice.InvoiceId = 0;
                    }
                    else
                    {
                        invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString());
                    }
                    if (invoice.InvoiceId != 0)
                    {
                        if (returnedPackage.InvoiceExist(invoice.InvoiceId))
                        {
                            isReturned = true;
                            invoice.NetPrice = invoice.NetPrice - Convert.ToDecimal(returnedPackage.getReturnAmount(invoice.InvoiceId).ToString());
                        }
                    }

                    if (dr["Date"] == DBNull.Value)
                    {
                        invoice.InvoiceDate = DefaultDate;
                    }
                    else
                    {
                        invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                    }


                    var row = dt.NewRow();
                    List<clsPackage> packageList = packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                    string description;
                    if (customer.CustomerId == 0)
                    {
                        description = "Made by Public Customer\nCustomer Name : " + customer.CustomerName + "\nat " +
                            invoice.InvoiceDate.ToShortDateString() + "\nInvoice Price : " + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol +
                             "\nReserve " + packageList.Count + " Package(s) :\n";
                    }
                    else
                    {
                        description = "Made by  " + customer.CustomerName + "\nat " + invoice.InvoiceDate.ToShortDateString() + "\nInvoice Price : "
                           + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol +
                            "\nReserve " + packageList.Count + " Package(s) :\n";
                    }
                    int index = 0;
                    foreach (clsPackage package in packageList)
                    {
                        index++;
                        description += index + ". " + package.packageName + " : " + package.CustomerName + "\n";
                    }
                    if (isReturned)
                    {
                        description += "\n(Includes Returned Items).";
                    }
                    row["Description"] = description;
                    row["InvoiceId"] = invoice.InvoiceId;
                    row["Date"] = invoice.InvoiceDate.ToShortDateString();
                    List<clsPackage> packagelist = packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                    var cost = (from package in packagelist select (package.Cost * package.CurrencyRate));
                    decimal costResult = 0;
                    foreach (decimal cs in cost)
                    {
                        costResult += decimal.Parse(cs.ToString());
                    }
                    row["CostPrice"] = (costResult) / currency.CurrencyRate;
                    row["Profit"] = (invoice.NetPrice - costResult) / currency.CurrencyRate;
                    dt.Rows.Add(row);
                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }



    /*    internal DataTable unPaidInvoicePackageList(string query)
        {


            DataTable dt;
            dt = new DataTable();
            try
            {
                SqlConnection sqlCon;
                BuildConnection aBuildConnection = new BuildConnection();

                sqlCon = aBuildConnection.AddSqlConnection();

                if (sqlCon.State != ConnectionState.Open)
                {
                    sqlCon.Open();
                }
                string sql = query;

                SqlCommand command = new SqlCommand(sql, sqlCon);
                command.Parameters.Add("@toDate", SqlDbType.Date).Value = toDate;
                command.Parameters.Add("@fromdate", SqlDbType.Date).Value = fromDate;
                SqlDataReader dr = command.ExecuteReader();
                clsPackageInvoice invoice;
                clsCustomer customer;
                //dt.Columns.Add("customerName");
                dt.Columns.Add("InvoiceId");
                dt.Columns.Add("Description");
                dt.Columns.Add("PaidAmount");
                dt.Columns.Add("Date");
                dt.Columns.Add("CostPrice");
                dt.Columns.Add("Profit");




                PackageInvoice packageConnection = new PackageInvoice();




                while (dr.Read())
                {
                    invoice = new clsPackageInvoice();
                    customer = new clsCustomer();



                    if (dr["CustomerId"] == DBNull.Value)
                    {
                        customer.CustomerId = 0;
                    }
                    else
                    {
                        customer.CustomerId = int.Parse(dr["CustomerId"].ToString());
                    }

                    if (dr["Name"] == DBNull.Value)
                    {
                        customer.CustomerName = string.Empty;
                    }
                    else
                    {
                        customer.CustomerName = dr["Name"].ToString();
                    }

                    if (dr["NetPrice"] == DBNull.Value)
                    {
                        invoice.NetPrice = 0;
                    }
                    else
                    {
                        invoice.NetPrice = decimal.Parse(dr["NetPrice"].ToString());
                    }

                    if (dr["InvoiceId"] == DBNull.Value)
                    {
                        invoice.InvoiceId = 0;
                    }
                    else
                    {
                        invoice.InvoiceId = int.Parse(dr["InvoiceId"].ToString());
                    }

                    if (invoice.InvoiceId != 0)
                    {
                        if (returnedPackage.InvoiceExist(invoice.InvoiceId))
                        {
                            invoice.NetPrice = invoice.NetPrice - Convert.ToDecimal(returnedPackage.getReturnAmount(invoice.InvoiceId).ToString());
                        }
                    }
                    if (dr["Date"] == DBNull.Value)
                    {
                        invoice.InvoiceDate = DefaultDate;
                    }
                    else
                    {
                        invoice.InvoiceDate = DateTime.Parse(dr["Date"].ToString());
                    }


                    var row = dt.NewRow();

                    string description;
                    if (customer.CustomerId == 0)
                    {
                        description = "Made by Public Customer\nCustomer Name : " + customer.CustomerName + "\nat " + invoice.InvoiceDate.ToShortDateString() + "\nInvoice Price : "
                        + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol + "\nReserves " + packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId).Count + " Package";
                    }
                    else
                    {
                        description = "Made by " + customer.CustomerName + "\nat " + invoice.InvoiceDate.ToShortDateString() + "\nInvoice Price : "
                         + (invoice.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol + "\nReserves " + packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId).Count + " Package";
                    }

                    row["InvoiceId"] = invoice.InvoiceId;
                    row["Description"] = description;
                    row["PaidAmount"] = 0;
                    row["Date"] = invoice.InvoiceDate.ToShortDateString();
                    List<clsPackage> packagelist = packageConnection.PackagesInvoicesArrayList(invoice.InvoiceId);
                    var cost = (from package in packagelist select (package.Cost * package.CurrencyRate));
                    decimal costResult = 0;
                    foreach (decimal cs in cost)
                    {
                        costResult += decimal.Parse(cs.ToString());
                    }
                    row["CostPrice"] = (costResult) / currency.CurrencyRate;
                    row["Profit"] = (invoice.NetPrice - costResult) / currency.CurrencyRate;
                    dt.Rows.Add(row);
                }


                dr.Close();
                command.Dispose();
                sqlCon.Close();
                sqlCon.Dispose();



                return dt; ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return dt;
            }
        }*/

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }
    }
}
