﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace SerhanTravel.reports
{
    public partial class frmSuppReturnedItems : Form
    {
        ReturnedSupplierTicket returnSupplierTicket;
        private bool skip;
        private string currencyFormat;
        ReturnedSupplierItem returnedSupplierItem;
        List<clsReturnedItems> ticketList;
        List<clsReturnedItems> itemlist;
        DateTime fromDate, toDate;
        private List<clsCurrency> currencies;
        clsCurrency currency;
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        clsSupplier supplierData;
        Currency currencyConnection;
        public frmSuppReturnedItems()
        {
            InitializeComponent();
        }

        private void frmSuppReturnedItems_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
             supplierConnection = new Supplier();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedIndex = -1;
            skip = true;
            currencyFormat = "#,##0.00;-#,##0.00;Zero";
            returnedSupplierItem = new ReturnedSupplierItem();
            returnSupplierTicket = new ReturnedSupplierTicket();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            currency = (clsCurrency)cmbCurrency.SelectedItem;
            fromDate = dtpFrom.Value;
            toDate = dtpTo.Value;
            supplierData = (clsSupplier)cmbSupplierName.SelectedItem;
            itemlist = returnedSupplierItem.getReturnedItemList(supplierData.SupplierId,fromDate, toDate);
            ticketList = returnSupplierTicket.getReturnedTicketsArrayList(supplierData.SupplierId, fromDate, toDate);
            DataTable dt = ConvertToDatatable(ticketList, itemlist);
            this.reportviewer1.LocalReport.DataSources.Clear();
            ReportDataSource reportsource = new ReportDataSource("dsReturnedItems", dt);

            this.reportviewer1.LocalReport.DataSources.Add(reportsource);


            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {



                             new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                             new Microsoft.Reporting.WinForms.ReportParameter("toDate", toDate.ToShortDateString()),
                              new Microsoft.Reporting.WinForms.ReportParameter("selectedRate",currency.CurrencyRate.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedSymbol",currency.CurrencySymbol),
                             new Microsoft.Reporting.WinForms.ReportParameter("returnedFrom","Supplier Returned Items")

                  };
            reportviewer1.Visible = true;
            this.reportviewer1.LocalReport.SetParameters(p);

            this.reportviewer1.RefreshReport();
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void cmbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbSupplierName.SelectedIndex != -1)
            {
                skip = true;
            }
        }

        private void cmbSupplierName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbSupplierName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbSupplierName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        DataTable ConvertToDatatable(List<clsReturnedItems> ticketlist, List<clsReturnedItems> itemList)
        {
            DataTable dt = new DataTable();

           
            dt.Columns.Add("Description");
            dt.Columns.Add("RemainedAmount");
            dt.Columns.Add("Name");
            dt.Columns.Add("ReturnedDate");
            dt.Columns.Add("ReturnAmount");
            dt.Columns.Add("ReturnType");
            foreach (clsReturnedItems ticket in ticketlist)
            {
                ticket.Description = "For " + ticket.Name + "\nTicket No :" + ticket.TicketNo + "\nCost: " + (ticket.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol + "\nDate : " + ticket.InvoiceDate.ToShortDateString();
                var row = dt.NewRow();
                row["ReturnType"] = ticket.ReturnType;
                row["ReturnAmount"] = ticket.ReturnAmount;
                row["ReturnedDate"] = ticket.ReturnedDate.ToShortDateString();
                row["Name"] = ticket.Name;
                row["RemainedAmount"] = ticket.RemainedAmount;
                row["Description"] = ticket.Description;
                dt.Rows.Add(row);
            }
            foreach (clsReturnedItems ticket in itemList)
            {
                ticket.Description = "For "+ticket.Name+"\nInvoice ID:" + ticket.InvoiceId + "\nCost : " + (ticket.NetPrice / currency.CurrencyRate).ToString(currencyFormat) + " " + currency.CurrencySymbol + "\nDate : " + ticket.InvoiceDate.ToShortDateString();
                var row = dt.NewRow();
                row["ReturnType"] = ticket.ReturnType;
                row["ReturnAmount"] = ticket.ReturnAmount;
                row["ReturnedDate"] = ticket.ReturnedDate.ToShortDateString();
                row["Name"] = ticket.Name;
                row["RemainedAmount"] = ticket.RemainedAmount;
                row["Description"] = ticket.Description;
                dt.Rows.Add(row);
            }
            return dt;
        }

    }
}
