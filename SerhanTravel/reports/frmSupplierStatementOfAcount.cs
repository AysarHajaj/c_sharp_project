﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmSupplierStatementOfAcount : Form
    {
        Company CompanyInfo;
        List<clsCompany> company;
        Ticket ticketConnection;
        ReturnedSupplierTicket returnedTickets;
        ReturnedSupplierItem returnedItems;
        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        clsSupplier supplierData;
        private List<clsCurrency> currencies;
        List<clsStatmentAcount> statmentAcount;
        private List<clsTickets> ticketList;
        SuppliersTicketPayment paymentConnection;
        List<clsSuppliersTickertPayment> ticketPaymentList;
        SupplierItemInvoice itemInvoiceConnection;
        List<clsSupplierItemInvoice> itemInvoiceList;
        SuppliersItemPayment itemPaymentConnection;
        List<clsItem> itemList;
        List<clsSuppliersItemPayment> itemInvoicepaymentList;
        clsSupplierInventory inventoryData;
        SupplierBalanceInventory inventoryConnection;
        decimal OpenningBalance;
        Currency currencyConnection;
        bool skip = false;
        public frmSupplierStatementOfAcount()
        {
            InitializeComponent();
        }
        
        private void frmSupplierStatementOfAcount_Load(object sender, EventArgs e)
        {
            CompanyInfo = new Company();
            currencyConnection = new Currency();
            company = CompanyInfo.CompanyInfoArrayList();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            ticketConnection = new Ticket();
            inventoryConnection = new SupplierBalanceInventory();
            returnedTickets = new ReturnedSupplierTicket();
            returnedItems = new ReturnedSupplierItem();
            lbResult.Visible = false;
            supplierConnection = new Supplier();
            statmentAcount = new List<clsStatmentAcount>();
            paymentConnection = new SuppliersTicketPayment();
            ticketPaymentList = new List<clsSuppliersTickertPayment>();
            itemInvoiceConnection = new SupplierItemInvoice();
            itemPaymentConnection = new SuppliersItemPayment();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            this.reportViewer1.RefreshReport();
        }


        static DataTable ConvertToDatatable(List<clsStatmentAcount> accountList)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("Date");
            dt.Columns.Add("Description");
            dt.Columns.Add("AmountDue");
            dt.Columns.Add("PaidAmount");
            dt.Columns.Add("Balance");

            foreach (var item in accountList)
            {
                var row = dt.NewRow();


                row["Date"] = item.Date;
                row["Description"] = item.Description;
                row["AmountDue"] = item.AmountDue;
                row["PaidAmount"] = item.PaidAmount;
                row["Balance"] = item.Balance;


                dt.Rows.Add(row);
            }

            return dt;
        }


        private void cmbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
          if(cmbSupplierName.SelectedIndex!=-1)
            {
                skip = true;
            }
        }




        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbSupplierName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbSupplierName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.DataSources.Clear();
            statmentAcount.Clear();
            this.reportViewer1.RefreshReport();
            OpenningBalance = 0;
            decimal totalPaid = 0, totalAmountDue = 0;
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbSupplierName.SelectedIndex != -1 && supplierList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                supplierData = (clsSupplier)cmbSupplierName.SelectedItem;
                ticketList = ticketConnection.SupplierTickets(fromDate,toDate,supplierData.SupplierId);
                inventoryData = inventoryConnection.SupplierLastInventory(supplierData.SupplierId);
                if (inventoryData == null)
                {
                    inventoryData = new clsSupplierInventory();
                    inventoryData.Date = DefaultDate;
                    inventoryData.Balance = 0;
                    inventoryData.SupplierId = supplierData.SupplierId;
                    inventoryData.InventoryId = 0;
                    inventoryData.CurrencyId = 0;
                }
                OpenningBalance += inventoryData.Balance;
                OpenningBalance += supplierConnection.getSupplierBalance(supplierData.SupplierId, inventoryData.Date, fromDate);
                OpenningBalance /= currency.CurrencyRate;
                totalAmountDue += OpenningBalance;
                decimal Paid = paymentConnection.TotalPaid(supplierData.SupplierId, inventoryData.Date, fromDate);
                Paid += itemPaymentConnection.TotalPaid(supplierData.SupplierId, inventoryData.Date, fromDate);
                Paid /= currency.CurrencyRate;
                totalPaid += Paid;
                clsStatmentAcount acount = new clsStatmentAcount();
                acount.AmountDue = OpenningBalance.ToString();
                acount.Description = "*OPENING ACCOUNT BALANCE*\n*TOTALS BEFORE REQUESTED PERIOD*";
                acount.PaidAmount = Paid.ToString();
                acount.Date = (fromDate.AddDays(-1));
                acount.Balance = (OpenningBalance - Paid).ToString();
                

                for (int i = 0; i < ticketList.Count; i++)
                {
                    clsStatmentAcount account = new clsStatmentAcount();
                    clsTickets tickets = ticketList[i];
                    account.Date = tickets.TicketDate;
                    account.Description = tickets.AirlineName + "  " + tickets.TicketNo + ":" + tickets.TravellerName;
                    account.AmountDue = ((tickets.Cost * tickets.CurrencyRate) / currency.CurrencyRate).ToString();
                    account.PaidAmount = "0";
                    account.Balance = "0";
                    statmentAcount.Add(account);

                    if (returnedTickets.TicektIDExist(tickets.TicketId))
                    {
                        decimal tCost = (tickets.Cost * tickets.CurrencyRate) - returnedTickets.getReturnAmount(tickets.TicketId);
                        totalAmountDue += tCost / currency.CurrencyRate;
                        clsReturnedSupplierTicket returnedTicket = returnedTickets.getReturnedTicket(tickets.TicketId);
                        clsStatmentAcount returnAcount = new clsStatmentAcount();
                        returnAcount.Date = returnedTicket.Date;
                        returnAcount.AmountDue = "0";
                        returnAcount.Description = "Return " + tickets.AirlineName + "  " + returnedTicket.Ticket.TicketNo + " : " + returnedTicket.Ticket.TravellerName; 
                        returnAcount.IsReturned = true;
                        returnAcount.PaidAmount =( returnedTickets.getReturnAmount(tickets.TicketId) / currency.CurrencyRate).ToString();
                        returnAcount.Balance = "0";
                        returnAcount.NetPrice = ((tickets.Cost * tickets.CurrencyRate) / currency.CurrencyRate);
                        statmentAcount.Add(returnAcount);
                    }
                    else
                    {
                        totalAmountDue += (tickets.Cost * tickets.CurrencyRate) / currency.CurrencyRate;

                    }
                }
                ticketPaymentList = paymentConnection.SuppliersTicketsPayments(supplierData.SupplierId,fromDate,toDate);
                for (int i2 = 0; i2 < ticketPaymentList.Count; i2++)
                {
                    clsStatmentAcount account2 = new clsStatmentAcount();
                    clsSuppliersTickertPayment payment = ticketPaymentList[i2];
                    account2.Date = payment.Date;
                    account2.AmountDue = "0";
                    account2.Balance = "0";
                    account2.PaidAmount = ((payment.Payment * payment.CurrencyRate) / currency.CurrencyRate).ToString();
                    totalPaid += ((payment.Payment * payment.CurrencyRate) / currency.CurrencyRate);
                    if (payment.PaymentType == 1)
                    {
                        account2.Description = "CHECK#" + payment.ChequeNo;
                    }
                    else
                    {
                        account2.Description = "PAYMENT ON ACCOUNT CASH";
                    }
                    statmentAcount.Add(account2);

                }


               itemInvoiceList = itemInvoiceConnection.SupplierInvoices(fromDate,toDate, supplierData.SupplierId);

                for (int i = 0; i < itemInvoiceList.Count; i++)
                {
                    
                    clsSupplierItemInvoice invoice = itemInvoiceList[i];
                    itemList = invoice.Items;
                    foreach (clsItem item    in itemList)
                    {
                        clsStatmentAcount account = new clsStatmentAcount();
                        account.PaidAmount = "0";
                        account.Balance = "0";
                        account.Date = invoice.Date;
                        account.AmountDue = ((item.Cost*item.CurrencyRate) / currency.CurrencyRate).ToString();
                        account.Description = item.itemName + " : " + item.CustomerName;
                        statmentAcount.Add(account);

                        if (returnedItems.IsItemReturned(invoice.InvoiceId,item.itemID,item.CustomerName))
                        {
                            decimal tCost = (item.Cost*item.CurrencyRate) - returnedItems.getReturnAmount(invoice.InvoiceId,item.itemID,item.CustomerName);
                            totalAmountDue += tCost / currency.CurrencyRate; ;
                            clsReturnedSupplierItem returnedItem = returnedItems.getReturnedItem(invoice.InvoiceId, item.itemID, item.CustomerName);
                            clsStatmentAcount returnAcount = new clsStatmentAcount();
                            returnAcount.Date = returnedItem.Date;
                            decimal amountdue = tCost / currency.CurrencyRate;
                            returnAcount.AmountDue = "0";
                            returnAcount.Balance = "0";
                            returnAcount.IsReturned = true;
                            returnAcount.NetPrice= ((item.Cost * item.CurrencyRate) / currency.CurrencyRate);
                            string description;
                            description = "Return "+item.itemName+ " : "+item.CustomerName;
                            returnAcount.Description = description;
                            returnAcount.PaidAmount = ((returnedItem.ReturnAmount * returnedItem.CurrencyRate) / currency.CurrencyRate).ToString();
                            statmentAcount.Add(returnAcount);
                        }
                        else
                        {
                            totalAmountDue += ((item.Cost * item.CurrencyRate) / currency.CurrencyRate);

                        }
                    }
  
                }
                itemInvoicepaymentList = itemPaymentConnection.SupplierPayment(supplierData.SupplierId,fromDate,toDate);
                for (int i2 = 0; i2 < itemInvoicepaymentList.Count; i2++)
                {
                    clsStatmentAcount account2 = new clsStatmentAcount();
                    clsSuppliersItemPayment payment = itemInvoicepaymentList[i2];

                    account2.Date = payment.Date;
                    account2.AmountDue = "0";
                    account2.Balance = "0";
                    account2.PaidAmount = (payment.Payment * payment.CurrencyRate / currency.CurrencyRate).ToString();
                    totalPaid += ((payment.Payment * payment.CurrencyRate / currency.CurrencyRate));
                    if (payment.PaymentType == 1)
                    {
                        account2.Description = "CHECK#" + payment.ChequeNo;
                    }
                    else
                    {
                        account2.Description = "PAYMENT ON ACCOUNT CASH";
                    }
                    statmentAcount.Add(account2);

                }
                string format = "#,##0.00;(#,##0.00)";
                statmentAcount.Sort((x, y) => x.Date.CompareTo(y.Date));
                statmentAcount.Insert(0, acount);
                for (int index = 1; index <= statmentAcount.Count - 1; index++)
                {
                    clsStatmentAcount accout = statmentAcount[index];
                    if (accout.IsReturned)
                    {
                        accout.Balance = (decimal.Parse(statmentAcount[index - 1].Balance)-
                            decimal.Parse(accout.PaidAmount.ToString())).ToString();
                    }
                    else
                    {
                        accout.Balance = (decimal.Parse(statmentAcount[index - 1].Balance) + decimal.Parse(accout.AmountDue.ToString())
                            - decimal.Parse(accout.PaidAmount.ToString())).ToString();
                    }
                }




                DataTable dtt = ConvertToDatatable(statmentAcount);



                this.reportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsCustomerStatmentOfAcount", dtt);

                this.reportViewer1.LocalReport.DataSources.Add(reportsource);


                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {
                            new Microsoft.Reporting.WinForms.ReportParameter("customerName",supplierData.SupplierName),
                            new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                            new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("totalPaid",totalPaid.ToString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                            new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                            new Microsoft.Reporting.WinForms.ReportParameter("totalAmountDue",totalAmountDue.ToString())

                      };
                reportViewer1.Visible = true;
                this.reportViewer1.LocalReport.SetParameters(p);

                this.reportViewer1.RefreshReport();
                skip = true;

            }
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
