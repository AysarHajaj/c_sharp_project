﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    public partial class frmSupplierSummeryOfPayments : Form
    {        DateTime DefaultDate = new DateTime(1900, 1, 1);
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
        clsSupplier supplierData;
        ReturnedSupplierItem returnedItem;
        ReturnedSupplierTicket returnedTicket;
        private List<clsCurrency> currencies;
        private List<clsTickets> ticketList;
        SuppliersTicketPayment paymentConnection;
        List<clsSuppliersTickertPayment> invoicepaymentList;
        Ticket ticketConnection;
        SupplierItemInvoice itemInvoiceConnection;
        List<clsSupplierItemInvoice> itemInvoiceList;
        SuppliersItemPayment itemPaymentConnection;
        List<clsItem> itemList;
        List<clsSuppliersItemPayment> itemInvoicepaymentList;
        bool skip = false;
        int ticketsSold;
        int itemsSold;
        decimal ticketsPrice;
        decimal itemsPrice;
        decimal itemsPaid;
        decimal ticketsPaid;
        decimal OpenningBalance;
        clsSupplierInventory inventoryData;
        SupplierBalanceInventory inventoryConnection;
        List<clsCompany> company;
        Company CompanyInfo;
        Currency currencyConnection;
        public frmSupplierSummeryOfPayments()
        {
            InitializeComponent();
        }

        private void SupplierSummeryOfPayments_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            CompanyInfo = new Company();
            company = CompanyInfo.CompanyInfoArrayList();
            ticketConnection = new Ticket();
            returnedTicket = new ReturnedSupplierTicket();
            returnedItem = new ReturnedSupplierItem();
            lbResult.Visible = false;
            inventoryConnection = new SupplierBalanceInventory();
            paymentConnection = new SuppliersTicketPayment();
            invoicepaymentList = new List<clsSuppliersTickertPayment>();
            itemInvoiceConnection = new SupplierItemInvoice();
            itemPaymentConnection = new SuppliersItemPayment();
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            supplierConnection = new Supplier();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplierName.ValueMember = "supplierId";
            cmbSupplierName.DataSource = supplierList;
            cmbSupplierName.DisplayMember = "supplierName";
            cmbSupplierName.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            this.reportViewer1.RefreshReport();
        }
    
        private void cmbSupplierName_TabStopChanged(object sender, EventArgs e)
        {

        }



        private void cmbSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;
        }


        private void cmbSupplierName_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;
                return;
            }
            lbResult.Visible = false;
            string textToSearch = cmbSupplierName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {

                skip = true;
                cmbSupplierName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            this.reportViewer1.LocalReport.DataSources.Clear();
            OpenningBalance = 0;
            this.reportViewer1.RefreshReport();
            string returnedTickets, returnedItems;
            int nmOfReturnedTickets, nmOfReturnedItem;
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbSupplierName.SelectedIndex != -1 && supplierList.Count > 0)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                nmOfReturnedTickets = 0;
                nmOfReturnedItem = 0;
                returnedTickets = " ";
                returnedItems = " ";
                supplierData = (clsSupplier)cmbSupplierName.SelectedItem;
                ticketList = ticketConnection.SupplierTickets(fromDate, toDate,supplierData.SupplierId);
                inventoryData = inventoryConnection.SupplierLastInventory(supplierData.SupplierId);
                if (inventoryData == null)
                {
                    inventoryData = new clsSupplierInventory();
                    inventoryData.Date = DefaultDate;
                    inventoryData.Balance = 0;
                    inventoryData.SupplierId = supplierData.SupplierId;
                    inventoryData.InventoryId = 0;
                    inventoryData.CurrencyId = 0;
                }
                OpenningBalance += inventoryData.Balance;
                OpenningBalance += supplierConnection.getSupplierBalance(supplierData.SupplierId, inventoryData.Date, fromDate);
                OpenningBalance /= currency.CurrencyRate;
                decimal Paid = paymentConnection.TotalPaid(supplierData.SupplierId, inventoryData.Date, fromDate);
                Paid += itemPaymentConnection.TotalPaid(supplierData.SupplierId, inventoryData.Date, fromDate);
                Paid /= currency.CurrencyRate;
                OpenningBalance -= Paid;
                ticketsSold = ticketList.Count;
                itemsSold = 0;
                ticketsPrice = 0;
                itemsPrice = 0;
                itemsPaid = 0;
                ticketsPaid = 0;

                for (int i = 0; i < ticketList.Count; i++)
                {
                    clsTickets tickets = ticketList[i];

                    if (returnedTicket.TicektIDExist(tickets.TicketId))
                    {
                        ticketsPrice += tickets.Cost * tickets.CurrencyRate - returnedTicket.getReturnAmount(tickets.TicketId);
                        nmOfReturnedTickets++;
                    }
                    else
                    {
                        ticketsPrice += tickets.Cost * tickets.CurrencyRate;
                    }
                }

                invoicepaymentList = paymentConnection.SuppliersTicketsPayments(supplierData.SupplierId, fromDate, toDate);

                for (int i2 = 0; i2 < invoicepaymentList.Count; i2++)
                {

                    ticketsPaid += invoicepaymentList[i2].Payment * invoicepaymentList[i2].CurrencyRate;
                }

                itemInvoiceList = itemInvoiceConnection.SupplierInvoices(fromDate,toDate, supplierData.SupplierId);



                for (int i = 0; i < itemInvoiceList.Count; i++)
                {

                    clsSupplierItemInvoice invoice = itemInvoiceList[i];

                    itemList = invoice.Items;
                    itemsSold += itemList.Count;


                    foreach(clsItem item in itemList)
                    {
                        if (returnedItem.IsItemReturned(invoice.InvoiceId,item.itemID,item.CustomerName))
                        {
                            itemsPrice += (item.Cost*item.CurrencyRate) - returnedItem.getReturnAmount(invoice.InvoiceId, item.itemID, item.CustomerName);
                            nmOfReturnedItem ++;

                        }
                        else
                        {
                            itemsPrice += (item.Cost * item.CurrencyRate);
                        }
                    }
                  

                }
                itemInvoicepaymentList = itemPaymentConnection.SupplierPayment(supplierData.SupplierId, fromDate, toDate);

                for (int i2 = 0; i2 < itemInvoicepaymentList.Count; i2++)
                {

                    itemsPaid += itemInvoicepaymentList[i2].Payment * itemInvoicepaymentList[i2].CurrencyRate;
                }
                if (nmOfReturnedTickets > 0)
                {
                    returnedTickets = nmOfReturnedTickets + " Ticket(s) Returned.";
                }
                if (nmOfReturnedItem > 0)
                {
                   
                    returnedItems = nmOfReturnedItem + " Item(s) Returned.";
                }
                DataTable dtt = new DataTable();

                itemsPaid /= currency.CurrencyRate;
                ticketsPaid /= currency.CurrencyRate;
                itemsPrice /= currency.CurrencyRate;
                ticketsPrice /= currency.CurrencyRate;

                this.reportViewer1.LocalReport.DataSources.Clear();
                ReportDataSource reportsource = new ReportDataSource("dsSummaryofAccount", dtt);

                this.reportViewer1.LocalReport.DataSources.Add(reportsource);


                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {

                            new Microsoft.Reporting.WinForms.ReportParameter("supplierName",supplierData.SupplierName),

                            new Microsoft.Reporting.WinForms.ReportParameter("ticketsSold",ticketsSold.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("packagesSold",itemsSold.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("ticketsPrice",ticketsPrice.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("packagesPrice",itemsPrice.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("ticketsPaid",ticketsPaid.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("packagesPaid",itemsPaid.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("openingBalance",OpenningBalance.ToString()),

                            new Microsoft.Reporting.WinForms.ReportParameter("fromDate", fromDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("toDate", toDate.ToShortDateString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("returnedTickets",returnedTickets),
                            new Microsoft.Reporting.WinForms.ReportParameter("returnedItems",returnedItems),

                            new Microsoft.Reporting.WinForms.ReportParameter("branchName",company[0].CompanyName),
                            new Microsoft.Reporting.WinForms.ReportParameter("companyPhone",company[0].PhoneNumber.ToString()),
                            new Microsoft.Reporting.WinForms.ReportParameter("companyAddress",company[0].Address),
                            new Microsoft.Reporting.WinForms.ReportParameter("currencySymbol",currency.CurrencySymbol),
                            new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())
                      };
                reportViewer1.Visible = true;
                this.reportViewer1.LocalReport.SetParameters(p);

                this.reportViewer1.RefreshReport();
                skip = true;
                
            }
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }
    }
}
