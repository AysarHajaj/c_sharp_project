﻿using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerhanTravel.reports
{
    
    public partial class frmTicketIssued : Form
    {
        List<clsSupplier> supplierList;
        Supplier supplierConnection;
       
        bool skip = false;
        int SupplierID = 0;
        List<clsTickets> SupplierticketList;
        Ticket ticketConnection;
        public frmTicketIssued()
        {
            InitializeComponent();
        }

        private void frmTicketIssued_Load(object sender, EventArgs e)
        {
            supplierConnection = new Supplier();
            ticketConnection = new Ticket();
            supplierList = supplierConnection.SupplierArrayList();
            cmbSupplier.DataSource = null;
            cmbSupplier.ValueMember = "supplierId";
            cmbSupplier.DataSource = supplierList;
            cmbSupplier.DisplayMember = "supplierName";
            cmbSupplier.SelectedIndex = -1;
            skip = true;
            reportViewer1.Visible = false;
            lbResult.Visible = false;
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void cmbSupplier_TextChanged(object sender, EventArgs e)
        {
            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbSupplier.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsSupplier[] result = (from i in supplierList
                                    where i.SupplierName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "supplierId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "supplierName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResult.SelectedIndex != -1)
            {
                skip = true;
                
                cmbSupplier.SelectedItem = lbResult.SelectedItem;

            }
            lbResult.Visible = false;
        }

        static DataTable ConvertToDatatable(List<clsTickets> ticketlist)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("ticketNo");
            dt.Columns.Add("Description");
            dt.Columns.Add("flightNo");
            
            foreach (var item in ticketlist)
            {
                var row = dt.NewRow();


                row["ticketNo"] = item.TicketNo;
                row["Description"] = item.Description;
                
                row["flightNo"] = Convert.ToString(item.FlightNo);
                

                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.DataSources.Clear();
            lbResult.Visible = false;
            if (cmbSupplier.SelectedIndex != -1 && supplierList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;
                reportViewer1.Visible = true;
                SupplierID = int.Parse(cmbSupplier.SelectedValue.ToString());
                SupplierticketList = ticketConnection.SupplierTickets(fromDate,toDate,SupplierID);
                DataTable dtt = ConvertToDatatable(SupplierticketList);
                ReportDataSource reportsource = new ReportDataSource("dsTicketIssued", dtt);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                  {
                      
                               new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())

                  };


                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.Refresh();
                this.reportViewer1.RefreshReport();
                skip = true;
            }
        }
    }
}
