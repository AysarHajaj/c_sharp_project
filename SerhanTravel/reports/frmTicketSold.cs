﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using SerhanTravel.Connections;
using SerhanTravel.Objects;

namespace SerhanTravel.reports
{
    public partial class frmTicketSold : Form
    {
        List<clsCustomer> customerList;
        Customer customerConnection;
        private bool skip;
        Ticket ticketConnection;
        List<clsTickets> CustomerticketList;
        ReturnedCustomerTicket returnedTickets;
        Currency currencyConnection;
        public int CustomerID { get; private set; }
        private List<clsCurrency> currencies;
        public frmTicketSold()
        {
            InitializeComponent();
        }

        private void frmTicketSold_Load(object sender, EventArgs e)
        {
            currencyConnection = new Currency();
            customerConnection = new Customer();
            ticketConnection = new Ticket();
            customerList = customerConnection.CustomersArrayList();
            clsCustomer cus = new clsCustomer();
            cus.CustomerName = "Public Customer";
            cus.CustomerId = 0;
            customerList.Insert(0, cus);
            returnedTickets = new ReturnedCustomerTicket();
            cmbCustomerName.ValueMember = "CustomerId";
            cmbCustomerName.DataSource = customerList;
            cmbCustomerName.DisplayMember = "CustomerName";
            cmbCustomerName.SelectedIndex = -1;
            skip = true;
            currencies = currencyConnection.CurrencyArrayList();
            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            reportViewer1.Visible = false;
            lbResult.Visible = false;
            this.reportViewer1.RefreshReport();
        }

       

        private void cmbCustomerName_TextChanged(object sender, EventArgs e)
        {

            if (skip)
            {
                skip = false;

                return;
            }

            lbResult.Visible = false;
            string textToSearch = cmbCustomerName.Text.ToLower();
            if (string.IsNullOrEmpty(textToSearch))
            {

                return;
            }


            clsCustomer[] result = (from i in customerList
                                    where i.CustomerName.ToLower().Contains(textToSearch)
                                    select i).ToArray();
            if (result.Length == 0)
            {

                return; // return with listbox's Visible set to false if nothing found

            }
            else
            {




                lbResult.Items.Clear(); // remember to Clear before Add
                lbResult.ValueMember = "CustomerId";
                lbResult.Items.AddRange(result);
                lbResult.DisplayMember = "CustomerName";
                lbResult.Visible = true; // show the listbox again
                lbResult.Height = 100;
            }
        }

        private void lbResult_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbResult.SelectedIndex != -1)
            {
                skip = true;

                cmbCustomerName.SelectedItem = lbResult.SelectedItem;

            }


            lbResult.Visible = false;
        }

         DataTable ConvertToDatatable(List<clsTickets> ticketlist)
        {
            DataTable dt = new DataTable();


            dt.Columns.Add("ticketNo");
            dt.Columns.Add("destination");
            dt.Columns.Add("flightNo");
            dt.Columns.Add("TravellerName");
            dt.Columns.Add("price");
            dt.Columns.Add("Status");
            dt.Columns.Add("CurrecnySymbol");
            dt.Columns.Add("CurrencyRate");
            foreach (clsTickets ticket in ticketlist)
            {
                var row = dt.NewRow();
                string status = "Not Returned";
                if(returnedTickets.TicketReturned(ticket.TicketId))
                {
                    status = "Returned";
                    ticket.Price = ((ticket.Price * ticket.CurrencyRate) - returnedTickets.getReturnAmount(ticket.TicketId))/ticket.CurrencyRate;
                }
                ticket.Status = status;
                row["ticketNo"] = ticket.TicketNo;
                row["destination"] = ticket.Destination;
                row["TravellerName"] = ticket.TravellerName;
                row["flightNo"] = Convert.ToString(ticket.FlightNo);
                row["price"] = Convert.ToString(ticket.Price);
                row["Status"] = ticket.Status;
                row["CurrecnySymbol"] = ticket.CurrecnySymbol;
                row["CurrencyRate"] = ticket.CurrencyRate;

                dt.Rows.Add(row);
            }

            return dt;
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            skip = true;
        }

        private void cmbCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCurrency.SelectedIndex != -1)
            {
                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                txtRate.Text = currency.CurrencyRate.ToString();

            }
            else
            {
                txtRate.Text = "";
            }
        }

        private void cmbCurrency_Click(object sender, EventArgs e)
        {
            int id = 0;
            if (cmbCurrency.SelectedIndex != -1)
                id = int.Parse(cmbCurrency.SelectedValue.ToString());
            currencies = currencyConnection.CurrencyArrayList();

            cmbCurrency.ValueMember = "CurrencyId";
            cmbCurrency.DataSource = currencies;
            cmbCurrency.DisplayMember = "CurrencyName";
            cmbCurrency.SelectedValue = id;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            reportViewer1.Visible = true;
            this.reportViewer1.LocalReport.DataSources.Clear();
            lbResult.Visible = false;
            if (cmbCurrency.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Currency to Continue...");
                return;
            }
            if (cmbCustomerName.SelectedIndex != -1 && customerList.Count > 0)
            {
                DateTime fromDate = dtpFrom.Value;
                DateTime toDate = dtpTo.Value;

                clsCurrency currency = (clsCurrency)cmbCurrency.SelectedItem;
                string customerName;
                clsCustomer customerData = (clsCustomer)cmbCustomerName.SelectedItem;
                customerName = customerData.CustomerName;
                if (customerData.CustomerId == 0)
                {
                    customerName = " ";
                }
                CustomerID = int.Parse(cmbCustomerName.SelectedValue.ToString());
                CustomerticketList = ticketConnection.CustomerTickets(fromDate,toDate,CustomerID);
                DataTable dtt = ConvertToDatatable(CustomerticketList);
                ReportDataSource reportsource = new ReportDataSource("dsTicketSold", dtt);
                this.reportViewer1.LocalReport.DataSources.Clear();
                this.reportViewer1.LocalReport.DataSources.Add(reportsource);

                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[]
                      {
                        new Microsoft.Reporting.WinForms.ReportParameter("customerName",customerName),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedRate",currency.CurrencyRate.ToString()),
                        new Microsoft.Reporting.WinForms.ReportParameter("selectedSymbol",currency.CurrencySymbol),
                               new Microsoft.Reporting.WinForms.ReportParameter("fromDate",fromDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("toDate",toDate.ToShortDateString()),
                                  new Microsoft.Reporting.WinForms.ReportParameter("date",DateTime.Today.ToShortDateString())

                      };


                this.reportViewer1.LocalReport.SetParameters(p);
                this.reportViewer1.LocalReport.Refresh();
                this.reportViewer1.RefreshReport();
                skip = true;
            }
        }
    }
}
